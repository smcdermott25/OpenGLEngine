#include "Server.h"

Server::Server()
	:ipAddress("localhost"),
	port("80"),
	bufferLength(sizeof(glm::mat4)),
	numClients(0),
	blocking(true)
{
	std::cout << "Setting non blocking mode" << std::endl;
	std::cout << "Init being called: " << std::endl;
	init();
}

Server::Server(char* ipAddress, char* port, int bufferLength, bool blocking)
	:ipAddress(ipAddress),
	port(port),
	bufferLength(bufferLength),
	numClients(0),
	blocking(blocking)
{
	init();
}

Server::~Server()
{

}

void Server::init()
{
	WSADATA wsaData;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
	}

	std::cout << "Winsock initialized: " << std::endl;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(ipAddress, port, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed: %d\n", iResult);
		WSACleanup();
	}

	std::cout << "Address: " << iResult << std::endl;
	std::cout << "Server address and port resolved: " << std::endl;

	ptr = result;

	initServerSocketConnection();
	initClientSocketConnection();
}

void Server::initServerSocketConnection()
{
	// Create a SOCKET for the server to listen for client connections
	listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

	if (listenSocket == INVALID_SOCKET) {
		printf("Error at socket(): %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
	}

	// Setup the TCP listening socket
	iResult = bind(listenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(listenSocket);
		WSACleanup();
	}
	else
	{
		freeaddrinfo(result);
		std::cout << "Free local address port variable after binding " << std::endl;
	}

	if (listen(listenSocket, SOMAXCONN) == SOCKET_ERROR) {
		printf("Listen failed with error: %ld\n", WSAGetLastError());
		closesocket(listenSocket);
		WSACleanup();
	}

	std::cout << "Listening on socket: " << std::endl;
}

void Server::initClientSocketConnection()
{
	clientSocket[numClients] = INVALID_SOCKET;

	// Accept a client socket
	clientSocket[numClients] = accept(listenSocket, NULL, NULL);
	if (clientSocket[numClients] == INVALID_SOCKET) {
		printf("accept failed: %d\n", WSAGetLastError());
		closesocket(listenSocket);
		WSACleanup();
	}
	u_long iMode = 1;
	if (!blocking)
	{
		std::cout << "Non blocking mode " << std::endl;
		ioctlsocket(clientSocket[numClients], FIONBIO, &iMode);
	}
	std::cout << "Client connected, ID: " << id << std::endl;
	sendId();
	numClients++;
}

void Server::checkForConnection()
{
	u_long iMode = 1;
	ioctlsocket(listenSocket, FIONBIO, &iMode);
	listen(listenSocket, 1);

	SOCKET TempSock = SOCKET_ERROR;

	TempSock = accept(listenSocket, NULL, NULL);
	if (TempSock != INVALID_SOCKET)
	{
		clientSocket[numClients] = TempSock;
		if (!blocking)
		{
			ioctlsocket(clientSocket[numClients], FIONBIO, &iMode);
		}
		std::cout << "Client connected, ID: " << id << std::endl;
		sendId();
		numClients++;
	}
}

void Server::runClientInThread(int index)
{
}

void Server::closeConnection()
{
	WSACleanup();
}

void Server::receiveMat4(int clientIndex)
{
	char recvbuf[sizeof(glm::mat4)];
	int recvbuflen = bufferLength;

	iResult = recv(clientSocket[clientIndex], recvbuf, recvbuflen, 0);
	memcpy(&lastModelReceived[clientIndex], recvbuf, recvbuflen);
}

void Server::receiveMultipleMat4()
{
	for (int i = 0; i < numClients; i++)
	{
		receiveMat4(i);
	}
}

void Server::sendMat4(char* sendBuffer, int clientIndex)
{
		iResult = send(clientSocket[clientIndex], sendBuffer, bufferLength, 0);
}

void Server::sendMultipleMat4()
{
	for (int i = 0; i < numClients; i++)
	{
		iResult = send(clientSocket[i], (char*)&(numClients), sizeof(int), 0);
		if (iResult == SOCKET_ERROR)
		{
			std::cout << "Client disconnected" << std::endl;
			closesocket(clientSocket[i]);
			numClients--;
		}
	}

	for (int i = 0; i < numClients; i++)
	{
		for (int j = 0; j < numClients; j++)
		{
			sendMat4((char*)&lastModelReceived[i], j);
		}

	}
}

void Server::sendId()
{
	iResult = send(clientSocket[numClients], (char*)&id, bufferLength, 0);
	id++;
}

glm::mat4 Server::getLastModel()
{
	return lastModelReceived[0];
}
