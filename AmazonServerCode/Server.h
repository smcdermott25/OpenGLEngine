#pragma once
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>

#include <thread>
#include <functional>
#include <iostream>
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN sizeof(glm::mat4)
#define MAX_CLIENTS 10

class Server
{
private:
	int numClients;
	int id = 0;
	int numReceived = 0;

	const char* port;
	const char* ipAddress;
	const int bufferLength;

	int iResult;
	struct addrinfo *result = NULL;
	struct addrinfo	*ptr = NULL;
	struct addrinfo	hints;
	bool blocking;

	SOCKET listenSocket = INVALID_SOCKET;

	SOCKET clientSocket[MAX_CLIENTS];
	glm::mat4 lastModelReceived[MAX_CLIENTS];
	std::thread* clientThreads[MAX_CLIENTS];

	void init();
	void initServerSocketConnection();
	void runClientInThread(int index);

public:
	Server();
	Server(char* ipAddress, char* port, int bufferLength, bool blocking);
	~Server();

	void initClientSocketConnection();
	void checkForConnection();
	void closeConnection();

	void receiveMat4(int clientIndex);
	void receiveMultipleMat4();
	void sendMat4(char* sendBuffer, int clientIndex);
	void sendMultipleMat4();

	void sendId();

	glm::mat4 getLastModel();
};