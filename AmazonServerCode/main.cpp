#include "Server.h"
#include <chrono>

int main()
{
	std::cout << "Starting server " << std::endl;
	Server server;

	std::cout << "Server initalized" << std::endl;

	int count = 0;
	double  value = 0;
	
	while (1)
	{
		server.checkForConnection();
		server.receiveMultipleMat4();
		server.sendMultipleMat4();
	}
}