#pragma once
#include "AlphaBlenderTool.h"

#include <iostream>


AlphaBlenderTool::AlphaBlenderTool()
{

}
AlphaBlenderTool::~AlphaBlenderTool()
{

}

//Basic sorter method needed if render order of objects is important* note that inneffiency makes this method impractical with large arrays
std::vector<glm::vec3> AlphaBlenderTool::sortPositions(glm::vec3 instancePositions[], glm::vec3 cameraPosition, GLint amountOfInstances)
{
	std::map<GLfloat , glm::vec3> sorted;
	for (GLuint i = 0; i < amountOfInstances; i++)
	{
		GLfloat distance = glm::length(instancePositions[i] - cameraPosition);
		sorted[distance] = instancePositions[i];
	}

	std::vector<glm::vec3> sortedPositions;

	for (std::map<GLfloat , glm::vec3>::reverse_iterator it = sorted.rbegin(); it != sorted.rend(); ++it)
	{
		sortedPositions.push_back(it->second);
	}

	return sortedPositions;
}

//Basic sorter method needed if render order of objects is important
std::vector<glm::mat4>  AlphaBlenderTool::sortPositionsOfMatrices(glm::mat4 matrices[], glm::vec3 cameraPosition, GLint amountOfInstances)
{
	std::multimap<GLfloat , glm::mat4> sorted;
	std::vector<glm::mat4> sortedMatrices;

	for (GLuint i = 0; i < amountOfInstances; i++)
	{
		Orientation orientation;
		orientation.setModelMatrix(matrices[i]);

		glm::vec3 test = glm::vec3(matrices[i][3][0], matrices[i][3][1], matrices[i][3][2]);
		GLfloat distance = glm::length(test - cameraPosition);
		sorted.insert(std::pair<GLfloat , glm::mat4>(distance, matrices[i]));
	}


	for (std::map<GLfloat , glm::mat4>::reverse_iterator it = sorted.rbegin(); it != sorted.rend(); ++it)
	{
		sortedMatrices.push_back(it->second);
	}

	return sortedMatrices;
}

GLvoid AlphaBlenderTool::transparentSetUp()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ZERO);
}

GLvoid AlphaBlenderTool::transclucentSetUp()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

GLvoid AlphaBlenderTool::disableBlending()
{
	glDisable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ZERO);
}