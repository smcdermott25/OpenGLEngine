#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <map>
#include <glew.h> 
#include <glfw3.h>
#include <stdlib.h>
#include <vector>
#include "Orientation.h"
class AlphaBlenderTool
{
public:
	AlphaBlenderTool();
	~AlphaBlenderTool();

	// Methods to sort vectors and matrices based on position
	std::vector<glm::vec3> sortPositions(glm::vec3 positions[], glm::vec3 cameraPosition, GLint amountOfInstances);
	std::vector<glm::mat4> sortPositionsOfMatrices(glm::mat4 positions[], glm::vec3 cameraPosition, GLint amountOfInstances);
	
	GLvoid transparentSetUp();
	GLvoid transclucentSetUp();
	GLvoid disableBlending();
};

