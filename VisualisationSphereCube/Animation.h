#pragma once
#include <iostream>
#include <vector>
#include <map>

#include <glew.h>
#include <glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Bones.h"

class Animation
{
private:
	const aiScene* scene;

public:
	std::map<std::string, const aiNodeAnim*> nameAnimationMap;

	Animation(const aiScene* scene)
	{
		this->scene = scene;
	}

	GLvoid loadAnimation(const aiNode* node, Bones& bones)
	{
		std::string nodeName(node->mName.data);
		const aiAnimation* animation = scene->mAnimations[0];

		aiMatrix4x4 nodeTransformation(node->mTransformation);
		const aiNodeAnim* nodeAnimation = nullptr;

		for (GLuint i = 0; i < animation->mNumChannels; i++)
		{
			if (animation->mChannels[i]->mNodeName.data == nodeName)
			{
				// Every bone, only nodes which are bones are affected
				nodeAnimation = animation->mChannels[i];
			}
		}

		if (nodeAnimation)
		{
			nameAnimationMap[nodeName] = nodeAnimation;
		}

		for (GLuint i = 0; i < node->mNumChildren; i++)
		{
			loadAnimation(node->mChildren[i], bones);
		}
	}
};
