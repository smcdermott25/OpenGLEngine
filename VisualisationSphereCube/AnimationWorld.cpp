#include "AnimationWorld.h"

AnimationWorld::AnimationWorld()
	:player("models/ArmyPilot/ArmyPilot.dae"),
	tCam{player}
{
	player.model->orientation->addPosition(glm::vec3(0, 200, -80));
	controls = Controls::getInstance();
}

AnimationWorld::~AnimationWorld()
{
}

GLvoid AnimationWorld::update(GLfloat deltaTime)
{
	player.update(deltaTime);
	tCam.move();
	this->deltaTime = deltaTime;
}

GLvoid AnimationWorld::draw()
{
	player.draw(tCam.getView(), tCam.getProjection());
}

GLvoid AnimationWorld::input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode)
{
	if (key == GLFW_KEY_ESCAPE)
	{
		exit(1);
	}

	if (key == GLFW_KEY_L && action == GLFW_RELEASE)
	{
		lines = !lines;
		if (lines)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	if (action == GLFW_PRESS)
	{
		camera.setKey(key, true);
		controls->setKey(key, true);
	}

	if (action == GLFW_RELEASE)
	{
		camera.setKey(key, false);
		controls->setKey(key, false);
		controls->mouseDY = 0;
		controls->mouseDX = 0;
	}

	if (key == GLFW_KEY_UP && action == GLFW_RELEASE)
	{
		camera.addCameraSpeed(100);
	}

	if (key == GLFW_KEY_DOWN && action == GLFW_RELEASE)
	{
		camera.addCameraSpeed(-100);
	}

}

GLvoid AnimationWorld::mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
{
	controls->updateMouseDifference(xPosition, yPosition);
}

GLvoid AnimationWorld::mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods)
{
	if (action == GLFW_PRESS)
	{
		camera.setKey(button, true);
		controls->setKey(button, true);
	}

	if (action == GLFW_RELEASE)
	{
		camera.setKey(button, false);
		controls->setKey(button, false);
	}
}

GLvoid AnimationWorld::scrollInput(GLFWwindow* window, GLdouble  xScroll, GLdouble  yScroll)
{
	controls->scrollValue += yScroll;
}