#pragma once
#include "GameWorld.h"
#include "Model.h"
#include "Controls.h"
#include "Player.h"
#include "ThirdPersonCamera.h"
#include "Terrain.h"

class AnimationWorld : public GameWorld
{
private:

	Player player;
	Controls* controls;
	ThirdPersonCamera tCam;
	Camera camera;
	GLboolean lines = false;

	std::string path;
	Model soldier;

	GLfloat deltaTime;


public:
	AnimationWorld();
	~AnimationWorld();
	GLvoid update(GLfloat deltaTime);
	GLvoid draw();

	GLvoid input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode);

	GLvoid mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition);
	GLvoid mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods);
	GLvoid scrollInput(GLFWwindow* window, GLdouble  xScroll, GLdouble  yScroll);
};