#include "Ball.h"

Ball::Ball()
	:Model("models/sphere/sphereo.obj", false, glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.01f, 0.01f, 0.01f))
{
	init(8.0f);
}

Ball::Ball(GLfloat radius)
	: Model("models/sphere/sphereo.obj", false, glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.01f, 0.01f, 0.01f))
{
	
	init(radius);
}

Ball::~Ball()
{
}

GLvoid Ball::init(GLfloat rRadius)
{
	radius = rRadius;
	setScale(glm::vec3(radius, radius, radius));
}

//passing boolean flags through to movement
GLvoid Ball::draw(glm::mat4 view, glm::mat4 projection, GLfloat deltaTime)
{
	if (moving)
	{
		moveTo(moveToPosition, deltaTime);
	}
	if (movingX)
	{
		movement->moveX(movement->getVelocity(), deltaTime);
	}
	if (movingY)
	{
		movement->moveY(movement->getVelocity(), deltaTime);
	}
	if (movingZ)
	{
		movement->moveZ(movement->getVelocity(), deltaTime);
	}
	if (dropping)
	{
		movement->move(deltaTime);
	}
	if (launching)
	{
		movement->launch(movement->getLaunchVelocity(), deltaTime);
	}
	drawing(projection, view, deltaTime);
}
