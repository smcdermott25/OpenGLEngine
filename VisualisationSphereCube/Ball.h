#pragma once
#include <glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "GameObject.h"
#include "Model.h"
#include "info.h"


class Ball : public Model
{
public:
	Ball();
	Ball(GLfloat radius);
	~Ball();

	GLvoid Ball::draw(glm::mat4 view, glm::mat4 projection, GLfloat deltaTime);
	GLvoid init(GLfloat radius);
	GLfloat radius;
};
