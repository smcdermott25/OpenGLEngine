
#include "BaseLight.h"
/*Simple class for holding the core values held by all lights*/
BaseLight::BaseLight(glm::vec3 nLightColour, glm::vec3 nAmbient, glm::vec3 nDiffuse, glm::vec3 nSpecular)
{
	lightColour = nLightColour;
	ambient = nAmbient;
	diffuse = nDiffuse;
	specular = nSpecular;
}
BaseLight::BaseLight(glm::vec3 nLightColour)
{
	setColourEasy(nLightColour);
}
/*Allows to easily set a color by adjusting ambient, diffuse and specular settings automatically*/
GLvoid BaseLight::setColourEasy(glm::vec3 newColour)
{
	lightColour = newColour;
	ambient = glm::vec3(newColour.x * 0.05f, newColour.y * 0.05f, newColour.z * 0.05f);
	diffuse = glm::vec3(newColour.x * 0.8f, newColour.y * 0.8f, newColour.z * 0.8f);
	specular = newColour;
}

glm::vec3 BaseLight::getAmbient()
{
	return ambient;
}
glm::vec3 BaseLight::getDiffuse()
{
	return diffuse;
}
glm::vec3 BaseLight::getSpecular()
{
	return specular;
}
glm::vec3 BaseLight::getColour()
{
	return lightColour;
}