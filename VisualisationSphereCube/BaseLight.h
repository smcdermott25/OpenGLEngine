#pragma once
#include "GameObject.h"
/*Base light class, all lights inherit this*/
class BaseLight{
protected:
	glm::vec3 lightColour;
	glm::vec3 ambient;
	glm::vec3 specular;
	glm::vec3 diffuse;

public:
	BaseLight(glm::vec3 nLightColour, glm::vec3 nAmbient, glm::vec3 nDiffuse, glm::vec3 nSpecular);
	BaseLight(glm::vec3 nLightColour);
	GLvoid setColourEasy(glm::vec3 newColour);
	glm::vec3 getAmbient();
	glm::vec3 getDiffuse();
	glm::vec3 getSpecular();
	glm::vec3 getColour();

};