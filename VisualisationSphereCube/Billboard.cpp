#pragma once
#include "Billboard.h"
#include "InstancedObject.h"

Billboard::Billboard(GLint  billboardType, GLchar* imagePath)
	:BillboardSetup(billboardType)
	, InstancedObject()
	, GameObject(&ShaderLoader::billboardShader)
{
	setUpAttributes(imagePath);
}
Billboard::~Billboard()
{

}
GLvoid Billboard::setUpAttributes(GLchar* imagePath)
{
	//billboardType 0 = cylindrical, 1 = spher
	helper.loadTextureWithImage2D(texture, imagePath, 0);
}
GLvoid Billboard::resetMatrixBuffer(GLint  maxAmountOfInstances)
{
	setUpDataForModelMatrixInstancing(vertexArray, maxAmountOfInstances, matrixAttributeIndex);
}
GLvoid Billboard::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{
	shaderProgram->useProgram();

	glBindTexture(GL_TEXTURE_2D, texture);

	//Get CameraRight and Up vectores to calulate rotations in shader code
	glm::vec3 CameraRight_worldspace = { view[0][0], view[1][0], view[2][0] };
	glm::vec3 CameraUp_worldspace = { view[0][1], view[1][1], view[2][1] };

	//Pass Uniforms to shader
	glUniform3f(glGetUniformLocation(shaderProgram->getProgram(), "CameraRight_worldspace"), CameraRight_worldspace.x, CameraRight_worldspace.y, CameraRight_worldspace.z);
	glUniform3f(glGetUniformLocation(shaderProgram->getProgram(), "CameraUp_worldspace"), CameraUp_worldspace.x, CameraUp_worldspace.y, CameraUp_worldspace.z);
	glUniform3f(glGetUniformLocation(shaderProgram->getProgram(), "cameraPosition"), cameraPosition.x, cameraPosition.y, cameraPosition.z);
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	glUniform1i(glGetUniformLocation(shaderProgram->getProgram(), "billboardType"), billboardType);//Not sure if this is how it will stay
	glUniform1f(glGetUniformLocation(shaderProgram->getProgram(), "time"), glfwGetTime());
	glUniform1f(glGetUniformLocation(shaderProgram->getProgram(), "random"), 1);

	glBindVertexArray(vertexArray);
	//Draw
	glDrawArraysInstanced(GL_TRIANGLES, 0, verticesPerBillboard, amountOfInstances);

	glDisable(GL_BLEND);
	//Release VertexArray
	glBindVertexArray(0);

}