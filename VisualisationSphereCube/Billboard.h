#pragma once
#include "BillboardSetup.h"
#include "InstancedObject.h"
#include "GameObject.h"

class Billboard : public BillboardSetup, public InstancedObject,public GameObject
{

public:

	Billboard(GLint  billboardType, GLchar* imagePath);
	~Billboard();
	GLvoid resetMatrixBuffer(GLint  maxAmountOfInstances);
	GLvoid setUpAttributes(GLchar* imagePath);
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition);

private:
	const GLint matrixAttributeIndex = 3;
	const GLint verticesPerBillboard = 6;
};

