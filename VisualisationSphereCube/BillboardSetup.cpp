#include "BillboardSetup.h"

BillboardSetup::BillboardSetup(GLint billboardType)

{
	this->billboardType = billboardType;//Spherical or cylindrical
	setUpVerticeData(200.0f, 200.0f);//default size
}

BillboardSetup::BillboardSetup(GLint billboardType, GLfloat xSize, GLfloat ySize)

{
	this->billboardType = billboardType;
	setUpVerticeData(xSize, ySize);
}
BillboardSetup::~BillboardSetup()
{
}
GLvoid BillboardSetup::setUpVerticeData(GLfloat xSize, GLfloat ySize)
{
	//Set up basic square
	glm::vec3 objectPosition = glm::vec3(0.0f, 0.0f, 0.0f);
	GLfloat radius = xSize / 2;
	GLfloat radiusY = ySize / 2;
	GLfloat verticeData[] = {
		objectPosition.x - radius, objectPosition.y - radiusY,  0.0f ,
		objectPosition.x - radius, objectPosition.y + radiusY,  0.0f ,
		objectPosition.x + radius, objectPosition.y - radiusY,  0.0f,
		objectPosition.x - radius, objectPosition.y + radiusY, 0.0f ,
		objectPosition.x + radius, objectPosition.y + radiusY, 0.0f ,
		objectPosition.x + radius, objectPosition.y - radiusY, 0.0f ,
	};

	std::copy(verticeData, verticeData + 18, vertices);
	verticeSize = sizeof(verticeData);

	GLfloat textureCoordinateData[]
	{
		0, 1,
		0, 0,
		1, 1,
		0, 0,
		1, 0,
		1, 1,
	};
	std::copy(textureCoordinateData, textureCoordinateData + 12, textureCoordinates);
	textureCoordinatesSize = sizeof(textureCoordinateData);

	//Create vertexArray and vertexBuffer like normal
	helper.createVertexArray(vertexArray);
	helper.fillVertexArrayWithVertices(vertexArray, vertexBuffer, vertices, verticeSize, vertexAttributeIndex);

	//Apply texturing like normal
	helper.fillVertexArrayWithTextureCoords(vertexArray, textureCoordsBuffer, textureCoordinates, textureCoordinatesSize, textureAttributeIndex);
	glGenTextures(1, &texture);
	helper.texture2DSetRepeatAndLinear(texture);
}