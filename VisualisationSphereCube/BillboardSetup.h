#pragma once
#include "GameObject.h"

class BillboardSetup //sets up a rectancle
{

public:

	BillboardSetup(GLint billboardType);
	BillboardSetup(GLint billboardType, GLfloat xSize, GLfloat ySize);
	~BillboardSetup();

	GLvoid setUpVerticeData(GLfloat xSize, GLfloat ySize);

protected:
	GLuint vertexArray, vertexBuffer, texture, textureCoordsBuffer;

	GLfloat vertices[18];
	GLfloat textureCoordinates[12];
	GLint verticeSize;
	GLint textureCoordinatesSize;
	GLint billboardType;

private:
	const GLint  vertexAttributeIndex = 0;
	const GLint  textureAttributeIndex = 2;
};