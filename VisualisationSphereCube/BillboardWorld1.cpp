#include "BillboardWorld1.h"

GLboolean anti_aliasing = true;
GLboolean bloom = true;
GLuint samples = 4;
Environment milkyway;
BillboardWorld1::BillboardWorld1()
{
//Create terrain and assets
	GLchar* defaultTextures[3]{ "Texture/alienMountain.jpg","Texture/aleinDirt.png","Texture/aleinDirt.png" };
	defaultNoise = new FractalNoise(log2(257), 10000000, 0.50);
	terrain = new Terrain(glm::vec3(0, 0, 0), 10000000, 700, 257, 3072, defaultNoise, defaultTextures);
	rocks = new RockGenerator(glm::vec3(0, 0, 0), 3000, 3000);
	collisions = new Collisions();

	ball = new Ball(0.03f);
	ball1 = new Ball(0.03f);	
	SpaceShip spaceShip = SpaceShip();
	//PBR
	PBR pbr = PBR();
	
	milkyway.bg = pbr.equirectToCubemap("skybox/Milkyway_BG.jpg");
	milkyway.env = pbr.equirectToCubemap("skybox/Milkyway_small.hdr");
	milkyway.ref = pbr.equirectToCubemap("skybox/Milkyway_Light.hdr");
	milkyway.prefilter = pbr.prefilterCubemap(milkyway.env);
	GLuint brdfLUT = helper.loadTexture("skybox/ibl_brdf_lut.png");
	sphere = new PBRSphere(brdfLUT);
	sphere->orientation.setScale(glm::vec3(10, 10, 10));
	sphere->orientation.setPosition(glm::vec3(100,100,100));
	//Particles
	particleFactory = new ParticleFactory();
	//Lighting
	directionalLight = DirectionalLight(glm::vec4(-0.2f, -1.0f, -0.3f, 0.0));

	//SetUpTerrain
	terrain->createAndFillVertexArrayObject();
	terrain->createTextures();

	//setting up assets
	std::vector<glm::vec3>* rockPositions = rocks->getGridPositions();
	for (GLuint i = 0; i < rockPositions->size(); i++)
	{
		rockPositions->at(i).y = terrain->getTerrainHeight(rockPositions->at(i).x, rockPositions->at(i).z);
	}
	rocks->InitialUpdate(true);
	skyBox.texture = milkyway.bg;

	for (GLint  i = 0; i < rocks->getRockLights().size(); i++)
	{
		pointLightList.push_back(rocks->getRockLights().at(i));
	}

	//Ball info

	ball->setPosition(glm::vec3(1600, 600, 1600));
	ball1->setPosition(glm::vec3(1400, 600, 1600));
	andrewsBallList.push_back(ball);
	andrewsBallList.push_back(ball1);
	spaceShips.push_back(spaceShip);
		
	//Set up particle effect to track space ship
	particleFactory->addMovingEffect(spaceShip.getPosition() + glm::vec3(0, 55, 0));
	
	//spaceShip.setPosition(glm::vec3(400, 500, 400));
	//soldier.setPosition(glm::vec3(400, 300, 400));

	//(GLchar* path, GLboolean animated, glm::vec3 scale, glm::vec3 position, GLchar* vertex, GLchar* frag)
	//pretendFlyingSaucer = MultiLightCube();
	//pretendFlyingSaucer.setPosition(glm::vec3(2000, 700, 2000));
	//pretendFlyingSaucer.setScale(glm::vec3(50, 50, 50));
	//pretendFlyingSaucer.updatePointLightList(pointLightList);
	//spaceShips.at(0).setPosition(glm::vec3(400, 500, 300));

	//objectsThatCastShadow.push_back(std::make_shared<MultiLightCube>(pretendFlyingSaucer));

	//Set up Lighting
	updateObjectShadows();
	terrain->shadow->setNearPlane(1.0f);
	terrain->shadow->setFarPlane(900.0f);
	terrain->shadow->setOrthoDimension(500.0f);
	terrain->updatePointLightList(pointLightList, pointLightList.size(), spaceShips[0].getPosition());

}

BillboardWorld1::~BillboardWorld1()
{

}


GLvoid BillboardWorld1::update(GLfloat deltaTime)
{
	updateObjectShadows();

	camera.move(deltaTime);
	glm::vec3 cameraPos = camera.getCameraPosition();
	glm::mat4 view = camera.getView();
	glm::mat4 projection = camera.getProjection();

	spaceShips[0].update();
	terrain->shadow->setPositionOfLightForShadow(glm::vec3(spaceShips[0].getPosition().x + 10, spaceShips[0].getPosition().y + 100, spaceShips[0].getPosition().z + 10));
	terrain->shadow->setPositionOfLightTargetForShadow(glm::vec3(spaceShips[0].getPosition().x, spaceShips[0].getPosition().y, spaceShips[0].getPosition().z));
	sphere->update(view, projection, cameraPos,0);
	updatePostpone++;

	if (updatePostpone >= 1000)

	{
		terrain->updatePointLightList(pointLightList, 10, spaceShips[0].getPosition());
		updatePostpone = 0;

	}
	
	//Test Collisions
	collisions->objectCollisions(andrewsBallList);
	collisionPoints = collisions->terrainCollisions(andrewsBallList, terrain);
	particleFactory->addCollisionEffects(collisionPoints);
	particleFactory->update(deltaTime, cameraPos, view, spaceShips[0].getPosition() + glm::vec3(0, 55, 0));

	//Does imposter need to update?
	rocks->update(view, projection, cameraPos);
}

GLvoid BillboardWorld1::draw()
{

	GLfloat deltaTime = glfwGetTime() - previousTime;
	previousTime = glfwGetTime();

	glm::mat4 view = camera.getView();
	glm::mat4 projection = camera.getProjection();
	glm::vec3 cameraPos = camera.getCameraPosition();
	glm::mat4 view1;

	glDepthFunc(GL_LEQUAL);
	
	postprocess.useFrameBuffer(anti_aliasing);
	drawObjects(view, projection, cameraPos, deltaTime);
	
	glDepthFunc(GL_LESS);
	particleFactory->draw(view, projection, cameraPos);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	postprocess.drawPostProcess();
	
	if (lines)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	

	
}

GLvoid BillboardWorld1::input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode)
{
	if (key == GLFW_KEY_L && action == GLFW_RELEASE)
	{
		lines = !lines;
		if (lines)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	if (key == GLFW_KEY_P && action == GLFW_RELEASE)
	{
		andrewsBallList.at(0)->launch(glm::vec3(-180.0f, 90.0f, 1.0f));
		andrewsBallList.at(1)->launch(glm::vec3(180.0f, 90.0f, 1.0f));
	}

	if (action == GLFW_PRESS)
	{
		camera.setKey(key, true);
	}

	if (action == GLFW_RELEASE)
	{
		camera.setKey(key, false);
	}

	if (key == GLFW_KEY_T)
	{
		camera.addCameraSpeed(100);
	}

	if (key == GLFW_KEY_DOWN)
	{
		camera.addCameraSpeed(-100);
	}

	if (key == GLFW_KEY_H && action == GLFW_RELEASE)
	{
		anti_aliasing = !anti_aliasing;
	}

	if (key == GLFW_KEY_G && action == GLFW_RELEASE)
	{
		bloom = !bloom;
		postprocess.enableBloom(bloom);
	}

	if (key == GLFW_KEY_KP_ADD && action == GLFW_RELEASE)
	{
		if (samples <= 4)
		{
			samples *= 2;
			postprocess.setSamples(samples);
		}

	}
	if (key == GLFW_KEY_KP_SUBTRACT && action == GLFW_RELEASE)
	{
		if (samples >= 2) 
		{
		samples /= 2;
		postprocess.setSamples(samples);
		}
	}

	if (key == GLFW_KEY_EQUAL && action == GLFW_RELEASE)
	{
		if (postprocess.amount < 1000)
		{
			postprocess.amount += 10;
		}
	}
	if (key == GLFW_KEY_MINUS && action == GLFW_RELEASE)
	{
		if (postprocess.amount > 10)
		{
			postprocess.amount -= 10;
		}
	}

}

GLvoid BillboardWorld1::mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
{
	camera.updateFront(xPosition, yPosition);
}

GLvoid BillboardWorld1::scrollInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
{
}

GLvoid BillboardWorld1::mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods)
{
	if (action == GLFW_PRESS)
	{
		camera.setKey(button, true);
	}

	if (action == GLFW_RELEASE)
	{
		camera.setKey(button, false);
	}
}

GLvoid BillboardWorld1::drawObjects(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, GLfloat deltaTime)
{
	skyBox.draw(view, projection);
	terrain->draw(view, projection, cameraPos, directionalLight, pointLightList, spaceShips[0].getSpotlight(), objectsThatCastShadow);
	rocks->draw(view, projection, cameraPos, 1.0f);
	sphere->renderSphere(milkyway);
	//balls collisions
	for (GLint  i = 0; i < andrewsBallList.size(); i++)
	{
		andrewsBallList.at(i)->draw(view, projection, deltaTime);
	}

	spaceShips.at(0).draw(view, projection, 1.0f);//Renders to texture
}

GLvoid BillboardWorld1::updateObjectShadows()
{
	for (GLint  i = 0; i < objectsThatCastShadow.size(); i++)
	{
		objectsThatCastShadow.pop_back();
	}
	//adding space ships
	objectsThatCastShadow.push_back(std::make_shared<SpaceShip>(spaceShips.at(0)));
	
	//adding balls
	for (GLint  i = 0; i < andrewsBallList.size(); i++)
	{
		objectsThatCastShadow.push_back(std::make_shared<Ball>(*andrewsBallList.at(i)));
	}
}
