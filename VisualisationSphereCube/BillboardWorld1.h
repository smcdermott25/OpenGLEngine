#pragma once
#include "GameWorld.h"
#include "Billboard.h"
#include "Cube.h"
#include "GrassSystem.h"
#include "FlatGround.h"
#include "CloudSystem.h"
#include "Model.h"
#include "FrameBuffer.h"
#include "Imposter.h"
#include "InstancedModel.h"
#include "IsoSurfaceGrid.h"
#include "WaterFountain.h"

#include "RockGenerator.h"
#include "TerrainGenerator.h"
#include "ParticleFactory.h"
#include "PostProcess.h"
#include "Ball.h"
#include "SpaceShip.h"
#include "Collisions.h"
#include "PBRSphere.h"

class BillboardWorld1 : public GameWorld
{
private:
	Camera camera;
	SkyBox skyBox;
	GLboolean lines = false;
	RockGenerator* rocks;
	Terrain* terrain;
	Noise* defaultNoise;
	ParticleFactory* particleFactory;
	PostProcess postprocess;
	DirectionalLight directionalLight;
	std::vector<std::shared_ptr<ShadowCastingObject>> objectsThatCastShadow;
	std::vector<PointLight> pointLightList;
	std::vector<Ball*> andrewsBallList;
	std::vector<glm::vec3> collisionPoints;
	Ball* ball;
	Ball* ball1;
	Collisions* collisions;
	GLfloat previousTime = 0.0f;
	std::vector<SpaceShip>spaceShips;
	GLint updatePostpone = 0;
	PBRSphere* sphere;

public:
	BillboardWorld1();
	~BillboardWorld1();
	GLvoid update(GLfloat deltaTime);
	GLvoid draw();
	GLvoid input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode);
	GLvoid mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods);
	GLvoid mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition);
	GLvoid scrollInput(GLFWwindow* window, GLdouble  xScroll, GLdouble  yScroll);

	GLvoid drawObjects(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, GLfloat deltaTime);
	GLvoid updateObjectShadows();
};

