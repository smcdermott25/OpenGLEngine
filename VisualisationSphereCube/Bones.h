#pragma once
#include <iostream>
#include <vector>
#include <map>

#include <glew.h>
#include <glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Shader.h"
#include "Helper.h"

class Bones
{
private:

	// Keep track of the number of bones within a model
	GLuint boneCount = 0;

	// vector containing the names of all the bones already added
	std::vector<std::string> names;

	GLboolean  flag = true;

	// convert from an aiMatrix4x4 (column order) to glm::mat4 (row order)
	// This is currently done in the gpu instead of cpu 
	GLvoid copyMat(aiMatrix4x4* from, glm::mat4 &to)
	{
		to[0][0] = from->a1; to[1][0] = from->a2;
		to[2][0] = from->a3; to[3][0] = from->a4;
		to[0][1] = from->b1; to[1][1] = from->b2;
		to[2][1] = from->b3; to[3][1] = from->b4;
		to[0][2] = from->c1; to[1][2] = from->c2;
		to[2][2] = from->c3; to[3][2] = from->c4;
		to[0][3] = from->d1; to[1][3] = from->d2;
		to[2][3] = from->d3; to[3][3] = from->d4;
	}

	// Add final transformation for a bone for this specific frame
	GLvoid addFinalTransformation(std::string name, aiMatrix4x4 final)
	{
		finalTransformations.at(boneNameIdMap[name]) = final;
	}

public:

	// Map of a bones name to it's id within it's location of offsetMatrices etc
	std::map<std::string, GLuint> boneNameIdMap;

	/*
	* Each bone contains the following :
	* offsetMatrices -> move to joint / bone space
	* finalTransformation -> final transformation of join / bone after interpolation between positions
	*/
	std::vector<aiMatrix4x4> offsetMatrices;
	std::vector<aiMatrix4x4> mTransformations;
	std::vector<aiMatrix4x4> finalTransformations;

	// Return the index of a specific bone
	GLuint getBoneIndex(std::string name)
	{
		return boneNameIdMap[name];
	}

	// add bone to the vector of bones if it has not already been encountered
	GLvoid addBone(std::string name, GLuint index)
	{
		if (boneNameIdMap.find(name) == boneNameIdMap.end())
		{
			boneNameIdMap[name] = boneCount;
			names.push_back(name);
			boneCount++;
			//std::cout << "Bone count: " << boneCount << std::endl;
			if (boneCount == 63)
			{
				for (GLuint i = 0; i < 63; i++)
				{
					//std::cout << names.at(i) << std::endl;
				}
			}
			flag = true;
		}
		else
		{
			//std::cout << "Already added this bone: " << std::endl;
			flag = false;
		}

	}

	// Add offset matrix for a specific bone
	GLvoid addOffsetMatrix(std::string name, aiMatrix4x4 offset)
	{
		if (boneNameIdMap.find(name) != boneNameIdMap.end() && flag == true)
		{
			//std::cout << "size of offset matrix: " << offsetMatrices.size() << std::endl;
			offsetMatrices.push_back(offset);
		}
	}

	/*
	* Used by the client to insert a bones final transformation
	* This calls the private method addFinalTransformation of this class.
	*
	*/
	GLvoid insertBoneMatrix(aiMatrix4x4 matrix, std::string name)
	{
		addFinalTransformation(name, matrix);
	}

	/*
	* Each frame all of the bones final transformations need to be sent to the vertex shader,
	* Each vertex already has it's id and weight of a particular transformation.
	*/
	GLvoid setUniforms(Shader shader)
	{
		for (GLuint i = 0; i < names.size(); i++)
		{
			std::ostringstream matrixLocation;
			matrixLocation << "bones[" << boneNameIdMap[names.at(i)] << "]";
			glUniformMatrix4fv(glGetUniformLocation(shader.getProgram(),
				matrixLocation.str().c_str()),
				1,
				GL_FALSE,
				finalTransformations.at(i)[0]);
		}

	}


};