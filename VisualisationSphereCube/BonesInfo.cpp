#include "BonesInfo.h"

GLfloat BonesInfo::getTotalWeightValue()
{
	GLfloat value = 0;
	for (GLint  i = 0; i < weightCount; i++)
	{
		value += weights[i];
	}

	//std::cout << value << std::endl;

	return value;
}

GLvoid BonesInfo::addBoneAndWeight(GLfloat weight, std::string name, Bones& bones)
{
	// Max of 4 bones per vertex
	if (weightCount < 4)
	{
		weights[weightCount] = weight;
		boneIds[weightCount] = bones.getBoneIndex(name);
		weightCount++;
		total++;
	}
	else
	{
		// Take bones with the most effect, greatest weights
		for (GLint i = 0; i < 4; i++)
		{
			if (weights[i] < weight)
			{
				weights[i] = weight;
			}
		}
	}
}