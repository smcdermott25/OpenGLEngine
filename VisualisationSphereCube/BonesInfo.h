#pragma once
#include <iostream>
#include <vector>
#include <map>

#include <glew.h>
#include <glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Bones.h"

#define NUM_BONES_PER_VERTEX 4

class BonesInfo
{
private:
	GLuint weightCount = 0;
	GLuint total = 0;
	GLboolean moreThan4Bones = true;

public:
	GLint boneIds[NUM_BONES_PER_VERTEX];
	GLfloat weights[NUM_BONES_PER_VERTEX];

	GLfloat getTotalWeightValue();
	GLvoid addBoneAndWeight(GLfloat weight, std::string name, Bones& bones);
};