#pragma once
#include <iostream>

#include <glew.h>
#include <glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "info.h"

/*
 * This class defines a camera within the game world.
*/
class Camera
{

protected:
	// state of keys
	GLboolean keys[1024] = { false };

	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;
	glm::mat4 projectionViewMatrix;

	glm::vec3 cameraPosition;
	glm::vec3 cameraTarget;
	glm::vec3 cameraFront;
	glm::vec3 upVector;
	glm::vec3 rightVector;
	glm::vec3 worldUp;

	GLfloat pitch = 0.0f;
	GLfloat yaw = -90.0f;

	GLfloat lastXPosition = WIDTH / 2;
	GLfloat lastYPosition = HEIGHT / 2;

	GLboolean firstMouseInWindow = true;

	GLfloat sensitivity = 0.05f;
	GLfloat cameraSpeed = 30.0f;
	GLfloat zoom = 45.0f;

	GLboolean  staticCamera;

	GLvoid Camera::calculateLookAtForStaticCamera();
	GLvoid Camera::calculateFrontVectorForStaticCamera();

	GLvoid calculateLookAt();

public:
	Camera();
	Camera(glm::vec3 newCameraPosition, glm::vec3 newCameraTarget);
	~Camera();

	glm::vec3 getCameraPosition();
	glm::mat4 getView();
	glm::mat4 getProjection();
	glm::mat4 getProjectionViewMatrix();

	GLvoid setKey(GLint key, GLboolean value);
	GLvoid addCameraSpeed(GLint speed);
	GLvoid move(GLfloat deltaTime);
	GLvoid updateFront(GLfloat xPosition, GLfloat yPosition);
	GLvoid setCameraPosition(glm::vec3 newCameraPosition);
	GLvoid setCameraTarget(glm::vec3 newCameraTarget);
	glm::vec3 getCameraFront();
};

