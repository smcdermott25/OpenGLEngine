#include "Client.h"

// Example Client used for Demo
Client::Client()
{
	// Default Ip address and port
	ipAddress = "localhost";
	port = "80";
	blocking = true;

	// For demo purposes of having multiple players sending their model matrices
	bufferLength = sizeof(glm::mat4);
	for (GLint i = 0; i < numClients; i++)
	{
		lastModelsReceived[i] = glm::mat4();
	}
	init();
}

Client::Client(GLchar* ipAddress, GLchar* port, GLint bufferLength, GLboolean blocking)
	:ipAddress(ipAddress),
	port(port),
	bufferLength(bufferLength),
	blocking(blocking)
{
	init();
}

Client::~Client()
{

}

GLvoid Client::init()
{
	WSADATA wsaData;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) 
	{
		printf("WSAStartup failed: %d\n", iResult);
	}

	std::cout << "Winsock initialized: " << std::endl;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Set the server address and port
	iResult = getaddrinfo(ipAddress, port, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed: %d\n", iResult);
		WSACleanup();
	}

	std::cout << "Address: " << iResult << std::endl;
	std::cout << "Server address and port resolved: " << std::endl;

	ptr = result;

	initSocketConnection();
}

GLvoid Client::initSocketConnection()
{
	// Create a SOCKET for connecting to the server
	connectionSocket = socket(ptr->ai_family, ptr->ai_socktype,
		ptr->ai_protocol);

	if (connectionSocket == INVALID_SOCKET) 
	{
		std::cout << "Socket error " << std::endl;
		printf("Error at socket(): %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
	}

	std::cout << "Socket created " << std::endl;

	// Connect to server.
	iResult = connect(connectionSocket, ptr->ai_addr, (GLint )ptr->ai_addrlen);
	if (iResult == SOCKET_ERROR) 
	{
		std::cout << "Socket error" << std::endl;
		closesocket(connectionSocket);
		connectionSocket = INVALID_SOCKET;
	}

	// No longer need this info
	freeaddrinfo(result);

	if (connectionSocket == INVALID_SOCKET) 
	{
		printf("Unable to connect to server!\n");
		WSACleanup();
	}
	else
	{
		std::cout << "Connected to server: " << std::endl;

		// Receive id
		receiveId();

		// Set non-blocking mode on socket
		if (blocking != true)
		{
			std::cout << "Not blocking" << std::endl;
			u_long iMode = 1;
			ioctlsocket(connectionSocket, FIONBIO, &iMode);
		}
	}
}

GLvoid Client::loop()
{
	while (1)
	{
		sendMat4();
		receiveMultipleMat4();
	}
}

GLvoid Client::startLoop()
{
	clientThread = new std::thread(&Client::loop, this);
}

GLvoid Client::receiveId()
{
	GLint  recvbuflen = bufferLength;
	GLchar recvbuf[sizeof(glm::mat4)];

	iResult = recv(connectionSocket, recvbuf, recvbuflen, 0);

	id = *(GLint*)recvbuf;
	std::cout << "ID: " << id << std::endl;
}

GLvoid Client::receiveMat4(GLint index)
{
	GLint  recvbuflen = bufferLength;
	GLchar recvbuf[sizeof(glm::mat4)];

	iResult = recv(connectionSocket, recvbuf, recvbuflen, 0);
	GLint  nError = WSAGetLastError();

	if (nError != WSAEWOULDBLOCK && nError != 0)
	{
	}
	else if (iResult == SOCKET_ERROR)
	{
		std::cout << "Client receive failed, error code: " << WSAGetLastError() << std::endl;
	}
	else if (iResult == 64)
	{
		memcpy(&lastModelsReceived[index], recvbuf, sizeof(glm::mat4));
	}
}

GLvoid Client::receiveMultipleMat4()
{
	GLchar recvbuf[sizeof(GLint )];
	GLint num = numClients;
	
	iResult = recv(connectionSocket, recvbuf, sizeof(GLint ), 0);

	GLint  nError = WSAGetLastError();

	if (nError == WSAEWOULDBLOCK)
	{

	}
	else if (nError != WSAEWOULDBLOCK && nError != 0)
	{
		std::cout << "Error code: " << nError << std::endl;
		std::cout << "Server disconnected " << std::endl;
	}
	else if (iResult == SOCKET_ERROR)
	{
		//std::cout << "Client receive failed, error code: " << WSAGetLastError() << std::endl;
	} 
	else if (iResult > 0)
	{
		num = *(GLint*)recvbuf;
		
		if (num < MAX_CLIENTS)
		{
			numClients = num;
		}

		for (GLint i = 0; i < numClients; i++)
		{
			receiveMat4(i);
		}
	}
}

GLvoid Client::sendMat4()
{
	iResult = send(connectionSocket, (GLchar*)&clientModel, bufferLength, 0);
}

GLvoid Client::closeConnection()
{
	WSACleanup();
}

GLvoid Client::setModel(glm::mat4& model)
{
	clientModel = model;
}

GLint Client::getNumClients()
{
	return numClients;
}

glm::mat4 Client::getModel(GLint index)
{
	return lastModelsReceived[index];
}

GLint Client::getId()
{
	return id;
}