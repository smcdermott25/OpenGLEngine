#pragma
#include <glew.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>

#include <thread>
#include <functional>
#include <iostream>
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#pragma comment(lib, "Ws2_32.lib")

#define MAX_CLIENTS 10

class Client
{
private:
	volatile GLint numClients = 0;
	std::thread* clientThread;

	GLchar* ipAddress;
	GLchar* port;
	GLint bufferLength;
	GLint id;
	GLboolean blocking;

	GLint iResult;
	struct addrinfo *result = NULL;
	struct addrinfo	*ptr = NULL;
	struct addrinfo	hints;

	SOCKET connectionSocket = INVALID_SOCKET;
	glm::mat4 lastModelsReceived[MAX_CLIENTS];
	glm::mat4 clientModel;

	GLvoid init();
	GLvoid initSocketConnection();

	GLvoid receiveId();
	GLvoid loop();

public:
	Client();
	Client(GLchar* ipAddress, GLchar* port, GLint bufferLength, GLboolean blocking);
	~Client();



	GLint getNumClients();
	GLint getId();
	glm::mat4 getModel(GLint index);

	GLvoid startLoop();
	GLvoid receiveMat4(GLint index);
	GLvoid receiveMultipleMat4();
	GLvoid sendMat4();

	GLvoid setModel(glm::mat4& model);

	GLvoid closeConnection();
};