#include "CloudSystem.h"

CloudSystem::CloudSystem()

{
	generatePositions();
}

CloudSystem::~CloudSystem()
{
	delete(billboard);
	delete(billboards);
}

GLvoid CloudSystem::generatePositions()

{
	GLint  rangeMin = -30000;
	GLint  rangeMax = -20000;

	GLint  rangeMinY = 20000;
	GLint  rangeMaxY = 40000;

	for (GLint  a = 0; a < 1; a++)
	{

		GLint  randx = rand() % (rangeMax - rangeMin + 1) + rangeMin;
		GLint  randz = rand() % (rangeMax - rangeMin + 1) + rangeMin;
		GLint  randy = rand() % (rangeMaxY - rangeMinY + 1) + rangeMinY;
		GLint  randScale = rand() % (50- 10 + 1) + 10;

		Orientation orientation;
		orientation.setScale(glm::vec3(randScale, randScale, randScale));

		GLfloat  angle = 90.0f;
		//orientation.setPosition(glm::vec3( centrePoint.x +(i/100.0f), centrePoint.y + (i / 100.0f),  centrePoint.z));
		orientation.setPosition(glm::vec3(randx, randy, randz));


		objectMatrices.push_back(orientation.getModelMatrix());

	}

//	billboard = new Billboard(1, objectMatrices, "Billboards/cloud.png");

}
GLvoid CloudSystem::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{

	billboard->draw(view, projection, cameraPosition);
}