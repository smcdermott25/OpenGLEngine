#pragma once
#include "Billboard.h"

class CloudSystem
{

public:
	CloudSystem();
	CloudSystem::~CloudSystem();

	GLvoid generatePositions();
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition);

private:
	Billboard* billboard;
	std::vector<Billboard>* billboards;
	std::vector<glm::mat4> objectMatrices;
};