#include "CollisionWorld.h"

CollisionWorld::CollisionWorld()
{
	glm::vec3 instances[1] = { glm::vec3(0.0f, 0.0f, 0.0f) };
	glm::vec3 cameraPos = camera.getCameraPosition();
	glm::mat4 m;
	//Adding ball 1
	ball = new Ball(0.03f);
	gameObjects.push_back(ball);
	ball->setPosition(glm::vec3(500.0f, 500.0f, 500.0f));
	//Adding ball 2
	ball1 = new Ball(0.02f);
	gameObjects.push_back(ball1);
	ball1->setPosition(glm::vec3(600.0f, 500.0f, 500.0f));
	//Adding ball 3
	ball2 = new Ball(0.03f);
	gameObjects.push_back(ball2);
	ball2->setPosition(glm::vec3(550.0f, 500.0f, 450.0f));
	//Adding ball 4
	ball3 = new Ball(0.02f);
	gameObjects.push_back(ball3);
	ball3->setPosition(glm::vec3(550.0f, 500.0f, 550.0f));

	//adding Collisions
	collisions = new Collisions();

	terrainGenerator = new TerrainGenerator(glm::vec3(0, 0, 0), 10000000, 700, 257, 3072);
	light = new Light(glm::vec3(-1.0f, 5.0f, -20.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	camera.setCameraPosition(glm::vec3(10.0f, 500.0f, 650.0f));
}


CollisionWorld::~CollisionWorld()
{
}



GLvoid  CollisionWorld::update(GLfloat deltaTime)
{
	if (moving == true)
	{
	std::vector<glm::vec3> collisionPoints = collisions->objectCollisions(gameObjects);
	std::vector<glm::vec3> TerrainCollisionPoints = collisions->terrainCollisions(gameObjects, terrainGenerator->terrain);
	}
	camera.move(deltaTime);
	glm::vec3 cameraPos = camera.getCameraPosition();
	terrainGenerator->update(deltaTime, camera);
}

GLvoid  CollisionWorld::draw()
{
	GLfloat deltaTime = glfwGetTime() - previousTime;
	previousTime = glfwGetTime();
	glm::mat4 view = camera.getView();
	glm::mat4 projection = camera.getProjection();
	glm::vec3 cameraPos = camera.getCameraPosition();
	glm::vec3 lightPosition = light->getPosition();
	glm::vec3 lightColour = light->getColour();

	glDepthFunc(GL_LEQUAL);
	skyBox.draw(camera.getView(), camera.getProjection());
	light->draw(camera.getView(), camera.getProjection());
	ball->draw(view, projection, deltaTime);
	ball1->draw(view, projection, deltaTime);
	ball2->draw(view, projection, deltaTime);
	ball3->draw(view, projection, deltaTime);
	terrainGenerator->draw(view, projection, camera.getCameraPosition());



	
	glDepthFunc(GL_LESS);
}

GLvoid CollisionWorld::input(GLFWwindow* window, GLint key, GLint scancode, GLint action, GLint mode)
{
	if (key == GLFW_KEY_L && action == GLFW_RELEASE)
	{
		lines = !lines;
		if (lines)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		moving = true;
	}

	if (key == GLFW_KEY_E && action == GLFW_RELEASE)
	{
		ball->launch(glm::vec3(300.0f, 80.0f, 0.0f));
		ball1->launch(glm::vec3(-90.0f, 80.0f, 0.0f));

		moving = true;

	}
	if (key == GLFW_KEY_R && action == GLFW_RELEASE)
	{
		ball2->launch(glm::vec3(0.0f, 90.0f, 60.0f));
		ball3->launch(glm::vec3(0.0f, 90.0f, -59.0f));

		moving = true;
	}

	if (action == GLFW_PRESS)
	{
		camera.setKey(key, true);
	}

	if (action == GLFW_RELEASE)
	{
		camera.setKey(key, false);
	}

	if (key == GLFW_KEY_UP)
	{
		camera.addCameraSpeed(100);
	}

	if (key == GLFW_KEY_DOWN)
	{
		camera.addCameraSpeed(-100);
	}
}


GLvoid CollisionWorld::mouseInput(GLFWwindow* window, GLdouble xPosition, GLdouble yPosition)
{
	camera.updateFront(xPosition, yPosition);
}

GLvoid CollisionWorld::scrollInput(GLFWwindow* window, GLdouble xPosition, GLdouble yPosition)
{
}

GLvoid CollisionWorld::mouseButton(GLFWwindow* window, GLint button, GLint action, GLint mods)
{
	if (action == GLFW_PRESS)
	{
		camera.setKey(button, true);
	}

	if (action == GLFW_RELEASE)
	{
		camera.setKey(button, false);
	}
}
