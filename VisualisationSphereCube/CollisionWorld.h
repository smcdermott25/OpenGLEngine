#pragma once

#include <glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "GameWorld.h"
#include "info.h"
#include "Shader.h"
#include "GameObject.h"

#include "Camera.h"
#include "SkyBox.h"
#include "Cube.h"
#include "Ball.h"
#include "Light.h"
#include "TerrainGenerator.h"
#include "Collisions.h"


class CollisionWorld : public GameWorld
{
private:
	Camera camera;
	SkyBox skyBox;
	Ball* ball;
	Ball* ball1;
	Ball* ball2;
	Ball* ball3;
	Ball* ball4;
	Light* light;
	TerrainGenerator* terrainGenerator;
	GLfloat previousTime = 0.0f;
	GLboolean moving = false;
	std::vector<Ball*> gameObjects;
	Collisions* collisions;

	GLboolean lines = false;

	GLuint size = 500;
	GLuint vertexCount = 4;

	GLuint frame = 0;

public:
	CollisionWorld();
	~CollisionWorld();

	GLvoid update(GLfloat deltaTime);


	GLvoid draw();

	GLvoid input(GLFWwindow* window, GLint key, GLint scancode, GLint action, GLint mode);
	GLvoid mouseButton(GLFWwindow* window, GLint button, GLint action, GLint mods);
	GLvoid mouseInput(GLFWwindow* window, GLdouble xPosition, GLdouble yPosition);
	GLvoid scrollInput(GLFWwindow* window, GLdouble xScroll, GLdouble yScroll);
};
