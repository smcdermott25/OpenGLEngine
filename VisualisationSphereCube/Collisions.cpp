
#include "Collisions.h"

Collisions::Collisions()
{
}

std::vector<glm::vec3> Collisions::objectCollisions(std::vector<Ball*> objectsList)
{
	std::vector<glm::vec3> pointsOfCollision;
	if(frameIndex % buffer == 0)
	{
	GLint  index1 = 0;

	//resetting newVelocities size to zero
	while (newVelocities.size() > 0)
	{
		newVelocities.pop_back();
	}
	//resetting changedVelocity size to zero
	while (changedVelocity.size() > 0)
	{
		changedVelocity.pop_back();
	}

	while (index1 < objectsList.size())
	{
		GLint  index2 = 0;
		while (index2 < objectsList.size())
		{
			if (index1 == index2)
			{
				index2++;
			}
			if (index2 < objectsList.size())
			{
				glm::vec3 collisionPoint = checkCollisions(objectsList.at(index1), objectsList.at(index2));

				if (collisionPoint.x != 0.0f && collisionPoint.y != 0.0f && collisionPoint.z != 0.0f)
				{
					pointsOfCollision.push_back(collisionPoint);
				}
				index2++;
			}
		}
		index1++;
	}
	if (pointsOfCollision.size() == 0)
	{
		buffer = 1;
	}
	else
	{
		buffer = 500;
	}
	collisionUpdate(objectsList);
	frameIndex = 1;
	}
	else
	{
		frameIndex++;
	}
	
	return pointsOfCollision;
}

std::vector<glm::vec3> Collisions::terrainCollisions(std::vector<Ball*> objectsList, Terrain* terrain)
{
	GLint  index = 0;

	std::vector<glm::vec3> pointsOfTerrainCollision;

	while (index < objectsList.size())
	{
		
		//method to grab coordinates on terrain close to object
		glm::vec3 terrainCollisionPoint = checkTerrainCollisions(objectsList.at(index), terrain->getTerrainCollisionCoordinates(objectsList.at(index)));
		if (terrainCollisionPoint.x != 0.0f && terrainCollisionPoint.y != 0.0f && terrainCollisionPoint.z != 0.0f)
		{
			pointsOfTerrainCollision.push_back(terrainCollisionPoint);
		}
		index++;


		//POINTOFTERRAINCOLLISION IS THE COORDINATES YOU SHOULD NEED FOR PARTICLE EFFECTS ON TERRAIN AT POINT OF COLLISION
	}
	return pointsOfTerrainCollision;
}

glm::vec3 Collisions::checkCollisions(GameObject* obj1, GameObject* obj2)
{
	glm::vec3 pointOfCollision;
	GLint collided = 0;

	//obj1 info
	glm::vec3 obj1Vector = obj1->getPosition();
	GLfloat radiusObj1 = obj1->getRadius();
	
	//obj2 info
	glm::vec3 obj2Vector = obj2->getPosition();
	GLfloat radiusObj2 = obj2->getRadius();

	//check if the distance between the objects x coordinates is less than their radius
	if (((obj1Vector.x - obj2Vector.x) > -(radiusObj1 + radiusObj2)) && ((obj1Vector.x - obj2Vector.x) < (radiusObj1 + radiusObj2)))
	{
		collided++;
	}
	
	//check if the distance between the objects y coordinates is less than their radius
	if (((obj1Vector.y - obj2Vector.y) > -(radiusObj1 + radiusObj2)) && ((obj1Vector.y - obj2Vector.y) < (radiusObj1 + radiusObj2)))
	{
		collided++;
	}
	
	//check if the distance between the objects z coordinates is less than their radius
	if (((obj1Vector.z - obj2Vector.z) > -(radiusObj1 + radiusObj2)) && ((obj1Vector.z - obj2Vector.z) < (radiusObj1 + radiusObj2)))
	{
		collided++;
	}
	
	//to be colliding there has to be intersections across 3 axis, so if any one axis is not intersecting, a collision has not occurred, or the object is no longer in collision
	if (collided == 3)
	{
			GLfloat lastCoord;
			//if the objects are have the exact same values on two of the planes, the point of collision will be exactly one of the objects + its radius
			//if x and y are 0
			if ((fabs(obj1Vector.x) - fabs(obj2Vector.x) == 0) && (fabs(obj1Vector.y) - fabs(obj2Vector.y) == 0))
			{
				if ((fabs(obj1Vector.z) - fabs(obj2Vector.z)) < 0)
				{
					lastCoord = obj1Vector.z + radiusObj1;
				}
				else
				{
					lastCoord = obj1Vector.z - radiusObj1;
				}
				pointOfCollision = glm::vec3(obj1Vector.x, obj2Vector.y, lastCoord);
			}
			//if x and z are 0
			else if ((fabs(obj1Vector.x) - fabs(obj2Vector.x) == 0) && (fabs(obj1Vector.z) - fabs(obj2Vector.z) == 0))
			{
				if ((fabs(obj1Vector.y) - fabs(obj2Vector.y)) < 0)
				{
					lastCoord = obj1Vector.y + radiusObj1;
				}
				else
				{
					lastCoord = obj1Vector.y - radiusObj1;
				}
				pointOfCollision = glm::vec3(obj1Vector.x, lastCoord, obj2Vector.z);
			}

		//if y and z are 0
		else if ((fabs(obj1Vector.y) - fabs(obj2Vector.y) == 0) && (fabs(obj1Vector.z) - fabs(obj2Vector.z) == 0))
		{
			if ((fabs(obj1Vector.x) - fabs(obj2Vector.x)) < 0)
			{
				lastCoord = obj1Vector.x + radiusObj1;
			}
			else
			{
				lastCoord = obj1Vector.x - radiusObj1;
			}
			pointOfCollision = glm::vec3(lastCoord, obj1Vector.y, obj2Vector.z);
		}
		//if they aren't on two of the same planes
		else
		{
			GLfloat xDifferenceHalf = fabs(obj1Vector.x - obj2Vector.x) / 2;
			GLfloat yDifferenceHalf = fabs(obj1Vector.y - obj2Vector.y) / 2;
			GLfloat zDifferenceHalf = fabs(obj1Vector.z - obj2Vector.z) / 2;
			//GLfloat xDifferenceSquared = pow((obj1Vector.x - obj2Vector.x), 2.0);
			//GLfloat yDifferenceSquared = pow((obj1Vector.y - obj2Vector.y), 2.0);
			//GLfloat zDifferenceSquared = pow((obj1Vector.z - obj2Vector.z), 2.0);
			//GLfloat distanceBetween = sqrt((xDifferenceSquared + yDifferenceSquared + zDifferenceSquared));
			//printf("\n\nDistance between: %f\n\n", distanceBetween);

			//get X collision point
			if (obj1Vector.x > obj2Vector.x)
			{
				pointOfCollision.x = obj2Vector.x + xDifferenceHalf;
			}
			else
			{
				pointOfCollision.x = obj1Vector.x + xDifferenceHalf;
			}
			//get Y collision point
			if (obj1Vector.y > obj2Vector.y)
			{
				pointOfCollision.y = obj2Vector.y + yDifferenceHalf;
			}
			else
			{
				pointOfCollision.y = obj1Vector.y + yDifferenceHalf;
			}
			//get Z collision point
			if (obj1Vector.z > obj2Vector.z)
			{
				pointOfCollision.z = obj2Vector.z + zDifferenceHalf;
			}
			else
			{
				pointOfCollision.z = obj1Vector.z + zDifferenceHalf;
			}

		}
		changedVelocity.push_back(true);
		newVelocities.push_back(obj1->movement->applyMomentum(obj1->getVelocity(), obj2->getVelocity(), obj1->getWeight(), obj2->getWeight()));
		
	}
	else
	{
		changedVelocity.push_back(false);
	}
	return pointOfCollision;
}

glm::vec3 Collisions::checkTerrainCollisions(GameObject* obj, GLfloat terrainHeight)
{
	glm::vec3 objVector = obj->getPosition();
	GLfloat radius = obj->getRadius();

	glm::vec3 pointOfTerrainCollision;
	if ((objVector.y - radius) < terrainHeight)
	{
		terrainCollisionUpdate(obj);
		pointOfTerrainCollision = glm::vec3(objVector.x, terrainHeight, objVector.z);
	}
	//returning the point of collision
	return pointOfTerrainCollision;
}

GLvoid Collisions::collisionUpdate(std::vector<Ball*> objectsList)
{
	int i = 0;
	int j = 0;
	while (i < changedVelocity.size())
	{
		GLfloat k = floor((i / (GLfloat)changedVelocity.size()) * (GLfloat)objectsList.size());
		if(changedVelocity.at(i) == true)
		{
			//negating collisions for a short time
			if (objectsList.at(k)->movement->getBouncing() == true)
			{
				objectsList.at(k)->movement->setVelocity(newVelocities.at(j));
				objectsList.at(k)->movement->setBouncing(false);
				objectsList.at(k)->movement->setDropping(true);
			}
			else if (objectsList.at(k)->movement->getDropping() == true)
			{
				objectsList.at(k)->movement->setVelocity(newVelocities.at(j));
				objectsList.at(k)->movement->setBouncing(true);
				objectsList.at(k)->movement->setDropping(false);
			}
			else if (objectsList.at(k)->movement->getUpwards() == true)
			{
				objectsList.at(k)->movement->setVelocity(newVelocities.at(j));
				objectsList.at(k)->movement->setBouncing(true);
				objectsList.at(k)->movement->setDropping(false);
			}
			j++;
		}
		i++;
	}
}

GLvoid  Collisions::terrainCollisionUpdate(GameObject* obj)
{
	if (obj->movement->getBouncing() == true)
	{
		//printf("\nbouncing -> not bouncing\n");
		obj->movement->setBouncing(false);
		obj->bouncing = false;
		obj->movement->setDropping(true);
	}
	else if (obj->movement->getBouncing() == false && obj->movement->getRolling() == true)
	{
		obj->movement->setRolling(true);
		obj->bouncing = false;
		//movement->setBouncing(true);
		obj->movement->setDropping(false);
	}
	else
	{
		obj->bouncing = true;
		obj->movement->setBouncing(true);
		obj->movement->setDropping(false);
	}

}