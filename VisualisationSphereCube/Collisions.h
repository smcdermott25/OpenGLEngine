#pragma once
#include "Ball.h"
#include "Terrain.h"
#include "FlatGround.h"
#include "CloudSystem.h"
#include "Model.h"
#include "FrameBuffer.h"
#include "Imposter.h"
#include "InstancedModel.h"
#include "ShadowCastingObject.h"

class Collisions
{
public:

Collisions::Collisions();
std::vector<glm::vec3> Collisions::objectCollisions(std::vector<Ball*> objectsList);
std::vector<glm::vec3> Collisions::terrainCollisions(std::vector<Ball*> objectsList, Terrain* terrain);

private:

GLuint frameIndex = 1;
GLuint buffer = 1;
std::vector<glm::vec3> newVelocities;
std::vector<GLboolean> changedVelocity;

glm::vec3 Collisions::checkCollisions(GameObject* obj1, GameObject* obj2);
glm::vec3 Collisions::checkTerrainCollisions(GameObject* obj, GLfloat terrainHeight);
GLvoid Collisions::collisionUpdate(std::vector<Ball*> objectsList);
GLvoid Collisions::terrainCollisionUpdate(GameObject* obj);

};