#pragma once
#include <glew.h>
#include <glfw3.h>

/*
* Interface for a controller, Individual controller classes will wrap their input
* to the methods below.
* Every controller will be mapped to the xbox controller button names,
* e.g. x button on ps4 will be called the a button.
*/
class Controller
{
protected:
	GLboolean aButton = 0;
	GLboolean xButton = 0;
	GLboolean yButton = 0;
	GLboolean bButton = 0;

	GLboolean leftBumperOne = 0;
	GLboolean rightBumperOne = 0;

	GLfloat leftBumperTwo = 0;
	GLfloat rightBumperTwo = 0;

	GLfloat leftStickHorizontalValue = 0;
	GLfloat leftStickVerticleValue = 0;
	GLfloat rightStickHorizontalValue = 0;
	GLfloat rightStickVerticleValue = 0;

	static GLint count;

public:
	virtual GLvoid updateAxis() = 0;
	virtual GLvoid updateButtons() = 0;

	inline GLboolean getAButton()
	{
		return aButton;
	}

	inline GLboolean getXButton()
	{
		return xButton;
	}

	inline GLboolean getYButton()
	{
		return yButton;
	}

	inline GLboolean getBButton()
	{
		return bButton;
	}

	inline GLboolean getLeftBumperOne()
	{
		return leftBumperOne;
	}

	inline GLboolean getRightBumperOne()
	{
		return rightBumperOne;
	}

	inline GLfloat getLeftBumperTwo()
	{
		return leftBumperTwo;
	}

	inline GLfloat getRightBumperTwo()
	{
		return rightBumperTwo;
	}

	inline GLfloat getLeftHorizontal()
	{
		return leftStickHorizontalValue;
	}

	inline GLfloat getLeftVertical()
	{
		return leftStickVerticleValue;
	}

	inline GLfloat getRightHorizontal()
	{
		return rightStickHorizontalValue;
	}

	inline GLfloat getRightVertical()
	{
		return rightStickVerticleValue;
	}
};