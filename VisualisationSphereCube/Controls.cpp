#include "Controls.h"
#include "XboxController.h"
#include "PS4Controller.h"

Controls* Controls::instance = 0;
std::vector<Controller*> Controls::controllers;

Controls::Controls()
{                            
	controllers.push_back(new XboxController(0));
	controllers.push_back(new PS4Controller(1));
}

Controls::~Controls()
{
	for (GLint i = 0; i < controllers.size(); i++)
	{
		delete(controllers.at(i));
	}
}

Controls* Controls::getInstance()
{
	if (instance == nullptr)
	{
		instance = new Controls();
	}

	return instance;
}

GLvoid Controls::controllerConnected(std::string name)
{
	std::cout << "Controller connected: " << name.c_str() << std::endl;
	
}

Controller* Controls::getController(GLint id)
{
	if (id < controllers.size())
	{
		return controllers.at(0);
	}
	else
	{
		return nullptr;
	}
}

// Mutators
GLvoid Controls::setKey(GLint key, GLboolean value)
{
	keys[key] = value;
	if (key == GLFW_KEY_X)
	{
		std::cout << "xbox " << std::endl;
		controllers.push_back(new XboxController(0));
	}
	else if (key == GLFW_KEY_P)
	{
		controllers.push_back(new PS4Controller(1));
	}
}

GLvoid Controls::updateMouseDifference(GLdouble x, GLdouble y)
{
	if (keys[GLFW_MOUSE_BUTTON_LEFT] )
	{
		if (firstClickedLeft)
		{
			lastYPos = y;
			firstClickedLeft = false;
		}

		mouseDY = y - lastYPos;
		lastYPos = y;
	}

	if (keys[GLFW_MOUSE_BUTTON_RIGHT])
	{
		if (firstClickedRight)
		{
			lastXPos = x;
			firstClickedRight = false;
		}

		std::cout << x << std::endl;
		mouseDX = x - lastXPos;
		lastXPos = x;
	}
}

GLvoid Controls::updateAxis(GLint id)
{
	GLint count = 0;
	controllers.at(id)->updateAxis();
}

GLvoid Controls::updateButtons(GLint id)
{
	GLint count = 0;
	controllers.at(id)->updateButtons();
}




