#pragma once
#include <vector>
#include <glew.h>
#include <glfw3.h>

#include "iostream";

#include "Controller.h"


class Controls
{
private:
	Controls();
	~Controls();

	static Controls* instance;
	static std::vector<Controller*> controllers;
	GLboolean xboxC = true;
public:
	// state of keys
	GLboolean keys[1024] = { false };
	GLfloat mouseDY = 0;
	GLfloat mouseDX = 0;
	GLdouble scrollValue = 0;

	GLfloat lastYPos;
	GLfloat lastXPos;
	GLboolean firstClickedRight = false;
	GLboolean firstClickedLeft = false;

	GLvoid updateMouseDifference(GLdouble x, GLdouble y);
	GLvoid setKey(GLint key, GLboolean value);

	// Keyboard controls / mouse controls
	static Controls* getInstance();

	// Controller 
	static GLvoid Controls::updateAxis(GLint id);
	static GLvoid Controls::updateButtons(GLint id);
	static GLvoid controllerConnected(std::string name);
	static GLvoid controllerDisconnected(std::string name);
	static Controller* getController(GLint id);
};