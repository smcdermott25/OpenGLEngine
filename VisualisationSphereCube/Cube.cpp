#include "Cube.h"




Cube::Cube()
	:GameObject("Shader/vertex.txt", "Shader/fragment.txt")
{
	init(10, glm::vec3(1.0f, 1.0f, 1.0f));
}

Cube::Cube(GLfloat radius, glm::vec3 colour)
	:GameObject("Shader/vertex.txt", "Shader/fragment.txt")
{
	init(radius, colour);
}

Cube::~Cube()
{
}

GLvoid Cube::init(GLfloat rRadius, glm::vec3 colour)
{
	radius = rRadius;
	modelMatrix = glm::mat4(1);

	shaderProgram->useProgram();
	glUniform3f(glGetUniformLocation(shaderProgram->getProgram(), "objectColour"), colour.x, colour.y, colour.z);

	GLfloat vertices[] =
	{
		// back face
		-radius, -radius, -radius,
		-radius,  radius, -radius,
		radius,  radius, -radius,

		radius,  radius, -radius,
		radius, -radius, -radius,
		-radius, -radius, -radius,

		// front face
		-radius, -radius,  radius,
		-radius,  radius,  radius,
		radius, -radius,  radius,

		-radius,  radius,  radius,
		radius,  radius,  radius,
		radius, -radius,  radius,

		// left face
		-radius, -radius,  radius,
		-radius,  radius,  radius,
		-radius,  radius, -radius,

		-radius,  radius, -radius,
		-radius, -radius, -radius,
		-radius, -radius,  radius,

		// right face
		radius, -radius,  radius,
		radius,  radius,  radius,
		radius,  radius, -radius,

		radius,  radius, -radius,
		radius, -radius, -radius,
		radius, -radius,  radius,

		// bottom face
		-radius, -radius,  radius,
		-radius, -radius, -radius,
		radius, -radius, -radius,

		radius, -radius, -radius,
		radius, -radius,  radius,
		-radius, -radius,  radius,

		// top face
		-radius,  radius,  radius,
		-radius,  radius, -radius,
		radius,  radius, -radius,

		radius,  radius, -radius,
		radius,  radius,  radius,
		-radius,  radius,  radius,

	};

	GLfloat  normals[]
	{
		// back face
		0, 0, -1,
		0, 0, -1,
		0, 0, -1,
		0, 0, -1,
		0, 0, -1,
		0, 0, -1,

		// front face
		0, 0, 1,
		0, 0, 1,
		0, 0, 1,
		0, 0, 1,
		0, 0, 1,
		0, 0, 1,

		// left face
		-1, 0, 0,
		-1, 0, 0,
		-1, 0, 0,
		-1, 0, 0,
		-1, 0, 0,
		-1, 0, 0,

		// right face 
		1, 0, 0,
		1, 0, 0,
		1, 0, 0,
		1, 0, 0,
		1, 0, 0,
		1, 0, 0,

		// bottom face
		0, -1, 0,
		0, -1, 0,
		0, -1, 0,
		0, -1, 0,
		0, -1, 0,
		0, -1, 0,

		// top face
		0, 1, 0,
		0, 1, 0,
		0, 1, 0,
		0, 1, 0,
		0, 1, 0,
		0, 1, 0
	};

	GLfloat textureCoordinates[]
	{
		0, 0,
		0, 1,
		1, 1,
		1, 1,
		1, 0,
		0, 0,

		0, 0,
		0, 1,
		1, 0,
		0, 1,
		1, 1,
		1, 0,

		0, 0,
		0, 1,
		1, 1,
		1, 1,
		1, 0,
		0, 0,

		0, 0,
		0, 1,
		1, 1,
		1, 1,
		1, 0,
		0, 0,

		0, 1,
		1, 1,
		1, 0,
		1, 0,
		0, 0,
		0, 1,

		0, 1,
		1, 1,
		1, 0,
		1, 0,
		0, 0,
		0, 1
	};


	// Generate Vertex Array object, fill with data
	helper.createVertexArray(vertexArray);
	helper.fillVertexArrayWithVertices(vertexArray, vertexBuffer, vertices, sizeof(vertices), 0);
	helper.fillVertexArrayWithNormals(vertexArray, normalBuffer, normals, sizeof(normals), 1);
	helper.fillVertexArrayWithTextureCoords(vertexArray, textureCoordsBuffer, textureCoordinates, sizeof(textureCoordinates), 2);

	glGenBuffers(1, &texture);
	helper.texture2DSetRepeatAndLinear(texture);
	helper.loadTextureWithImage2D(texture, "skybox/ram.jpg", 0);	
}

GLuint Cube::getTexture()
{
	return texture;
}

GLvoid Cube::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 lightPosition, glm::vec3 lightColour, glm::vec3 cameraPosition, GLfloat  deltaTime)
{
	if (expanding)
	{
		expandAndContract(deltaTime);
	}

	if (wobbling)
	{
		wobble(deltaTime);
	}

	if (moving)
	{
		movingX = false;
		movingY = false;
		movingZ = false;
		dropping = false;
		moveTo(moveToPosition, deltaTime);
	}

	if (movingX)
	{
		movement->moveX(movement->getVelocity(), deltaTime);
	}
	if (movingY)
	{
		movement->moveX(movement->getVelocity(), deltaTime);
	}
	if (movingZ)
	{
		movement->moveX(movement->getVelocity(), deltaTime);
	}
	if (dropping)
	{
		movement->move(deltaTime);
	}

	shaderProgram->useProgram();
	GLuint program = shaderProgram->getProgram();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	

	glUniform3f(glGetUniformLocation(program, "lightColour"), lightColour.x, lightColour.y, lightColour.z);
	glUniform3f(glGetUniformLocation(program, "lightPosition"), lightPosition.x, lightPosition.y, lightPosition.z);
	glUniform3f(glGetUniformLocation(program, "cameraPosition"), cameraPosition.x, cameraPosition.y, cameraPosition.z);

	glUniformMatrix4fv(glGetUniformLocation(program, "model"), 1, GL_FALSE, glm::value_ptr(orientation->getModelMatrix()));
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	glBindVertexArray(vertexArray);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}


GLvoid Cube::setExpand()
{	
	this->expanding = !expanding;
}

GLvoid Cube::expandAndContract(GLfloat deltaTime)
{
	GLfloat scaleValue = expandSensitivity * deltaTime;

	if (expandTurn)
	{
		scaleValues += scaleValue;
		modelMatrix = glm::scale(modelMatrix, glm::vec3(1 + scaleValue, 1 + scaleValue, 1 + scaleValue));

		if (scaleValues.x > 2)
		{
			expandTurn = false;
		}
	}
	else if (!expandTurn)
	{
		scaleValues -= scaleValue;
		modelMatrix = glm::scale(modelMatrix, glm::vec3(1 - scaleValue, 1 - scaleValue, 1 - scaleValue));
		if (scaleValues.x < 1)
		{
			GLfloat diff = 1 - scaleValues.x;
			modelMatrix = glm::scale(modelMatrix, glm::vec3(1 + diff, 1 + diff, 1 + diff));
			scaleValues += diff;
			expandTurn = true;
		}
	}
}


GLvoid Cube::setWobble()
{
	this->wobbling = !wobbling;
}

GLvoid Cube::wobble(GLfloat deltaTime)
{

	// 0.5 = wobble sensitivity
	GLfloat sensitivity = 0.5;
	GLfloat change = sensitivity * deltaTime;

	if (positiveZTurn)
	{
		angle += change;
		modelMatrix = glm::rotate(modelMatrix, sensitivity * deltaTime, glm::vec3(0, 0, 1));
		if (angle > 0.5)
		{
			positiveZTurn = false;
		}
	}
	else
	{
		angle -= change;
		modelMatrix = glm::rotate(modelMatrix, -sensitivity * deltaTime, glm::vec3(0, 0, 1));
		if (angle < -0.5)
		{
			positiveZTurn = true;
		}
	}
	
}

GLfloat Cube::getRadius()
{
	return radius;
}
