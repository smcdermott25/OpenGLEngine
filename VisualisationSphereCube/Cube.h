#pragma once
#include <glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "GameObject.h"
#include "info.h"


class Cube : public GameObject
{
private:

	GLuint vertexArray;
	GLuint vertexBuffer;
	GLuint normalBuffer;
	GLuint textureCoordsBuffer;

	GLuint    texture;

	GLboolean expanding = false;
	GLdouble  expandTurn = true;

	GLboolean wobbling = false;
	GLdouble  positiveZTurn = false;
	GLfloat   angle = 0;

	GLfloat expandSensitivity = 0.7;
	GLuint  timeExpanding = 5;

	glm::vec3 scaleValues = glm::vec3(1, 1, 1);
	glm::mat4 modelMatrix;

	


public:
	Cube();
	Cube(GLfloat radius, glm::vec3 colour);
	~Cube();

	GLvoid init(GLfloat radius, glm::vec3 colour);
	GLuint getTexture();
	GLfloat radius;

	GLvoid draw(glm::mat4 view,
		glm::mat4 projection,
		glm::vec3 lightPosition, 
		glm::vec3 lightColour,
		glm::vec3 cameraPosition,
		GLfloat  deltaTime);

	GLvoid setExpand();
	GLvoid expandAndContract(GLfloat deltaTime);

	GLvoid setWobble();
	GLvoid wobble(GLfloat deltaTime);
	GLfloat Cube::getRadius();
};

