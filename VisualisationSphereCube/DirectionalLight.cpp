#include "DirectionalLight.h"
/*Directional light - simple and only requires direction and standard light variables to be set*/
DirectionalLight::DirectionalLight()
	:BaseLight(glm::vec3(1.0,1.0,1.0), glm::vec3(0.05f, 0.05f, 0.05f), glm::vec3(0.4f, 0.4f, 0.4f), glm::vec3(0.5f, 0.5f, 0.5f))
{
	DirectionalLight(glm::vec4(0,1,0,0));
}
DirectionalLight::DirectionalLight(glm::vec4 newDirectionOfLight)
	:BaseLight(glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.5f, 0.5f, 0.5f), glm::vec3(0.8f, 0.8f, 0.8f), glm::vec3(0.8f, 0.8f, 0.8f))
{
	directionOfLight = newDirectionOfLight;
}
glm::vec4 DirectionalLight::getDirectionOfLight()
{
	return directionOfLight;
}

GLint  DirectionalLight::setDirectionalLight(glm::vec4 newDirection)
{
	if (newDirection.w != 0.0f)
	{
		std::cout << "This is not a directional light, W does not equal 0.0" << std::endl;
		return 0;
	}
	directionOfLight = newDirection;
	return 1;

}

