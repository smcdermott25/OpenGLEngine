#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "BaseLight.h"
/*Class is intended to be a singluar directional light of which there will be only one per scene with no exact position/origin - useful for suns*/
class DirectionalLight:public BaseLight
{
private:
	glm::vec4 directionOfLight;

public:
	DirectionalLight();
	DirectionalLight(glm::vec4 directionOfLight);
	glm::vec4 getDirectionOfLight();
	GLint  setDirectionalLight(glm::vec4);
};
