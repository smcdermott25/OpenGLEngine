#include "FlatGround.h"

FlatGround::FlatGround()
	:GameObject("Shader/basicVertex.txt", "Shader/textureNoLightFragment.txt")
{

	setUpVerticeData();
}
FlatGround::~FlatGround()
{

}
GLvoid FlatGround::setUpVerticeData()
{
	glm::vec3 objPos1 = glm::vec3(0.0f, 0.0f, 0.0f);


	GLfloat  radius = 500.0f;
	GLfloat verticeData[] = {
		objPos1.x - radius,  0.0f, objPos1.y - radius ,
		objPos1.x - radius,  0.0f, objPos1.y + radius,
		objPos1.x + radius,  0.0f, objPos1.y - radius,
		objPos1.x - radius,  0.0f, objPos1.y + radius,
		objPos1.x + radius,  0.0f, objPos1.y + radius,
		objPos1.x + radius,  0.0f, objPos1.y - radius,
	};


	std::copy(verticeData, verticeData + 18, vertices);
	verticeSize = sizeof(verticeData);

	GLfloat textureCoordinateData[]
	{
		0, 1,
		0, 0,
		1, 1,
		0, 0,
		1, 0,
		1, 1,
	};
	std::copy(textureCoordinateData, textureCoordinateData + 12, textureCoordinates);
	textureCoordinatesSize = sizeof(textureCoordinateData);


	//Create vertexArray and vertexBuffer like normal
	helper.createVertexArray(vertexArray);
	helper.fillVertexArrayWithVertices(vertexArray, vertexBuffer, verticeData, sizeof(verticeData), 0);

	//Apply texturing like normal
	std::cout << "Size of text " << sizeof(textureCoordinateData) << std::endl;
	std::cout << "Check value1 " << textureCoordinateData[0] << std::endl;
	std::cout << "Check 2 " << textureCoordinateData[1] << std::endl;
	helper.fillVertexArrayWithTextureCoords(vertexArray, textureCoordsBuffer, textureCoordinateData, sizeof(textureCoordinateData), 2);

	//glGenTextures(1, &texture);
	helper.texture2DSetRepeatAndLinear(texture);

	imagePath = "Billboards/darkGreen.jpg";
	helper.loadTextureWithImage2D(texture, imagePath, 0);

	orientation->setPosition(glm::vec3(0.0f, -3.50f, 0.0f));
}
GLvoid FlatGround::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{
	glDisable(GL_BLEND);
	shaderProgram->useProgram();
	//blendTool.fullyTransclucentSetUp(texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	

	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "model"), 1, GL_FALSE, glm::value_ptr(orientation->getModelMatrix()));
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));



	glBindVertexArray(vertexArray);
	//Draw
	glDrawArrays(GL_TRIANGLES, 0, 6);

	//Release VertexArray
	glBindVertexArray(0);

}