#pragma once

#include "GameObject.h"
#include "AlphaBlenderTool.h"



class FlatGround : public GameObject
{
protected:
	AlphaBlenderTool blendTool;

	GLchar* imagePath;

	GLuint vertexArray;
	GLuint vertexBuffer;
	GLfloat vertices[18];
	GLint verticeSize;

	GLfloat textureCoordinates[12];
	GLint textureCoordinatesSize;
	GLuint textureCoordsBuffer;
	GLuint texture;

public:

	FlatGround();
	~FlatGround();

	GLvoid setUpVerticeData();
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition);

};


