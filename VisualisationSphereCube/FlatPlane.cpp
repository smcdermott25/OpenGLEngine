#include"FlatPlane.h"
#include "glm/ext.hpp"

/*Somewhat deprecated, this could quickly be added back in to take shadow casting objects and allow shadows to be drawn on it*/
FlatPlane::FlatPlane()
	:GameObject("Flate Plane", "Shader/multiCasterVertex.txt", "Shader/multiCasterFragment.txt")
{
	shaderProgram->useProgram();
	materialShininess = 32.0f;
	GLfloat planeVertices[] = {
		// Positions          // Normals         // Texture Coords
		25.0f, -0.5f, 25.0f, 0.0f, 1.0f, 0.0f, 25.0, 0.0f,
		-25.0f, -0.5f, -25.0f, 0.0f, 1.0f, 0.0f, 0.0f, 25.0f,
		-25.0f, -0.5f, 25.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,

		25.0f, -0.5f, 25.0f, 0.0f, 1.0f, 0.0f, 25.0f, 0.0f,
		25.0f, -0.5f, -25.0f, 0.0f, 1.0f, 0.0f, 25.0f, 25.0f,
		-25.0f, -0.5f, -25.0f, 0.0f, 1.0f, 0.0f, 0.0f, 25.0f
	};

	// Setup plane VAO
	glGenVertexArrays(1, &planeVAO);
	glGenBuffers(1, &planeVBO);
	glBindVertexArray(planeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), &planeVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
	glBindVertexArray(0);

	// Load textures
	glGenTextures(1, &woodTexture);
	glGenTextures(1, &specularTexture);
	int width, height,components;
	unsigned char* image;
	GLenum format;
	// Diffuse map woodPanel.png
	image = stbi_load("Texture/woodPanel.png", &width, &height,&components, 0);
	if (components == 1)
		format = GL_RED;
	else if (components == 3)
		format = GL_RGB;
	else
		format = GL_RGBA;

	glBindTexture(GL_TEXTURE_2D, woodTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);

	// Specular map
	image = stbi_load("Texture/woodPanel.png", &width, &height,&components, 0);
	
	if (components == 1)
		format = GL_RED;
	else if (components == 3)
		format = GL_RGB;
	else
		format = GL_RGBA;

	glBindTexture(GL_TEXTURE_2D, specularTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 1);
	shadow.createShadowTextureAndFBO();
}


GLvoid FlatPlane::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow)
{
	shadow.renderDepthSceneToTexture(directionalLight, objectsThatCastShadow);
	shaderProgram->useProgram();
	GLuint program = shaderProgram->getProgram();
	glUniform1f(glGetUniformLocation(program, "shadowsOn"), 1.0);
	glUniformMatrix4fv(glGetUniformLocation(program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(shadow.getLightSpaceMatrix()));
	glUniform3fv(glGetUniformLocation(program, "lightPos"), 1, glm::value_ptr(shadow.getLightPositionForShadow()));
	// Bind diffuse map
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, woodTexture);
	glUniform1i(glGetUniformLocation(program, "material.diffuse"), 0);
	// Bind specular map
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, specularTexture);
	glUniform1i(glGetUniformLocation(program, "material.specular"), 1);
	// Bind Depth map
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, shadow.getDepthMapTexture());
	glUniform1i(glGetUniformLocation(program, "shadowMap"), 2);
	glActiveTexture(GL_TEXTURE0);


	glUniform1f(glGetUniformLocation(program, "material.shininess"), materialShininess);
	glUniform3f(glGetUniformLocation(program, "viewPos"), cameraPos.x, cameraPos.y, cameraPos.z);

	glUniform3f(glGetUniformLocation(program, "dirLight.direction"), directionalLight.getDirectionOfLight().x, directionalLight.getDirectionOfLight().y, directionalLight.getDirectionOfLight().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.ambient"), directionalLight.getAmbient().x, directionalLight.getAmbient().y, directionalLight.getAmbient().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.diffuse"), directionalLight.getDiffuse().x, directionalLight.getDiffuse().y, directionalLight.getDiffuse().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.specular"), directionalLight.getSpecular().x, directionalLight.getSpecular().y, directionalLight.getSpecular().z);
	//Iterate over all point lights
	for (GLint  i = 0; i < pointLightList.size(); i++)
	{

		std::string number = std::to_string(i);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].position").c_str()), pointLightList.at(i).getPosition().x, pointLightList.at(i).getPosition().y, pointLightList.at(i).getPosition().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].ambient").c_str()), pointLightList.at(i).getAmbient().x, pointLightList.at(i).getAmbient().y, pointLightList.at(i).getAmbient().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].diffuse").c_str()), pointLightList.at(i).getDiffuse().x, pointLightList.at(i).getDiffuse().y, pointLightList.at(i).getDiffuse().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].specular").c_str()), pointLightList.at(i).getSpecular().x, pointLightList.at(i).getSpecular().y, pointLightList.at(i).getSpecular().z);
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].constant").c_str()), pointLightList.at(i).getConstant());
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].linear").c_str()), pointLightList.at(i).getLinear());
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].quadratic").c_str()), pointLightList.at(i).getQuadratic());

	}
	// SpotLight
	glUniform3f(glGetUniformLocation(program, "spotLight.position"), spotLight.getPosition().x, spotLight.getPosition().y, spotLight.getPosition().z);
	//printf("Spotlight position is: X %f, Y %f Z %f", spotLight.getPosition().x, spotLight.getPosition().y, spotLight.getPosition().z);
	glUniform3f(glGetUniformLocation(program, "spotLight.direction"), spotLight.getFront().x, spotLight.getFront().y, spotLight.getFront().z);
	//printf("Front is: X %f, Y %f Z %f", spotLight.getFront().x, spotLight.getFront().y, spotLight.getFront().z);
	glUniform3f(glGetUniformLocation(program, "spotLight.ambient"), spotLight.getAmbient().r, spotLight.getAmbient().g, spotLight.getAmbient().b);
	glUniform3f(glGetUniformLocation(program, "spotLight.diffuse"), spotLight.getDiffuse().r, spotLight.getDiffuse().g, spotLight.getDiffuse().b);
	glUniform3f(glGetUniformLocation(program, "spotLight.specular"), spotLight.getSpecular().r, spotLight.getSpecular().g, spotLight.getSpecular().b);
	glUniform1f(glGetUniformLocation(program, "spotLight.constant"), spotLight.getConstant());
	glUniform1f(glGetUniformLocation(program, "spotLight.linear"), spotLight.getLinear());
	glUniform1f(glGetUniformLocation(program, "spotLight.quadratic"), spotLight.getQuadratic());
	glUniform1f(glGetUniformLocation(program, "spotLight.cutOff"), glm::cos(glm::radians(12.5f)));
	glUniform1f(glGetUniformLocation(program, "spotLight.outerCutOff"), glm::cos(glm::radians(15.0f)));

	// Pass the matrices to the shader
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(glGetUniformLocation(program, "model"), 1, GL_FALSE, glm::value_ptr(orientation->getModelMatrix()));

	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}
/*This is the old way of choosing to update point lights would be deprecated if made a shadowCastingObject*/
GLvoid FlatPlane::updatePointLightList(std::vector<PointLight> &allPointLights)
{
	pointLightList = calculateClosestLightsToObject(allPointLights, 4);
}
/*This is the old way of choosing to update point lights, would be deprecated if made a shadowCastingObject*/
std::vector<PointLight> FlatPlane::calculateClosestLightsToObject(std::vector<PointLight> &objectList, GLint  numberOfObjectsToReturn)
{
	GLint  distanceFromObject = -1;
	GLint  indexOfFurthestNearObject = -1;
	if (objectList.size() < numberOfObjectsToReturn)
	{
		std::cout << "You have not passed a long enough to need to sort";
		return objectList;
	}

	std::vector<PointLight> nearObjects;
	for (GLint  objectInPassedList = 0; objectInPassedList < objectList.size(); objectInPassedList++)
	{
		if (nearObjects.size() < numberOfObjectsToReturn)
		{
			nearObjects.push_back(objectList.at(objectInPassedList));
		}
		else
		{
			indexOfFurthestNearObject = -1;

			distanceFromObject = std::fabsf(glm::distance(getPosition(), objectList.at(objectInPassedList).getPosition()));
			for (GLint  nearObject = 0; nearObject < nearObjects.size(); nearObject++)
			{
				//If we find an object further away in our nearest list
				if (distanceFromObject < distanceFromOtherGameObject(nearObjects.at(nearObject).getGameObject()))
				{
					//Record it's index if index is -1
					if (indexOfFurthestNearObject == -1)
					{
						indexOfFurthestNearObject = nearObject;
					}
					//Record it's index if this is the new max distance in our nearest list
					else if (distanceFromOtherGameObject(nearObjects.at(nearObject).getGameObject())
				> distanceFromOtherGameObject(nearObjects.at(indexOfFurthestNearObject).getGameObject()))
					{
						indexOfFurthestNearObject = nearObject;
					}
				}
			}
			if (indexOfFurthestNearObject != -1)
			{
				nearObjects.at(indexOfFurthestNearObject) = objectList.at(objectInPassedList);
			}
		}

	}
	return nearObjects;
}

