#pragma once
#pragma once
#include <glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ShadowCastingObject.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include "Spotlight.h"
#include "MultiLightCube.h"
#include "FrameBuffer.h"
#include "Shadow.h"
#include "ShadowCastingObject.h"

class FlatPlane :public GameObject
{
private:
	GLuint planeVBO;
	GLuint planeVAO;
	GLuint specularTexture;
	GLfloat   angle = 0;
	GLfloat expandSensitivity = 0.7;
	GLuint  timeExpanding = 5;
	Shadow shadow;
	glm::vec3 scaleValues = glm::vec3(1, 1, 1);
	GLfloat materialShininess;
	std::vector<PointLight> pointLightList;
	GLuint cubeVAO = 0;
	GLuint cubeVBO = 0;


public:
	GLuint woodTexture;
	FlatPlane::FlatPlane();
	GLvoid FlatPlane::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow);
	GLvoid FlatPlane::updatePointLightList(std::vector<PointLight> &allPointLights);
	std::vector<PointLight> FlatPlane::calculateClosestLightsToObject(std::vector<PointLight> &objectList, GLint  numberOfObjectsToReturn);
	
};
