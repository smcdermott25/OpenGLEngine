#include "FractalNoise.h"

FractalNoise::FractalNoise(GLint iterations, GLint seed, GLfloat roughness)
	: Noise()
{
	srand(seed);
	if (iterations < 1 || roughness < 0)
	{
		// handle error
	}

	this->iterations = iterations;
	this->seed = seed;

	// (2 to the power n) + 1
	divisions = 1 << iterations;
	size = (1 << iterations) + 1;

	rough = 0.9;
	this->roughness = roughness;

	min = 0;
	max = 0;

	// Create memory on heap, calloc -> fill memory with zeros after allocating it 
	map = (GLfloat **)calloc(size * size, sizeof(GLfloat));
	
	// Make fractal nosie map, used for texture / terrain etc
	makeNoise();
}

FractalNoise::~FractalNoise()
{
	for (GLint i = 0; i < size; i++)
	{
		free(map[i]);
	}

	free(map);
}

GLvoid FractalNoise::diamond(GLint x, GLint y, GLint side, GLfloat scale)
{
	if (side > 1)
	{
		GLint  half = side / 2;
		GLdouble  average = (map[x][y] + map[x + side][y] + map[x + side][y + side] + map[x][y + side]) * 0.25;
		GLfloat random = (GLfloat)rand() / (GLfloat)RAND_MAX;
		random *= 2;
		random -= 1;
		map[x + half][y + half] = average + random  * scale;
	}
}

GLvoid FractalNoise::square(GLint x, GLint y, GLint side, GLfloat scale)
{
	GLint  half = side / 2;
	GLdouble  average = 0.0, sum = 0.0;

	if (x >= 0)
	{
		average += map[x][y + half]; 
		sum += 1.0;
	}

	if (y >= 0)
	{
		average += map[x + half][y];
		sum += 1.0;
	}

	if (x + side <= divisions)
	{
		average += map[x + side][y + half];
		sum += 1.0;
	}

	if (y + side <= divisions)
	{
		average += map[x + half][y + side];
		sum += 1.0;
	}

	GLfloat random = (GLfloat)rand() / (GLfloat)RAND_MAX;
	random *= 2;
	random -= 1;
	map[x + half][y + half] = (average /  sum) + random * scale;

}

GLvoid FractalNoise::makeNoise()
{
	for (GLint i = 0; i < size; i++)
	{
		map[i] = (GLfloat *)calloc(size, sizeof(GLfloat));
	}

	GLint maxIndex = size-1;
	arrayLength = maxIndex;

	 //seed corners
	map[0][0] = 0.2;
	map[0][maxIndex] = 0.4;
	map[maxIndex][0] = 0.25;
	map[maxIndex][maxIndex] = 0.152;

	for (GLint i = 0; i < iterations; ++i)
	{
		GLint q = 1 << i;
		GLint r = 1 << (iterations - i);
		GLint s = r >> 1;

		for (GLint j = 0; j < divisions; j += r)
		{
			for (GLint k = 0; k < divisions; k += r)
			{
				diamond(j, k, r, rough);
			}

		}
			
		if (s > 0)
		{
			for (GLint j = 0; j <= divisions; j += s)
			{

				for (GLint k = (j + s) % r; k <= divisions; k += r)
				{
					square(j - s, k - s, r, rough);
				}
			}

		}

		rough *= roughness;

	}

	for (GLint  i = 0; i <= divisions; ++i)
	{
		for (GLint  j = 0; j <= divisions; ++j)
		{
			if (map[i][j] < min)
			{
				min = map[i][j];
			}
			else if (map[i][j] > max)
			{
				max = map[i][j];
			}
		}
	}
}

GLfloat FractalNoise::getNoise(GLint x, GLint y)
{
	GLfloat value = 0;
	GLfloat altitude = map[x][y];
	value = (altitude - min) / (max - min);
	return value;
}

//GLvoid FractalNoise::diamondStep(GLint minCoordinate, GLint size, GLfloat** map, GLint variation)
//{
//	for (GLint x = minCoordinate; x < (arrayLength); x += size)
//	{
//		for (GLint y = minCoordinate; y < (arrayLength); y += size)
//		{
//			GLint left = x - minCoordinate;
//			GLint right = x + minCoordinate;
//			GLint up = y + minCoordinate;
//			GLint down = y - minCoordinate;
//
//			// four corner values
//			GLint valueOne = map[left][down]; 
//			GLint valueTwo = map[right][down];
//			GLint valueThree = map[left][up];
//			GLint valueFour = map[right][up];
//
//			//std::cout << "Diamond Step: " << std::endl;
//			//std::cout << "left: " << left << std::endl;
//			//std::cout << "right: " << right << std::endl;
//			//std::cout << "up: " << up << std::endl;
//			//std::cout << "down: " << down << std::endl;
//			//std::cout << std::endl;
//			//std::cout << std::endl;
//			calculateAndInsertAverage(valueOne, valueTwo, valueThree, valueFour, variation, map, x, y);
//		}
//	}
//}
//
//GLvoid FractalNoise::squareStepEven(GLint minCoordinate, GLfloat** map, GLint size, GLint maxIndex, GLint variation)
//{
//	for (GLint x = minCoordinate; x < arrayLength; x += size)
//	{
//		for (GLint y = 0; y < arrayLength; y += size)
//		{
//			GLint left = x - minCoordinate;
//			GLint right = x + minCoordinate;
//
//			GLint valueOne = map[left][0];
//			GLint valueTwo = map[right][0];
//			GLint valueThree = map[minCoordinate][minCoordinate];
//
//			calculateAndInsertAverage(valueOne, valueTwo, valueThree, 0, variation, map, x, 0);
//
//			std::cout << "end square Step: " << std::endl;
//		}
//	}
//}
//
//GLvoid FractalNoise::squareStepOdd(GLfloat** map, GLint size, GLint minCoordinate, GLint maxIndex, GLint variation)
//{
//	for (GLint x = minCoordinate; x < arrayLength; x += size)
//	{
//		for (GLint y = minCoordinate; y < arrayLength; y += size)
//		{
//			if (x == maxIndex)
//			{
//				map[x][y] = map[0][y];
//				continue;
//			}
//
//			GLint left = 0;
//			GLint right = x + minCoordinate;
//			GLint down = y + minCoordinate;
//			GLint up = y - minCoordinate;
//
//			if (x == 0)
//			{
//				left = maxIndex - minCoordinate;
//			}
//			else
//			{
//				left = x - minCoordinate;
//			}
//
//
//			// four corner values
//			GLint valueOne = map[left][y];
//			GLint valueTwo = map[x][up];
//			GLint valueThree = map[right][y];
//			GLint valueFour = map[x][down];
//
//			calculateAndInsertAverage(valueOne, valueTwo, valueThree, valueFour, variation, map, x, y);
//		}
//	}
//}
//
//GLvoid FractalNoise::calculateAndInsertAverage(GLint valueOne, GLint valueTwo, GLint valueThree, GLint valueFour, GLint variation, GLfloat** map, GLint x, GLint y)
//{
//	srand(0);
//	GLint average = (valueOne + valueTwo + valueThree + valueFour) / 4;
//	GLint variance = (GLint)((rand() * ((variation << 1) + 1)) - variation);
//	map[x][y] = (average);
//}