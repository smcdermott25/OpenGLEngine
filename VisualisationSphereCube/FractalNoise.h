#pragma once
#include <glew.h>
#include <time.h>
#include <stdlib.h>

#include "Noise.h"
#include "helper.h"

class FractalNoise : public Noise
{
private:
	GLint iterations;
	GLint variation;
	GLint arrayLength;

	GLfloat rough;
	GLfloat roughness;

	GLint size;
	GLint divisions;
	GLfloat min, max;

	GLvoid makeNoise();
	GLvoid diamond(GLint x, GLint y, GLint side, GLfloat scale);
	GLvoid square(GLint x, GLint y, GLint side, GLfloat scale);

	GLfloat** map;
	GLuint texture;

public:
	FractalNoise(GLint resolution, GLint seed, GLfloat roughness);
	~FractalNoise();

	GLfloat getNoise(GLint x, GLint y);
};