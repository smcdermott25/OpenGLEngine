#pragma once

#include <glew.h> 
#include <iostream>
#include "ShaderLoader.h"

class FrameBuffer
{
protected:
	GLuint fbo;
	
	GLuint rbo;

	GLuint quadVAO, quadVBO;

	Shader screenShader;
	

public:
	FrameBuffer::FrameBuffer(GLint  WIDTH, GLint  HEIGHT, GLboolean  HDR);
	FrameBuffer::FrameBuffer();
	FrameBuffer(GLint  width, GLint  height, GLboolean  HDR, GLint multisamples);

	GLvoid useBuffer(GLint  width, GLint  height);
	GLvoid useBuffer();
	GLvoid clearBuffer();
	GLvoid releaseBuffer();
	GLvoid drawBuffer();
	GLvoid drawToBuffer();

	GLuint generateAttachmentTexture(GLboolean depth, GLboolean stencil, GLint  WIDTH, GLint  HEIGHT, GLboolean  HDR);
	GLuint textureColorbuffer;
	GLuint textureColorbuffer2;

	GLvoid setShader(Shader shader);
	GLvoid generateBuffers(GLint  WIDTH, GLint  HEIGHT, GLboolean  HDR);
	GLvoid generateMultisampleBuffers(GLint  width, GLint  height, GLboolean  HDR, GLint samples);
	GLuint generateMultisampleTexture(GLboolean depth, GLboolean stencil, GLint  WIDTH, GLint  HEIGHT, GLboolean  HDR,GLint samples);

	GLuint getFBO();
private:
	GLint lastFBOBounded;
	GLvoid initQuad();
};