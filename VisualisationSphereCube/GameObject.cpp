// GLM Mathemtics

#include <iostream>
#include <string>
#include "GameObject.h"



GameObject::GameObject()
{
	orientation = new Orientation();
	movement = new Movement(orientation);
}

GameObject::GameObject(const GLchar* vertexShader, const GLchar* fragmentShader)
{
	shaderProgram = new Shader(vertexShader, fragmentShader);
	orientation = new Orientation();
	movement = new Movement(orientation);
}

GameObject::GameObject(Shader* shader)
{
	shaderProgram = shader;
	orientation = new Orientation();
	movement = new Movement(orientation);
}

GameObject::GameObject(GLchar* shaderName, const GLchar* vertexShader, const GLchar* fragmentShader)
{
	shaderProgram = new Shader(shaderName, vertexShader, fragmentShader);
	orientation = new Orientation();
	movement = new Movement(orientation);
}

glm::vec3 GameObject::getPosition(GLvoid )
{
	return orientation->getPosition();
}

GLvoid  GameObject::setPosition(glm::vec3 newPosition)
{
	orientation->setPosition(newPosition);
}

GLvoid  GameObject::setScale(glm::vec3 newScale)
{
	orientation->setScale(newScale);
}

GLvoid  GameObject::moveTo(glm::vec3 newPosition, GLfloat deltaPosition)
{
	moveToPosition = newPosition;
	moving = movement->moveToPosition(newPosition, movement->getVelocity(), deltaPosition);
}

GLvoid  GameObject::moveAlongX(GLfloat newVelocity)
{
	if (movingX == true)
	{
		movingX = false;
	}
	else
	{
		movingX = true;
	}
	movement->setVelocity(glm::vec3(movement->getVelocity().x + newVelocity, movement->getVelocity().y, movement->getVelocity().z));
}

GLvoid  GameObject::moveAlongY(GLfloat newVelocity)
{
	if (movingY == true)
	{
		movingY = false;
	}
	else
	{
		movingY = true;
	}
	movement->setVelocity(glm::vec3(movement->getVelocity().x + newVelocity, movement->getVelocity().y, movement->getVelocity().z));
}

GLvoid  GameObject::moveAlongZ(GLfloat newVelocity)
{
	if (movingZ == true)
	{
		movingZ = false;
	}
	else
	{
		movingZ = true;
	}
	movement->setVelocity(glm::vec3(movement->getVelocity().x + newVelocity, movement->getVelocity().y, movement->getVelocity().z));
}

GLvoid  GameObject::dropDown()
{
	if (dropping == true)
	{
		dropping = false;
	}
	else
	{
		dropping = true;
	}
}

GLvoid  GameObject::launch(glm::vec3 launchVector)
{
	if (launching == true)
	{
		launching = false;
	}
	else
	{
		movement->setLaunchVelocity(launchVector);
		launching = true;
	}
}

GLfloat GameObject::distanceFromOtherGameObject(GameObject otherGameObject)
{
	return std::fabsf(glm::distance(getPosition(), otherGameObject.getPosition()));
}

//1. Destructor - uncommented for demo purposes, breaks scenes on some people's machine, this is a known memory leak
GameObject::~GameObject()
{
	//delete(shaderProgram);
	//delete(orientation);
}
//2.Copy constructor - movement section needs refactored, it can't be copied properely without crashing.
GameObject::GameObject(const GameObject &other)
{
	shaderProgram = new Shader("", "");
	*shaderProgram = *other.shaderProgram;
	orientation = new Orientation();
	*orientation = *other.orientation;
	//movement = new Movement();
	//*movement = *obj.movement;
}
//3.Move constructor
GameObject::GameObject(GameObject && other) noexcept :
	shaderProgram(other.shaderProgram),
	orientation(other.orientation)
{
	other.shaderProgram = nullptr;
	other.orientation == nullptr;
}

glm::vec3 GameObject::getVelocity()
{
	return movement->getVelocity();
}



	