#pragma once
#include <iostream>
#include "ShaderLoader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Orientation.h"
#include "ParticleEmitter.h"
#include "Movement.h"
#include "info.h"

typedef struct
{
	GLfloat modelHeight;
	GLfloat modelWidth;
	GLfloat modelDepth;
	GLfloat largestWidth;
	GLfloat largestHeight;
	glm::vec3 centrePoint;

}boundingBox2D;

class GameObject
{
public:

	Shader*   shaderProgram;
	Orientation* orientation;
	Movement* movement;

	GLfloat frames = 100.0f;
	GLboolean moving = false;
	GLboolean movingX = false;
	GLboolean movingY = false;
	GLboolean movingZ = false;
	GLboolean dropping = false;
	GLboolean launching = false;
	GLboolean bouncing = false;

	glm::vec3 moveToPosition;


public:
	GameObject(const GLchar* vertexShader, const GLchar* fragmentShader);
	GameObject(Shader* shader);
	GameObject(GLchar* shaderName, const GLchar* vertexShader, const GLchar* fragmentShader);
	//1.Destructor
	GameObject::~GameObject();
	//2.Copy constructor
	GameObject::GameObject(const GameObject &obj);
	//3.Move constructor
	GameObject::GameObject(GameObject && obj)  noexcept;
	//4.Copy assignment operator
	GameObject& operator= (const GameObject& other)
	{
		GameObject tmp(other);         // re-use copy-constructor
		*this = std::move(tmp);	// re-use move-assignment
		return *this;
	}
	//5.Move assignment operator
	GameObject& operator= (GameObject&& other) noexcept
	{
		//If this errors, ensure you're initialising your game object properely
		delete(shaderProgram);
		delete(orientation);
		shaderProgram = other.shaderProgram;
		orientation = other.orientation;
		other.shaderProgram = nullptr;
		other.orientation = nullptr;
		return *this;
	}

	GameObject::GameObject();
	glm::vec3 getPosition(GLvoid );
	GLvoid setPosition(glm::vec3 newPosition);
	GLvoid setScale(glm::vec3 setScale);

	GLvoid moveTo(glm::vec3 newPosition, GLfloat deltaPosition);
	GLvoid moveAlongX(GLfloat newVelocity);
	GLvoid moveAlongY(GLfloat newVvelocity);
	GLvoid moveAlongZ(GLfloat newVelocity);
	GLvoid dropDown();
	GLvoid launch(glm::vec3 launchVelocity);
	GLfloat distanceFromOtherGameObject(GameObject otherGameObject);
	virtual GLint draw()
	{
		return 0;
	}
	glm::vec3 GameObject::getVelocity();
	glm::vec3 GameObject::getScale()
	{
		return orientation->getScale();
	}

	GLfloat getRadius()
	{
		return 500.0f * getScale().y;
		printf("\ngetScale().y", getScale().y);
	}
	
	GLfloat getWeight()
	{
		return 100.0f * getRadius();
	}

};