#pragma once
#include <stdlib.h>
#include <glew.h> 
#include <glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "info.h"
#include "Shader.h"

#include "Camera.h"
#include "SkyBox.h"

/*
 * This class provides a way for creating different game world instances
*/
class GameWorld
{
public:
	virtual ~GameWorld()
	{
	};

	virtual GLvoid update(GLfloat deltaTime) = 0;
	virtual GLvoid draw() = 0;

	virtual GLvoid input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode) = 0;
	virtual GLvoid mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods) = 0;
	virtual GLvoid mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition) = 0;
	virtual GLvoid scrollInput(GLFWwindow* window, GLdouble  xScroll, GLdouble  yScroll) = 0;
	//virtual GLboolean mapChecker() = 0;
	
};

