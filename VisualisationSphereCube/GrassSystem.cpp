#include "GrassSystem.h"


GrassSystem::GrassSystem(glm::vec3 cornerPosition, GLfloat width, GLfloat depth)
{
	setGrassPositions(cornerPosition, width, depth);
}

GrassSystem::~GrassSystem()
{
	delete(grassBillboard);
	delete(grass);
}

GLvoid GrassSystem::setGrassPositions(glm::vec3 cornerPosition, GLfloat width, GLfloat depth)
{
	addComponents(cornerPosition, width, depth);

	grassBillboard = new Billboard(0, "Billboards/grassClump.png");
	grass = new InstancedModel("models/grass/grass1.obj", false, glm::vec3(0.5, 0.5, 0.5));
	grass->resetBuffers(2000);
	grassBillboard->resetMatrixBuffer(4500);

}

GLvoid GrassSystem::addComponents(glm::vec3 cornerPosition, GLfloat width, GLfloat depth)
{
	position = cornerPosition;
	grassModelOrientations.clear();
	grassBillboardOrientations.clear();
	grassModelMatrices.clear();
	grassBillboardMatrices.clear();
	//grass

	//Currently positions are random
	GLint rangeMinx = cornerPosition.x;
	GLint rangeMaxx = cornerPosition.x + width;

	GLint rangeMinz = cornerPosition.z;
	GLint rangeMaxz = cornerPosition.z + depth;

	//Set up orienatino data for eash bit of grass (model)
	for (GLint a = 0; a < 20; a++)
	{
		GLint randx = rand() % (rangeMaxx - rangeMinx + 1) + rangeMinx;
		GLint randz = rand() % (rangeMaxz - rangeMinz + 1) + rangeMinz;
		//The grass cump method generates multiple orientations aropund one point to create a cluster effect
		grassClump(glm::vec3(randx, 0.0f, randz), 10, 100, 200, &grassModelOrientations, glm::vec3(0.5f, 0.5f, 0.5f));
	}

	//Set up orienatino data for eash bit of grass (Billboard)
	for (GLint a = 0; a < 100; a++)
	{
		GLint randx = rand() % (rangeMaxx - rangeMinx + 1) + rangeMinx;
		GLint randz = rand() % (rangeMaxz - rangeMinz + 1) + rangeMinz;
		//The grass cump method generates multiple orientations aropund one point to create a cluster effect
		grassClump(glm::vec3(randx, 0.0f, randz), 5, 100, 200, &grassBillboardOrientations, glm::vec3(0.5f, 0.5f, 0.5f));
	}
}
//Neccessary to allow other classes to alter and update model matrices
GLvoid GrassSystem::InitialUpdate()
{
	std::cout << "The size for grass " << grassBillboardOrientations.size() << std::endl;
	//Currently the height is checked to ensure grass cant appear on snow areas in terrain
	for (GLint i = 0; i < grassModelOrientations.size(); i++)
	{
		if ((grassModelOrientations[i].getPosition().y < 350) & (grassModelOrientations[i].getPosition().y > 100))
		{
			grassModelMatrices.push_back(grassModelOrientations[i].getModelMatrix());
		}
	}

	for (GLint i = 0; i < grassBillboardOrientations.size(); i++)
	{
		//if ((grassBillboardOrientations[i].getPosition().y < 350) & (grassBillboardOrientations[i].getPosition().y>100))
		grassBillboardMatrices.push_back(grassBillboardOrientations[i].getModelMatrix());
	}

	if (grassModelMatrices.size() > 0)
	{
		grass->addMatrixData(grassModelMatrices);
	}
	if (grassBillboardMatrices.size() > 0)
	{
		grassBillboard->addMatrixData(grassBillboardMatrices);
	}

	for (GLint i = 0; i < imposterableModels.size(); i++)
	{
		//imposterableModels[i]->updateImposterModelMatrix(glm::vec3(imposterableModels[i]->orientation->getPosition()), imposterableModels[i]->orientation->getScale());
	}
}

GLvoid GrassSystem::update(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{
	for (GLint i = 0; i < imposterableModels.size(); i++)
	{
		//imposterableModels[i]->testAndUpdateTexture(projection, view, cameraPosition);
	}
}

GLvoid GrassSystem::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{

	blenderTool.transparentSetUp();
	grassBillboard->draw(view, projection, cameraPosition);

	blenderTool.transclucentSetUp();
	grass->drawInstanced(projection, view, glfwGetTime());
	for (GLint i = 0; i < imposterableModels.size(); i++)
	{
		//imposterableModels[i]->drawWithImposter(projection, view, cameraPosition);
	}
	blenderTool.disableBlending();
}
//Takes in one position and creates a clump of orientations based around position
GLvoid GrassSystem::grassClump(glm::vec3 position, GLint amount, GLint min, GLint max, std::vector<Orientation>* vector, glm::vec3 scale)
{
	for (GLint i = 0; i < amount; i++)
	{
		GLint randx = rand() % (max - min + 1) + min;
		GLint randz = rand() % (max - min + 1) + min;

		Orientation orientation;
		orientation.setScale(scale);
		orientation.setPosition(glm::vec3(randx + position.x, 0.0f, randz + position.z));
		vector->push_back(orientation);
	}
}

std::vector<Orientation>* GrassSystem::getgrassModelOrientations()
{
	return &grassModelOrientations;
}

std::vector<Orientation>* GrassSystem::getgrassBillboardOrientations()
{
	return &grassBillboardOrientations;
}

std::vector<Model*>* GrassSystem::getImposterableObjects()
{
	return &imposterableModels;
}
GLvoid GrassSystem::drawInstancedModels(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{
	blenderTool.transclucentSetUp();
	grass->drawInstanced(projection, view, glfwGetTime());
	blenderTool.disableBlending();
}