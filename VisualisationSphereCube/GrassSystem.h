#pragma once


#include "Billboard.h"
#include "InstancedModel.h"
#include "AlphaBlenderTool.h"

class GrassSystem
{
public:

	GrassSystem(glm::vec3 cornerPostion,GLfloat width, GLfloat depth);
	~GrassSystem();

	glm::vec3 position;
	GLvoid setGrassPositions(glm::vec3 cornerPosition, GLfloat width, GLfloat depth);
	GLvoid InitialUpdate();//Neccessary to update and assign all model Matrices 
	GLvoid update(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition);
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition);
	GLvoid drawInstancedModels(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition);
	GLvoid addComponents(glm::vec3 cornerPosition, GLfloat width, GLfloat depth);

	//Getters allows other classes to modify object matrices. For example terrain changes object height
	std::vector<Orientation>* getgrassModelOrientations();
	std::vector<Orientation>* getgrassBillboardOrientations();
	std::vector<Model*>* getImposterableObjects();

private:
	AlphaBlenderTool blenderTool;//As many components require alpha blending, blending settings will be controlled in the grass system using the blender tool
	//Vectors for all objects the vegetation system is responsible for
	std::vector<Orientation> grassBillboardOrientations;
	std::vector<Orientation> grassModelOrientations;

	std::vector<glm::mat4> grassBillboardMatrices;
	std::vector<glm::mat4> grassModelMatrices;
	
	Billboard* grassBillboard;
	InstancedModel* grass;
	std::vector<Model*> imposterableModels;

	GLvoid grassClump(glm::vec3 position, GLint amount, GLint min, GLint max, std::vector<Orientation>* vector, glm::vec3 scale);
};


