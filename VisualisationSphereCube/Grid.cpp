#pragma once
#include "Grid.h"
//#include <SOIL.h>

Grid::Grid():GameObject("MetaballVertex.txt","MetaballFrag.txt")
{
	cubeGrid = grid{ 0,NULL,0,NULL };

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &Normals);
	GLfloat dummy[]{ 0,0,0 };
	helper.fillVertexArrayWithVertices(VAO, VBO, dummy,3,0);
	helper.fillVertexArrayWithNormals(VAO,Normals,dummy,3,1);			
}

Grid::~Grid()
{
}
bool Grid::createMemory(int maxGridSize, int minGridSize)
{
	cubeGrid.vertices = (gridVertex *)malloc(sizeof(gridVertex)*(maxGridSize + 1)*(maxGridSize + 1)*(maxGridSize + 1));
	if (!cubeGrid.vertices) { return false; }
	cubeGrid.cubes = (cube *)malloc(sizeof(cube)*maxGridSize*maxGridSize*maxGridSize);
	if (!cubeGrid.cubes) { return false; }
	return true;
}

bool Grid::createGrid(glm::vec3 position, int gridSize, float width)
{
	if (currentGridWidth != width || currentGridSize != gridSize)
	{
		gridChanged = true;
		currentGridWidth = width;
		currentGridSize = gridSize;

	}
	if (gridSize >= 100)
		gridSize = 100;

	if (gridSize <= 1)
		gridSize = 1;

	float doubleWidth = width * 2;
	int currentCube;
	int currentVertex = 0;

	cubeGrid.numVertices = (gridSize + 1)*(gridSize + 1)*(gridSize + 1);
	for (int i = 0; i < gridSize + 1; i++)
	{
		for (int j = 0; j < gridSize + 1 ; j++)
		{
			for (int k = 0; k < gridSize + 1; k++)
			{
				cubeGrid.vertices[currentVertex].position.x = (i*doubleWidth) / (gridSize)-width;
				cubeGrid.vertices[currentVertex].position.y = ((j)*doubleWidth) / (gridSize)-width+position.y;
				cubeGrid.vertices[currentVertex].position.z = (k*doubleWidth) / (gridSize)-width;
				currentVertex++;
			}
		}
	}
	cubeGrid.numCubes = (gridSize)*(gridSize)*(gridSize);
	currentCube = 0;
	for (int i = 0; i < gridSize; i++)
	{
		for (int j = 0; j < gridSize; j++)
		{
			for (int k = 0; k < gridSize; k++)
			{
				cubeGrid.cubes[currentCube].vertices[0] = &cubeGrid.vertices[(i*(gridSize + 1) + j)*(gridSize + 1) + k];
				cubeGrid.cubes[currentCube].vertices[1] = &cubeGrid.vertices[(i*(gridSize + 1) + j)*(gridSize + 1) + k + 1];
				cubeGrid.cubes[currentCube].vertices[2] = &cubeGrid.vertices[(i*(gridSize + 1) + (j + 1))*(gridSize + 1) + k + 1];
				cubeGrid.cubes[currentCube].vertices[3] = &cubeGrid.vertices[(i*(gridSize + 1) + (j + 1))*(gridSize + 1) + k];
				cubeGrid.cubes[currentCube].vertices[4] = &cubeGrid.vertices[((i + 1)*(gridSize + 1) + j)*(gridSize + 1) + k];
				cubeGrid.cubes[currentCube].vertices[5] = &cubeGrid.vertices[((i + 1)*(gridSize + 1) + j)*(gridSize + 1) + k + 1];
				cubeGrid.cubes[currentCube].vertices[6] = &cubeGrid.vertices[((i + 1)*(gridSize + 1) + (j + 1))*(gridSize + 1) + k + 1];
				cubeGrid.cubes[currentCube].vertices[7] = &cubeGrid.vertices[((i + 1)*(gridSize + 1) + (j + 1))*(gridSize + 1) + k];

				//Code to attempt to draw using elements
				//Try and assign neighbours
				cubeGrid.cubes[currentCube].neighbors[0] = currentCube - 1;
				cubeGrid.cubes[currentCube].neighbors[1] = currentCube + 1;
				cubeGrid.cubes[currentCube].neighbors[2] = currentCube + gridSize;
				cubeGrid.cubes[currentCube].neighbors[3] = currentCube - gridSize;
				cubeGrid.cubes[currentCube].neighbors[4] = currentCube + gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[5] = currentCube + gridSize - 1;
				cubeGrid.cubes[currentCube].neighbors[6] = currentCube - gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[7] = currentCube - gridSize - 1;

				cubeGrid.cubes[currentCube].neighbors[8] = currentCube + gridSize*gridSize;
				cubeGrid.cubes[currentCube].neighbors[10] = currentCube + (gridSize * gridSize) + 1;
				cubeGrid.cubes[currentCube].neighbors[12] = currentCube + (gridSize * gridSize) - 1;
				cubeGrid.cubes[currentCube].neighbors[13] = currentCube + (gridSize * gridSize) + gridSize;
				cubeGrid.cubes[currentCube].neighbors[14] = currentCube + (gridSize * gridSize) + gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[15] = currentCube + (gridSize * gridSize) + gridSize - 1;
				cubeGrid.cubes[currentCube].neighbors[16] = currentCube + (gridSize * gridSize) - gridSize;
				cubeGrid.cubes[currentCube].neighbors[17] = currentCube + (gridSize * gridSize) - gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[18] = currentCube + (gridSize * gridSize) - gridSize - 1;

				cubeGrid.cubes[currentCube].neighbors[19] = currentCube - gridSize*gridSize;
				cubeGrid.cubes[currentCube].neighbors[20] = currentCube - (gridSize * gridSize) + 1;
				cubeGrid.cubes[currentCube].neighbors[21] = currentCube - (gridSize * gridSize) - 1;
				cubeGrid.cubes[currentCube].neighbors[22] = currentCube - (gridSize * gridSize) + gridSize;
				cubeGrid.cubes[currentCube].neighbors[23] = currentCube - (gridSize * gridSize) + gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[24] = currentCube - (gridSize * gridSize) + gridSize - 1;
				cubeGrid.cubes[currentCube].neighbors[25] = currentCube - (gridSize * gridSize) - gridSize;
				cubeGrid.cubes[currentCube].neighbors[26] = currentCube - (gridSize * gridSize) - gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[27] = currentCube - (gridSize * gridSize) - gridSize - 1;

				currentCube++;
			}
		}
	}
	return true;
}



void Grid::update(Metaball metaballs[], int amount)
{

	if (ballLocations.size() > 0 && ballLocations.size() == amount)
	{
		changeOccurred = false;
		for (int i = 0; i < amount; i++)
		{
			if (ballLocations[i] != metaballs[i].getPosition() || ballSizes[i] != metaballs[i].getSquaredRadius() || gridChanged)
			{
				changeOccurred = true;
				gridChanged = false;
			}
		}
	}
	else
		changeOccurred = true;

	if (changeOccurred)
	{
		ballLocations.clear();
		ballSizes.clear();
		//clear the field
		for (int i = 0; i < cubeGrid.numVertices; i++)
		{
			cubeGrid.vertices[i].value = 0.0f;
			cubeGrid.vertices[i].normal.x = 0.0;
			cubeGrid.vertices[i].normal.y = 0.0;
			cubeGrid.vertices[i].normal.z = 0.0;
		}

		//evaluate the scalar field at each point
		glm::vec3 ballToPoint;
		float squaredRadius;
		glm::vec3 ballPosition;
		float normalScale, squaredDistance;


		for (int i = 0; i < amount; i++)
		{
			ballLocations.push_back(metaballs[i].getPosition());
			ballSizes.push_back(metaballs[i].getSquaredRadius());

			squaredRadius = metaballs[i].getSquaredRadius();
			ballPosition = metaballs[i].getPosition();
			for (int j = 0; j < cubeGrid.numVertices; j++)
			{

				ballToPoint.x = cubeGrid.vertices[j].position.x - ballPosition.x;
				ballToPoint.y = cubeGrid.vertices[j].position.y - ballPosition.y;
				ballToPoint.z = cubeGrid.vertices[j].position.z - ballPosition.z;

				//get squared distance from ball to point
				squaredDistance = ballToPoint.x*ballToPoint.x +
					ballToPoint.y*ballToPoint.y +
					ballToPoint.z*ballToPoint.z;

				if (squaredDistance == 0.0f)squaredDistance = 0.0001f;
				//value = r^2/d^2
				cubeGrid.vertices[j].value += squaredRadius / squaredDistance;
				//normal = (r^2 * v)/d^4
				normalScale = squaredRadius / (squaredDistance*squaredDistance);
				cubeGrid.vertices[j].normal.x += ballToPoint.x*normalScale;
				cubeGrid.vertices[j].normal.y += ballToPoint.y*normalScale;
				cubeGrid.vertices[j].normal.z += ballToPoint.z*normalScale;
			}
		}
	}
}
void Grid::drawSurface(float threshold, glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{
	if (changeOccurred)
	{
		int count = 0;
		int  currentEdge;
		int usedEdges;
		float delta;
		int count1 = 0;

		surfaceVertex edgeVertices[12];
		//loop through cubes
		//std::cout << "What is happening " << g->numCubes << std::endl;

		surfaceVertices.clear();
		normals.clear();
		textureCos.clear();

		for (int i = 0; i < cubeGrid.numCubes; i++)
		{
			//cubeGrid.cubes[cubeGrid.cubes[1].neighbors[1]].vertices[0]->value = 90;

			//calculate which vertices are inside the surface
			unsigned char cubeIndex = 0;
			if (cubeGrid.cubes[i].vertices[0]->value < threshold)
				cubeIndex |= 1;
			if (cubeGrid.cubes[i].vertices[1]->value < threshold)
				cubeIndex |= 2;
			if (cubeGrid.cubes[i].vertices[2]->value < threshold)
				cubeIndex |= 4;
			if (cubeGrid.cubes[i].vertices[3]->value < threshold)
				cubeIndex |= 8;
			if (cubeGrid.cubes[i].vertices[4]->value < threshold)
				cubeIndex |= 16;
			if (cubeGrid.cubes[i].vertices[5]->value < threshold)
				cubeIndex |= 32;
			if (cubeGrid.cubes[i].vertices[6]->value < threshold)
				cubeIndex |= 64;
			if (cubeGrid.cubes[i].vertices[7]->value < threshold)
				cubeIndex |= 128;
			//look this value up in the edge table to see which edges to interpolate along
			usedEdges = edgeTable[cubeIndex];
			//if the cube is entirely within/outside surface, no faces   
			if (usedEdges == 0 || usedEdges == 255)continue;
			//update these edges
			for (currentEdge = 0; currentEdge < 12; currentEdge++)
			{
				if (usedEdges & 1 << currentEdge)
				{
					gridVertex*v1 = cubeGrid.cubes[i].vertices[verticesAtEndsOfEdges[2 * currentEdge]];
					gridVertex*v2 = cubeGrid.cubes[i].vertices[verticesAtEndsOfEdges[2 * currentEdge + 1]];
					delta = (threshold - v1->value) / (v2->value - v1->value);
					edgeVertices[currentEdge].position.x = v1->position.x + delta*(v2->position.x - v1->position.x);
					edgeVertices[currentEdge].position.y = v1->position.y + delta*(v2->position.y - v1->position.y);
					edgeVertices[currentEdge].position.z = v1->position.z + delta*(v2->position.z - v1->position.z);
					edgeVertices[currentEdge].normal.x = v1->normal.x + delta*(v2->normal.x - v1->normal.x);
					edgeVertices[currentEdge].normal.y = v1->normal.y + delta*(v2->normal.y - v1->normal.y);
					edgeVertices[currentEdge].normal.z = v1->normal.z + delta*(v2->normal.z - v1->normal.z);

					edgeVertices[currentEdge].textureCo[0] = v1->textureCo[0] + delta*(v2->textureCo[0] - v1->textureCo[0]);
					edgeVertices[currentEdge].textureCo[1] = v1->textureCo[1] + delta*(v2->textureCo[1] - v1->textureCo[1]);
				}
			}
			count1++;

			//render the vertices of the triangle
			for (int k = 0; triTable[cubeIndex][k] != -1; k += 3)
			{
				for (int i = 0; i < 26; i++)
				{
					//if (cubeGrid.cubes[cubeGrid.cubes->neighbors[0]].vertices[0])
					//{

					//}
				}
				count++;
				surfaceVertices.push_back(edgeVertices[triTable[cubeIndex][k + 0]].position);
				surfaceVertices.push_back(edgeVertices[triTable[cubeIndex][k + 2]].position);
				surfaceVertices.push_back(edgeVertices[triTable[cubeIndex][k + 1]].position);

				normals.push_back(edgeVertices[triTable[cubeIndex][k + 0]].normal);
				//normals.push_back(edgeVertices[triTable[cubeIndex][k + 2]].normal);
				//normals.push_back(edgeVertices[triTable[cubeIndex][k + 1]].normal);

			}
		}

		shaderProgram->useProgram();
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, surfaceVertices.size() * 12, &surfaceVertices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, Normals);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * 12, &normals[0], GL_STATIC_DRAW);

	}
	shaderProgram->useProgram();
	glBindVertexArray(VAO);

	//Other Uniforms
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	//Draw
	glDrawArrays(GL_TRIANGLES, 0, surfaceVertices.size());
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

}

void freeMemory(grid *cubegrid)
{
	if (cubegrid->vertices)free(cubegrid->vertices);
	cubegrid->vertices = NULL;
	cubegrid->numVertices = 0;
	if (cubegrid->cubes)free(cubegrid->cubes);
	cubegrid->cubes = NULL;
	cubegrid->numCubes = 0;
}
