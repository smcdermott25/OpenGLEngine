#pragma once
#include <glew.h>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

//#include "SOIL.h"
#include "stb_image.h"

/*
 * This class is used to provide an easy access to methods that are used over and over
 * by many objects in the codebase. 
 * Some exmaples are setting up vertex array objects, buffers and the loading of textures. 
*/
class Helper
{

private:

public:
	Helper();
	~Helper();

	// Buffer methods
	GLvoid createVertexArray(GLuint& vertexArray);
	GLvoid createEmptyVBO(GLuint& vbo, GLuint maxFloats);
	GLvoid fillVertexArrayWithVertices(GLuint vertexArray, GLuint& vertexBuffer, GLfloat vertices[], GLint length, GLuint index);
	GLvoid fillVertexArrayWithIndices(GLuint vertexArray, GLuint& indexBuffer, GLuint indices[], GLuint length);
	GLvoid fillVertexArrayWithNormals(GLuint vertexArray, GLuint& normalBuffer, GLfloat vertices[], GLint length, GLuint index);
	GLvoid fillVertexArrayWithTextureCoords(GLuint vertexArray, GLuint& textureBuffer, GLfloat vertices[], GLint length, GLuint index);

	//Instancing Methods
	//GLvoid positionInstance(GLuint vertexArray, GLuint& instanceVBO, glm::vec3  positions[], GLint length, GLuint index,GLint divisor);
	//GLvoid addInstancedAttrib(GLuint vao, GLuint vbo, GLint attribute, GLint size, GLint dataLength, GLint offset);
	//GLvoid instancing(GLuint vertexArray, GLuint& instanceVBO, glm::mat4  matrices[], GLint length, GLuint index, GLint divisor);
	//GLvoid refreshBuffer(GLuint& instanceVBO, glm::mat4  modelMatrices[], GLint length);

	// Texture methods
	GLvoid texture2DSetRepeatAndLinear(GLuint texture);
	GLvoid textureSetWrap(GLuint texture);
	GLvoid loadTextureWithImage2D(GLuint texture, GLchar* imagePath, GLuint textureSampler);

	GLuint loadTexture(GLchar const* path);
	//Calculate closest pointlights
	//std::vector<PointLight> Helper::calculateClosestLightsToObject(GameObject gameObject, std::vector<PointLight> &objectList, GLint  numberOfObjectsToReturn);
};

