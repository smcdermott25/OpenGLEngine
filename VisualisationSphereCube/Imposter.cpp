#pragma once
#include "Imposter.h"

Imposter::Imposter(glm::vec3 position, GLchar* modelName, glm::vec3 scale, GLfloat width, GLfloat height, GLfloat modelHeight)
	:BillboardSetup(1, width, height)
	, GameObject("Shader/imposterVertex.txt", "Shader/textureNoLightFragment.txt")
{
	orientation->setPosition(position);//sets the billboards position
	orientation->setScale(scale);
	frameBuffer = FrameBuffer(WIDTH, HEIGHT, false);
	this->modelHeight = modelHeight;
}

Imposter::Imposter()
	:BillboardSetup(1, 1, 1)
	, GameObject(" ", " ")
{
}
Imposter::~Imposter()
{
}
GLvoid Imposter::updateImposterModelMatrix(glm::vec3 position, glm::vec3 scale)
{
	orientation->setPosition(glm::vec3(position.x, (position.y + ((modelHeight*scale.y) / 2)), position.z));//sets the billboards position
	orientation->setScale(scale);
}

GLvoid Imposter::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{
	glDisable(GL_BLEND);

	shaderProgram->useProgram();
	glBindTexture(GL_TEXTURE_2D, frameBuffer.textureColorbuffer);//get texture from frame buffer

	//Get CameraRight and Up vectores to calulate rotations in shader code
	glm::vec3 CameraRight_worldspace = { view[0][0], view[1][0], view[2][0] };
	glm::vec3 CameraUp_worldspace = { view[0][1], view[1][1], view[2][1] };

	//Pass Uniforms to shader
	glUniform3f(glGetUniformLocation(shaderProgram->getProgram(), "CameraRight_worldspace"), CameraRight_worldspace.x, CameraRight_worldspace.y, CameraRight_worldspace.z);
	glUniform3f(glGetUniformLocation(shaderProgram->getProgram(), "CameraUp_worldspace"), CameraUp_worldspace.x, CameraUp_worldspace.y, CameraUp_worldspace.z);
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "model"), 1, GL_FALSE, glm::value_ptr(orientation->getModelMatrix()));

	glBindVertexArray(vertexArray);
	//Draw
	glDrawArrays(GL_TRIANGLES, 0, 6);

	glBindVertexArray(0);
}

FrameBuffer* Imposter::getFrameBuffer()
{
	return &frameBuffer;
}