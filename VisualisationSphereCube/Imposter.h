#pragma once
#include "GameObject.h"
#include "BillboardSetup.h"
#include <glew.h>
#include "FrameBuffer.h"

class Imposter : public BillboardSetup, public GameObject	//BillboardSetup gives access to square structure
{
public:

	Imposter(glm::vec3 position, GLchar* modelName, glm::vec3 scale, GLfloat width, GLfloat height, GLfloat modelHeight);
	Imposter();
	~Imposter();

	GLvoid updateImposterModelMatrix(glm::vec3 postion, glm::vec3 scale);
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition);

	FrameBuffer* getFrameBuffer();

private:
	GLfloat modelHeight;
	FrameBuffer frameBuffer;//Frame buffer used to render to
};


