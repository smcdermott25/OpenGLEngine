#pragma once
#include "Imposter.h"

class Imposterable 
{
public:
	
	Imposterable();
	Imposterable(glm::vec3 postion, glm::vec3 scale);
	~Imposterable();

	GLvoid setUpBoundingBoxData();
	GLvoid updateImposterModelMatrix(glm::vec3 postion, glm::vec3 scale);

	GLvoid testAndUpdateTexture(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition);
	GLvoid drawWithImposter(glm::mat4 projection, glm::mat4 view, glm::vec3 cameraPosition);
	virtual GLvoid drawForImposter(glm::mat4 projection, glm::mat4 view)=0;

protected:
	boundingBox2D boundingBoxForImposter;

private:

	Imposter* imposter;
	glm::vec3 position;
	glm::vec3 scale;

	GLboolean changeHasOccured = false;
	glm::vec3 lastCameraPosition;
	GLboolean drawModel = false;
	GLboolean firstDraw;
};


