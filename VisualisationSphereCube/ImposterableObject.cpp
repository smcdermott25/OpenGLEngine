#include "Imposterable.h"

Imposterable::Imposterable()
{
}
Imposterable::~Imposterable()
{
}
Imposterable::Imposterable(glm::vec3 postion, glm::vec3 scale)
{
	this->position = postion;
	this->scale = scale;
}

GLvoid Imposterable::setUpBoundingBoxData()
{
	imposter = new Imposter(glm::vec3(position.x, (position.y + ((boundingBoxForImposter.modelHeight*scale.y) / 2)), position.z),
		"h", scale, boundingBoxForImposter.largestWidth, boundingBoxForImposter.largestHeight, boundingBoxForImposter.modelHeight);
}

GLvoid Imposterable::updateImposterModelMatrix(glm::vec3 postion, glm::vec3 scale)
{
	this->position = postion;
	this->scale = scale;
	imposter->updateImposterModelMatrix(position, scale);
}

GLvoid Imposterable::testAndUpdateTexture(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{

	glm::vec3 billboardPosition = imposter->orientation->getPosition();

	//Squared distance betwwen camera and billboard
	GLfloat  distanceSquared = (billboardPosition.x - cameraPosition.x)*(billboardPosition.x - cameraPosition.x) +
		(billboardPosition.y - cameraPosition.y)*(billboardPosition.y - cameraPosition.y) +
		(billboardPosition.z - cameraPosition.z)*(billboardPosition.z - cameraPosition.z);

	if (distanceSquared < 10000 * (scale.y*boundingBoxForImposter.largestHeight))
	{
		drawModel = true;//close enough to draw the model
	}
	else
	{
		drawModel = false;//far enough away to draw the imposeter
	}
	if (!drawModel)
	{
		//Get the cosAngle between the camera position when the imposter was last drawn and the current position
		GLfloat cosAngle = glm::dot(cameraPosition, lastCameraPosition) / (glm::length(cameraPosition)*glm::length(lastCameraPosition));

		//If there is a difference of 3.4 degrees or there has been a change from model to imposter
		if (cosAngle <= 0.998 || changeHasOccured || !firstDraw)
		{
			lastCameraPosition = cameraPosition;// reset last camera position as imposter will be updated
			changeHasOccured = false;

			//Set up view and projection matrix for the model to draw to the framebuffer
			glm::mat4 imposterView = glm::lookAt(
				cameraPosition,
				glm::vec3((boundingBoxForImposter.centrePoint.x) + position.x, (boundingBoxForImposter.centrePoint.y*scale.x) + position.y, position.z),
				glm::vec3(0.0, 1.0, 0.0));

			glm::mat4 imposterProjection = glm::ortho((boundingBoxForImposter.largestWidth*scale.x) / -2.0f, (boundingBoxForImposter.largestWidth*scale.x) / 2.0f, (boundingBoxForImposter.largestHeight*scale.x) / -2.0f, (boundingBoxForImposter.largestHeight*scale.x) / 2.0f, -10000.0f, 10000.0f);

			imposter->getFrameBuffer()->useBuffer();
			drawForImposter(imposterView, imposterProjection);//Renders to texture
			imposter->getFrameBuffer()->releaseBuffer();//Unbind frame buffer
		}
	}
}

GLvoid Imposterable::drawWithImposter(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{
	if (!firstDraw)//Has first update happened 
	{
		lastCameraPosition = cameraPosition;
		testAndUpdateTexture(view, projection, cameraPosition);
		firstDraw = true;

	}
	if (drawModel)
	{
		lastCameraPosition = cameraPosition;
		changeHasOccured = true;
		drawForImposter(view, projection);
	}
	else
	{
		if (changeHasOccured)
		{
			testAndUpdateTexture(view, projection, cameraPosition);
		}
		imposter->draw(view, projection, cameraPosition);
	}
}