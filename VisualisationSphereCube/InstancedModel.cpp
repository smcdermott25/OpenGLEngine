#include "InstancedModel.h"


InstancedModel::InstancedModel(GLchar* path, GLboolean animated, glm::vec3 scale, std::vector<glm::mat4> modelMatrices)
	:Model(path, animated, scale, glm::vec3(0, 0, 0), "Shader/modelWithInstancingVertex.txt", "Shader/modelFragment.txt")
	, InstancedObject()
{
	assignBuffer(ModelPool::getInstance()->returnBuffer(path));
	maxAmountOfInstances = modelMatrices.size();
	reassignMatriceVector(modelMatrices);
	setUpBufferData(animated);
}

InstancedModel::InstancedModel(GLchar* path, GLboolean animated, glm::vec3 scale)
	:Model(path, animated, scale, glm::vec3(0, 0, 0), "Shader/modelWithInstancingVertex.txt", "Shader/modelFragment.txt")
	, InstancedObject()
{
	assignBuffer(ModelPool::getInstance()->returnBuffer(path));
	maxAmountOfInstances = 0;
	setUpBufferData(animated);
}
GLvoid InstancedModel::setUpBufferData(GLboolean animated)
{
	if (animated)
	{
		isAnimated = 1;
	}
	resetBuffers(maxAmountOfInstances);
}
InstancedModel::~InstancedModel()
{

}

GLvoid InstancedModel::resetBuffers(GLint maxAmountOfInstances)
{
	this->maxAmountOfInstances = maxAmountOfInstances;
	std::vector<GLuint>vertexArrays;

	for (GLuint i = 0; i < meshes.size(); i++)
	{
		vertexArrays.push_back(meshes[i].getVertexArray());//VAO for every mesh in model needs instance buffer
	}
	setUpDataForModelMatrixInstancingWithMeshes(&vertexArrays[0], vertexArrays.size(), matrixAttributeIndex);

}
GLvoid InstancedModel::drawInstanced(glm::mat4 projection, glm::mat4 view, GLfloat deltaTime)
{
	shaderProgram->useProgram();
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniform1i(glGetUniformLocation(shaderProgram->getProgram(), "animated"), isAnimated);

	for (GLuint i = 0; i < meshes.size(); i++)
	{
		if (isAnimated == 1)
		{
			meshes[0].animate(*shaderProgram, deltaTime);//Models can still be animated when instanced
		}
		meshes[i].drawInstanced(*shaderProgram, deltaTime, amountOfInstances);

	}
}