#pragma once
#include "Model.h"
#include "InstancedObject.h"

class InstancedModel : public Model, public InstancedObject
{
public:
	InstancedModel(GLchar* path, GLboolean animated, glm::vec3 scale, std::vector<glm::mat4> modelMatrices);
	InstancedModel(GLchar* path, GLboolean animated, glm::vec3 scale);
	~InstancedModel();


	GLvoid setUpBufferData(GLboolean animated);
	GLvoid resetBuffers(GLint maxAmountOfInstances);//called to resize the instance matrix bufffer
	GLvoid drawInstanced(glm::mat4 projection, glm::mat4 view, GLfloat deltaTime);

private:
	
	const GLint matrixAttributeIndex = 5;

	GLint isAnimated = 0;
};
