#include "InstancedObject.h"
#include <iostream>

InstancedObject::InstancedObject()
{
}

InstancedObject::~InstancedObject()
{
	//delete(&instanceMatrixVBO);
	//delete(&instancePositionsVBO);
	//delete(&amountOfInstances);
	//delete(&localpositions);
	//delete(&localVector);
}

//********METHODS TO SET UP ATTRIBUTES
GLvoid InstancedObject::setUpInstanceBuffer(GLuint* vertexArray, GLuint* VBO, GLint instanceVertexAttributeIndex, GLint size, GLint maxAmountOfInstances)
{
	// Generate a buffer that will hold instance data and set its size
	glBindVertexArray(*vertexArray);
	glGenBuffers(1, VBO);
	glBindBuffer(GL_ARRAY_BUFFER, *VBO);
	glBufferData(GL_ARRAY_BUFFER, size * maxAmountOfInstances, NULL, GL_DYNAMIC_DRAW);

	//Setting up attributes (works for one or more)
	setUpMultipleAttributes(size, instanceVertexAttributeIndex);
}

/* Stuart */
GLvoid InstancedObject::addInstancedAttrib(GLuint vao, GLuint vbo, GLint attribute, GLint size, GLint dataLength, GLint offset)
{
	//Used for muliple attributes on the same buffer
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(attribute, size, GL_FLOAT, GL_FALSE, dataLength * 4, (GLvoid*)(offset * 4));
	glVertexAttribDivisor(attribute, 1);//tells open gl this is an instanced attribute
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

//This method is to be used objects with manually defined vertices. It does not take into account model matrices
GLvoid InstancedObject::setUpDataForPositionInstancing(GLuint vertexArray, GLint maxAmountOfInstances, GLint instanceVertexAttributeIndex)
{
	bufferOffset = 0;// bufferOffset is at 0 to point at start of buffer
	this->maxAmountOfInstances = maxAmountOfInstances;
	setUpInstanceBuffer(&vertexArray, &instanceMatrixVBO, instanceVertexAttributeIndex, vec3Size, maxAmountOfInstances);
}

GLvoid InstancedObject::setUpAndInitiateDataForPositionInstancing(GLuint vertexArray, glm::vec3 instances[], GLint length, GLint instanceVertexAttributeIndex)
{
	//Sets up buffer and populates it
	setUpDataForPositionInstancing(vertexArray, maxAmountOfInstances, instanceVertexAttributeIndex);
	addDataToBuffer(&instancePositionsVBO, instances, length, 0);
}

GLvoid InstancedObject::setUpDataForModelMatrixInstancing(GLuint vertexArray, GLint maxAmountOfInstances, GLint instanceVertexAttributeIndex)
{
	bufferOffset = 0;
	this->maxAmountOfInstances = maxAmountOfInstances;
	setUpInstanceBuffer(&vertexArray, &instanceMatrixVBO, instanceVertexAttributeIndex, mat4Size, maxAmountOfInstances);
}

GLvoid InstancedObject::setUpAndInitiateDataForModelMatrixInstancing(GLuint vertexArray, glm::mat4 instances[], GLint maxAmountOfInstances, GLint instanceVertexAttributeIndex)
{
	setUpDataForModelMatrixInstancing( vertexArray,  maxAmountOfInstances,  instanceVertexAttributeIndex);
	addDataToBuffer(&instanceMatrixVBO, instances, maxAmountOfInstances, 0);
}

GLvoid InstancedObject::setUpDataForModelMatrixInstancingWithMeshes(GLuint vertexArray[], GLuint vertexArraySize, GLint instanceVertexAttributeIndex)
{
	//Models are treated differently as each mesh vbo must use the instance buffer
	bufferOffset = 0;
	glBindBuffer(GL_ARRAY_BUFFER, instanceMatrixVBO);
	glBufferData(GL_ARRAY_BUFFER, mat4Size * maxAmountOfInstances, NULL, GL_DYNAMIC_DRAW);

	for (GLuint i = 0; i < vertexArraySize; i++)
	{
		glBindVertexArray(vertexArray[i]);
		setUpMultipleAttributes(mat4Size, instanceVertexAttributeIndex);
	}
}

//*********METHODS TO ADD DATA TO BUFFERS
GLvoid InstancedObject::addMatrixData(std::vector<glm::mat4>matrices)//add data in next position in buffer
{
	localVector = matrices;
	addDataToBuffer(&instanceMatrixVBO, &matrices[0], matrices.size(), bufferOffset);

	if (amountOfInstances == 0 || amountOfInstances != previousOffset)//if no instances being drawn or buffer has not been filled
		amountOfInstances += matrices.size();
	
	if (bufferOffset + matrices.size() <= maxAmountOfInstances)//Where in buffer will the next piece of data go
		bufferOffset += matrices.size();

	else // if there is not enough room in buffer to add data
	{
		previousOffset = bufferOffset;// previous offset is the last offset before offset is reset
		amountOfInstances = bufferOffset;// if new data is now added to front of buffer, all other data is still used to draw instances
		bufferOffset = 0;
	}
}
GLvoid InstancedObject::addMatrixData(std::vector<glm::mat4>matrices, GLint offset)// add data in particualer position in buffer
{
	addDataToBuffer(&instanceMatrixVBO, &matrices[0], matrices.size(), offset);
}

GLvoid InstancedObject::addVec3Data(std::vector<glm::vec3>positions)//add data in next position in buffer
{
	localpositions = positions;
	addDataToBuffer(&instanceMatrixVBO, &positions[0], positions.size(), bufferOffset);

	if (amountOfInstances == 0 || amountOfInstances != previousOffset)
		amountOfInstances += positions.size();
	
	if (bufferOffset + positions.size() <= maxAmountOfInstances)
		bufferOffset += positions.size();
	else
	{
		previousOffset = bufferOffset;
		amountOfInstances = bufferOffset;
		bufferOffset = 0;
	}
}
GLvoid InstancedObject::addVec3Data(std::vector<glm::vec3>positions, GLint offset)// add data in particualer position in buffer
{
	addDataToBuffer(&instanceMatrixVBO, &positions[0], positions.size(), offset);
}


GLvoid InstancedObject::resetPositionVector(std::vector<glm::vec3>positions)
{
	localpositions = positions;
}

GLvoid InstancedObject::reassignMatriceVector(std::vector<glm::mat4>matrices)
{
	localVector = matrices;
}

GLvoid InstancedObject::resetPositionAndMatriceVectors(std::vector<glm::vec3>positions, std::vector<glm::mat4>matrices)
{
	//Would be used if the programmer needs assess to instance data
	//Note that these local vectors will not hold all the data in the instance buffer
	//if multiple adds have occurred
	resetPositionVector(positions);
	reassignMatriceVector(matrices);

	if (localVector.size() > localpositions.size())
		amountOfInstances = localVector.size();
	else
		amountOfInstances = localpositions.size();
}

template< class T >
GLvoid InstancedObject::addDataToBuffer(GLuint* VBO, T data[], GLint dataSize, GLint offSet)
{
	glBindBuffer(GL_ARRAY_BUFFER, *VBO);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(data[0]) *offSet, sizeof(data[0]) * dataSize, &data[0]);
}

GLvoid InstancedObject::assignBuffer(GLuint* buffer)
{
	glGenBuffers(1, buffer);
	instanceMatrixVBO = *buffer;
}

GLvoid InstancedObject::setUpMultipleAttributes(GLint size, GLint instanceVertexAttributeIndex)
{
	//Attribute has a limited size equivalent to a vec4
	//Data types larger than this fill multiple attributes
	
	for (GLint  i = 0; i < (size / maxAttributeSize); i++)
	{
		glEnableVertexAttribArray(instanceVertexAttributeIndex + i);
		glVertexAttribPointer(instanceVertexAttributeIndex + i, size / maxAttributeSize, GL_FLOAT, GL_FALSE, size, (GLvoid*)(maxAttributeSize*i));
		glVertexAttribDivisor(instanceVertexAttributeIndex + i, 1); // Tell OpenGL this is an instanced vertex attribute.														
	}
	glBindVertexArray(0);
}