#pragma once

#include "AlphaBlenderTool.h"

class InstancedObject
{
public:
	InstancedObject();
	InstancedObject::~InstancedObject();

	//********METHODS TO SET UP ATTRIBUTES

	//Setting up a attribute in its lowest form. Used called by other methods or directly if all other methods will not suit the programmers need
	GLvoid setUpInstanceBuffer(GLuint* vao, GLuint* vbo, GLint attribute, GLint size, GLint dataLength);
	//Setting up an attribute with pre existing buffers
	GLvoid addInstancedAttrib(GLuint vao, GLuint vbo, GLint attribute, GLint size, GLint dataLength, GLint offset);

	//Methods to easily set up vec 3 attributes - common instance type
	GLvoid setUpDataForPositionInstancing(GLuint vertexArray, GLint maxAmountOfInstances, GLint instanceVertexAttributeIndex);
	GLvoid setUpAndInitiateDataForPositionInstancing(GLuint vertexArray, glm::vec3 instances[], GLint maxAmountOfInstances, GLint instanceVertexAttributeIndex);

	//Methods to easily set up vec mat4 attributes - common instance type
	GLvoid setUpDataForModelMatrixInstancing(GLuint vertexArray, GLint maxAmountOfInstances, GLint instanceVertexAttributeIndex);
	GLvoid setUpAndInitiateDataForModelMatrixInstancing(GLuint vertexArray, glm::mat4 instances[], GLint maxAmountOfInstances, GLint instanceVertexAttributeIndex);
	GLvoid setUpDataForModelMatrixInstancingWithMeshes(GLuint vertexArray[], GLuint vertexArraySize, GLint instanceVertexAttributeIndex);

	//*********METHODS TO ADD DATA TO BUFFERS
	GLvoid addMatrixData(std::vector<glm::mat4>matrices);
	GLvoid addMatrixData(std::vector<glm::mat4>matrices, GLint offset);
	GLvoid addVec3Data(std::vector<glm::vec3>positions);
	GLvoid addVec3Data(std::vector<glm::vec3>positions, GLint offset);

	//called by all add methods, uses template so that multiple types can be used
	template< class T >
	GLvoid addDataToBuffer(GLuint* VBO, T data[], GLint dataSize, GLint offSet);

	//used to update local vectors for changes 
	GLvoid resetPositionVector(std::vector<glm::vec3>positions);
	GLvoid reassignMatriceVector(std::vector<glm::mat4>matrices);
	GLvoid resetPositionAndMatriceVectors(std::vector<glm::vec3>positions, std::vector<glm::mat4>matrices);

	//Used if a pre-existing buffer is to be used
	GLvoid assignBuffer(GLuint* buffer);

protected:
	//By default instanced objects have a local model matrix and position VBO to be instanced.
	//Objects that are instanced with additional attributes can still use the methods to set up the 
	//attributes apropriately

	GLuint instanceMatrixVBO;
	GLuint instancePositionsVBO;
	GLint amountOfInstances;//So object knows how many instances to draw
	GLint maxAmountOfInstances;// used to tests if added data will over fill buffer

	std::vector<glm::vec3> localpositions;
	std::vector<glm::mat4> localVector;

private:
	const GLint maxAttributeSize = sizeof(glm::vec4);
	const GLint mat4Size = sizeof(glm::mat4);
	const GLint vec3Size = sizeof(glm::vec3);


	GLvoid setUpMultipleAttributes(GLint size, GLint instanceVertexAttributeIndex);
	GLint previousOffset = 0;
	GLint bufferOffset = 0;
};