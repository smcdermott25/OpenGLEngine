#include"InteriorShadowBox.h"
#include "glm/ext.hpp"
//"glm/gtx/string_cast.hpp"

InteriorShadowBox::InteriorShadowBox()
	:GameObject("multiCasterInsideCube", "Shader/multiCasterInsideCube.vs", "Shader/multiCasterInsideCube.frag")
{
	shaderProgram->useProgram();
	materialShininess = 32.0f;
	GLfloat vertices[] = {
		// Back face
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, // Bottom-left
		0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f, // top-right
		0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
		0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,  // top-right
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,  // bottom-left
		-0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,// top-left
		// Front face
		-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom-left
		0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,  // bottom-right
		0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,  // top-right
		0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, // top-right
		-0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,  // top-left
		-0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,  // bottom-left
		// Left face
		-0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
		-0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-left
		-0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-left
		-0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-left
		-0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,  // bottom-right
		-0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-right
		// Right face
		0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, // top-left
		0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, // bottom-right
		0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top-right         
		0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,  // bottom-right
		0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,  // top-left
		0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, // bottom-left     
		// Bottom face
		-0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
		0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f, // top-left
		0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,// bottom-left
		0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f, // bottom-left
		-0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, // bottom-right
		-0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, // top-right
		// Top face
		-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
		0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
		0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, // top-right     
		0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom-right
		-0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,// top-left
		-0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f // bottom-left        
	};

	// Setup plane VAO
	glGenVertexArrays(1, &cubeVAO);
	glGenBuffers(1, &cubeVAO);
	glBindVertexArray(cubeVAO);
	glBindBuffer(GL_ARRAY_BUFFER, cubeVAO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), &vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
	glBindVertexArray(0);

	// Load textures
	glGenTextures(1, &woodTexture);
	glGenTextures(1, &specularTexture);
	int width, height;
	unsigned char* image;
	// Diffuse map woodPanel.png
	image = SOIL_load_image("Texture/woodPanel.png", &width, &height, 0, SOIL_LOAD_RGBA);
	glBindTexture(GL_TEXTURE_2D, woodTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);

	// Specular map
	image = SOIL_load_image("Texture/woodPanel.png", &width, &height, 0, SOIL_LOAD_RGBA);
	glBindTexture(GL_TEXTURE_2D, specularTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 1);
	shadow.createShadowCubeTextureAndCubeFBO();
}


void InteriorShadowBox::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<MultiLightCube> & objectsThatCastShadow)
{
	shadow.renderDepthSceneToTexture(pointLightList.at(0), objectsThatCastShadow);
	shaderProgram->useProgram();
	GLuint program = shaderProgram->getProgram();
	glDisable(GL_CULL_FACE);
	glUniform1i(glGetUniformLocation(shadow.simpleDepthShader.getProgram(), "reverse_normals"), 1);
	glUniform1f(glGetUniformLocation(program, "shadowsOn"), 1.0);
	glUniformMatrix4fv(glGetUniformLocation(program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(shadow.getLightSpaceMatrix()));
	glUniform3fv(glGetUniformLocation(program, "lightPos"), 1, glm::value_ptr(shadow.getLightPositionForShadow()));
	// Bind diffuse map
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, woodTexture);
	glUniform1i(glGetUniformLocation(program, "material.diffuse"), 0);
	// Bind specular map
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, specularTexture);
	glUniform1i(glGetUniformLocation(program, "material.specular"), 1);
	// Bind Depth map
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, shadow.getDepthCubeMapTexture());
	glUniform1i(glGetUniformLocation(program, "shadowMap"), 2);
	glActiveTexture(GL_TEXTURE0);


	glUniform1f(glGetUniformLocation(program, "material.shininess"), materialShininess);
	glUniform3f(glGetUniformLocation(program, "viewPos"), cameraPos.x, cameraPos.y, cameraPos.z);

	glUniform3f(glGetUniformLocation(program, "dirLight.direction"), directionalLight.getDirectionOfLight().x, directionalLight.getDirectionOfLight().y, directionalLight.getDirectionOfLight().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.ambient"), directionalLight.getAmbient().x, directionalLight.getAmbient().y, directionalLight.getAmbient().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.diffuse"), directionalLight.getDiffuse().x, directionalLight.getDiffuse().y, directionalLight.getDiffuse().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.specular"), directionalLight.getSpecular().x, directionalLight.getSpecular().y, directionalLight.getSpecular().z);
	//Iterate over all point lights
	for (int i = 0; i < pointLightList.size(); i++)
	{

		std::string number = std::to_string(i);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].position").c_str()), pointLightList.at(i).getPosition().x, pointLightList.at(i).getPosition().y, pointLightList.at(i).getPosition().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].ambient").c_str()), pointLightList.at(i).getAmbient().x, pointLightList.at(i).getAmbient().y, pointLightList.at(i).getAmbient().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].diffuse").c_str()), pointLightList.at(i).getDiffuse().x, pointLightList.at(i).getDiffuse().y, pointLightList.at(i).getDiffuse().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].specular").c_str()), pointLightList.at(i).getSpecular().x, pointLightList.at(i).getSpecular().y, pointLightList.at(i).getSpecular().z);
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].constant").c_str()), pointLightList.at(i).getConstant());
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].linear").c_str()), pointLightList.at(i).getLinear());
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].quadratic").c_str()), pointLightList.at(i).getQuadratic());

	}
	// SpotLight
	glUniform3f(glGetUniformLocation(program, "spotLight.position"), spotLight.getPosition().x, spotLight.getPosition().y, spotLight.getPosition().z);
	//printf("Spotlight position is: X %f, Y %f Z %f", spotLight.getPosition().x, spotLight.getPosition().y, spotLight.getPosition().z);
	glUniform3f(glGetUniformLocation(program, "spotLight.direction"), spotLight.getFront().x, spotLight.getFront().y, spotLight.getFront().z);
	//printf("Front is: X %f, Y %f Z %f", spotLight.getFront().x, spotLight.getFront().y, spotLight.getFront().z);
	glUniform3f(glGetUniformLocation(program, "spotLight.ambient"), spotLight.getAmbient().r, spotLight.getAmbient().g, spotLight.getAmbient().b);
	glUniform3f(glGetUniformLocation(program, "spotLight.diffuse"), spotLight.getDiffuse().r, spotLight.getDiffuse().g, spotLight.getDiffuse().b);
	glUniform3f(glGetUniformLocation(program, "spotLight.specular"), spotLight.getSpecular().r, spotLight.getSpecular().g, spotLight.getSpecular().b);
	glUniform1f(glGetUniformLocation(program, "spotLight.constant"), spotLight.getConstant());
	glUniform1f(glGetUniformLocation(program, "spotLight.linear"), spotLight.getLinear());
	glUniform1f(glGetUniformLocation(program, "spotLight.quadratic"), spotLight.getQuadratic());
	glUniform1f(glGetUniformLocation(program, "spotLight.cutOff"), glm::cos(glm::radians(12.5f)));
	glUniform1f(glGetUniformLocation(program, "spotLight.outerCutOff"), glm::cos(glm::radians(15.0f)));

	// Pass the matrices to the shader
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(glGetUniformLocation(program, "model"), 1, GL_FALSE, glm::value_ptr(orientation->getModelMatrix()));

	glBindVertexArray(cubeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	glEnable(GL_CULL_FACE);
}

void InteriorShadowBox::updatePointLightList(std::vector<PointLight> &allPointLights)
{
	pointLightList = calculateClosestLightsToObject(allPointLights, 4);
}
std::vector<PointLight> InteriorShadowBox::calculateClosestLightsToObject(std::vector<PointLight> &objectList, int numberOfObjectsToReturn)
{
	int distanceFromObject = -1;
	int indexOfFurthestNearObject = -1;
	if (objectList.size() < numberOfObjectsToReturn)
	{
		std::cout << "You have not passed a long enough to need to sort";
		return objectList;
	}

	std::vector<PointLight> nearObjects;
	for (int objectInPassedList = 0; objectInPassedList < objectList.size(); objectInPassedList++)
	{
		if (nearObjects.size() < numberOfObjectsToReturn)
		{
			nearObjects.push_back(objectList.at(objectInPassedList));
		}
		else
		{
			indexOfFurthestNearObject = -1;

			distanceFromObject = std::fabsf(glm::distance(getPosition(), objectList.at(objectInPassedList).getPosition()));
			for (int nearObject = 0; nearObject < nearObjects.size(); nearObject++)
			{
				//If we find an object further away in our nearest list
				if (distanceFromObject < distanceFromOtherGameObject(nearObjects.at(nearObject).getGameObject()))
				{
					//Record it's index if index is -1
					if (indexOfFurthestNearObject == -1)
					{
						indexOfFurthestNearObject = nearObject;
					}
					//Record it's index if this is the new max distance in our nearest list
					else if (distanceFromOtherGameObject(nearObjects.at(nearObject).getGameObject())
				> distanceFromOtherGameObject(nearObjects.at(indexOfFurthestNearObject).getGameObject()))
					{
						indexOfFurthestNearObject = nearObject;
					}
				}
			}
			if (indexOfFurthestNearObject != -1)
			{
				nearObjects.at(indexOfFurthestNearObject) = objectList.at(objectInPassedList);
			}
		}

	}
	return nearObjects;
}

