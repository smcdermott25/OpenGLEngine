#pragma once
#include <glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "GameObject.h"
#include "GameWorld.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include "Spotlight.h"
#include "MultiLightCube.h"
#include "FrameBuffer.h"
#include "PointLightShadow.h"

class InteriorShadowBox :public GameObject
{
private:
	GLuint specularTexture;
	GLfloat   angle = 0;
	//FrameBuffer fb = FrameBuffer(WIDTH, HEIGHT);

	GLfloat expandSensitivity = 0.7;
	GLuint  timeExpanding = 5;
	PointLightShadow shadow;


	glm::vec3 scaleValues = glm::vec3(1, 1, 1);
	GLfloat materialShininess;
	std::vector<PointLight> pointLightList;

	void InteriorShadowBox::RenderCube();
	GLuint cubeVAO = 0;
	GLuint cubeVBO = 0;


public:
	GLuint woodTexture;
	InteriorShadowBox::InteriorShadowBox();
	void InteriorShadowBox::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<MultiLightCube> & objectsThatCastShadow);
	void InteriorShadowBox::updatePointLightList(std::vector<PointLight> &allPointLights);
	std::vector<PointLight> InteriorShadowBox::calculateClosestLightsToObject(std::vector<PointLight> &objectList, int numberOfObjectsToReturn);

};
#pragma once
