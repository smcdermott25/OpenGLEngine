#include "Interpolation.h"

GLfloat Interpolation::linearInterpolate(GLfloat valueOne, GLfloat valueTwo, GLfloat weight)
{
	return (GLfloat)(weight * valueTwo + ((1 - weight) * valueOne));
}

/*
 * Covert value to angle,
 * Change range of return to 0 - 1,
 * now carry out linear interpolation
*/
GLfloat Interpolation::cosInterpolate(GLfloat blend)
{
	GLdouble theta = blend * M_PI;
	GLfloat f = (1.0f - (GLfloat)cos(theta)) * 0.5f;

	return f;
}

GLfloat Interpolation::cosInterpolateTwo(GLfloat valueOne, GLfloat valueTwo, GLfloat blend)
{
	GLdouble theta = blend * M_PI;
	GLfloat f = (1.0f - (GLfloat)cos(theta)) * 0.5f;

	return (valueOne * (1.0f - f)) + valueTwo * f;
}

GLfloat Interpolation::sinInterpolate(GLfloat blend)
{
	GLdouble theta = blend * M_PI;
	GLfloat f = (1.0f - (GLfloat)sin(theta)) * 0.5f;

	return f;
}
