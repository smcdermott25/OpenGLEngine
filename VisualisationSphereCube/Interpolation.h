#pragma once
#include <glew.h> 

#define _USE_MATH_DEFINES
#include <math.h>

class Interpolation
{
private:

public:
	// Interpolation methods used by most types of noise / within games
	GLfloat linearInterpolate(GLfloat valueOne, GLfloat valueTwo, GLfloat weight);
	GLfloat cosInterpolate(GLfloat weight);
	GLfloat cosInterpolateTwo(GLfloat valueOne, GLfloat valueTwo, GLfloat blend);
	GLfloat sinInterpolate(GLfloat weight);
};