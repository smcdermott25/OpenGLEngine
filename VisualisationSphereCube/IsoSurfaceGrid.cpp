#pragma once
#include "IsoSurfaceGrid.h"
#include "stb_image.h"

IsoSurfaceGrid::IsoSurfaceGrid(GLchar* texturePath) :ShadowCastingObject("Metaball", "Shader/metaballVertex.txt", "Shader/metaballFragment.txt"),Imposterable(glm::vec3(0,0,0), glm::vec3(1,1,1))
{
	setUp(texturePath);
}

IsoSurfaceGrid::IsoSurfaceGrid(GLchar* texturePath, glm::vec3 lightColour):ShadowCastingObject("Metabal","Shader/lightVertex.txt", "Shader/lightFragment.txt")
{
	this->lightColour = lightColour;
	setUp(texturePath);
}
GLvoid IsoSurfaceGrid::setUp(GLchar* texturePath)
{
	cubeGrid = grid{ 0,NULL,0,NULL };

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &Normals);
	glGenBuffers(1, &textureBuffer);
	
	GLfloat dummy[]{ 0,0,0 };
	helper.fillVertexArrayWithVertices(VAO, VBO, dummy, 3, vertexAttributeIndex);
	helper.fillVertexArrayWithNormals(VAO, Normals, dummy, 3, normalAttributeIndex);
	helper.fillVertexArrayWithTextureCoords(VAO, textureBuffer,dummy,3,textureAttributeIndex);
	//helper.loadTextureWithImage2D(texture, texturePath, 0);

	//Learn opengl used to get texture image as helper class caused a bug
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	// Set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// Set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// Load image, create texture and generate mipmaps
	int width, height, components;
	unsigned char* image = stbi_load(texturePath, &width, &height, &components, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image);
	glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture 

									 

}

IsoSurfaceGrid::~IsoSurfaceGrid()
{
}
GLboolean IsoSurfaceGrid::createMemory(GLint maxGridSize, GLint minGridSize)
{
	cubeGrid.vertices = (gridVertex *)malloc(sizeof(gridVertex)*(maxGridSize + 1)*(maxGridSize + 1)*(maxGridSize + 1));
	if (!cubeGrid.vertices) { return false; }
	cubeGrid.cubes = (cube *)malloc(sizeof(cube)*maxGridSize*maxGridSize*maxGridSize);
	if (!cubeGrid.cubes) { return false; }
	return true;
}

GLboolean IsoSurfaceGrid::createGrid(glm::vec3 position, GLint gridSize, GLfloat width, GLfloat height)
{

	//boundingBoxForImposter
	boundingBoxForImposter.modelHeight = height;
	boundingBoxForImposter.modelWidth = width;
	boundingBoxForImposter.modelDepth = width;

	//Length of cross section corner to corner
	boundingBoxForImposter.largestWidth = glm::sqrt(glm::pow(boundingBoxForImposter.modelDepth, 2) + glm::pow(boundingBoxForImposter.modelWidth, 2));
	boundingBoxForImposter.largestHeight = glm::sqrt(glm::pow(boundingBoxForImposter.modelHeight, 2) + glm::pow(boundingBoxForImposter.largestWidth, 2));

	//boundingBoxForImposter.centrePoint = (glm::vec3(0, 0, 0) + glm::vec3(boundingBoxForImposter.largestWidth, height, boundingBoxForImposter.largestWidth));
	boundingBoxForImposter.centrePoint = glm::vec3(0, height/2,0);

	
	this->gridHeight = height;
	this->gridWidth = width;
	orientation->setPosition(position);
	gridPosition = position;
	if (currentGridWidth != width || currentGridSize != gridSize)
	{
		gridChanged = true;
		currentGridWidth = width;
		currentGridSize = gridSize;

	}
	if (gridSize >= 100)
		gridSize = 100;

	if (gridSize <= 1)
		gridSize = 1;

	GLfloat doubleWidth = width * 2;
	GLint currentCube;
	GLint currentVertex = 0;

	cubeGrid.numVertices = (gridSize + 1)*(gridSize + 1)*(gridSize + 1);
	for (GLint i = 0; i < gridSize + 1; i++)
	{
		for (GLint j = 0; j < gridSize + 1; j++)
		{
			for (GLint k = 0; k < gridSize + 1; k++)
			{
				cubeGrid.vertices[currentVertex].position.x = (i*doubleWidth) / (gridSize)-width;
				cubeGrid.vertices[currentVertex].position.y = ((j)*height) / (gridSize);
				cubeGrid.vertices[currentVertex].position.z = (k*doubleWidth) / (gridSize)-width;
				currentVertex++;
			}
		}
	}
	cubeGrid.numCubes = (gridSize)*(gridSize)*(gridSize);
	currentCube = 0;
	for (GLint i = 0; i < gridSize; i++)
	{
		for (GLint j = 0; j < gridSize; j++)
		{
			for (GLint k = 0; k < gridSize; k++)
			{
				cubeGrid.cubes[currentCube].vertices[0] = &cubeGrid.vertices[(i*(gridSize + 1) + j)*(gridSize + 1) + k];
				cubeGrid.cubes[currentCube].vertices[1] = &cubeGrid.vertices[(i*(gridSize + 1) + j)*(gridSize + 1) + k + 1];
				cubeGrid.cubes[currentCube].vertices[2] = &cubeGrid.vertices[(i*(gridSize + 1) + (j + 1))*(gridSize + 1) + k + 1];
				cubeGrid.cubes[currentCube].vertices[3] = &cubeGrid.vertices[(i*(gridSize + 1) + (j + 1))*(gridSize + 1) + k];
				cubeGrid.cubes[currentCube].vertices[4] = &cubeGrid.vertices[((i + 1)*(gridSize + 1) + j)*(gridSize + 1) + k];
				cubeGrid.cubes[currentCube].vertices[5] = &cubeGrid.vertices[((i + 1)*(gridSize + 1) + j)*(gridSize + 1) + k + 1];
				cubeGrid.cubes[currentCube].vertices[6] = &cubeGrid.vertices[((i + 1)*(gridSize + 1) + (j + 1))*(gridSize + 1) + k + 1];
				cubeGrid.cubes[currentCube].vertices[7] = &cubeGrid.vertices[((i + 1)*(gridSize + 1) + (j + 1))*(gridSize + 1) + k];

				//Code to attempt to draw using elements
				//Try and assign neighbours
				cubeGrid.cubes[currentCube].neighbors[0] = currentCube - 1;
				cubeGrid.cubes[currentCube].neighbors[1] = currentCube + 1;
				cubeGrid.cubes[currentCube].neighbors[2] = currentCube + gridSize;
				cubeGrid.cubes[currentCube].neighbors[3] = currentCube - gridSize;
				cubeGrid.cubes[currentCube].neighbors[4] = currentCube + gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[5] = currentCube + gridSize - 1;
				cubeGrid.cubes[currentCube].neighbors[6] = currentCube - gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[7] = currentCube - gridSize - 1;

				cubeGrid.cubes[currentCube].neighbors[8] = currentCube + gridSize*gridSize;
				cubeGrid.cubes[currentCube].neighbors[10] = currentCube + (gridSize * gridSize) + 1;
				cubeGrid.cubes[currentCube].neighbors[12] = currentCube + (gridSize * gridSize) - 1;
				cubeGrid.cubes[currentCube].neighbors[13] = currentCube + (gridSize * gridSize) + gridSize;
				cubeGrid.cubes[currentCube].neighbors[14] = currentCube + (gridSize * gridSize) + gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[15] = currentCube + (gridSize * gridSize) + gridSize - 1;
				cubeGrid.cubes[currentCube].neighbors[16] = currentCube + (gridSize * gridSize) - gridSize;
				cubeGrid.cubes[currentCube].neighbors[17] = currentCube + (gridSize * gridSize) - gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[18] = currentCube + (gridSize * gridSize) - gridSize - 1;

				cubeGrid.cubes[currentCube].neighbors[19] = currentCube - gridSize*gridSize;
				cubeGrid.cubes[currentCube].neighbors[20] = currentCube - (gridSize * gridSize) + 1;
				cubeGrid.cubes[currentCube].neighbors[21] = currentCube - (gridSize * gridSize) - 1;
				cubeGrid.cubes[currentCube].neighbors[22] = currentCube - (gridSize * gridSize) + gridSize;
				cubeGrid.cubes[currentCube].neighbors[23] = currentCube - (gridSize * gridSize) + gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[24] = currentCube - (gridSize * gridSize) + gridSize - 1;
				cubeGrid.cubes[currentCube].neighbors[25] = currentCube - (gridSize * gridSize) - gridSize;
				cubeGrid.cubes[currentCube].neighbors[26] = currentCube - (gridSize * gridSize) - gridSize + 1;
				cubeGrid.cubes[currentCube].neighbors[27] = currentCube - (gridSize * gridSize) - gridSize - 1;

				currentCube++;
			}
		}
	}
	setUpBoundingBoxData();
	updateImposterModelMatrix(orientation->getPosition(), orientation->getScale());
	return true;
}

GLvoid IsoSurfaceGrid::update(Metaball metaballs[], GLint amount)
{

	if (ballLocations.size() > 0 && ballLocations.size() == amount)
	{
		changeOccurred = false;
		for (GLint  i = 0; i < amount; i++)
		{
			if (ballLocations[i] != metaballs[i].getPosition() || ballSizes[i] != metaballs[i].getSquaredRadius() || gridChanged)
			{
				changeOccurred = true;
				gridChanged = false;
			}
		}
	}
	else
	{
		changeOccurred = true;
	}

	if (changeOccurred)
	{

		ballLocations.clear();
		ballSizes.clear();
		//clear the field
		for (GLint i = 0; i < cubeGrid.numVertices; i++)
		{
			cubeGrid.vertices[i].value = 0.0f;
			cubeGrid.vertices[i].normal.x = 0.0;
			cubeGrid.vertices[i].normal.y = 0.0;
			cubeGrid.vertices[i].normal.z = 0.0;
		}

		//evaluate the scalar field at each point
		glm::vec3 ballToPoint;
		GLfloat squaredRadius;
		glm::vec3 ballPosition;
		GLfloat normalScale, squaredDistance;


		for (GLint i = 0; i < amount; i++)
		{
			ballLocations.push_back(metaballs[i].getPosition());
			ballSizes.push_back(metaballs[i].getSquaredRadius());

			squaredRadius = metaballs[i].getSquaredRadius();
			ballPosition = metaballs[i].getPosition();
			for (GLint j = 0; j < cubeGrid.numVertices; j++)
			{

				ballToPoint.x = cubeGrid.vertices[j].position.x - (ballPosition.x);
				ballToPoint.y = cubeGrid.vertices[j].position.y - (ballPosition.y);
				ballToPoint.z = cubeGrid.vertices[j].position.z - (ballPosition.z);

				//get squared distance from ball to point
				squaredDistance = ballToPoint.x*ballToPoint.x +
					ballToPoint.y*ballToPoint.y +
					ballToPoint.z*ballToPoint.z;

				if (squaredDistance == 0.0f)squaredDistance = 0.0001f;
				//value = r^2/d^2
				cubeGrid.vertices[j].value += squaredRadius / squaredDistance;
				//normal = (r^2 * v)/d^4
				normalScale = squaredRadius / (squaredDistance*squaredDistance);
				cubeGrid.vertices[j].normal.x += ballToPoint.x*normalScale;
				cubeGrid.vertices[j].normal.y += ballToPoint.y*normalScale;
				cubeGrid.vertices[j].normal.z += ballToPoint.z*normalScale;
			}
		}
	}

	generateVertices(1.0f);

	cubeGrid.vertices = NULL;
	cubeGrid.cubes = NULL;
	cubeGrid.numCubes = 0;
}

GLvoid IsoSurfaceGrid::drawSimpleVAO()
{
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, surfaceVertices.size());
	glBindVertexArray(0);
}

GLvoid IsoSurfaceGrid::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow)
{
	
}
GLvoid IsoSurfaceGrid::drawSurface(GLfloat threshold, glm::mat4 view, glm::mat4 projection)
{

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glUniform1i(glGetUniformLocation(shaderProgram->getProgram(), "textureImage"), 0);

	//generateVertices(1.0f);
	shaderProgram->useProgram();
	glBindVertexArray(VAO);

	//Other Uniforms
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram->getProgram(), "model"), 1, GL_FALSE, glm::value_ptr(orientation->getModelMatrix()));

	//Bellow used if mesh acts as light
	glUniform3f(glGetUniformLocation(shaderProgram->getProgram(), "lightColour"), lightColour.x,lightColour.y,lightColour.z);
	glUniform1i(glGetUniformLocation(shaderProgram->getProgram(), "textureOn"), 1);

	//Draw
	glDrawArrays(GL_TRIANGLES, 0, surfaceVertices.size());
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

GLvoid  IsoSurfaceGrid::freeMemory(grid cubegrid)
{
	if (cubegrid.vertices)free(cubegrid.vertices);
	cubegrid.vertices = NULL;
	cubegrid.numVertices = 0;
	if (cubegrid.cubes)free(cubegrid.cubes);
	cubegrid.cubes = NULL;
	cubegrid.numCubes = 0;
}

GLvoid  IsoSurfaceGrid::generateVertices(GLfloat threshold)
{

	GLint  count = 0;
	GLint   currentEdge;
	GLint  usedEdges;
	GLfloat  delta;
	GLint  count1 = 0;

	surfaceVertex edgeVertices[12];
	//loop through cubes
	surfaceVertices.clear();
	normals.clear();
	textureCo.clear();

	for (GLint i = 0; i < cubeGrid.numCubes; i++)
	{
	
		//calculate which vertices are inside the surface
		GLuint cubeIndex = 0;
		if (cubeGrid.cubes[i].vertices[0]->value < threshold)
			cubeIndex |= 1;
		if (cubeGrid.cubes[i].vertices[1]->value < threshold)
			cubeIndex |= 2;
		if (cubeGrid.cubes[i].vertices[2]->value < threshold)
			cubeIndex |= 4;
		if (cubeGrid.cubes[i].vertices[3]->value < threshold)
			cubeIndex |= 8;
		if (cubeGrid.cubes[i].vertices[4]->value < threshold)
			cubeIndex |= 16;
		if (cubeGrid.cubes[i].vertices[5]->value < threshold)
			cubeIndex |= 32;
		if (cubeGrid.cubes[i].vertices[6]->value < threshold)
			cubeIndex |= 64;
		if (cubeGrid.cubes[i].vertices[7]->value < threshold)
			cubeIndex |= 128;
		//look this value up in the edge table to see which edges to interpolate along
		usedEdges = edgeTable[cubeIndex];
		//if the cube is entirely within/outside surface, no faces   
		if (usedEdges == 0 || usedEdges == 255)continue;
		//update these edges
		for (currentEdge = 0; currentEdge < 12; currentEdge++)
		{
			if (usedEdges & 1 << currentEdge)
			{
				gridVertex*v1 = cubeGrid.cubes[i].vertices[verticesAtEndsOfEdges[2 * currentEdge]];
				gridVertex*v2 = cubeGrid.cubes[i].vertices[verticesAtEndsOfEdges[2 * currentEdge + 1]];
				delta = (threshold - v1->value) / (v2->value - v1->value);
				edgeVertices[currentEdge].position.x = v1->position.x + delta*(v2->position.x - v1->position.x);
				edgeVertices[currentEdge].position.y = v1->position.y + delta*(v2->position.y - v1->position.y);
				edgeVertices[currentEdge].position.z = v1->position.z + delta*(v2->position.z - v1->position.z);
				edgeVertices[currentEdge].normal.x = v1->normal.x + delta*(v2->normal.x - v1->normal.x);
				edgeVertices[currentEdge].normal.y = v1->normal.y + delta*(v2->normal.y - v1->normal.y);
				edgeVertices[currentEdge].normal.z = v1->normal.z + delta*(v2->normal.z - v1->normal.z);

				edgeVertices[currentEdge].textureCo[0] = v1->textureCo[0] + delta*(v2->textureCo[0] - v1->textureCo[0]);
				edgeVertices[currentEdge].textureCo[1] = v1->textureCo[1] + delta*(v2->textureCo[1] - v1->textureCo[1]);
			}
		}
		count1++;

		//render the vertices of the triangle
		for (GLint k = 0; triTable[cubeIndex][k] != -1; k += 3)
		{
			count++;
			surfaceVertices.push_back(edgeVertices[triTable[cubeIndex][k + 0]].position);
			surfaceVertices.push_back(edgeVertices[triTable[cubeIndex][k + 2]].position);
			surfaceVertices.push_back(edgeVertices[triTable[cubeIndex][k + 1]].position);

			glm::vec3 normal1[3];
	
			
			normal1[0]= edgeVertices[triTable[cubeIndex][k + 0]].normal;
			normal1[1]= edgeVertices[triTable[cubeIndex][k + 2]].normal;
			normal1[2]= edgeVertices[triTable[cubeIndex][k + 1]].normal;

			normals.push_back(normal1[0]);
			normals.push_back(normal1[1]);
			normals.push_back(normal1[2]);

			//Spherical texture mapping 
			for (GLint t = 0; t < 3; t++)
			{
				glm::vec3 normalForTexture = glm::normalize(normal1[t]);
				GLfloat xTex = normalForTexture.x / 2 + 0.5;
				GLfloat yTex = normalForTexture.y / 2 + 0.5;

				textureCo.push_back(xTex);
				textureCo.push_back(yTex);
			}
			
		}
	}
	shaderProgram->useProgram();
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, surfaceVertices.size() * 12, &surfaceVertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, Normals);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * 12, &normals[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, textureBuffer);
	glBufferData(GL_ARRAY_BUFFER, textureCo.size() * 4, &textureCo[0], GL_STATIC_DRAW);

}

GLvoid IsoSurfaceGrid::drawForImposter(glm::mat4 view, glm::mat4 projection)
{
	this->drawSurface(1.0f, view, projection);
}