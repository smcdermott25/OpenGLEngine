#pragma once
#include "GameObject.h"
#include "IsoSurfaceGridStructures.h"
#include "MarchingCubesLookUpTables.h"
#include "Metaball.h"
#include <glm/gtc/type_ptr.hpp>
#include "Imposterable.h"
#include "ShadowCastingObject.h"
class IsoSurfaceGrid: public ShadowCastingObject, public Imposterable
{	
public:
	glm::vec3 lightColour;
	GLfloat gridWidth;
	GLfloat gridHeight;

	IsoSurfaceGrid(GLchar* texturePath);
	IsoSurfaceGrid(GLchar* texturePath, glm::vec3 lightColour);

	~IsoSurfaceGrid();

	GLvoid setUp(GLchar* texture);
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight>& pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>>& objectsThatCastShadow);
	GLvoid drawSurface(GLfloat threshold, glm::mat4 view, glm::mat4 projection);
	GLvoid drawSimpleVAO();
	GLvoid drawForImposter(glm::mat4 view, glm::mat4 projection);
	GLvoid update(Metaball metaball[], GLint amount);
	GLboolean createMemory(GLint maxGridSize, GLint minGridSize);
	GLboolean createGrid(glm::vec3 position, GLint gridSize, GLfloat width, GLfloat height);

private:
	GLuint VBO, VAO, Normals, textureBuffer, texture;

	grid cubeGrid;

	std::vector<glm::vec3> ballLocations;
	std::vector<glm::vec3> surfaceVertices;
	std::vector<glm::vec3> normals;

	std::vector<GLfloat> ballSizes;
	std::vector<GLfloat> textureCo;

	glm::vec3 gridPosition;

	GLint currentGridWidth = 0;
	GLint currentGridSize = 0;

	GLboolean gridChanged = false;
	GLboolean useLights = false;
	GLboolean changeOccurred;



	GLvoid generateVertices(GLfloat threshold);
	GLvoid freeMemory(grid cubegrid);

	const GLint vertexAttributeIndex = 0;
	const GLint normalAttributeIndex = 1;
	const GLint textureAttributeIndex = 2;

};


