#pragma once
#include <glm/glm.hpp>
#include <glfw3.h>
//"Practical Algorithms for 3d Computer Graphics" was used to as a guide for iso surface generation
typedef struct
{
	glm::vec3 position;
	GLfloat value; //the value of the scalar field at this point
	glm::vec3 normal;
	glm::vec2 textureCo;
} gridVertex;

//vertex from linear interpolation along edge
typedef struct
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 textureCo;
} surfaceVertex;


typedef struct
{
	gridVertex * vertices[8]; //pointers to vertices
	GLint neighbors[26];
} cube;


typedef struct
{
	GLint numVertices;
	gridVertex * vertices;
	GLint numCubes;
	cube * cubes;
} grid;