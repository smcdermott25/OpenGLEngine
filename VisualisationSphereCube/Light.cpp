#include "Light.h"

/*Old deprecated light class - instead use pointlight.cpp when creating future scenes*/
Light::Light(glm::vec3 position, glm::vec3 colour)
{
	lightPosition = position;
	lightColour = colour;

	model = glm::mat4(1);
	model = glm::translate(model, position);

	shader = &ShaderLoader::lightShader;
	shader->useProgram();
	glUniform3f(glGetUniformLocation(shader->getProgram(), "lightColour"), lightColour.x, lightColour.y, lightColour.z);


	GLfloat radius = 1.0;
	GLfloat vertices[] =
	{
		// back face
		-radius, -radius, -radius,
		-radius,  radius, -radius,
		radius,  radius, -radius,

		radius,  radius, -radius,
		radius, -radius, -radius,
		-radius, -radius, -radius,

		// front face
		-radius, -radius,  radius,
		-radius,  radius,  radius,
		radius, -radius,  radius,

		-radius,  radius,  radius,
		radius,  radius,  radius,
		radius, -radius,  radius,

		// left face
		-radius, -radius,  radius,
		-radius,  radius,  radius,
		-radius,  radius, -radius,

		-radius,  radius, -radius,
		-radius, -radius, -radius,
		-radius, -radius,  radius,

		// right face
		radius, -radius,  radius,
		radius,  radius,  radius,
		radius,  radius, -radius,

		radius,  radius, -radius,
		radius, -radius, -radius,
		radius, -radius,  radius,

		// bottom face
		-radius, -radius,  radius,
		-radius, -radius, -radius,
		radius, -radius, -radius,

		radius, -radius, -radius,
		radius, -radius,  radius,
		-radius, -radius,  radius,

		// top face
		-radius,  radius,  radius,
		-radius,  radius, -radius,
		radius,  radius, -radius,

		radius,  radius, -radius,
		radius,  radius,  radius,
		-radius,  radius,  radius,

	};

	// Generate Vertex Array object and vertex buffer
	helper.createVertexArray(vertexArray);
	helper.fillVertexArrayWithVertices(vertexArray, vertexBuffer, vertices, sizeof(vertices), 0);

	glGenBuffers(1, &textureBuffer);
}

Light::~Light()
{
	delete(shader);
}

GLvoid Light::draw(glm::mat4 view, glm::mat4 projection)
{

	shader->useProgram();
	glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "model"), 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	glBindVertexArray(vertexArray);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);

}

glm::vec3 Light::getPosition()
{
	return lightPosition;
}

glm::vec3 Light::getColour()
{
	return lightColour;
}

GLvoid Light::addPosition(glm::vec3 newPosition)
{
	lightPosition = newPosition;
	model = glm::translate(model, newPosition);
}