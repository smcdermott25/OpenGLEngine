#pragma once
#include <glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ShaderLoader.h"
#include "info.h"
/*Deprecated light - please use PointLight or define your own point light type*/
class Light
{

private:
	GLuint vertexArray;
	GLuint vertexBuffer;
	GLuint textureBuffer;

	Shader *shader;

	GLuint numOfVertices;
	GLsizei sizeOfVertices;

	GLuint texture;

	glm::vec3 lightPosition;
	glm::vec3 lightColour;
	glm::mat4 model;

public:
	GLfloat ambientStrength = 0.4f;
	Light(glm::vec3 position, glm::vec3 colour);
	~Light();
	glm::vec3 getPosition();
    glm::vec3 getColour();
	GLvoid addPosition(glm::vec3 newPosition);
	GLvoid draw(glm::mat4 view, glm::mat4 projection);
};