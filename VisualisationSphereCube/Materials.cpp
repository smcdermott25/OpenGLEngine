#include "Materials.h"

//images courtesy of http://freepbr.com

Materials::Material Materials::aluminium;
Materials::Material Materials::rusted_iron;

Materials::Materials()
{
	Helper helper = Helper();

	aluminium.albedo = helper.loadTexture("Texture/PBRMaterials/Aluminium/albedo.png");
	aluminium.normal = helper.loadTexture("Texture/PBRMaterials/Aluminium/normal.png");
	aluminium.metallic = helper.loadTexture("Texture/PBRMaterials/Aluminium/metallic.png");
	aluminium.roughness = helper.loadTexture("Texture/PBRMaterials/Aluminium/roughness.png");
	aluminium.ao = aluminium.albedo;// no ao texture, therefore just reuse a white texture

	rusted_iron.albedo = helper.loadTexture("Texture/PBRMaterials/RustedIron/albedo.png");
	rusted_iron.normal = helper.loadTexture("Texture/PBRMaterials/RustedIron/normal.png");
	rusted_iron.metallic = helper.loadTexture("Texture/PBRMaterials/RustedIron/metallic.png");
	rusted_iron.roughness = helper.loadTexture("Texture/PBRMaterials/RustedIron/roughness.png");
	rusted_iron.ao = aluminium.ao;// no ao texture, therefore just reuse a white texture

}
