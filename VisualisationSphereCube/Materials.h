#pragma once
#include "Helper.h"

class Materials
{
public:
	struct Material
	{

		GLuint albedo;
		GLuint normal;
		GLuint metallic;
		GLuint roughness;
		GLuint ao;
		GLuint depth;
	};

	static Material aluminium;
	static Material rusted_iron;

	Materials::Materials();


};