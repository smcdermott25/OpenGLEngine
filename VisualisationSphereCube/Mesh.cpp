#include "Mesh.h"

// Robert Finns code
Mesh::Mesh
(
	std::vector<Vertex>  vertices,
	std::vector<GLuint>  indices,
	std::vector<Texture> textures,
	std::vector<BonesInfo> bonesInfo,
	const aiScene* scene
) : vertices(vertices),
	indices(indices),
	textures(textures),
	bonesInfo(bonesInfo),
	scene(scene)
{
	this->setUpBuffers();
};

Mesh::~Mesh()
{
}

GLuint Mesh::getVertexArray()
{
	return vertexArray;
}

GLvoid Mesh::setBones(Bones& bones)
{
	this->bones = bones;
	animation = new Animation(scene);
	// Load animation 0
	if (scene->HasAnimations())
	{
		animation->loadAnimation(scene->mRootNode, bones);
	}

}

GLvoid Mesh::setUpBuffers()
{
	glGenVertexArrays(1, &this->vertexArray);
	glGenBuffers(1, &this->vertexBuffer);
	glGenBuffers(1, &this->boneBuffer);
	glBindVertexArray(this->vertexArray);
	glGenBuffers(1, &this->elementBuffer);

	glBindVertexArray(this->vertexArray);
	glBindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->elementBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoords));

	glBindBuffer(GL_ARRAY_BUFFER, this->boneBuffer);
	glBufferData(GL_ARRAY_BUFFER, this->bonesInfo.size() * sizeof(BonesInfo), &this->bonesInfo[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(3);
	glVertexAttribIPointer(3, 4, GL_INT, sizeof(BonesInfo), (GLvoid*)offsetof(BonesInfo, boneIds));

	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(BonesInfo), (GLvoid*)offsetof(BonesInfo, weights[0]));

	glBindVertexArray(0);
}

aiMatrix4x4 Mesh::boneTransform(GLfloat timeInSeconds, std::vector<aiMatrix4x4>& transforms, Bones& bones)
{
	aiMatrix4x4 identity;
	aiAnimation* sceneAnimation = scene->mAnimations[0];

	GLfloat ticksPerSecond = sceneAnimation->mTicksPerSecond != 0 ?
		sceneAnimation->mTicksPerSecond : 25.0f;

	GLfloat timeInTicks = timeInSeconds * ticksPerSecond;
	GLfloat animationTime = fmod(timeInTicks, sceneAnimation->mDuration);

	readNodeHierarchy(animationTime, scene->mRootNode, identity);

	return identity;
}

GLvoid Mesh::testingNewAnimationMethod(std::vector<const aiNodeAnim*> animations, std::vector<std::string> names, GLfloat timeInSeconds, aiMatrix4x4 parentTransform, GLint  count)
{

}

GLvoid Mesh::readNodeHierarchy(GLfloat animationTime, const aiNode* node, const aiMatrix4x4& parentTransform)
{
	aiMatrix4x4 nodeTransformation = node->mTransformation;
	std::string nodeName = node->mName.data;

	if (animation->nameAnimationMap.find(nodeName) != animation->nameAnimationMap.end())
	{
		const aiNodeAnim* nodeAnimation = animation->nameAnimationMap[nodeName];

		// gen scaling transformation matrix
		aiVector3D scaling;
		calculateInterpolatedScaling(scaling, animationTime, nodeAnimation);
		aiMatrix4x4 scalingMatrix;
		scalingMatrix.Scaling(scaling, scalingMatrix);

		// rotation
		aiQuaternion rotation;
		calculateInterpolatedRotation(rotation, animationTime, nodeAnimation);
		aiMatrix4x4 rotationMatrix = aiMatrix4x4(rotation.GetMatrix());

		aiVector3D translation;
		calculateInterpolatedPosition(translation, animationTime, nodeAnimation);
		aiMatrix4x4 translationMatrix;
		translationMatrix.Translation(translation, translationMatrix);

		// combine transformations
		nodeTransformation = translationMatrix * rotationMatrix * scalingMatrix;
	}

	// Each bone is affected by it's parent's bones as well
	aiMatrix4x4 globalTransformation;

	if (bones.boneNameIdMap.find(nodeName) != bones.boneNameIdMap.end())
	{
		//Looking at a bone node
		globalTransformation = parentTransform * nodeTransformation;
		aiMatrix4x4 final = globalTransformation * globalInverseTransform * bones.offsetMatrices.at(bones.boneNameIdMap[nodeName]);
		bones.insertBoneMatrix(final, nodeName);
	}
	else
	{
		globalTransformation = parentTransform;
	}


	// Pass transformation to this node's children
	for (GLuint i = 0; i < node->mNumChildren; i++)
	{
		readNodeHierarchy(animationTime, node->mChildren[i], globalTransformation);
	}
}

GLvoid Mesh::calculateInterpolatedPosition(aiVector3D& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation)
{
	aiVectorKey* positionKeys = nodeAnimation->mPositionKeys;
	GLuint numPositionKeys = nodeAnimation->mNumPositionKeys;
	GLuint positionIndex;

	for (GLuint i = 0; i < numPositionKeys; i++)
	{
		GLuint value = i + 1;
		if (animationTime < (GLfloat )positionKeys[value].mTime)
		{
			positionIndex = i;
			break;
		}
	}

	GLuint nextPositionIndex = (positionIndex + 1);

	GLfloat timeTwo = positionKeys[nextPositionIndex].mTime;
	GLfloat timeOne = positionKeys[positionIndex].mTime;
	GLfloat deltaTime = timeTwo - timeOne;

	GLfloat factor = (animationTime - (GLfloat)positionKeys[positionIndex].mTime) / deltaTime;

	const aiVector3D& startPosition = positionKeys[positionIndex].mValue;
	const aiVector3D& endPosition = positionKeys[nextPositionIndex].mValue;

	aiVector3D delta = endPosition - startPosition;
	out = startPosition + factor * delta;
}

GLvoid Mesh::calculateInterpolatedScaling(aiVector3D& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation)
{
	aiVectorKey* scalingKeys = nodeAnimation->mScalingKeys;
	GLuint numScalingKeys = nodeAnimation->mNumScalingKeys;
	GLuint scalingIndex = 0;

	for (GLuint i = 0; i < numScalingKeys; i++)
	{
		GLuint value = i + 1;
		if (animationTime < (GLfloat )scalingKeys[value].mTime)
		{
			scalingIndex = i;
			break;
		}
	}

	GLuint nextScalingIndex = (scalingIndex + 1);

	GLfloat timeTwo = scalingKeys[nextScalingIndex].mTime;
	GLfloat timeOne = scalingKeys[scalingIndex].mTime;
	GLfloat deltaTime = timeTwo - timeOne;

	GLfloat factor = (animationTime - (GLfloat)scalingKeys[scalingIndex].mTime) / deltaTime;

	const aiVector3D& startScaling = scalingKeys[scalingIndex].mValue;
	const aiVector3D& endScaling = scalingKeys[nextScalingIndex].mValue;

	aiVector3D delta = endScaling - startScaling;
	out = startScaling + factor * delta;
}

GLvoid Mesh::calculateInterpolatedRotation(aiQuaternion& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation)
{
	aiQuatKey* rotationKeys = nodeAnimation->mRotationKeys;
	GLuint numRotationKeys = nodeAnimation->mNumRotationKeys;

	if (numRotationKeys < 2)
	{
		std::cout << "Less than 2 rotation keys: " << std::endl;
	}

	GLuint rotationIndex = findRotation(animationTime, nodeAnimation);
	GLuint nextRotationIndex = (rotationIndex + 1);


	GLfloat timeTwo = rotationKeys[nextRotationIndex].mTime;
	GLfloat timeOne = rotationKeys[rotationIndex].mTime;
	GLfloat deltaTime = timeTwo - timeOne;

	GLfloat factor = (animationTime - (GLfloat)rotationKeys[rotationIndex].mTime) / deltaTime;

	const aiQuaternion& startRotation = rotationKeys[rotationIndex].mValue;
	const aiQuaternion& endRotation = rotationKeys[nextRotationIndex].mValue;
	aiQuaternion::Interpolate(out, startRotation, endRotation, factor);
	out = out.Normalize();
}

GLuint Mesh::findRotation(GLfloat animationTime, const aiNodeAnim* nodeAnimation)
{
	GLuint numRotationKeys = nodeAnimation->mNumRotationKeys;

	assert(numRotationKeys > 0);

	for (GLuint i = 0; i < numRotationKeys; i++)
	{
		GLuint value = i + 1;
		if (animationTime < (GLfloat )nodeAnimation->mRotationKeys[value].mTime)
		{
			return i;
		}
	}

	// if no correct rotation is found, assert
	std::cout << "No rotation found: " << std::endl;
	assert(0);
}

GLvoid Mesh::animate(Shader shader, GLfloat deltaTime)
{
	shader.useProgram();

	std::vector<aiMatrix4x4> transforms;
	boneTransform(deltaTime, transforms, bones);

	bones.setUniforms(shader);
}

GLvoid Mesh::draw(Shader shader, GLfloat deltaTime)
{
	GLuint diffuseNumber = 1;
	GLuint specularNumber = 1;

	for (GLuint i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);

		std::stringstream s;
		std::string number;
		std::string name = textures[i].type;

		// if both strings are equal
		if ("texture_diffuse" == name)
		{
			s << diffuseNumber++;
		}
		else if ("texture_specular" == name)
		{
			s << specularNumber++;
		}

		number = s.str();
		glUniform1f(glGetUniformLocation(shader.getProgram(),
			(name + number).c_str()), i);
		glBindTexture(GL_TEXTURE_2D, textures[i].id);
	}

	glUniform1f(glGetUniformLocation(shader.getProgram(), "material.shininess"), 16.0f);

	// Drawing functions
	glBindVertexArray(vertexArray);
	glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	for (GLuint i = 0; i < this->textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, 0);
	}


}

// Ryan Gallagers code
GLvoid Mesh::drawInstanced(Shader shader, GLfloat deltaTime,GLint amount)
{
	GLuint diffuseNumber = 1;
	GLuint specularNumber = 1;

	for (GLuint i = 0; i < textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);

		std::stringstream s;
		std::string number;
		std::string name = textures[i].type;

		// if both strings are equal
		if ("texture_diffuse" == name)
		{
			s << diffuseNumber++;
		}
		else if ("texture_specular" == name)
		{
			s << specularNumber++;
		}

		number = s.str();
		glUniform1f(glGetUniformLocation(shader.getProgram(),
			(name + number).c_str()), i);
		glBindTexture(GL_TEXTURE_2D, textures[i].id);
	}

	glUniform1f(glGetUniformLocation(shader.getProgram(), "material.shininess"), 16.0f);

	// Drawing functions
	glBindVertexArray(vertexArray);
	//glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);

	glDrawElementsInstanced(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0, amount);
	glBindVertexArray(0);

	for (GLuint i = 0; i < this->textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}