#pragma once
#include <iostream>
#include <vector>
#include <map>

#include <glew.h>
#include <glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Bones.h"
#include "BonesInfo.h"
#include "Shader.h"
#include "Helper.h"
#include "Animation.h"

// Convert to bone space
static aiMatrix4x4 globalInverseTransform;

class Vertex
{
public:
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texCoords;
};

class Texture
{
public:
	GLuint id;
	std::string type;
	std::string name;
};

class Mesh
{
public:
	Mesh
	(
		std::vector<Vertex>  vertices,
		std::vector<GLuint>  indices,
		std::vector<Texture> textures,
		std::vector<BonesInfo> bonesInfo,
		const aiScene* scene
	);

	~Mesh();

	std::vector<Vertex>  vertices;
	std::vector<GLuint>  indices;
	std::vector<Texture> textures;
	std::vector<BonesInfo>  bonesInfo;

	aiMatrix4x4 globalTransformation5;
	Bones bones;
	Animation* animation;

	GLint size = 3;

	GLuint getVertexArray();
	GLvoid setBones(Bones& bones);
	GLvoid animate(Shader shader, GLfloat deltaTime);
	GLvoid draw(Shader shader, GLfloat deltaTime);
	GLvoid drawInstanced(Shader shader, GLfloat deltaTime, GLint amount);

private:

	const aiScene* scene;

	GLuint vertexArray;
	GLuint vertexBuffer;
	GLuint boneBuffer;
	GLuint elementBuffer;

	aiMatrix4x4 boneTransform(GLfloat timeInSeconds, std::vector<aiMatrix4x4>& transforms, Bones& bones);
	GLvoid readNodeHierarchy(GLfloat animationTime, const aiNode* node, const aiMatrix4x4& parentTransform);

	GLvoid calculateInterpolatedPosition(aiVector3D& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation);
	GLvoid calculateInterpolatedScaling(aiVector3D& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation);
	GLvoid calculateInterpolatedRotation(aiQuaternion& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation);

	GLuint findRotation(GLfloat animationTime, const aiNodeAnim* nodeAnimation);

	GLvoid Mesh::testingNewAnimationMethod(std::vector<const aiNodeAnim*> animations, std::vector<std::string> names,
		GLfloat timeInSeconds, aiMatrix4x4 identity, GLint  count);

	GLvoid setUpBuffers();
};
