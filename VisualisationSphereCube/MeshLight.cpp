#include "MeshLight.h"
/*Rocky light that gives out light - used in alien world demo*/
MeshLight::MeshLight(glm::vec3 color, IsoSurfaceGrid* grid)
	:PointLight(color)
{
	mGrid = grid;
	gameObject.orientation = grid->orientation;
	lightPosition = glm::vec3(gameObject.orientation->getPosition().x, gameObject.orientation->getPosition().y+(grid->gridHeight*grid->orientation->getScale().y), gameObject.orientation->getPosition().z);
}
/*Draw our meta ball lights*/
GLvoid MeshLight::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition, GLfloat  bloomValue)
{
	mGrid->drawSurface(1.0f,view, projection);
}
