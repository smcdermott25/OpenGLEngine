#pragma once
#include "PointLight.h"
#include "IsoSurfaceGrid.h"
/*Used on the alien planet to produce rock meshes that double as pointlights*/
class MeshLight : public PointLight
{
public:
	IsoSurfaceGrid* mGrid;
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition, GLfloat  bloomValue);
	MeshLight(glm::vec3 color, IsoSurfaceGrid* grid);
};