#include "Metaball.h"

Metaball::Metaball(glm::vec3 position, GLfloat squaredRadius)
{
	//Metaball acts as a force that creates an iso surface in the grid class
	this->position = position;
	this->initialPosition = position;
	this->squaredRadius = squaredRadius;
}

Metaball::~Metaball()
{
}

glm::vec3 Metaball::getInitialPosition()
{
	return initialPosition;
}

glm::vec3 Metaball::getPosition()
{
	return position;
}
GLfloat Metaball::getSquaredRadius()
{
	return squaredRadius;
}

GLfloat Metaball::getRadius()
{
	return glm::sqrt(squaredRadius);
}
GLvoid Metaball::setPosition(glm::vec3 position)
{
	this->position = position;
}

GLvoid Metaball::setXPosition(GLfloat x)
{
	position.x = x;
}
GLvoid Metaball::setYPosition(GLfloat y)
{
	position.y = y;
}
GLvoid Metaball::setZPosition(GLfloat z)
{
	position.z = z;
}

GLvoid Metaball::incrementX()
{
	position.x++;
}
GLvoid Metaball::incrementY()
{
	position.y++;
}
GLvoid Metaball::incrementZ()
{
	position.z++;
}
GLvoid Metaball::decrementX()
{
	position.x--;
}
GLvoid Metaball::decrementY()
{
	position.y--;
}
GLvoid Metaball::decrementZ()
{
	position.z--;
}

GLvoid Metaball::setSquaredRadius(GLfloat squaredRadius)
{
	this->squaredRadius = squaredRadius;
}

