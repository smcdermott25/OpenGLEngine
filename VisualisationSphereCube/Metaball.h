#pragma once
#include <glm/glm.hpp>
#include <glfw3.h>
class Metaball
{
public:
	
	Metaball(glm::vec3 position, GLfloat squaredRadius);
	Metaball::~Metaball();

	glm::vec3 getInitialPosition();
	glm::vec3 getPosition();
	GLfloat getSquaredRadius();
	GLfloat getRadius();

	GLvoid setPosition(glm::vec3 position);
	GLvoid setXPosition(GLfloat x);
	GLvoid setYPosition(GLfloat y);
	GLvoid setZPosition(GLfloat z);
	GLvoid incrementX();
	GLvoid incrementY();
	GLvoid incrementZ();
	GLvoid decrementX();
	GLvoid decrementY();
	GLvoid decrementZ();
	GLvoid setSquaredRadius(GLfloat squaredRadius);
	
private:
	glm::vec3 position;
	glm::vec3 initialPosition;
	GLfloat squaredRadius;
};

