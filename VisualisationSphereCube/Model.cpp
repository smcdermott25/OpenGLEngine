#include "Model.h"

/*
* Tree structure of Model loading library Assimp
*/

GLint TextureFromFile(const GLchar* path, std::string directory);
std::vector<std::string> names;
Model::Model()
	:ShadowCastingObject("", "", "")
{
	
}
Model::Model(GLchar* path, GLboolean animated, glm::vec3 scale, glm::vec3 position, GLchar* vertex, GLchar* frag)
	:ShadowCastingObject("",vertex, frag),
	Imposterable(position, scale)
{

	
	orientation->setPosition(position);
	orientation->setScale(scale);

	ModelPool* pool = ModelPool::getInstance();
	if (pool->checkExists(path, meshes))
	{
		meshes = pool->returnMesh(path);
		
	}
	else
	{
		loadModel(path);
		pool->setVector(path, meshes);
		//pool->setBuffer(path, tempInsttancedMatrixBuffer);
	}
	//tempInsttancedMatrixBuffer = pool->returnBuffer(path);
	//glGenBuffers(1, &tempInsttancedMatrixBuffer);
	setUpBoundingBoxData();
	drawMethod = &Model::draw;
}

Model::Model(GLchar* path, GLboolean animated, glm::vec3 scale, glm::vec3 position)
	:ShadowCastingObject("","",""),
	Imposterable(position, scale)
	//:GameObject(" "," ")
	
{
	
	orientation->setPosition(position);
	orientation->setScale(scale);
	
	ModelPool* pool = ModelPool::getInstance();
	if (pool->checkExists(path, meshes))
	{
		meshes = pool->returnMesh(path);
	}
	else
	{
		loadModel(path);
		pool->setVector(path, meshes);
	}

	drawMethod = &Model::draw;

	// set draw method 
	if (animated)
	{
		drawMethod = &Model::drawAnimated;
		GLchar* shaderName("Model shader");
		shader = &ShaderLoader::animationShader;
	}
	else
	{
		drawMethod = &Model::draw;
		shader = &ShaderLoader::modelShader;
	}

	setUpBoundingBoxData();
	
}


Model::~Model()
{
	//delete(shader);
}

//Orientation Model::getOrientation()
//{
//	return orientation;
//}

GLvoid Model::loadModel(std::string path)
{
	scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_GenNormals);
	importer.SetPropertyInteger(AI_CONFIG_PP_LBW_MAX_WEIGHTS, 5);
	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "ASSIMP Scene load error. Function loadModel, error message: " <<
			importer.GetErrorString() << std::endl;
		return;
	}

	directory = path.substr(0, path.find_last_of('/'));
	std::cout << "directory: " << directory << std::endl;

	// Initialise global inverse transform matrix for meshes
	globalInverseTransform = scene->mRootNode->mTransformation;
	globalInverseTransform.Inverse();

	processNode(scene->mRootNode, scene);
}

GLvoid Model::processNode(aiNode* node, const aiScene* scene)
{
	// Keep parent-child relationship by looping through
	// nodes in a recursive manner
	for (GLuint i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.push_back(processMesh(node, mesh, scene));
	}

	for (GLuint i = 0; i < node->mNumChildren; i++)
	{
		processNode(node->mChildren[i], scene);
	}

	for (GLuint i = 0; i < meshes.size(); i++)
	{
		meshes.at(i).setBones(bones);
	}

	bones.finalTransformations.resize(bones.offsetMatrices.size());
}

Mesh Model::processMesh(aiNode* node, aiMesh* mesh, const aiScene* scene)
{
	using namespace std;
	// Data to fill
	vector<Vertex>  vertices;
	vector<GLuint>  indices;
	vector<Texture> textures;
	vector<BonesInfo> boneInfo;

	//std::cout << "Mesh: " << mesh->mName.C_Str() << " num vertices: " << mesh->mNumVertices << std::endl;

	// Walk through each of the mesh's vertices
	for (GLuint i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		glm::vec3 vector;

		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		vertex.position = vector;

		vector.x = mesh->mNormals[i].x;
		vector.y = mesh->mNormals[i].y;
		vector.z = mesh->mNormals[i].z;
		vertex.normal = vector;

		// Texture Coordinates
		if (mesh->mTextureCoords[0])
		{
			glm::vec2 vec;
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.texCoords = vec;
		}
		else
		{
			vertex.texCoords = glm::vec2(0.0f, 0.0f);
		}

		if (vertex.position.x > maxX) { maxX = vertex.position.x; }
		if (vertex.position.x < minX) { minX = vertex.position.x; }
		if (vertex.position.y > maxY) { maxY = vertex.position.y; }
		if (vertex.position.y < minY) { minY = vertex.position.y; }
		if (vertex.position.z < minZ) { minZ = vertex.position.z; }
		if (vertex.position.y > maxZ) { maxZ = vertex.position.z; }
		vertices.push_back(vertex);
	}
	//Generate bounding boxes
	//Generate 2d rectanglur box for imposter
	boundingBoxForImposter.modelHeight = maxY - minY;
	boundingBoxForImposter.modelWidth = maxX - minX;
	boundingBoxForImposter.modelDepth = maxZ - minZ;

	//for a sphere i only need the diameter
	sphereRadius = (maxY - minY)/2;

	//Length of cross section corner to corner
	boundingBoxForImposter.largestWidth = glm::sqrt(glm::pow(boundingBoxForImposter.modelDepth, 2) + glm::pow(boundingBoxForImposter.modelWidth,2));
	boundingBoxForImposter.largestHeight = glm::sqrt(glm::pow(boundingBoxForImposter.modelHeight, 2) + glm::pow(boundingBoxForImposter.largestWidth, 2));

	boundingBoxForImposter.centrePoint = (glm::vec3(minX, minY, maxZ) + glm::vec3(maxX, maxY, minZ));
	boundingBoxForImposter.centrePoint = glm::vec3(boundingBoxForImposter.centrePoint.x / 2, boundingBoxForImposter.centrePoint.y/2, boundingBoxForImposter.centrePoint.z/2);
	//centerPoint = centerPoint*position;
	for (GLuint i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		for (GLuint j = 0; j < face.mNumIndices; j++)
		{
			indices.push_back(face.mIndices[j]);
		}
	}

	if (mesh->mMaterialIndex >= 0)
	{
		std::cout << "Materials > 0" << std::endl;
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

		vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

		vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
	}


	// Bones related to this mesh
	boneInfo.resize(mesh->mNumVertices);

	for (GLuint i = 0; i < mesh->mNumBones; i++)
	{
		// Update bones data structure
		bones.addBone(mesh->mBones[i]->mName.data, mesh->mBones[i]->mNumWeights + i);
		bones.addOffsetMatrix(mesh->mBones[i]->mName.data, mesh->mBones[i]->mOffsetMatrix);

		// Vertices with weights associated with this particular bone
		for (GLuint j = 0; j < mesh->mBones[i]->mNumWeights; j++)
		{
			GLuint vertexLocation = mesh->mBones[i]->mWeights[j].mVertexId;
			boneInfo.at(vertexLocation).addBoneAndWeight(mesh->mBones[i]->mWeights[j].mWeight, mesh->mBones[i]->mName.data, bones);
		}
	}

	Mesh meshName(vertices, indices, textures, boneInfo, scene);
	


	std::cout << "  largestWidth  " << boundingBoxForImposter.largestWidth << " largestm height " << boundingBoxForImposter.largestHeight << std::endl;
	return meshName;
}


std::vector<Texture> Model::loadMaterialTextures
(aiMaterial* material, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;
	for (GLuint i = 0; i < material->GetTextureCount(type); i++)
	{
		aiString str;
		material->GetTexture(type, i, &str);

		Texture texture;
		texture.id = TextureFromFile(str.C_Str(), directory);
		texture.type = typeName;
		textures.push_back(texture);
	}

	return textures;
}

//This methdo is used for models that are loaded on the wrong axis
GLvoid Model::flipBoundingBoxData()
{
	
	/*typedef struct
	{
	GLfloat modelHeight;
	GLfloat modelWidth;
	GLfloat modelDepth;
	GLfloat largestWidth;
	GLfloat largestHeight;
	glm::vec3 centrePoint;
	}boundingBox2D;
	*/

	std::cout << "The width before is " << boundingBoxForImposter.modelWidth << "The height is " << boundingBoxForImposter.modelHeight << "The depth is " << boundingBoxForImposter.modelDepth << std::endl;

	orientation->setRotationAxis(glm::vec3(1, 0, 0));
	orientation->addRotation(-90);
	GLfloat newModelHeight = boundingBoxForImposter.modelDepth;
	GLfloat newModelDepth = boundingBoxForImposter.modelHeight;
	GLfloat newLargestWidth = boundingBoxForImposter.largestHeight;
	GLfloat newLargestHeight = boundingBoxForImposter.largestWidth;
	//glm::vec3 newCentrePoint = 

	//boundingBoxForImposter.modelDepth = newModelDepth;
	//boundingBoxForImposter.modelHeight = newModelHeight;
//	boundingBoxForImposter.largestHeight = newLargestHeight;
//	boundingBoxForImposter.largestWidth = newLargestWidth;

	//GLfloat  temp = minY;
	minY = minZ;
	//minZ = temp;
	//temp = maxY;
	//maxY = maxZ;
	//maxZ = temp;
	//boundingBoxForImposter.centrePoint = (glm::vec3(minX, minY, maxZ) + glm::vec3(maxX, maxY, minZ));
	//boundingBoxForImposter.centrePoint = glm::vec3(boundingBoxForImposter.centrePoint.x / 2, boundingBoxForImposter.centrePoint.y / 2, boundingBoxForImposter.centrePoint.z / 2);
	boundingBoxForImposter.centrePoint.y = (boundingBoxForImposter.modelHeight/2);
	setUpBoundingBoxData();//reset the imposter
}
GLvoid Model::draw(glm::mat4 projection, glm::mat4 view, GLfloat deltaTime)
{
		shader->useProgram();

		glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "model"), 1, GL_FALSE, glm::value_ptr(orientation->getModelMatrix()));
		glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "view"), 1, GL_FALSE, glm::value_ptr(view));

		for (GLuint i = 0; i < meshes.size(); i++)
		{
			meshes[i].draw(*shader, deltaTime);
		}
	
}



GLvoid Model::drawForImposter(glm::mat4 view, glm::mat4 projection)
{
//	imposter->orientation->setModelMatrix(orientation->getModelMatrix());
	this->drawing(projection, view, glfwGetTime());
}

GLvoid Model::drawAnimatedTest(glm::mat4 projection, glm::mat4 view, glm::mat4 model, GLfloat deltaTime)
{
	shader->useProgram();
	//std::cout << "In draw animatesd test: ********************* a1: " << model[0][0] << std::endl;
	// animate
	meshes[0].animate(*shader, deltaTime);

	glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "model"), 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "view"), 1, GL_FALSE, glm::value_ptr(view));

	for (GLuint i = 0; i < meshes.size(); i++)
	{
		meshes[i].draw(*shader, deltaTime);
	}
}

GLvoid Model::drawAnimated(glm::mat4 projection, glm::mat4 view, GLfloat deltaTime)
{
	shader->useProgram();

	// animate
	meshes[0].animate(*shader, deltaTime);

	glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "model"), 1, GL_FALSE, glm::value_ptr(orientation->getModelMatrix()));
	glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(glGetUniformLocation(shader->getProgram(), "view"), 1, GL_FALSE, glm::value_ptr(view));

	for (GLuint i = 0; i < meshes.size(); i++)
	{
		meshes[i].draw(*shader, deltaTime);
	}
}

GLint TextureFromFile(const GLchar* path, std::string directory)
{
	stbi_set_flip_vertically_on_load(false);
	//Generate texture ID and load texture data 
	std::string filename = std::string(path);
	filename = directory + '/' + filename;
	GLuint textureID;
	glGenTextures(1, &textureID);
	int width, height,components;
	GLenum format;
	std::cout << "Texture image: " << filename;
	unsigned char* image = stbi_load(filename.c_str(), &width, &height,&components,0);
	
	if (components == 1)
		format = GL_RED;
	else if (components == 3)
		format = GL_RGB;
	else
		format = GL_RGBA;
// Assign texture to ID
	glBindTexture(GL_TEXTURE_2D, textureID);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	//glGenerateMipmap(GL_TEXTURE_2D);

	//unsigned char* image = SOIL_load_image(imagePath, &width, &height, 0, SOIL_LOAD_RGBA);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	// Parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	stbi_image_free(image);
	return textureID;

}

// Moving of interpolation code from mesh to model
aiMatrix4x4 Model::boneTransform(GLfloat timeInSeconds, std::vector<aiMatrix4x4>& transforms, Bones& bones)
{
	//std::cout << scene->mNumAnimations << std::endl;
	aiMatrix4x4 identity;

	GLfloat ticksPerSecond = scene->mAnimations[0]->mTicksPerSecond != 0 ?
		scene->mAnimations[0]->mTicksPerSecond : 25.0f;

	GLfloat timeInTicks = timeInSeconds * ticksPerSecond;
	GLfloat animationTime = fmod(timeInTicks, scene->mAnimations[0]->mDuration);

	readNodeHierarchy(animationTime, scene->mRootNode, identity);


	return identity;
}

GLvoid Model::readNodeHierarchy(GLfloat animationTime, const aiNode* node, const aiMatrix4x4& parentTransform)
{
	std::string nodeName(node->mName.data);
	const aiAnimation* animation = scene->mAnimations[0];
	aiMatrix4x4 nodeTransformation(node->mTransformation);
	const aiNodeAnim* nodeAnimation = nullptr;

	for (GLuint i = 0; i < animation->mNumChannels; i++)
	{
		if (animation->mChannels[i]->mNodeName.data == nodeName)
		{
			// Every bone, only nodes which are bones are affected
			nodeAnimation = animation->mChannels[i];
		}
	}

	if (nodeAnimation)
	{
		//std::cout << "Bone Name: " << nodeName << std::endl;

		// gen scaling transformation matrix
		aiVector3D scaling;
		calculateInterpolatedScaling(scaling, animationTime, nodeAnimation);
		aiMatrix4x4 scalingMatrix;
		scalingMatrix.Scaling(scaling, scalingMatrix);

		// rotation
		aiQuaternion rotation;
		calculateInterpolatedRotation(rotation, animationTime, nodeAnimation);
		aiMatrix4x4 rotationMatrix = aiMatrix4x4(rotation.GetMatrix());

		aiVector3D translation;
		calculateInterpolatedPosition(translation, animationTime, nodeAnimation);
		aiMatrix4x4 translationMatrix;
		translationMatrix.Translation(translation, translationMatrix);

		// combine transformations
		nodeTransformation = translationMatrix * rotationMatrix * scalingMatrix;
	}

	// Each bone is affected by it's parent's as well
	aiMatrix4x4 globalTransformation = parentTransform * nodeTransformation;

	//Looking at a bone node
	if (bones.boneNameIdMap.find(nodeName) != bones.boneNameIdMap.end())
	{
		aiMatrix4x4 final = globalTransformation * globalInverseTransform * bones.offsetMatrices.at(bones.boneNameIdMap[nodeName]);
		bones.insertBoneMatrix(final, nodeName);
	}

	// Pass transformation to this node's children
	for (GLuint i = 0; i < node->mNumChildren; i++)
	{
		readNodeHierarchy(animationTime, node->mChildren[i], globalTransformation);
	}
}

GLvoid Model::calculateInterpolatedPosition(aiVector3D& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation)
{
	//std::cout << "Num position keys: " << nodeAnimation->mNumPositionKeys;

	GLuint numPositionKeys = nodeAnimation->mNumPositionKeys;
	GLuint positionIndex;

	for (GLuint i = 0; i < numPositionKeys; i++)
	{
		GLuint value = i + 1;
		if (animationTime < (GLfloat )nodeAnimation->mPositionKeys[value].mTime)
		{
			positionIndex = i;
			break;
		}
	}

	GLuint nextPositionIndex = (positionIndex + 1);

	GLfloat timeTwo = nodeAnimation->mPositionKeys[nextPositionIndex].mTime;
	GLfloat timeOne = nodeAnimation->mPositionKeys[positionIndex].mTime;
	GLfloat deltaTime = timeTwo - timeOne;

	GLfloat factor = (animationTime - (GLfloat)nodeAnimation->mPositionKeys[positionIndex].mTime) / deltaTime;
	assert(factor > 0.0f && factor <= 1.0f);

	const aiVector3D& startPosition = nodeAnimation->mPositionKeys[positionIndex].mValue;
	const aiVector3D& endPosition = nodeAnimation->mPositionKeys[nextPositionIndex].mValue;

	aiVector3D delta = endPosition - startPosition;
	out = startPosition + factor * delta;
}

GLvoid Model::calculateInterpolatedScaling(aiVector3D& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation)
{

	GLuint numScalingKeys = nodeAnimation->mNumScalingKeys;
	GLuint scalingIndex = 0;

	for (GLuint i = 0; i < numScalingKeys; i++)
	{
		GLuint value = i + 1;
		if (animationTime < (GLfloat )nodeAnimation->mScalingKeys[value].mTime)
		{
			scalingIndex = i;
			break;
		}
	}

	GLuint nextScalingIndex = (scalingIndex + 1);

	GLfloat timeTwo = nodeAnimation->mScalingKeys[nextScalingIndex].mTime;
	GLfloat timeOne = nodeAnimation->mScalingKeys[scalingIndex].mTime;
	GLfloat deltaTime = timeTwo - timeOne;

	GLfloat factor = (animationTime - (GLfloat)nodeAnimation->mScalingKeys[scalingIndex].mTime) / deltaTime;
	assert(factor > 0.0f && factor <= 1.0f);

	const aiVector3D& startScaling = nodeAnimation->mScalingKeys[scalingIndex].mValue;
	const aiVector3D& endScaling = nodeAnimation->mScalingKeys[nextScalingIndex].mValue;

	aiVector3D delta = endScaling - startScaling;
	out = startScaling + factor * delta;
}

GLvoid Model::calculateInterpolatedRotation(aiQuaternion& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation)
{
	// at least 2 values needed to interpolate
	//std::cout << "Num of rotation keys  " << nodeAnimation->mNumRotationKeys << std::endl;
	if (nodeAnimation->mNumRotationKeys < 2)
	{
		std::cout << "Less than 2 rotation keys: " << std::endl;
	}

	GLuint rotationIndex = findRotation(animationTime, nodeAnimation);
	GLuint nextRotationIndex = (rotationIndex + 1);

	assert(nextRotationIndex < nodeAnimation->mNumRotationKeys);

	GLfloat timeTwo = nodeAnimation->mRotationKeys[nextRotationIndex].mTime;
	GLfloat timeOne = nodeAnimation->mRotationKeys[rotationIndex].mTime;
	GLfloat deltaTime = timeTwo - timeOne;

	GLfloat factor = (animationTime - (GLfloat)nodeAnimation->mRotationKeys[rotationIndex].mTime) / deltaTime;
	assert(factor > 0.0f && factor <= 1.0f);

	const aiQuaternion& startRotation = nodeAnimation->mRotationKeys[rotationIndex].mValue;
	const aiQuaternion& endRotation = nodeAnimation->mRotationKeys[nextRotationIndex].mValue;
	aiQuaternion::Interpolate(out, startRotation, endRotation, factor);
	out = out.Normalize();
}

GLuint Model::findRotation(GLfloat animationTime, const aiNodeAnim* nodeAnimation)
{
	GLuint numRotationKeys = nodeAnimation->mNumRotationKeys;

	assert(numRotationKeys > 0);

	for (GLuint i = 0; i < numRotationKeys; i++)
	{
		GLuint value = i + 1;
		if (animationTime < (GLfloat )nodeAnimation->mRotationKeys[value].mTime)
		{
			return i;
		}
	}

	// if no correct rotation is found, assert
	std::cout << "No rotation found: " << std::endl;
	assert(0);
}

GLvoid Model::drawSimpleVAO()
{
	for (GLint  i = 0; i < meshes.size(); i++)
	{

		glBindVertexArray(meshes.at(i).getVertexArray());
		glDrawElements(GL_TRIANGLES, meshes.at(i).indices.size(), GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);

	}
	//printf("Plz don't draw me like this\n");
	//glBindVertexArray(VAO);
	//glDrawArrays(GL_TRIANGLES, 0, surfaceVertices.size());
	glBindVertexArray(0);
}

GLvoid Model::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow)
{
	
}
