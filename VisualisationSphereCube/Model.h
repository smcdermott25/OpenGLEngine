#pragma once

//#include <SOIL.h>
#include "stb_image.h"
#include "Imposterable.h"
#include "Mesh.h"
#include "InstancedObject.h"
#include "GameObject.h"
#include "ShadowCastingObject.h"
#include "ModelPool.h"


class Model :public ShadowCastingObject, public Imposterable
{
public:

	Model();
	// Constructor, animated decides which methods to use for loading / drawing the model
	Model(GLchar* path, GLboolean animated, glm::vec3 scale, glm::vec3 position, GLchar*  vertex, GLchar* frag);
	Model(GLchar* path, GLboolean animated, glm::vec3 scale, glm::vec3 position);
	~Model();

	GLvoid Model::drawing(glm::mat4 projection, glm::mat4 view, GLfloat deltaTime)
	{
		(this->*drawMethod)(projection, view, deltaTime);
	}



	GLvoid Model::drawAnimatedTest(glm::mat4 projection, glm::mat4 view, glm::mat4 model, GLfloat deltaTime);
	GLvoid flipBoundingBoxData();

	GLvoid(Model::*drawMethod)(glm::mat4 projection, glm::mat4 view, GLfloat deltaTime);

protected:
	std::vector<Mesh> meshes;

	GLfloat sphereRadius = 0;


private:
	GLboolean firstCreated;

	//Bounding box Data
	GLfloat minX = 0;
	GLfloat maxX = 0;
	GLfloat minY = 0;
	GLfloat maxY = 0;
	GLfloat minZ = 0;
	GLfloat maxZ = 0;


	const aiScene* scene;
	Assimp::Importer importer;
	Shader* shader;

	std::string directory;

	// If a model is animated it contains a number of bones
	Bones bones;

	GLvoid loadModel(std::string path);
	GLvoid processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiNode* node, aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> loadMaterialTextures
	(
		aiMaterial* material,
		aiTextureType type,
		std::string typeName
	);

	// Moving interpolating code from mesh to model
	aiMatrix4x4 boneTransform(GLfloat timeInSeconds, std::vector<aiMatrix4x4>& transforms, Bones& bones);
	GLvoid readNodeHierarchy(GLfloat animationTime, const aiNode* node, const aiMatrix4x4& parentTransform);

	GLvoid calculateInterpolatedPosition(aiVector3D& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation);
	GLvoid calculateInterpolatedScaling(aiVector3D& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation);
	GLvoid calculateInterpolatedRotation(aiQuaternion& out, GLfloat animationTime, const aiNodeAnim* nodeAnimation);
	GLvoid drawSimpleVAO();
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight>& pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>>& objectsThatCastShadow);

	GLuint findRotation(GLfloat animationTime, const aiNodeAnim* nodeAnimation);

	// method pointer to decide which draw method to use
	// e.g. whether this model is animated or not, this is decided upon contruction of the model
	GLvoid Model::draw(glm::mat4 projection, glm::mat4 view, GLfloat deltaTime);
	GLvoid Model::drawAnimated(glm::mat4 projection, glm::mat4 view, GLfloat deltaTime);
	GLvoid drawForImposter(glm::mat4 projection, glm::mat4 view);
};

