#include "ModelPool.h"

ModelPool* ModelPool::instance = 0;

ModelPool::ModelPool()
{

}

ModelPool::~ModelPool()
{

}

ModelPool* ModelPool::getInstance()
{
	if (instance == nullptr)
	{
		instance = new ModelPool();
	}
	else
	{
		return instance;
	}
}

GLboolean  ModelPool::checkExists(std::string ref, std::vector<Mesh>& mesh)
{
	if (meshes.find(ref) != meshes.end())
	{
		//mesh = *meshes[ref];
		return true;
	}
	else
	{
		GLuint tempInsttancedMatrixBuffer;
		glGenBuffers(1, &tempInsttancedMatrixBuffer);
		buffers[ref] = tempInsttancedMatrixBuffer;
		return false;
	}
}
GLvoid ModelPool::setVector(std::string ref, std::vector<Mesh> mesh)
{
	meshes[ref] = mesh;
}
std::vector<Mesh>& ModelPool::returnMesh(std::string ref)
{
	return meshes[ref];

}

GLuint* ModelPool::returnBuffer(std::string ref)
{
	return &(buffers[ref]);
}