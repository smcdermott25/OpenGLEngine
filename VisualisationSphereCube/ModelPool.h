#pragma once


// Different types of Noise
#include "Mesh.h"


class ModelPool
{
public:
	static ModelPool* getInstance();
	GLboolean  checkExists(std::string, std::vector<Mesh>& mesh);
	GLvoid setVector(std::string, std::vector<Mesh> mesh);
	std::vector<Mesh>& returnMesh(std::string ref);
	GLuint* returnBuffer(std::string ref);

private:
	ModelPool();
	~ModelPool();

	static ModelPool* instance;
	std::map<std::string, std::vector<Mesh>> meshes;
	std::map<std::string, GLuint> buffers;
};