#include "Mountain.h"

Mountain::Mountain(glm::vec3 position, GLint seed)
	: Terrain(position, seed, 700, 257, 3200, new FractalNoise(log2(257), seed, 0.66))
{
	
}

Mountain::~Mountain()
{

}