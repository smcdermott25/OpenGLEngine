#pragma once
#include "Terrain.h"

class Mountain : public Terrain
{
private:

public:
	Mountain(glm::vec3 position, GLint seed);
	~Mountain();
};