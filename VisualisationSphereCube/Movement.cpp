#include "Movement.h"
#include <iostream>

Movement::Movement(Orientation* ori)
{
	orientation = ori;
}

Movement::Movement()
{

}

GLvoid Movement::move(GLfloat deltaTime)
{
	
	if (rolling == true)
	{
		mRolling(deltaTime, getYPosition());
	}
	else if (dropping == true)
	{
		mDropping(deltaTime);
	}
	else if (bouncing == true)
	{
		mBouncing(deltaTime);
	}
	else if (upwards == true)
	{
		mUpwards(deltaTime);
	}
}

GLvoid Movement::mUpwards(GLfloat deltaTime)
{
	acceleration = -1.0f * gravity;
	energyDissapationFactor = 0.8f;
	glm::vec3 newVelocity = glm::vec3(getVelocity().x, (getVelocity().y + (acceleration* deltaTime)), getVelocity().z);
	newPosition.x = getVelocity().x * deltaTime;
	GLfloat temp = newPosition.y;
	newPosition.y = (energyDissapationFactor *((newVelocity.y * newVelocity.y) - (getVelocity().y * getVelocity().y))) / (2.0f * acceleration);
	newPosition.z = getVelocity().z * deltaTime;
	orientation->setPosition(glm::vec3((orientation->getPosition().x + newPosition.x), (orientation->getPosition().y + newPosition.y), (orientation->getPosition().z + newPosition.z)));
	
	

	setVelocity(newVelocity);
	applyRotation(velocity, deltaTime);
	if (newPosition.y < 0.001f)
	{
		dropping = true;
		upwards = false;
	}

}

GLvoid Movement::mDropping(GLfloat deltaTime)
{
	//setting variables relevant to dropping
	energyDissapationFactor = 1.0f;
	acceleration = gravity;

	// use v = u + at to work out v
	glm::vec3 newVelocity = glm::vec3(getVelocity().x, (getVelocity().y + (acceleration* deltaTime)), getVelocity().z);
	
	// then sub v into v^2 = u^2 + 2as (s = (v^2 - u^2)/2a )
	newPosition.x = orientation->getPosition().x + getVelocity().x * deltaTime * energyDissapationFactor;
	newPosition.y = orientation->getPosition().y - ((energyDissapationFactor *((newVelocity.y * newVelocity.y) - (getVelocity().y * getVelocity().y))) / (2.0f * acceleration));
	newPosition.z = orientation->getPosition().z + getVelocity().z * deltaTime * energyDissapationFactor;

	orientation->setPosition(glm::vec3(newPosition.x, newPosition.y, newPosition.z));
	setVelocity(newVelocity);

	//applying rotation
	applyRotation(velocity, deltaTime);
}

GLvoid Movement::mBouncing(GLfloat deltaTime)
{
	//setting variables relevant to bouncing specifically
	acceleration = -1.0f * gravity;
	energyDissapationFactor = 0.8f;
	glm::vec3 newVelocity = glm::vec3(getVelocity().x, (getVelocity().y + (acceleration* deltaTime)), getVelocity().z);
	newPosition.x = orientation->getPosition().x + (getVelocity().x * deltaTime);
	GLfloat difference = energyDissapationFactor *((newVelocity.y * newVelocity.y) - (getVelocity().y * getVelocity().y)) / (2.0f * acceleration);
	newPosition.y = orientation->getPosition().y + difference;
	printf("\nGet radius %f\n", getRadius());
	if (newPosition.y - getRadius() < yPosition)
	{
		//if the y veloicty goes low enough we switch to rolling
		if (getVelocity().y < 10.0f)
		{
			bouncing = false;
			rolling = true;
		}
		newPosition.y = yPosition + radius;
	}
	newPosition.z = orientation->getPosition().z + (getVelocity().z * deltaTime);
	orientation->setPosition(glm::vec3((newPosition.x), ( newPosition.y), (newPosition.z)));
	if (bouncing == true && bounceFrame == true)
	{
		GLfloat totalVelocity = newVelocity.x + newVelocity.y + newVelocity.z;
		newVelocity.x = normal.x * totalVelocity;
		newVelocity.y = normal.y * totalVelocity;
		newVelocity.z = normal.z * totalVelocity;
		bounceFrame = false;
	}
	setVelocity(newVelocity);

	applyRotation(velocity, deltaTime);
	//when it reaches the top of its bounce this switches it back to dropping
	if (difference < 0.0001f)
	{
		dropping = true;
		bouncing = false;
		bounceFrame = true;
	}

}


GLvoid Movement::mRolling(GLfloat deltaTime, GLfloat yPosition)
{
		bouncing = false;
		dropping = false;
		energyDissapationFactor = 0.99f;
		acceleration = gravity;
		// use v = u + at to work out v
		GLfloat totalVelocity = fabs(getVelocity().x) +  fabs(getVelocity().y) +  fabs(getVelocity().z);
		normal.x = normal.x + (normal.y / 2.0f);
		normal.z = normal.z + (normal.y / 2.0f);
		glm::vec3 newVelocity = glm::vec3((totalVelocity * normal.x * energyDissapationFactor), (getVelocity().y*energyDissapationFactor), (totalVelocity * normal.z * energyDissapationFactor));
		// then sub v into v^2 = u^2 + 2as (s = (v^2 - u^2)/2a )
		newPosition.x = orientation->getPosition().x + (newVelocity.x * deltaTime);
		newPosition.y = yPosition + getRadius();
		newPosition.z = orientation->getPosition().z + (newVelocity.z * deltaTime);

		if (getVelocity().y > 3.0f)
		{
			rolling = false;
			bouncing = true;
		}

		orientation->setRotationAngle((0.5f * deltaTime * normal.y));

		orientation->setPosition(glm::vec3(newPosition.x, newPosition.y, newPosition.z));
		setVelocity(newVelocity);
}


GLvoid Movement::launch(glm::vec3 initialVelocity, GLfloat deltaTime)
{
	if (firstLaunch == true)
	{
		if(initialVelocity.y > 0.0f)
		{ 
			upwards = true;
			dropping = false;
		}
		else
		{
			upwards = false;
			dropping = true;
		}
		
		setVelocity(initialVelocity);
		firstLaunch = false;
	}
	move(deltaTime);
}


GLboolean Movement::moveToPosition(glm::vec3 newPosition, glm::vec3 initialVelocity, GLfloat deltaTime)
{
	if (moveTurn == 1)
	{
		startingPosition = orientation->getPosition();
		initialVelocity = getVelocity();
	}
	GLboolean newPos = newPosition.x > 0.0f;
	GLboolean oldPos = startingPosition.x > 0.0f;

	//IF STARTING AND ENDING POSITIONS ARE POSITIVE
	if (newPos == true && oldPos == true)
	{
		//IF YOU ARE MOVING BACKWARDS
		if (startingPosition.x > newPosition.x)
		{
			if (moveTurn == 1)
			{
				difference = startingPosition.x - newPosition.x;
				deltaPosition = -1.0f * ((startingPosition.x - newPosition.x) / initialVelocity.x);
				moveTurn--;
			}
			difference += deltaPosition;
		}
		//IF YOU ARE MOVING FORWARDS
		else
		{
			if (moveTurn == 1)
			{
				difference = -1.0f * (startingPosition.x - newPosition.x);
				deltaPosition = (startingPosition.x - newPosition.x) / initialVelocity.x;
				moveTurn--;
			}
			difference += deltaPosition;
		}
	}
	//IF STARTING AND ENDING POSITIONS ARE NEGATIVE
	else if (newPos == false && oldPos == false)
	{
		//IF STARTING POSITION IS HIGHER THAN END POSITION
		if (startingPosition.x > newPosition.x)
		{
			if (moveTurn == 1)
			{
				difference = (startingPosition.x - newPosition.x);
				moveTurn--;
			}
			deltaPosition = -1.0f * ((startingPosition.x - newPosition.x) / initialVelocity.x);
			difference += deltaPosition;
		}
		else
		{
			if (moveTurn == 1)
			{
				difference = -1.0f * (startingPosition.x - newPosition.x);
				moveTurn--;
			}
			deltaPosition = -1.0f * (startingPosition.x - newPosition.x) / initialVelocity.x;
			difference -= deltaPosition;
		}
	}
	//OTHER COMBINATIONS
	else
	{
		//IF END POSITION IS POSITIVE AND START POSITION IS NEGATIVE
		if (newPos == true)
		{
			if (moveTurn == 1)
			{
				difference = -1.0f * (startingPosition.x - newPosition.x);
				moveTurn--;
			}
			deltaPosition = -1.0f * (startingPosition.x - newPosition.x) / initialVelocity.x;
			difference -= deltaPosition;
		}
		//IF START POSITION IS POSITIVE AND END POSITION IS NEGATIVE
		else
		{
			if (moveTurn == 1)
			{
				difference = (startingPosition.x - newPosition.x);
				moveTurn--;
			}
			deltaPosition = -1.0f * (startingPosition.x - newPosition.x) / initialVelocity.x;
			difference += deltaPosition;
		}
	}

	//updating x, y, z variables
	glm::vec3 move;
	move.x = orientation->getPosition().x + deltaPosition;
	move.y = orientation->getPosition().y + (newPosition.y - orientation->getPosition().y) / initialVelocity.y;
	move.z = orientation->getPosition().z + (newPosition.z - orientation->getPosition().z) / initialVelocity.z;

	if (difference > 0.0f)
	{
		orientation->setPosition(move);
		return true;
	}
	else
	{
		moveTurn = 1;
		return false;
	}
}

GLvoid Movement::moveX(glm::vec3 newVelocity, GLfloat deltaTime)
{
	glm::vec3 move;
	move.x = orientation->getPosition().x + (newVelocity.x * deltaTime);
	move.y = orientation->getPosition().y;
	move.z = orientation->getPosition().z;
	orientation->setPosition(move);
}

GLvoid Movement::moveY(glm::vec3 newVelocity, GLfloat deltaTime)
{
	glm::vec3 move;
	move.x = orientation->getPosition().x;
	move.y = orientation->getPosition().y + (newVelocity.y * deltaTime);
	move.z = orientation->getPosition().z;
	orientation->setPosition(move);
}

GLvoid Movement::moveZ(glm::vec3 newVelocity, GLfloat deltaTime)
{
	glm::vec3 move;
	move.x = orientation->getPosition().x;
	move.y = orientation->getPosition().y;
	move.z = orientation->getPosition().z + (newVelocity.z * deltaTime);
	orientation->setPosition(move);

}

GLvoid Movement::setVelocity(glm::vec3 newVelocity)
{
	velocity = newVelocity;
}

glm::vec3 Movement::getVelocity()
{
	return velocity;
}

GLvoid Movement::setYPosition(GLfloat newYPosition)
{
	yPosition = newYPosition;
	
}

GLvoid Movement::setRadius(GLfloat newRadius)
{
	radius = newRadius;
}

GLvoid Movement::setLaunchVelocity(glm::vec3 newLaunchVelocity)
{
	launchVelocity = newLaunchVelocity;
}

GLfloat Movement::getYPosition()
{
	return yPosition;
}

glm::vec3 Movement::getLaunchVelocity()
{
	return launchVelocity;
}


GLfloat Movement::getRadius()
{
	return radius;
}

void Movement::calculateNewVelocity(glm::vec3 obj1Velocity, glm::vec3 obj2Velocity, GLfloat obj1Radius, GLfloat obj2Radius)
{
	glm::vec3 velocity = applyMomentum(obj1Velocity, obj2Velocity, obj1Radius, obj2Radius);
	setVelocity(glm::vec3(getVelocity().x + velocity.x, getVelocity().y + velocity.y, getVelocity().z + velocity.z));
}


glm::vec3 Movement::applyMomentum(glm::vec3 velocityObject1, glm::vec3 velocityObject2, GLfloat weightObject1, GLfloat weightObject2)
{
	GLfloat totalWeight = weightObject1 + weightObject2;
	GLfloat weightRatio1 = weightObject1/(weightObject1 + weightObject2);
	glm::vec3 momentumObject1 = glm::vec3(velocityObject1.x * weightObject1, velocityObject1.y * weightObject1, velocityObject1.z * weightObject1);
	glm::vec3 momentumObject2 = glm::vec3(velocityObject2.x * weightObject2, velocityObject2.y * weightObject2, velocityObject2.z * weightObject2);
	momentum = glm::vec3(momentumObject1.x + momentumObject2.x, momentumObject1.y + momentumObject2.y, momentumObject1.z + momentumObject2.z);
	glm::vec3 resultantVelocity = glm::vec3(((momentum.x / totalWeight) * weightRatio1),	((momentum.y / totalWeight) * weightRatio1),	((momentum.z / totalWeight ) * weightRatio1));
	
	return resultantVelocity;
}


void Movement::applyRotation(glm::vec3 velocity, GLfloat deltaTime)
{
	GLfloat XZResultantVector = velocity.x + velocity.z / 2;
	orientation->setRotationAngle(orientation->getRotationAngle()+ (0.0005f * deltaTime * XZResultantVector));
}
