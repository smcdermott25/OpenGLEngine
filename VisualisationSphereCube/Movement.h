#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glew.h>
#include "Orientation.h"


class Movement {
private:
	//physics stuff
	//const GLfloat gravity = 9.81f;
	const GLfloat gravity = 50.0f;
	GLfloat acceleration;
	GLfloat energyDissapationFactor = 1.0f;
	glm::vec3 normal = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 launchVelocity = glm::vec3(0.0f, 0.0f, 0.0f);
	GLboolean bounceFrame = true;
	GLfloat yPosition;

	//movement stuff		
	GLuint moveTurn = 1;										 // a turn GLint  for gradual movement
	GLuint startingDrop = 1;									 //a turn GLint  for dropping
	GLboolean firstLaunch = true;
	glm::vec3 newPosition = glm::vec3(0.0f, 0.0f, 0.0f);
	GLfloat startingPositionDrop = 0.0f;
	GLfloat difference;											 // difference for gradual movement
	GLfloat deltaPosition;										 // update used to update position
	glm::vec3 velocity = glm::vec3(1.0f, 1.0f, 1.0f);		     // velocity of the object
	glm::vec3 initialVelocity;									 // initialVelocity
	glm::vec3 startingPosition;									 // used for velocity
	Orientation* orientation;									 // orientation
	GLboolean bouncing = false;
	GLboolean dropping = false;
	GLboolean upwards = false;
	GLboolean rolling = false;
	glm::vec3 momentum = glm::vec3(0.0f, 0.0f, 0.0f);
	GLfloat radius;



public:
	
	Movement();
	~Movement();
	Movement(Orientation* ori);
	GLboolean moveToPosition(glm::vec3 newPosition, glm::vec3 initialVelocity, GLfloat deltaTime);
	GLvoid move(GLfloat deltaTime);
	GLvoid launch(glm::vec3 initialVelocity, GLfloat deltaTime);
	GLvoid mDropping(GLfloat deltaTime);
	GLvoid mBouncing(GLfloat deltaTime);
	GLvoid mUpwards(GLfloat deltaTime);
	GLvoid mRolling(GLfloat rolling, GLfloat yPosition);
	GLvoid moveX(glm::vec3 initialVelocity, GLfloat deltaTime);
	GLvoid moveY(glm::vec3 initialVelocity, GLfloat deltaTime);
	GLvoid moveZ(glm::vec3 initialVelocity, GLfloat deltaTime);
	GLvoid setVelocity(glm::vec3 newVelocity);
	glm::vec3 getVelocity();
	GLvoid setNormal(glm::vec3 newNormal)
	{
		normal = newNormal;
	}
	GLboolean getUpwards()
	{
		return upwards;
	}
	GLboolean getBouncing()
	{
		return bouncing;
	}
	GLvoid setBouncing(GLboolean newBouncing)
	{
		bouncing = newBouncing;
	}
	GLboolean getDropping()
	{
		return dropping;
	}
	GLvoid setDropping(GLboolean newDropping)
	{
		dropping = newDropping;
	}
	GLboolean getRolling()
	{
		return rolling;
	}
	GLvoid setRolling(GLboolean newRolling)
	{
		rolling = newRolling;
	}
	GLvoid Movement::setYPosition(GLfloat yPosition);
	GLvoid Movement::setRadius(GLfloat newRadius);
	GLvoid Movement::setLaunchVelocity(glm::vec3 launchVelocity);
	GLfloat Movement::getRadius();
	GLfloat Movement::getYPosition();
	glm::vec3 Movement::getLaunchVelocity();
	void Movement::calculateNewVelocity(glm::vec3 obj1Velocity, glm::vec3 obj2Velocity, GLfloat obj1radius, GLfloat obj2radius);
	glm::vec3 Movement::applyMomentum(glm::vec3 velocityObject1, glm::vec3 velocityObject2, GLfloat weightObject1, GLfloat weightObject2);
	void Movement::applyRotation(glm::vec3 velocity, GLfloat deltaTime);
};
