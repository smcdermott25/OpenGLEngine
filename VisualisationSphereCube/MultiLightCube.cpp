#include "MultiLightCube.h"


/*Reference: https://learnopengl.com/code_viewer.php?code=lighting/multiple_lights */

MultiLightCube::~MultiLightCube()
{
}
/*Cube has all applicable lighting and uses the correct draw calls. Can work with shadows and is ready to be casted on to, just need a shadow defined.*/
MultiLightCube::MultiLightCube()
	:ShadowCastingObject("MultilightCube", "Shader/multiCasterVertex.txt", "Shader/multiCasterFragment.txt")
{
	shaderProgram->useProgram();
	printf("Shader program number is when outside: %i\n", shaderProgram->getProgram());
	materialShininess = 32.0f;
	// Set up vertex data (and buffer(s)) and attribute pointers
	GLfloat vertices[] = {
		// Positions          // Normals           // Texture Coords
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
		0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f,
		0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
		0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,

		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
		0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f,
		0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
		0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
		-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

		0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
		0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
		0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
		0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
		0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
		0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,
		0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f,
		0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
		0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,

		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
		0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f,
		0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
		0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f
	};

	glGenVertexArrays(1, &containerVAO);
	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindVertexArray(containerVAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);
	glBindVertexArray(0);


	glGenTextures(1, &texture);
	glGenTextures(1, &specularTexture);
	int width, height, components;
	unsigned char* image;

	// Diffuse map
	image = stbi_load("Texture/container.png", &width, &height,&components, 0);

	GLenum format;

	if (components == 1)
		format = GL_RED;
	else if (components == 3)
		format = GL_RGB;
	else
		format = GL_RGBA;

	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	// Specular map
	image = stbi_load("Texture/container_specular.png", &width, &height,&components, 0);
	if (components == 1)
		format = GL_RED;
	else if (components == 3)
		format = GL_RGB;
	else
		format = GL_RGBA;
	glBindTexture(GL_TEXTURE_2D, specularTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);

}

GLuint MultiLightCube::getTexture()
{
	return texture;
}

GLvoid MultiLightCube::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow)
{
	shaderProgram->useProgram();
	GLuint program = shaderProgram->getProgram();
	// Bind diffuse map
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	// Bind specular map
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, specularTexture);
	glm::mat4 emptyMatrix;
	glUniformMatrix4fv(glGetUniformLocation(program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(emptyMatrix));
	glUniform1i(glGetUniformLocation(program, "shadowsOn"), 1);
	glUniform1i(glGetUniformLocation(program, "bloomOn"), 1);
	glUniform1f(glGetUniformLocation(program, "material.shininess"), materialShininess);
	glUniform3f(glGetUniformLocation(program, "viewPos"), cameraPos.x, cameraPos.y, cameraPos.z);
	
	glUniform1i(glGetUniformLocation(program, "material.diffuse"), 0);
	glUniform1i(glGetUniformLocation(program, "material.specular"), 1);
	glUniform3f(glGetUniformLocation(program, "dirLight.direction"), directionalLight.getDirectionOfLight().x, directionalLight.getDirectionOfLight().y, directionalLight.getDirectionOfLight().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.ambient"), directionalLight.getAmbient().x, directionalLight.getAmbient().y, directionalLight.getAmbient().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.diffuse"), directionalLight.getDiffuse().x, directionalLight.getDiffuse().y, directionalLight.getDiffuse().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.specular"), directionalLight.getSpecular().x, directionalLight.getSpecular().y, directionalLight.getSpecular().z);
	//Iterate over all point lights
	for (GLint  i = 0; i < pointLightList.size(); i++)
	{
		std::string number = std::to_string(i);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].position").c_str()), pointLightList.at(i).getPosition().x, pointLightList.at(i).getPosition().y, pointLightList.at(i).getPosition().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].ambient").c_str()), pointLightList.at(i).getAmbient().x, pointLightList.at(i).getAmbient().y, pointLightList.at(i).getAmbient().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].diffuse").c_str()), pointLightList.at(i).getDiffuse().x, pointLightList.at(i).getDiffuse().y, pointLightList.at(i).getDiffuse().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].specular").c_str()), pointLightList.at(i).getSpecular().x, pointLightList.at(i).getSpecular().y, pointLightList.at(i).getSpecular().z);
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].constant").c_str()), pointLightList.at(i).getConstant());
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].linear").c_str()), pointLightList.at(i).getLinear());
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].quadratic").c_str()), pointLightList.at(i).getQuadratic());

	}
	// SpotLight
	glUniform3f(glGetUniformLocation(program, "spotLight.position"), spotLight.getPosition().x , spotLight.getPosition().y, spotLight.getPosition().z);
	//printf("Spotlight position is: X %f, Y %f Z %f", spotLight.getPosition().x, spotLight.getPosition().y, spotLight.getPosition().z);
	glUniform3f(glGetUniformLocation(program, "spotLight.direction"), spotLight.getFront().x, spotLight.getFront().y, spotLight.getFront().z);
	//printf("Front is: X %f, Y %f Z %f", spotLight.getFront().x, spotLight.getFront().y, spotLight.getFront().z);
	glUniform3f(glGetUniformLocation(program, "spotLight.ambient"), spotLight.getAmbient().x, spotLight.getAmbient().y, spotLight.getAmbient().z);
	glUniform3f(glGetUniformLocation(program, "spotLight.diffuse"), spotLight.getDiffuse().x, spotLight.getDiffuse().y, spotLight.getDiffuse().z);
	glUniform3f(glGetUniformLocation(program, "spotLight.specular"), spotLight.getSpecular().x, spotLight.getSpecular().y, spotLight.getSpecular().z);
	glUniform1f(glGetUniformLocation(program, "spotLight.constant"), spotLight.getConstant());
	glUniform1f(glGetUniformLocation(program, "spotLight.linear"), spotLight.getLinear());
	glUniform1f(glGetUniformLocation(program, "spotLight.quadratic"), spotLight.getQuadratic());
	glUniform1f(glGetUniformLocation(program, "spotLight.cutOff"), glm::cos(glm::radians(12.5f)));
	glUniform1f(glGetUniformLocation(program, "spotLight.outerCutOff"), glm::cos(glm::radians(15.0f)));

	// Pass the matrices to the shader
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(glGetUniformLocation(program, "model"), 1, GL_FALSE, glm::value_ptr(orientation->getModelMatrix()));
	

	drawSimpleVAO();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	
}

GLvoid MultiLightCube::drawSimpleVAO()
{
	glBindVertexArray(containerVAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

GLvoid MultiLightCube::update()
{
	moveSideToSide();
}

GLvoid MultiLightCube::moveSideToSide()
{
	orientation->setPosition(glm::vec3((orientation->getPosition().x + 10), orientation->getPosition().y, orientation->getPosition().z));
	std::cout << orientation->getPosition().x << "\n";
}
