#pragma once
#include <glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "ShadowCastingObject.h"
#include "GameWorld.h"
#include "PointLight.h"
#include "DirectionalLight.h"
#include "Spotlight.h"
#include "Shadow.h"


class MultiLightCube :public ShadowCastingObject
{
private:
	GLuint VBO;
	GLuint containerVAO;

	GLuint texture;
	GLuint specularTexture;

	GLboolean expanding = false;
	GLdouble  expandTurn = true;

	GLboolean wobbling = false;
	GLdouble  positiveZTurn = false;
	GLfloat   angle = 0;

	GLfloat expandSensitivity = 0.7;
	GLuint  timeExpanding = 5;

	glm::vec3 scaleValues = glm::vec3(1, 1, 1);
	GLfloat materialShininess;
	//Shadow* shadow;


public:
	MultiLightCube();
	~MultiLightCube();
	GLuint getTexture();
	//GLvoid giveShadowTexture();
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow);
	GLvoid drawSimpleVAO();
	GLvoid MultiLightCube::moveSideToSide();
	GLvoid MultiLightCube::update();
};
