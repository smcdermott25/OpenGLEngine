//#include "MultiLightWorld.h"
//
//MultiLightWorld::MultiLightWorld()
//{
//	directionalLight = DirectionalLight(glm::vec4(-0.2f, -1.0f, -0.3f, 0.0));
//
//	myVerySpecialCube = MultiLightCube();
//	myVerySpecialCube.setPosition(glm::vec3(0, -20, 0));
//	myVerySpecialCube.setScale(glm::vec3(4, 4, 4));
//
//
//	//std::vector<PointLight> pointLightList;
//	pointLightList.reserve(20);
//	for (GLint  i = 0; i < 10; i++)
//	{
//		PointLight pointLight = PointLight(glm::vec3(1.0, 0.0, 1.0));
//		pointLight.setPosition(glm::vec3(i * 10.0, i * 5.0, 0));
//		pointLightList.push_back(pointLight);
//	}
//	pointLightList.at(0).setPosition(glm::vec3(0, -25, 0));
//	printf("Position is: %f,%f,%f time", pointLightList.at(0).getPosition().x, pointLightList.at(0).getPosition().y, pointLightList.at(0).getPosition().z);
//	pointLightList.at(0).setColourEasy(glm::vec3(0.0, 1.0, 0.0));
//	pointLightList.at(1).setPosition(glm::vec3(0,-20, -5));
//	pointLightList.at(2).setPosition(glm::vec3(1, -20, 5));
//	pointLightList.at(2).setColourEasy(glm::vec3(1.0, 0.0, 0.0));
//	pointLightList.at(3).setPosition(glm::vec3(-1, -20, 5));
//	pointLightList.at(3).setColourEasy(glm::vec3(0.0, 0.0, 3.0));
//
//	
//
//	//std::vector<MultiLightCube> cubeList;
//	cubeList.reserve(20);
//	for (GLint  i = 0; i < 10; i++)
//	{
//		MultiLightCube multiCube = MultiLightCube();
//		cubeList.push_back(multiCube);
//		multiCube.setPosition(glm::vec3(i * 4, 0, i * 4));
//	}
//
//
//	mySpotlight = SpotLight();
//	mySpotlight.setPosition(glm::vec3(myVerySpecialCube.getPosition().x, myVerySpecialCube.getPosition().y - 5, myVerySpecialCube.getPosition().z));
//	mySpotlight.setTarget(glm::vec3(myVerySpecialCube.getPosition().x, myVerySpecialCube.getPosition().y, myVerySpecialCube.getPosition().z));
//	myVerySpecialCube.updatePointLightList(pointLightList);
//
//}
//
//GLvoid MultiLightWorld::update(GLfloat deltaTime)
//{
//	camera.move(deltaTime);
//}
//
//GLvoid MultiLightWorld::draw()
//{
//	myVerySpecialCube.draw(camera.getView(), camera.getProjection(), camera.getCameraPosition(), directionalLight, pointLightList, mySpotlight);
//	for (GLint  i = 0; i < pointLightList.size(); i++)
//	{
//		pointLightList.at(i).draw(camera.getView(), camera.getProjection());
//	}
//}
//
//GLvoid MultiLightWorld::input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode)
//{
//	if (key == GLFW_KEY_ESCAPE)
//	{
//		exit(1);
//	}
//
//	if (key == GLFW_KEY_L && action == GLFW_RELEASE)
//	{
//		lines = !lines;
//		if (lines)
//			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//		else
//			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//	}
//
//	if (action == GLFW_PRESS)
//	{
//		camera.setKey(key, true);
//	}
//
//	if (action == GLFW_RELEASE)
//	{
//		camera.setKey(key, false);
//	}
//
//	if (key == GLFW_KEY_UP && action == GLFW_RELEASE)
//	{
//		camera.addCameraSpeed(100);
//	}
//
//	if (key == GLFW_KEY_DOWN && action == GLFW_RELEASE)
//	{
//		camera.addCameraSpeed(-100);
//	}
//}
//
//GLvoid MultiLightWorld::mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
//{
//	camera.updateFront(xPosition, yPosition);
//}
//
//GLvoid MultiLightWorld::scrollInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
//{
//}
//
//GLvoid MultiLightWorld::mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods)
//{
//	if (action == GLFW_PRESS)
//	{
//		camera.setKey(button, true);
//	}
//
//	if (action == GLFW_RELEASE)
//	{
//		camera.setKey(button, false);
//	}
//}
