/*This world is now deprecated - was used to demo multiple lights for first demo*/
//#pragma once
//#include "GameWorld.h"
//#include "Spotlight.h"
//#include "DirectionalLight.h"
//#include "PointLight.h"
//#include "MultiLightCube.h"
//class MultiLightWorld : public GameWorld
//{
//private:
//	Camera camera;
//	DirectionalLight directionalLight;
//	std::vector<PointLight> pointLightList;
//	std::vector<MultiLightCube> cubeList;
//	MultiLightCube myVerySpecialCube;
//	SpotLight mySpotlight;
//	GLboolean lines = false;
//
//
//public:
//	MultiLightWorld();
//	GLvoid update(GLfloat deltaTime);
//	GLvoid draw();
//	GLvoid input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode);
//	GLvoid mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods);
//	GLvoid mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition);
//	GLvoid scrollInput(GLFWwindow* window, GLdouble  xScroll, GLdouble  yScroll);
//};