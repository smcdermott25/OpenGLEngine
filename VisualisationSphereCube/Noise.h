#pragma once
#include <glew.h>

class Noise
{
private:

protected:
	GLint seed;

public:
	virtual ~Noise() {};
	virtual GLfloat getNoise(GLint x, GLint z) = 0;
};