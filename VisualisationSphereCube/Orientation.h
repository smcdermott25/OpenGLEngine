#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glew.h>
class Orientation {
private:
	GLint  moveTurn = 1;		 // a turn GLint 
	GLfloat difference;		 // difference for gradual movement
	GLfloat deltaPosition;	 // update used to update position
	glm::vec3 startingPosition;//used for velocity
	glm::vec3 mPosition;     // Object position
	glm::vec3 mScale;        // Scalar value
	glm::vec3 mForward;      // Local x
	glm::vec3 mRight;        // Local z
	glm::vec3 mUp;           // Local y
	glm::vec3 mRotationAxis; // Axis about which rotations occur
	GLfloat mRotationAngle;
	//Transformation matrices
	glm::mat4 mPositionMatrix;
	glm::mat4 mRotationMatrix;
	glm::mat4 mScaleMatrix;
	glm::mat4 mTempMatrix;
	glm::mat4 mModelMatrix;
	GLvoid setPositionMatrix(glm::vec3 position);
	GLvoid setScaleMatrix(glm::vec3 scale);
	GLvoid updateLocalAxis();
	
	glm::vec3 rotateVector(glm::vec3 vectorToRotate, glm::vec3 axis, GLfloat theta);


public:
	Orientation();
	~Orientation();
	GLvoid setModelMatrix(glm::mat4 matrix);
	GLvoid addPosition(glm::vec3 vector);
	GLvoid setPosition(glm::vec3 vector);
	GLvoid setPositionX(GLfloat x);
	GLvoid setPositionY(GLfloat y);
	GLvoid setPositionZ(GLfloat z);
	GLvoid setScale(glm::vec3 vector);
	GLvoid setScaleX(GLfloat x);
	GLvoid setScaleY(GLfloat y);
	GLvoid setScaleZ(GLfloat z);
	GLvoid setRotationAxis(glm::vec3 vector);
	GLvoid addRotation(GLfloat angleIncrementDegrees);
	GLvoid setRotationAngle(GLfloat angleDegrees);
	glm::vec3 getPosition();
	GLfloat getRotationAngle();
	glm::vec3 getUpVector();
	glm::vec3 getRightVector();
	glm::vec3 getScale();
	glm::vec3 getForwardVector();
	glm::mat4 getModelMatrix();
	GLvoid printMat4(glm::mat4 mat4Object);
	GLvoid printVec3(glm::vec3 vec3Object);
	glm::mat4 updateModelMatrix();

};