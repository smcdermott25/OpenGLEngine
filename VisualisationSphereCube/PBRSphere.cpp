#include "PBRSphere.h"

PBRSphere::PBRSphere(GLuint brdfLUTTexture)
{
	generateBuffers();
	shader = ShaderLoader::pbrShader;
	material = Materials::aluminium;
	brdfLUT = brdfLUTTexture;
	// set material texture uniforms
	shader.useProgram();

	glUniform1i(glGetUniformLocation(shader.getProgram(), "albedoMap"), 0);
	glUniform1i(glGetUniformLocation(shader.getProgram(), "normalMap"), 1);
	glUniform1i(glGetUniformLocation(shader.getProgram(), "metallicMap"), 2);
	glUniform1i(glGetUniformLocation(shader.getProgram(), "roughnessMap"), 3);
	glUniform1i(glGetUniformLocation(shader.getProgram(), "aoMap"), 4);
	glUniform1i(glGetUniformLocation(shader.getProgram(), "irradianceMap"), 5);
	glUniform1i(glGetUniformLocation(shader.getProgram(), "prefilterMap"), 6);
	glUniform1i(glGetUniformLocation(shader.getProgram(), "brdfLUT"), 7);
}

GLvoid PBRSphere::generateBuffers()
{
	glGenVertexArrays(1, &sphereVAO);

	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ebo);

	calculateSphere();

	glBindVertexArray(sphereVAO);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), &data[0], GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
	float stride = (3 + 2 + 3) * sizeof(float);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (GLvoid*)(3 * sizeof(float)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, stride, (GLvoid*)(5 * sizeof(float)));
}


GLvoid PBRSphere::calculateSphere()
{
	for (unsigned int y = 0; y <= Y_SEGMENTS; ++y)
	{
		for (unsigned int x = 0; x <= X_SEGMENTS; ++x)
		{
			float xSegment = (float)x / (float)X_SEGMENTS;
			float ySegment = (float)y / (float)Y_SEGMENTS;
			float xPos = std::cos(xSegment * 2.0f * PI) * std::sin(ySegment * PI);
			float yPos = std::cos(ySegment * PI);
			float zPos = std::sin(xSegment * 2.0f * PI) * std::sin(ySegment * PI);

			positions.push_back(glm::vec3(xPos, yPos, zPos));
			uv.push_back(glm::vec2(xSegment, ySegment));
			normals.push_back(glm::vec3(xPos, yPos, zPos));
		}
	}

	bool oddRow = false;
	for (int y = 0; y < Y_SEGMENTS; ++y)
	{
		if (!oddRow) // even rows: y == 0, y == 2; and so on
		{
			for (int x = 0; x <= X_SEGMENTS; ++x)
			{
				indices.push_back(y       * (X_SEGMENTS + 1) + x);
				indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
			}
		}
		else
		{
			for (int x = X_SEGMENTS; x >= 0; --x)
			{
				indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
				indices.push_back(y       * (X_SEGMENTS + 1) + x);
			}
		}
		oddRow = !oddRow;
	}
	indexCount = indices.size();


	for (int i = 0; i < positions.size(); ++i)
	{
		data.push_back(positions[i].x);
		data.push_back(positions[i].y);
		data.push_back(positions[i].z);
		if (uv.size() > 0)
		{
			data.push_back(uv[i].x);
			data.push_back(uv[i].y);
		}
		if (normals.size() > 0)
		{
			data.push_back(normals[i].x);
			data.push_back(normals[i].y);
			data.push_back(normals[i].z);
		}
	}
}


GLvoid PBRSphere::update(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, GLuint mat)
{
	shader.useProgram();

	glUniformMatrix4fv(glGetUniformLocation(shader.getProgram(), "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(shader.getProgram(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	// setup relevant shader uniforms
	glUniform3f(glGetUniformLocation(shader.getProgram(), "camPos"), cameraPos[0], cameraPos[1], cameraPos[2]);

	switch (mat)
	{
	case 1:material = Materials::aluminium; break;
	case 0:material = Materials::rusted_iron; break;
	}


	glUniformMatrix4fv(glGetUniformLocation(shader.getProgram(), "model"), 1, GL_FALSE, glm::value_ptr(orientation.getModelMatrix()));
	glUniform1f(glGetUniformLocation(shader.getProgram(), "brightness"), 1.0f);
}

GLvoid PBRSphere::renderSphere(Environment env)
{
	shader.useProgram();
	// set material
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, material.albedo);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, material.normal);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, material.metallic);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, material.roughness);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, material.ao);
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_CUBE_MAP, env.ref);
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_CUBE_MAP, env.prefilter);
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, brdfLUT);
	glBindVertexArray(sphereVAO);
	glDrawElements(GL_TRIANGLE_STRIP, indexCount, GL_UNSIGNED_INT, 0);
}
