#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Materials.h"
#include "PBR.h"
#include "Orientation.h"

class PBRSphere
{

private:
	GLuint sphereVAO,
		indexCount,
		vbo,
		ebo;

	std::vector<glm::vec3> positions;
	std::vector<glm::vec2> uv;
	std::vector<glm::vec3> normals;
	std::vector<unsigned int> indices;

	std::vector<float> data;
	GLuint brdfLUT;

	const unsigned int X_SEGMENTS = 64;
	const unsigned int Y_SEGMENTS = 64;
	const float PI = 3.14159265359;

	GLvoid generateBuffers();
	GLvoid calculateSphere();

public:
	Shader shader;
	Orientation orientation;
	Materials::Material material;

	PBRSphere(GLuint brdfLUTTexture);
	PBRSphere();
	GLvoid update(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, GLuint mat);

	GLvoid renderSphere(Environment env);
};