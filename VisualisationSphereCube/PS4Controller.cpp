#include "PS4Controller.h"
#include <iostream>

PS4Controller::PS4Controller(GLint id)
	:id(id)
{
	Controller::count++;
}

PS4Controller::~PS4Controller()
{

}

GLvoid PS4Controller::updateAxis()
{
	GLint count = 0;
	if (glfwGetJoystickAxes(id, &count) != NULL)
	{
		const GLfloat* axis = glfwGetJoystickAxes(id, &count);

		leftBumperTwo = axis[3];
		rightBumperTwo = axis[4];

		leftStickHorizontalValue = axis[0];
		leftStickVerticleValue = -(axis[1]);
		rightStickHorizontalValue = -(axis[2]);
		rightStickVerticleValue = -(axis[5]);
	}
}

GLvoid PS4Controller::updateButtons()
{
	GLint count = 0;
	if (glfwGetJoystickButtons(id, &count) != NULL)
	{
		const unsigned char* buttons = glfwGetJoystickButtons(id, &count);	
		aButton = buttons[1];
		xButton = buttons[0];
		yButton = buttons[3];
		bButton = buttons[2];
		leftBumperOne = buttons[4];
		rightBumperOne = buttons[5];
	}
}
