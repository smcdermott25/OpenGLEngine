#pragma once
#include "Controller.h"

class PS4Controller : public Controller
{
private:
	GLint id;
public:
	PS4Controller(GLint id);
	~PS4Controller();

	inline GLvoid updateAxis();
	inline virtual GLvoid updateButtons();
};