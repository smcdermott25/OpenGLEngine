#include "Player.h"

Player::Player(GLchar* modelPath, Terrain* terrain)
	: model{ new Model(modelPath, true, glm::vec3(100, 100, 100), glm::vec3(100.0f, 0.0f, 100.0f))},
	currentSpeed(0),
	currentTurnSpeed(0),
	upSpeed(0)
{
	this->terrain = terrain;
	onTopOfTerrain = true;
	model->orientation->setRotationAxis(glm::vec3(0, 1, 0)); // should only be done once
}

Player::Player(GLchar* modelPath, Terrain* terrain, GLint controllerId)
	: model{ new Model(modelPath, true, glm::vec3(0.55f, 0.55f, 0.55f), glm::vec3(100.0f, 0.0f, 100.0f))},
	currentSpeed(0),
	currentTurnSpeed(0),
	upSpeed(0),
	controllerId(controllerId)
{
	this->terrain = terrain;
	onTopOfTerrain = true;
	model->orientation->setRotationAxis(glm::vec3(0, 1, 0)); // should only be done once
}

Player::Player(GLchar* modelPath)
	: model{ new Model(modelPath, true, glm::vec3(3.0f, 3.0f, 3.0f), glm::vec3(0.0f, 0.0f, 0.0f))},
	currentSpeed(0),
	currentTurnSpeed(0),
	upSpeed(0)
{
	onTopOfTerrain = false;
	this->terrain = terrain;
	model->orientation->setScale(glm::vec3(0.1f, 0.1f, 0.1f));
	model->orientation->setRotationAxis(glm::vec3(0, 1, 0)); // should only be done once
}

Player::~Player()
{

}

GLint Player::getControllerId()
{
	return controllerId;
}

GLboolean Player::isMoving()
{
	return moving;
}

GLvoid Player::jump()
{
	upSpeed = JUMP;
}


GLvoid Player::increasePosition(GLfloat x, GLfloat y, GLfloat z)
{
	model->orientation->addPosition(glm::vec3(x, y, z));
}

GLvoid Player::increaseRotation(GLfloat x, GLfloat y, GLfloat z)
{
	rotationX += x;
	rotationY += y;
	rotationZ += z;

	model->orientation->setRotationAngle(y);
}

GLvoid Player::move()
{

}

GLvoid Player::checkInputs()
{
	if (Controls::getInstance()->keys[GLFW_KEY_W])
	{
		currentSpeed = RUNSPEED;
	}
	else if (Controls::getInstance()->keys[GLFW_KEY_S])
	{
		currentSpeed = -RUNSPEED;
	}
	else
	{
		currentSpeed = 0;
	}

	if (Controls::getInstance()->keys[GLFW_KEY_D])
	{
		currentTurnSpeed = TURNSPEED;
	}
	else if (Controls::getInstance()->keys[GLFW_KEY_A])
	{
		currentTurnSpeed = -TURNSPEED;
	}
	else
	{
		currentTurnSpeed = 0;
	}

	if (Controls::getInstance()->keys[GLFW_KEY_SPACE] || Controls::getInstance()->getController(0)->getAButton())
	{
		jump();
	}

	if (controllerId != 10)
	{
		Controls::getInstance()->updateAxis(controllerId);
		Controls::getInstance()->updateButtons(controllerId);
	}
}

GLvoid Player::updateTerrain(Terrain* terrain)
{
	this->terrain = terrain;
}

GLvoid Player::update(GLfloat deltaTime)
{
	// Get input
	checkInputs();

	if (currentSpeed != 0 || currentTurnSpeed != 0)
	{
		moving = true;
	}
	else
	{
		moving = false;
	}

	// Change current rotation
	increaseRotation(0, currentTurnSpeed * deltaTime, 0);

	Controller* controller = Controls::getInstance()->getController(controllerId);
	GLfloat distance = 0;
	if (controller != nullptr)
	{
		increaseRotation(0, Controls::getInstance()->getController(controllerId)->getLeftHorizontal() * -3, 0);
		distance = Controls::getInstance()->getController(controllerId)->getLeftVertical() * 3;
	}

	// Change distance 
    distance += deltaTime * currentSpeed;
	GLfloat x = distance * sin(glm::radians(rotationY));
	GLfloat z = distance * cos(glm::radians(rotationY));

	increasePosition(x, 0, z);

	// Y position
	upSpeed += GRAVITY * deltaTime;
	increasePosition(x, upSpeed * deltaTime, 0);

	if (onTopOfTerrain)
	{
		glm::vec3 position = model->orientation->getPosition();
		if (position.y < terrain->getTerrainHeight(position.x, position.z))
		{
			upSpeed = 0;
			model->orientation->setPositionY(terrain->getTerrainHeight(position.x, position.z));
		}
	}
	else
	{
		glm::vec3 position = model->orientation->getPosition();
		if (position.y < 0)
		{
			upSpeed = 0;
			model->orientation->setPositionY(0);
		}
	}

}

GLvoid Player::draw(glm::mat4 view, glm::mat4 projection)
{
	model->drawing(projection, view, glfwGetTime());
}

GLvoid Player::drawClient(glm::mat4 view, glm::mat4 projection, glm::mat4 model)
{
	this->model->drawAnimatedTest(projection, view, model, glfwGetTime());
}
