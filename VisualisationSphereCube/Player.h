#pragma once
#include <glew.h>
#include <math.h>

#include "info.h"
#include "GameObject.h"
#include "Model.h"
#include "Controls.h"
#include "Terrain.h"

class Player
{
private:
	const GLfloat RUNSPEED = 550;
	const GLfloat TURNSPEED = 300;
	const GLfloat GRAVITY = -500;
	const GLfloat JUMP = 200;

	Terrain* terrain;
	GLboolean onTopOfTerrain;

	GLfloat terrainHeight = 0;

	GLfloat currentSpeed;
	GLfloat currentTurnSpeed;
	GLfloat upSpeed;

	GLfloat rotationX;
	GLfloat rotationY;
	GLfloat rotationZ;
	GLfloat scale;

	GLvoid jump();
	GLvoid checkInputs();

	GLint controllerId = 10;
	GLboolean moving = false;
	
public:
	Player(GLchar* modelPath, Terrain* terrain);
	Player(GLchar* modelPath, Terrain* terrain, GLint controllerId);
	Player(GLchar* modelPath);
	~Player();

	Model* model;

	GLint getControllerId();
	GLboolean isMoving();
	GLvoid increasePosition(GLfloat x, GLfloat y, GLfloat z);
	GLvoid increaseRotation(GLfloat x, GLfloat y, GLfloat z);

	GLvoid updateTerrain(Terrain* terrain);

	GLvoid move();
	GLvoid update(GLfloat deltaTime);
	GLvoid draw(glm::mat4 view, glm::mat4 projection);
	GLvoid drawClient(glm::mat4 view, glm::mat4 projection, glm::mat4 model);
};