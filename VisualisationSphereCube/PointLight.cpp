#include "PointLight.h"
#include "IsoSurfaceGrid.h"

PointLight::PointLight(glm::vec3 colour)
	:BaseLight(colour), gameObject(GameObject("Shader/lightVertex.txt", "Shader/lightFragment.txt"))
{
	constant = 1.0f;
	linear = 0.09;
	quadratic = 0.00001;
	gameObject.shaderProgram->useProgram();

	GLfloat radius = 1.0;
	GLfloat vertices[] =
	{
		// back face
		-radius, -radius, -radius,
		-radius,  radius, -radius,
		radius,  radius, -radius,

		radius,  radius, -radius,
		radius, -radius, -radius,
		-radius, -radius, -radius,

		// front face
		-radius, -radius,  radius,
		-radius,  radius,  radius,
		radius, -radius,  radius,

		-radius,  radius,  radius,
		radius,  radius,  radius,
		radius, -radius,  radius,

		// left face
		-radius, -radius,  radius,
		-radius,  radius,  radius,
		-radius,  radius, -radius,

		-radius,  radius, -radius,
		-radius, -radius, -radius,
		-radius, -radius,  radius,

		// right face
		radius, -radius,  radius,
		radius,  radius,  radius,
		radius,  radius, -radius,

		radius,  radius, -radius,
		radius, -radius, -radius,
		radius, -radius,  radius,

		// bottom face
		-radius, -radius,  radius,
		-radius, -radius, -radius,
		radius, -radius, -radius,

		radius, -radius, -radius,
		radius, -radius,  radius,
		-radius, -radius,  radius,

		// top face
		-radius,  radius,  radius,
		-radius,  radius, -radius,
		radius,  radius, -radius,

		radius,  radius, -radius,
		radius,  radius,  radius,
		-radius,  radius,  radius,

	};
	// Generate Vertex Array object and vertex buffer
	helper.createVertexArray(vertexArray);
	helper.fillVertexArrayWithVertices(vertexArray, vertexBuffer, vertices, sizeof(vertices), 0);
	lightPosition = gameObject.orientation->getPosition();
}

PointLight::~PointLight()
{
}
/*Draw our point light - these allow for post processing brightness output*/
GLvoid PointLight::draw(glm::mat4 view, glm::mat4 projection)
{
	gameObject.shaderProgram->useProgram();
	GLuint program = gameObject.shaderProgram->getProgram();
	glUniform3f(glGetUniformLocation(program, "lightColour"), lightColour.x, lightColour.y, lightColour.z);
	glUniformMatrix4fv(glGetUniformLocation(program, "model"), 1, GL_FALSE, glm::value_ptr(gameObject.orientation->getModelMatrix()));
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glBindVertexArray(vertexArray);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	gameObject.draw();

}

GLfloat PointLight::getConstant()
{
	return constant;
}
GLfloat PointLight::getLinear()
{
	return linear;
}
GLfloat PointLight::getQuadratic()
{
	return quadratic;
}
GameObject& PointLight::getGameObject()
{
	return gameObject;
}

GLvoid PointLight::setPosition(glm::vec3 newPosition)
{
	gameObject.setPosition(newPosition);

	lightPosition = gameObject.orientation->getPosition();
}

glm::vec3 PointLight::getPosition()
{
	return lightPosition;
}
