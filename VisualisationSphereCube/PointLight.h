#pragma once
#include <glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Shader.h"
#include "BaseLight.h"
#include "GameWorld.h"
#include "GameObject.h"
/*Point light casts light in a sphere around it. Set linear and quadratic values to change how the light attenuates*/
class PointLight : public BaseLight
{
protected:
	GLuint vertexArray;
	GLuint vertexBuffer;
	GLuint textureBuffer;
	GLuint numOfVertices;
	GLsizei sizeOfVertices;

	GLuint texture;
	GLfloat constant;
	GLfloat linear;
	GLfloat quadratic;
	GameObject gameObject;
	glm::vec3 lightPosition;
public:

	PointLight(glm::vec3 colour);
	~PointLight();
	GLvoid draw(glm::mat4 view, glm::mat4 projection);
	GLfloat getConstant();
	GLfloat getLinear();
	GLfloat getQuadratic();
	GameObject& getGameObject();
	GLvoid setPosition(glm::vec3 newPosition);
	glm::vec3 getPosition();
	
};