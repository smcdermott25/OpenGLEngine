#include "PointLightShadow.h"


void PointLightShadow::createShadowCubeTextureAndCubeFBO()
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	// Configure depth map FBO
	glGenFramebuffers(1, &depthMapFBO);
	// Create depth cubemap texture
	glGenTextures(1, &depthCubeMap);
	glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubeMap);
	for (GLuint i = 0; i < 6; ++i)
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	// Attach cubemap as depth map FBO's color buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCubeMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer not complete!" << std::endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void PointLightShadow::renderDepthSceneToTexture(PointLight directionalLight, std::vector<MultiLightCube> &objectsThatCastShadow)
{
	GLfloat aspect = (GLfloat)SHADOW_WIDTH / (GLfloat)SHADOW_HEIGHT;
	glm::mat4 shadowProj = glm::perspective(90.0f, aspect, near_plane, far_plane);
	shadowTransforms.clear();
	lightPosition = directionalLight.getPosition();
	shadowTransforms.push_back(shadowProj * glm::lookAt(lightPosition, lightPosition + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms.push_back(shadowProj * glm::lookAt(lightPosition, lightPosition + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms.push_back(shadowProj * glm::lookAt(lightPosition, lightPosition + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)));
	shadowTransforms.push_back(shadowProj * glm::lookAt(lightPosition, lightPosition + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)));
	shadowTransforms.push_back(shadowProj * glm::lookAt(lightPosition, lightPosition + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms.push_back(shadowProj * glm::lookAt(lightPosition, lightPosition + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0)));

	// 1. Render scene to depth cubemap
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	simpleDepthShader.useProgram();
	for (GLuint i = 0; i < 6; ++i)
		glUniformMatrix4fv(glGetUniformLocation(simpleDepthShader.getProgram(), ("shadowMatrices[" + std::to_string(i) + "]").c_str()), 1, GL_FALSE, glm::value_ptr(shadowTransforms[i]));
	glUniform1f(glGetUniformLocation(simpleDepthShader.getProgram(), "far_plane"), far_plane);
	glUniform3fv(glGetUniformLocation(simpleDepthShader.getProgram(), "lightPos"), 1, &lightPosition[0]);

	for (int i = 0; i < objectsThatCastShadow.size(); i++)
	{
		glm::mat4 myLocalMatrix = objectsThatCastShadow.at(i).orientation->getModelMatrix();
		glUniformMatrix4fv(glGetUniformLocation(simpleDepthShader.getProgram(), "model"), 1, GL_FALSE, glm::value_ptr(myLocalMatrix));
		objectsThatCastShadow.at(i).drawShadow();
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, WIDTH, HEIGHT);
}


GLuint PointLightShadow::getDepthCubeMapTexture()
{
	return depthCubeMap;
}
GLuint PointLightShadow::getDepthMapFBO()
{
	return depthMapFBO;
}

glm::mat4 PointLightShadow::getLightSpaceMatrix()
{
	return lightSpaceMatrix;
}

glm::vec3 PointLightShadow::getLightPositionForShadow()
{
	return lightPosition;

}
void PointLightShadow::setPositionOfLightForShadow(glm::vec3 newLightPosition)
{
	lightPosition = newLightPosition;
}

glm::vec3 PointLightShadow::getLightTargetForShadow()
{
	return lightTarget;

}
void PointLightShadow::setPositionOfLightTargetForShadow(glm::vec3 newLightTarget)
{
	lightPosition = newLightTarget;
}

void PointLightShadow::setNearPlane(GLfloat newNearPlane)
{
	near_plane = newNearPlane;
}
void PointLightShadow::setFarPlane(GLfloat newFarPlane)
{
	far_plane = newFarPlane;
}
void PointLightShadow::setOrthoDimension(GLfloat newOrthoDimension)
{
	orthoDimension = newOrthoDimension;
}
