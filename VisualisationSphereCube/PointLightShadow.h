#pragma once
#include <glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "PointLight.h"
#include "MultiLightCube.h"

class PointLightShadow {
private:
	GLuint depthCubeMap;
	GLuint depthMapFBO;
	static const GLuint SHADOW_WIDTH = 2048;
	static const GLuint SHADOW_HEIGHT = 2048;
	std::vector<glm::mat4> shadowTransforms;
	glm::mat4 lightSpaceMatrix;
	glm::vec3 lightPosition = glm::vec3(2.0f, 40.0f, 2.0f);
	glm::vec3 lightTarget = glm::vec3(0, 0, 0);
	GLfloat near_plane = 1.0f, far_plane = 25.0f;
	GLfloat orthoDimension = 10.0f;


public:
	void createShadowCubeTextureAndCubeFBO();
	void renderDepthSceneToTexture(PointLight directionalLight, std::vector<MultiLightCube> &objectsThatCastShadow);
	GLuint getDepthCubeMapTexture();
	GLuint getDepthMapFBO();
	glm::mat4 getLightSpaceMatrix();
	glm::vec3 getLightPositionForShadow();
	glm::vec3 getLightTargetForShadow();
	Shader simpleDepthShader = Shader("PointLightShadow", "Shader/pointShadowsDepth.vs", "Shader/pointShadowsDepth.gs", "Shader/pointShadowsDepth.frag");

	void setPositionOfLightTargetForShadow(glm::vec3 newLightTarget);
	void setPositionOfLightForShadow(glm::vec3 newLightPosition);
	void setNearPlane(GLfloat newNearPlane);
	void setFarPlane(GLfloat newFarPlane);
	void setOrthoDimension(GLfloat newOrthoDimension);
};