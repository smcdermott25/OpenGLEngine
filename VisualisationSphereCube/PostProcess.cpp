#include "PostProcess.h"
#include "info.h"

PostProcess::PostProcess()
{
	msFB = FrameBuffer(WIDTH, HEIGHT, GL_TRUE, 4);
	hdrFB = FrameBuffer(WIDTH, HEIGHT, GL_TRUE);
	blurShader = ShaderLoader::blurShader;
	blurFB[0] = FrameBuffer(WIDTH, HEIGHT, GL_TRUE);
	blurFB[0].setShader(blurShader);
	blurFB[1] = FrameBuffer(WIDTH, HEIGHT, GL_TRUE);
	blurFB[1].setShader(blurShader);
	bloomShader = ShaderLoader::bloomShader;
	amount = 10;
}

GLvoid PostProcess::useFrameBuffer(GLboolean aa)
{
	anti_aliasing = aa;

	if (aa)
	{
		msFB.useBuffer();
		msFB.clearBuffer();
	}
	else
	{
		hdrFB.useBuffer();
		hdrFB.clearBuffer();
	}
}
GLvoid PostProcess::drawPostProcess()
{

	if (anti_aliasing) //converts Multisampled FrameBuffer to Single sample
	{
		glBindFramebuffer(GL_READ_FRAMEBUFFER, msFB.getFBO());
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, hdrFB.getFBO());

		glReadBuffer(GL_COLOR_ATTACHMENT0);
		glDrawBuffer(GL_COLOR_ATTACHMENT0);

		glBlitFramebuffer(0, 0, WIDTH, HEIGHT, 0, 0, WIDTH, HEIGHT, GL_COLOR_BUFFER_BIT, GL_NEAREST);
		glReadBuffer(GL_COLOR_ATTACHMENT1);
		glDrawBuffer(GL_COLOR_ATTACHMENT1);

		glBlitFramebuffer(0, 0, WIDTH, HEIGHT, 0, 0, WIDTH, HEIGHT, GL_COLOR_BUFFER_BIT, GL_NEAREST);
		GLuint attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
		glDrawBuffers(2, attachments);
	}

	//Blur bright fragments w/ two-pass Gaussian Blur 
	GLboolean horizontal = GL_TRUE, first_iteration = GL_TRUE;
	blurShader.useProgram();
	for (GLuint i = 0; i < amount; i++)
	{
		blurFB[horizontal].useBuffer();
		glUniform1i(glGetUniformLocation(blurShader.getProgram(), "horizontal"), horizontal);
		glBindTexture(GL_TEXTURE_2D, first_iteration ? hdrFB.textureColorbuffer2 : blurFB[!horizontal].textureColorbuffer);  // bind texture of other framebuffer (or scene if first iteration)
		blurFB[horizontal].drawToBuffer();
		horizontal = !horizontal;
		if (first_iteration)
			first_iteration = GL_FALSE;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	bloomShader.useProgram();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, hdrFB.textureColorbuffer);

	glUniform1i(glGetUniformLocation(bloomShader.getProgram(), "scene"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, blurFB[!horizontal].textureColorbuffer);
	glUniform1i(glGetUniformLocation(bloomShader.getProgram(), "bloomBlur"), 1);
	glUniform1i(glGetUniformLocation(bloomShader.getProgram(), "bloom"), bloom);
	glUniform1f(glGetUniformLocation(bloomShader.getProgram(), "exposure"), exposure);
	hdrFB.drawToBuffer();
}

GLvoid PostProcess::enableBloom(GLboolean boolean)
{
	bloom = boolean;
}

GLvoid PostProcess::setSamples(GLuint samples)
{
	msFB = FrameBuffer(WIDTH, HEIGHT, GL_TRUE, samples);
}