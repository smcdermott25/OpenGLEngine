#pragma once
#include "FrameBuffer.h"

class PostProcess //Perfoms Post processing functions
{
private:
	FrameBuffer msFB; //Multisampled FrameBuffer
	FrameBuffer hdrFB; // HDR FrameBuffer for final output
	FrameBuffer blurFB[2]; // Pair of FrameBuffers for blurring in the horizontal and vertical

	//Shaders
	Shader blurShader;
	Shader bloomShader;

	//Options
	GLboolean bloom = true;
	GLfloat exposure = 1.0f;
	GLboolean anti_aliasing = true;

public:

	PostProcess();
	
	//amount of blurring i.e. amount of bloom
	GLuint amount;

	GLvoid useFrameBuffer(GLboolean anti_aliasing);
	GLvoid drawPostProcess();
	GLvoid enableBloom(GLboolean bloom);
	GLvoid setSamples(GLuint samples);
};