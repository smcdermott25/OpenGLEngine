//#include "PostProcessWorld.h"
//
//PostProcessWorld :: PostProcessWorld()
//{
//	hdrFB = FrameBuffer(WIDTH, HEIGHT, true);
//	normFB = FrameBuffer(WIDTH, HEIGHT, false);
//	fb = hdrFB;
//	blurShader= Shader("screenvertex.vs", "blurfrag.fs");
//	blurFB[0] = FrameBuffer(WIDTH, HEIGHT, true);
//	blurFB[0].setShader(blurShader);
//	blurFB[1] = FrameBuffer(WIDTH, HEIGHT, true);
//	blurFB[1].setShader(blurShader);
//	bloomShader =Shader("screenvertex.vs", "bloomfrag.fs");
//	
//	postprocess = PostProcess();
//
//	directionalLight = DirectionalLight(glm::vec4(-0.2f, -1.0f, -0.3f, 0.0));
//
//	myVerySpecialCube = MultiLightCube();
//	myVerySpecialCube.setPosition(glm::vec3(0, -20, 0));
//	myVerySpecialCube.setScale(glm::vec3(4, 4, 4));
//
//	roomCube = MultiLightCube();
//	roomCube.setPosition(glm::vec3(0, -20, 0));
//	roomCube.setScale(glm::vec3(70, 70, 70));
//
//	//std::vector<PointLight> pointLightList;
//	pointLightList.reserve(20);
//	for (GLint  i = 0; i < 10; i++)
//	{
//		PointLight pointLight = PointLight(glm::vec3(1.0, 0.0, 1.0));
//		pointLight.setPosition(glm::vec3(i * 10.0, i * 5.0, 0));
//		pointLightList.push_back(pointLight);
//	}
//	pointLightList.at(0).setPosition(glm::vec3(0, -25, 0));
//	printf("Position is: %f,%f,%f time", pointLightList.at(0).getPosition().x, pointLightList.at(0).getPosition().y, pointLightList.at(0).getPosition().z);
//	pointLightList.at(0).setColourEasy(glm::vec3(0.0, 1.0, 0.0));
//	pointLightList.at(1).setPosition(glm::vec3(0, -20, -5));
//	pointLightList.at(2).setPosition(glm::vec3(1, -20, 5));
//	pointLightList.at(2).setColourEasy(glm::vec3(20.0, 0.0, 0.0));
//	pointLightList.at(3).setPosition(glm::vec3(-1, -20, 5));
//	pointLightList.at(3).setColourEasy(glm::vec3(0.0, 0.0, 3.0));
//
//
//
//	//std::vector<MultiLightCube> cubeList;
//	cubeList.reserve(20);
//	for (GLint  i = 0; i < 10; i++)
//	{
//		MultiLightCube multiCube = MultiLightCube();
//		cubeList.push_back(multiCube);
//		multiCube.setPosition(glm::vec3(i * 4, 0, i * 4));
//	}
//
//
//	mySpotlight = SpotLight();
//	mySpotlight.setPosition(glm::vec3(myVerySpecialCube.getPosition().x, myVerySpecialCube.getPosition().y - 5, myVerySpecialCube.getPosition().z));
//	mySpotlight.setTarget(glm::vec3(myVerySpecialCube.getPosition().x, myVerySpecialCube.getPosition().y, myVerySpecialCube.getPosition().z));
//	myVerySpecialCube.updatePointLightList(pointLightList);
//	roomCube.updatePointLightList(pointLightList);
//}
//
//GLvoid PostProcessWorld::update(GLfloat deltaTime)
//{
//	camera.move(deltaTime);
//}
//
//GLvoid PostProcessWorld::draw()
//{
//	//fb.useBuffer();
//	//fb.clearBuffer();
//
//	postprocess.useFrameBuffer();
//
//	myVerySpecialCube.draw(camera.getView(), camera.getProjection(), camera.getCameraPosition(), directionalLight, pointLightList, mySpotlight);
//	//roomCube.draw(camera.getView(), camera.getProjection(), camera.getCameraPosition(), directionalLight, pointLightList, mySpotlight);
//	for (GLint  i = 0; i < pointLightList.size(); i++)
//	{
//		pointLightList.at(i).draw(camera.getView(), camera.getProjection());
//	}
//
//	//fb.textureColorbuffer = fb.textureColorbuffer2;
//	//fb.drawBuffer();
//	
//	////2. Blur bright fragments w/ two-pass Gaussian Blur 
//	//GLboolean horizontal = true, first_iteration = true;
//	//GLuint amount = 10;
//	//
//	//blurShader.useProgram();
//	//for (GLuint i = 0; i < amount; i++)
//	//{
//	//	
//	//	blurFB[horizontal].useBuffer();
//	//	glUniform1i(glGetUniformLocation(blurShader.getProgram(), "horizontal"), horizontal);
//	//	glBindTexture(GL_TEXTURE_2D, first_iteration ? fb.textureColorbuffer2 : blurFB[!horizontal].textureColorbuffer);  // bind texture of other framebuffer (or scene if first iteration)
//	//	blurFB[horizontal].drawToBuffer();
//	//	horizontal = !horizontal;
//	//	if (first_iteration)
//	//		first_iteration = false;
//	//}
//
//	////fb.textureColorbuffer = blurFB[horizontal].textureColorbuffer;
//	////fb.drawBuffer();
//
//	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
//	//bloomShader.useProgram();
//	//glActiveTexture(GL_TEXTURE0);
//	//glBindTexture(GL_TEXTURE_2D,fb.textureColorbuffer);
//	//glUniform1i(glGetUniformLocation(bloomShader.getProgram(), "scene"), 0);
//	////fb.drawToBuffer();
//	//glActiveTexture(GL_TEXTURE1);
//	//glBindTexture(GL_TEXTURE_2D,blurFB[!horizontal].textureColorbuffer);
//	//glUniform1i(glGetUniformLocation(bloomShader.getProgram(), "bloomBlur"), 1);
//	//glUniform1i(glGetUniformLocation(bloomShader.getProgram(), "bloom"), bloom);
//	//glUniform1f(glGetUniformLocation(bloomShader.getProgram(), "exposure"), exposure);
//	//fb.drawToBuffer();
//
//	postprocess.drawPostProcess();
//
//}
//
//GLvoid PostProcessWorld::input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode)
//{
//	if (key == GLFW_KEY_ESCAPE)
//	{
//		exit(1);
//	}
//
//	if (key == GLFW_KEY_L && action == GLFW_RELEASE)
//	{
//		lines = !lines;
//		if (lines)
//			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//		else
//			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//	}
//
//	if (key == GLFW_KEY_F && action == GLFW_RELEASE)
//	{
//		hdr = !hdr;
//		if (hdr)
//			fb = hdrFB;
//		else
//			fb = normFB;
//	}
//
//	if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE)
//	{
//		bloom = !bloom;
//	}
//
//	if (action == GLFW_PRESS)
//	{
//		camera.setKey(key, true);
//	}
//
//	if (action == GLFW_RELEASE)
//	{
//		camera.setKey(key, false);
//	}
//
//	if (key == GLFW_KEY_UP && action == GLFW_RELEASE)
//	{
//		camera.addCameraSpeed(100);
//	}
//
//	if (key == GLFW_KEY_DOWN && action == GLFW_RELEASE)
//	{
//		camera.addCameraSpeed(-100);
//	}
//}
//
//GLvoid PostProcessWorld::mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
//{
//	camera.updateFront(xPosition, yPosition);
//}
//
//GLvoid PostProcessWorld::scrollInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
//{
//}
//
//GLvoid PostProcessWorld::mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods)
//{
//}
//
