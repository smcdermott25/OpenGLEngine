#pragma once
#include "GameWorld.h"
#include "Spotlight.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "MultiLightCube.h"
#include "PostProcess.h"

class PostProcessWorld : public GameWorld
{
private:
	Camera camera;
	DirectionalLight directionalLight;
	std::vector<PointLight> pointLightList;
	std::vector<MultiLightCube> cubeList;
	MultiLightCube myVerySpecialCube;
	MultiLightCube roomCube;
	SpotLight mySpotlight;
	GLboolean lines = false;
	GLboolean hdr = true;
	GLboolean bloom = true; 
	GLfloat exposure = 1.0f;
	FrameBuffer fb;
	FrameBuffer blurFB[2];
	FrameBuffer hdrFB;
	FrameBuffer normFB;

	Shader blurShader;
	Shader bloomShader;

public:
	PostProcessWorld::PostProcessWorld();
	GLvoid update(GLfloat deltaTime);
	GLvoid draw();

	PostProcess postprocess;

	GLvoid input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode);
	GLvoid mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition);
	GLvoid mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods);
	GLvoid scrollInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition);
};