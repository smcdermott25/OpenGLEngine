#pragma once
#include <glew.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <glm/glm.hpp>

inline GLfloat randomFloat()
{
	return (GLfloat)rand() / RAND_MAX;
}

inline GLfloat randomRange(GLfloat min, GLfloat max)
{
	if (min > max) std::swap(min, max);
	return (randomFloat() * (max - min)) + min;
}

inline glm::vec3 randUnitVec()
{
	GLfloat x = (randomFloat() * 2.0f) - 1.0f;
	GLfloat y = (randomFloat() * 2.0f) - 1.0f;
	GLfloat z = (randomFloat() * 2.0f) - 1.0f;

	return glm::normalize(glm::vec3(x, y, z));
}