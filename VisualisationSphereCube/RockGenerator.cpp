#include "RockGenerator.h"


RockGenerator::RockGenerator(glm::vec3 cornerPosition, GLfloat width, GLfloat depth)
{
	srand(5000);
	textures.push_back("Texture/alienScale.jpg");
	textures.push_back("Texture/alienMagma.jpg");
	textures.push_back("Texture/alienStone.jpg");
	textures.push_back("Texture/alienRock.jpg");
	textures.push_back("Texture/aleinDirt.png");
	
	setRockPositions(cornerPosition, width, depth);
}

RockGenerator::~RockGenerator()
{

}

GLvoid RockGenerator::setRockPositions(glm::vec3 cornerPosition, GLfloat width, GLfloat depth)
{
	
	position = cornerPosition;

	//Currently positions are random
	GLint rangeMinx = cornerPosition.x;
	GLint rangeMaxx = cornerPosition.x + width;

	GLint rangeMinz = cornerPosition.z;
	GLint rangeMaxz = cornerPosition.z + depth;

	//Set up orienatino data for eash bit of grass (model)
	for (GLint a = 0; a < 15; a++)
	{
		GLint randx = rand() % (rangeMaxx - rangeMinx + 1) + rangeMinx;
		GLint randz = rand() % (rangeMaxz - rangeMinz + 1) + rangeMinz;	

		stalagmite(glm::vec3(randx, 0.0f, randz),  5,  1, -5, 5, 1, 10);
	}
}

//Neccessary to allow other classes to alter and update model matrices
GLvoid RockGenerator::InitialUpdate(GLboolean  first)
{

	
	for (GLint  i = 0; i < grids.size(); i++)
	{
		grids[i].createMemory(30, 5);
		grids[i].createGrid(gridPositions[i], 30, 15, 20);
		grids[i].update(&metaballs[i][0], metaballs[i].size());
	}
		for (GLint  i = 0; i < amountOfLightRocks; i++)
		{
			rockLights.push_back(MeshLight(grids.at(i).lightColour*10.0f, &grids[i]));
		}
}

GLvoid RockGenerator::update(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{
	for (GLint  i = 5; i < grids.size(); i++)
	{
		grids[i].testAndUpdateTexture(view,projection,cameraPosition);
	}

}

GLvoid RockGenerator::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition,GLfloat  bloomValue)
{
	for (GLint  i = amountOfLightRocks; i < grids.size(); i++)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//grids.at(i).drawSurface(1.0f, view, projection);
		grids.at(i).drawWithImposter(view,projection,cameraPosition);

	}

	for (GLint  i = 0; i < amountOfLightRocks; i++)
	{
		rockLights.at(i).draw(view, projection, cameraPosition, bloomValue);
	}

	//grids.at(grids.size()-1).drawSurface(1.0f, view, projection);
}
//Takes in one position and creates a clump of orientations based around position
GLvoid RockGenerator::rockFormation(glm::vec3 position, GLint maxAmount, GLint minAmount, GLint minRadius, GLint maxRadius, GLint minScale, GLint maxScale)
{
	GLint randTexture = rand() % (4) + 1;
	GLint amount = rand() % (maxAmount - minAmount + 1) + minAmount;
	//std::cout << "The4 rand text   " << randTexture << std::endl;
	IsoSurfaceGrid gridSystem = IsoSurfaceGrid(textures[4]);
	
	
	std::vector<Metaball> meta;
	for (GLint i = 0; i < amount; i++)
	{
		GLint randx = rand() % (5 - 0 + 1) + 0;
		GLint randz = rand() % (5 - 0 + 1) + 0;
		GLint randy = rand() % (5 - 0 + 1) + 0;
	

		meta.push_back(Metaball(glm::vec3(randx, randy, randz), 1));

	}
	gridPositions.push_back(position);
	metaballs.push_back(meta);
	gridWidthHeight.push_back(std::make_pair(6,1));

	GLint scale = rand() % (maxScale - minScale + 1) + minScale;
	//gridSystem.orientation->setScale(glm::vec3(50, 50, 50));
	gridSystem.orientation->setScale(glm::vec3(50, 50, 50));
	grids.push_back(gridSystem);
	//rockLights.push_back(MeshLight(glm::vec3(2, 2, 2), &grids[grids.size() - 1]));
}
GLvoid RockGenerator::stalagmite(glm::vec3 position, GLint maxAmount, GLint minAmount, GLint minOffset, GLint maxOffset, GLint minScale, GLint maxScale)
{
	GLint randTexture = rand() % (4) + 1;
	GLint amount = rand() % (maxAmount - minAmount + 1) + minAmount;
	std::cout << "The4 rand text   " << randTexture << std::endl;
	
	IsoSurfaceGrid* gridSystem;
	if (grids.size() < amountOfLightRocks)
	{
		glm::vec3 lightColor;

		if (randTexture == 1)
			lightColor = glm::vec3(0, 2, 0);
		if (randTexture == 2)
			lightColor = glm::vec3(5, 0, 0);
		if (randTexture == 3)
			lightColor = glm::vec3(0, 0, 10);
		if (randTexture == 4)
			lightColor = glm::vec3(2, 2, 0);

			gridSystem = new IsoSurfaceGrid(textures[randTexture - 1], lightColor);
	}
	else
	{
		gridSystem = new IsoSurfaceGrid(textures[randTexture - 1]);
	}
	std::vector<Metaball> meta;
	for (GLint i = 0; i < amount; i++)
	{
		GLint randx = rand() % (maxOffset - minOffset + 1) + minOffset;
		GLint randz = rand() % (maxOffset - minOffset + 1) + minOffset;
		GLint height = rand() % (8) + 1;
		for (GLint  i = 0; i < height; i++)
		{
			GLint randRadius = rand() % (4) +1;
			meta.push_back(Metaball(glm::vec3(randx, i * 2, randz), randRadius));
		}
	}

	gridPositions.push_back(position);
	metaballs.push_back(meta);
	gridWidthHeight.push_back(std::make_pair(6, 1));

	GLint scale = rand() % (maxScale - minScale + 1) + minScale;
	gridSystem->orientation->setScale(glm::vec3(scale, scale, scale));
	grids.push_back(*gridSystem);
}
std::vector<glm::vec3>* RockGenerator::getGridPositions()
{
	return &gridPositions;
}

std::vector<MeshLight> RockGenerator::getRockLights()
{
	return rockLights;
}
std::vector<IsoSurfaceGrid> RockGenerator::getRocks()
{
	return grids;
}