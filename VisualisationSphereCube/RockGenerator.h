#pragma once

#include "AlphaBlenderTool.h"
#include "IsoSurfaceGrid.h"
#include "Metaball.h"
#include "MeshLight.h"

class RockGenerator
{
public:

	RockGenerator(glm::vec3 cornerPostion, GLfloat width, GLfloat depth);
	~RockGenerator();

	glm::vec3 position;
	GLvoid setRockPositions(glm::vec3 cornerPosition, GLfloat width, GLfloat depth);
	GLvoid InitialUpdate(GLboolean  first);//Neccessary to update and assign all model Matrices 
	GLvoid update(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition);
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition, GLfloat  bloomValue);
	std::vector<glm::vec3> gridPositions;
	std::vector<std::pair<GLint, GLint>> gridWidthHeight;
	std::vector<glm::vec3>* getGridPositions();
	std::vector<MeshLight> getRockLights();
	std::vector<IsoSurfaceGrid> getRocks();
	
	

private:

	std::vector<MeshLight> rockLights;
	std::vector<IsoSurfaceGrid> grids;

	std::vector<GLchar*> textures;
	std::vector<std::vector<Metaball>> metaballs;
	AlphaBlenderTool blender;

	const GLint amountOfLightRocks = 10;
	const GLint amountOfImposterRocks = 3;

	GLvoid rockFormation(glm::vec3 position, GLint maxAmount, GLint minAmount, GLint minRadius, GLint maxRadius, GLint minScale,GLint maxScale);
	GLvoid stalagmite(glm::vec3 position, GLint maxAmount, GLint minAmount, GLint minXOffset, GLint maxOffset, GLint minScale, GLint maxScale);

};


