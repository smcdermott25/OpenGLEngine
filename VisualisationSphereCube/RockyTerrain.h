#pragma once
#include "Terrain.h"

class RockyTerrain : public Terrain
{
private:

public:
	RockyTerrain(glm::vec3 position, GLint seed);
	~RockyTerrain();
};