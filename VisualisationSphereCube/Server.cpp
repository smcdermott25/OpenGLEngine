#include "Server.h"

Server::Server()
	:ipAddress("52.214.229.88"),
	port("400"),
	bufferLength(sizeof(glm::mat4)),
	numClients(0)
{
	init();
}



Server::Server(GLchar* ipAddress, GLchar* port, GLint  bufferLength)
	:ipAddress(ipAddress),
	port(port),
	bufferLength(bufferLength),
	numClients(0)
{
	init();
}

Server::~Server()
{

}

GLvoid Server::init()
{
	WSADATA wsaData;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
	}

	std::cout << "Winsock initialized: " << std::endl;


	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo("localhost", port, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed: %d\n", iResult);
		WSACleanup();
	}

	std::cout << "Address: " << iResult << std::endl;
	std::cout << "Server address and port resolved: " << std::endl;

	ptr = result;

	initServerSocketConnection();
	initClientSocketConnection();
}

GLvoid Server::initServerSocketConnection()
{
	// Create a SOCKET for the server to listen for client connections
	listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

	if (listenSocket == INVALID_SOCKET) {
		printf("Error at socket(): %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
	}

	// Setup the TCP listening socket
	iResult = bind(listenSocket, result->ai_addr, (GLint )result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(listenSocket);
		WSACleanup();
	}
	else
	{
		freeaddrinfo(result);
		std::cout << "Free local address port variable after binding " << std::endl;
	}

	if (listen(listenSocket, SOMAXCONN) == SOCKET_ERROR) {
		printf("Listen failed with error: %ld\n", WSAGetLastError());
		closesocket(listenSocket);
		WSACleanup();
	}

	std::cout << "Listening on socket: " << std::endl;
}

GLvoid Server::initClientSocketConnection()
{
	clientSocket[numClients] = INVALID_SOCKET;

	// Accept a client socket
	clientSocket[numClients] = accept(listenSocket, NULL, NULL);
	if (clientSocket[numClients] == INVALID_SOCKET) {
		printf("accept failed: %d\n", WSAGetLastError());
		closesocket(listenSocket);
		WSACleanup();
	}

	numClients++;
}

GLvoid Server::closeConnection()
{

}

GLvoid Server::receiveMat4(GLint clientIndex)
{
	GLchar recvbuf[sizeof(glm::mat4)];
	GLint  recvbuflen = bufferLength;

	// Receive until the peer shuts down the connection
	iResult = recv(clientSocket[clientIndex], recvbuf, recvbuflen, 0);
	if (iResult > 0)
	{
		lastModelReceived[clientIndex] = *reinterpret_cast<glm::mat4 *>(recvbuf);
		//lastModelReceived[i] = *(glm::mat4*)&recvbuf[0];
	}
	else
	{
		std::cout << "Failed to receive bytes: " << std::endl;
	}
}

GLvoid Server::receiveMultipleMat4()
{
	for (GLint i = 0; i < numClients; i++)
	{
		receiveMat4(i);
	}
}

GLvoid Server::sendMat4(glm::mat4 model)
{
	// Echo the buffer back to the sender
	GLchar* sendbuf = new GLchar(bufferLength);
	sendbuf = (GLchar*)&model;

	// Send an initial buffer
	iResult = send(clientSocket[0], sendbuf, bufferLength, 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed: %d\n", WSAGetLastError());
		closesocket(clientSocket[0]);
		WSACleanup();
	}
	printf("Bytes sent: %d\n", iResult);
	
}

GLvoid Server::sendMultipleMat4()
{
	GLchar* sendbuf = new GLchar(bufferLength);
	sendbuf = (GLchar*)&(numClients);

	for (GLint i = 0; i < numClients; i++)
	{
		iResult = send(clientSocket[i], sendbuf, bufferLength, 0);
		if (iResult == SOCKET_ERROR) {
			printf("send failed: %d\n", WSAGetLastError());
			closesocket(clientSocket[i]);
			WSACleanup();
		}
		printf("Bytes sent: %d\n", iResult);
	}

	
	for (GLint i = 0; i < numClients; i++)
	{
		GLchar* sendBuffer = new GLchar(bufferLength);
		sendBuffer = (GLchar*)&lastModelReceived[i];
		std::cout << "A1: " << lastModelReceived[i][0][0] << std::endl;
		for (GLint j = 0; j < numClients; j++)
		{
			iResult = send(clientSocket[j], sendBuffer, bufferLength, 0);
		}
	}
}

glm::mat4 Server::getLastModel()
{
	return lastModelReceived[0];
}
