#pragma once
#pragma
#include <glew.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>

#include <iostream>
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN sizeof(glm::mat4)

class Server
{
private:
	const GLchar* port;
	const GLchar* ipAddress;
	const GLint  bufferLength;

	GLint  iResult;
	struct addrinfo *result = NULL;
	struct addrinfo	*ptr = NULL;
	struct addrinfo	hints;

	SOCKET listenSocket = INVALID_SOCKET;

	SOCKET clientSocket [4];
	glm::mat4 lastModelReceived[4];

	GLvoid init();
	GLvoid initServerSocketConnection();

public:
	Server();
	Server(GLchar* ipAddress, GLchar* port, GLint  bufferLength);
	~Server();

	GLint numClients;

	GLvoid initClientSocketConnection();
	GLvoid closeConnection();

	GLvoid receiveMat4(GLint clientIndex);
	GLvoid receiveMultipleMat4();
	GLvoid sendMat4(glm::mat4 model);
	GLvoid sendMultipleMat4();
	glm::mat4 getLastModel();
};