#version 330 core
in vec2 TextureCo;
layout (location = 0) out vec4 colour;
layout (location = 1) out vec4 BrightColor;

uniform sampler2D textureImage;
uniform vec3 lightColour;
uniform bool textureOn;

void main()
{
	if (textureOn)
	{

		vec4 textureColor = texture(textureImage, vec2(TextureCo.x,TextureCo.y));
		colour =   textureColor;

		float brightness = dot(lightColour.rgb, vec3(0.2126, 0.7152, 0.0722));
   
		 if(brightness > 1.0)
		{
		 BrightColor = mix(vec4(lightColour.rgb, 1.0),textureColor,0.9);
		}
	}
	else

	{
	colour = vec4(lightColour.rgb,1.0);
	
	float brightness = dot(lightColour.rgb, vec3(0.2126, 0.7152, 0.0722));
    if(brightness > 1.0)
		 BrightColor = vec4(lightColour.rgb, 1.0);
	}

}