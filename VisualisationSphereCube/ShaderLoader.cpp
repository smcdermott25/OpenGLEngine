#include "ShaderLoader.h"

//Object shaders
Shader ShaderLoader::billboardShader;
Shader ShaderLoader::lightShader;

Shader ShaderLoader::modelShader;
Shader ShaderLoader::animationShader;

Shader ShaderLoader::particleShader;

Shader ShaderLoader::terrainShader;
Shader ShaderLoader::simpleDepthShader;

//Framebuffer shaders
Shader ShaderLoader::hdrScreenShader;
Shader ShaderLoader::screenShader;
//Post process shaders
Shader ShaderLoader::blurShader;
Shader ShaderLoader::bloomShader;
//PBR shaders
Shader ShaderLoader::equirectangularToCubemapShader;
Shader ShaderLoader::irradianceShader;
Shader ShaderLoader::prefilterShader;
Shader ShaderLoader::brdfShader;
Shader ShaderLoader::pbrShader;


ShaderLoader::ShaderLoader()
{
	billboardShader = Shader("Shader/billboardVertex.txt", "Shader/textureNoLightFragment.txt");
	lightShader = Shader("Shader/lightVertex.txt", "Shader/lightFragment.txt");
	
	modelShader = Shader("Shader/modelVertex.txt", "Shader/modelFragment.txt");
	animationShader = Shader("Shader/animationVertex.txt","Shader/animationFragment.txt");
	
	particleShader = Shader("Shader/particleVertex.txt", "Shader/particleFragment.txt");

	terrainShader = Shader("Shader/terrainVertex.txt", "Shader/terrainFragment.txt");
	simpleDepthShader = Shader("Shader/shadowMappingDepthVertex.txt", "Shader/shadowMappingDepthFragment.txt");
	hdrScreenShader = Shader("Shader/screenVertex.txt", "Shader/hdrFragment.txt");
	screenShader = Shader("Shader/screenVertex.txt", "Shader/screenFragment.txt");

	blurShader = Shader("Shader/screenVertex.txt", "Shader/blurFragment.txt");
	bloomShader = Shader("Shader/screenVertex.txt", "Shader/bloomFragment.txt");

	equirectangularToCubemapShader = Shader("cubemap.vs", "equirec_to_cubemap.fs");
	irradianceShader = Shader("cubemap.vs", "irrad_convolution.fs");
	prefilterShader = Shader("cubemap.vs", "prefilter.fs");
	brdfShader = Shader("brdf.vs", "brdf.fs");
	pbrShader = Shader("pbrvertex.vs","pbrfrag.fs");
	

}