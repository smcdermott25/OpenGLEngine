#pragma once
#include "Shader.h"

class ShaderLoader
{
public:

	static Shader billboardShader;
	static Shader lightShader;

	static Shader modelShader;
	static Shader animationShader;

	static Shader particleShader;

	static Shader terrainShader;
	static Shader simpleDepthShader;
	static Shader hdrScreenShader;
	static Shader screenShader;
	
	static Shader blurShader;
	static Shader bloomShader;

	static Shader equirectangularToCubemapShader;
	static Shader irradianceShader;
	static Shader prefilterShader;
	static Shader brdfShader;
	static Shader pbrShader;


	ShaderLoader::ShaderLoader();
};


