#include "Shadow.h"


GLvoid Shadow::createShadowTextureAndFBO()
{
	glEnable(GL_DEPTH_TEST);
	// Configure depth map FBO
	glGenFramebuffers(1, &depthMapFBO);
	// - Create depth texture
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	GLfloat borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

GLvoid Shadow::renderDepthSceneToTexture(DirectionalLight directionalLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow)
{
	//Create out light camera
	glm::mat4 lightProjection, lightView;
	lightProjection = glm::ortho(-orthoDimension, orthoDimension, -orthoDimension, orthoDimension, near_plane, far_plane);
	lightView = glm::lookAt(lightPosition, lightTarget, glm::vec3(0.0, 1.0, 0.0));
	lightSpaceMatrix = lightProjection * lightView;
	//Render scene from light camera's POV
	simpleDepthShader.useProgram();
	glUniformMatrix4fv(glGetUniformLocation(simpleDepthShader.getProgram(), "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	/*Save the buffer we're currently using (incase we're using post processing in our normal scene or similar*/
	GLint oldFrameBuffer;
	glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &oldFrameBuffer);
	/*Bind our depth map buffer and draw to the texture*/
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	/*Cull back faces helps prevent "peter panning"*/
	glCullFace(GL_FRONT);
	for (GLint  i = 0; i < objectsThatCastShadow.size(); i++)
	{
		/*Take model matrix and VAO of all our objects and draw them into the shadow*/
		glm::mat4 myLocalMatrix = objectsThatCastShadow.at(i)->orientation->getModelMatrix();
		glUniformMatrix4fv(glGetUniformLocation(simpleDepthShader.getProgram(), "model"), 1, GL_FALSE, glm::value_ptr(myLocalMatrix));
		objectsThatCastShadow.at(i)->drawSimpleVAO();
	}
	glCullFace(GL_BACK);
	/*Switch back to the previous buffer we were using*/
	glBindFramebuffer(GL_FRAMEBUFFER, oldFrameBuffer);
	glViewport(0, 0, WIDTH, HEIGHT);
}


GLuint Shadow::getDepthMapTexture()
{
	return depthMap;
}
GLuint Shadow::getDepthMapFBO()
{
	return depthMapFBO;
}

glm::mat4 Shadow::getLightSpaceMatrix()
{
	return lightSpaceMatrix;
}

glm::vec3 Shadow::getLightPositionForShadow()
{
		return lightPosition;

}
GLvoid Shadow::setPositionOfLightForShadow(glm::vec3 newLightPosition)
{
		lightPosition = newLightPosition;
}

glm::vec3 Shadow::getLightTargetForShadow()
{
	return lightTarget;

}
GLvoid Shadow::setPositionOfLightTargetForShadow(glm::vec3 newLightTarget)
{
	lightTarget = newLightTarget;
}

GLvoid Shadow::setNearPlane(GLfloat newNearPlane)
{
	near_plane = newNearPlane;
}
GLvoid Shadow::setFarPlane(GLfloat newFarPlane)
{
	far_plane = newFarPlane;
}
GLvoid Shadow::setOrthoDimension(GLfloat newOrthoDimension)
{
	orthoDimension = newOrthoDimension;
}
