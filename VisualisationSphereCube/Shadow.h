#pragma once
#include <glew.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "DirectionalLight.h"
#include "ShadowCastingObject.h"

/*Attach a shadow to an object for it to own that shadow. 
 *Defines an orthographic camera that creates a shadow map and sends it to a texture*/
class Shadow
{
private:
	GLuint depthMap;
	GLuint depthMapFBO;
	static const GLuint SHADOW_WIDTH = 4096;
	static const GLuint SHADOW_HEIGHT = 4096;
	Shader simpleDepthShader = ShaderLoader::simpleDepthShader;
	glm::mat4 lightSpaceMatrix;
	glm::vec3 lightPosition = glm::vec3(2.0f, 40.0f, 2.0f);
	glm::vec3 lightTarget = glm::vec3(0, 0, 0);
	GLfloat near_plane = 1.0f, far_plane = 50.0f;
	GLfloat orthoDimension = 10.0f;
public:
	GLvoid createShadowTextureAndFBO();
	GLvoid renderDepthSceneToTexture(DirectionalLight directionalLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow);
	GLuint getDepthMapTexture();
	GLuint getDepthMapFBO();
	glm::mat4 getLightSpaceMatrix();
	glm::vec3 getLightPositionForShadow();
	glm::vec3 getLightTargetForShadow();
	GLvoid setPositionOfLightTargetForShadow(glm::vec3 newLightTarget);
	GLvoid setPositionOfLightForShadow(glm::vec3 newLightPosition);
	GLvoid setNearPlane(GLfloat newNearPlane);
	GLvoid setFarPlane(GLfloat newFarPlane);
	GLvoid setOrthoDimension(GLfloat newOrthoDimension);
};