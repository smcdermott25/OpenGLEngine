#include "ShadowCastingObject.h"

ShadowCastingObject::ShadowCastingObject()
	:GameObject()
{
}

ShadowCastingObject::ShadowCastingObject(GLchar* name, const GLchar* vertexShader, const GLchar* fragmentShader)
	:GameObject(name, vertexShader, fragmentShader)
{
}

/* Function with a set value of 4 point lights, the default #define in our shaders for light numbers*/
GLvoid ShadowCastingObject::updatePointLightList(std::vector<PointLight> &allPointLights)
{
	pointLightList = calculateClosestLightsToObject(allPointLights, 4);
}

/*Function allowing you to specify a number of lights of your choosing*/
GLvoid ShadowCastingObject::updatePointLightList(std::vector<PointLight> &allPointLights, GLint  numberOfPointLightsToSelect)
{
	pointLightList = calculateClosestLightsToObject(allPointLights, numberOfPointLightsToSelect);
}

/*Function allowing you to pick pointlights based off another object, such as a player's position for example*/
GLvoid ShadowCastingObject::updatePointLightList(std::vector<PointLight> &allPointLights, GLint  numberOfPointLightsToSelect, glm::vec3 positionOfOtherObject)
{
	pointLightList = calculateClosestLightsToPosition(allPointLights, numberOfPointLightsToSelect, positionOfOtherObject);
}

std::vector<PointLight> ShadowCastingObject::calculateClosestLightsToPosition(std::vector<PointLight> &objectList, GLint  numberOfObjectsToReturn, glm::vec3 positionOfOtherObject)
{
	GLint  distanceFromObject = -1;
	GLint  indexOfFurthestNearObject = -1;
	if (objectList.size() < numberOfObjectsToReturn)
	{
		std::cout << "You have not passed a long enough to need to sort";
		return objectList;
	}
	std::vector<PointLight> nearObjects;
	for (GLint  objectInPassedList = 0; objectInPassedList < objectList.size(); objectInPassedList++)
	{
		if (nearObjects.size() < numberOfObjectsToReturn)
		{
			nearObjects.push_back(objectList.at(objectInPassedList));
		}
		else
		{
			indexOfFurthestNearObject = -1;

			distanceFromObject = std::fabsf(glm::distance(positionOfOtherObject, objectList.at(objectInPassedList).getPosition()));
			for (GLint  nearObject = 0; nearObject < nearObjects.size(); nearObject++)
			{
				//If we find an object further away in our nearest list
				if (distanceFromObject < distanceFromOtherPosition(positionOfOtherObject , nearObjects.at(nearObject).getGameObject()))
				{
					//Record it's index if index is -1
					if (indexOfFurthestNearObject == -1)
					{
						indexOfFurthestNearObject = nearObject;
					}
					//Record it's index if this is the new max distance in our nearest list
					else if (distanceFromOtherPosition(positionOfOtherObject, nearObjects.at(nearObject).getGameObject())
				> distanceFromOtherPosition(positionOfOtherObject, nearObjects.at(indexOfFurthestNearObject).getGameObject()))
					{
						indexOfFurthestNearObject = nearObject;
					}
				}
			}
			if (indexOfFurthestNearObject != -1)
			{
				nearObjects.at(indexOfFurthestNearObject) = objectList.at(objectInPassedList);
			}
		}
	}
	return nearObjects;
}

/*Simple function for returning distance between an object and position*/
GLfloat ShadowCastingObject::distanceFromOtherPosition(glm::vec3 position, GameObject otherGameObject)
{
	return std::fabsf(glm::distance(position, otherGameObject.getPosition()));
}

/*Calculate the closest pointlights to the object and return the closest ones to the number specified*/
std::vector<PointLight> ShadowCastingObject::calculateClosestLightsToObject(std::vector<PointLight> &objectList, GLint  numberOfObjectsToReturn)
{
	GLint  distanceFromObject = -1;
	GLint  indexOfFurthestNearObject = -1;
	if (objectList.size() < numberOfObjectsToReturn)
	{
		std::cout << "You have not passed a long enough to need to sort";
		return objectList;
	}

	std::vector<PointLight> nearObjects;
	for (GLint  objectInPassedList = 0; objectInPassedList < objectList.size(); objectInPassedList++)
	{
		if (nearObjects.size() < numberOfObjectsToReturn)
		{
			nearObjects.push_back(objectList.at(objectInPassedList));
		}
		else
		{
			indexOfFurthestNearObject = -1;
			distanceFromObject = std::fabsf(glm::distance(getPosition(), objectList.at(objectInPassedList).getPosition()));
			for (GLint  nearObject = 0; nearObject < nearObjects.size(); nearObject++)
			{
				//If we find an object further away in our nearest list
				if (distanceFromObject < distanceFromOtherGameObject(nearObjects.at(nearObject).getGameObject()))
				{
					//Record it's index if we have no objects in our list yet
					if (indexOfFurthestNearObject == -1)
					{
						indexOfFurthestNearObject = nearObject;
					}
					//Record it's index if this is closer than our furthest away object in the list
					else if (distanceFromOtherGameObject(nearObjects.at(nearObject).getGameObject())
				> distanceFromOtherGameObject(nearObjects.at(indexOfFurthestNearObject).getGameObject()))
					{
						indexOfFurthestNearObject = nearObject;
					}
				}
			}
			if (indexOfFurthestNearObject != -1)
			{
				nearObjects.at(indexOfFurthestNearObject) = objectList.at(objectInPassedList);
			}
		}

	}
	return nearObjects;
}