#pragma once
#include <glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "GameObject.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "Spotlight.h"
#include <memory>

class ShadowCastingObject : public GameObject
{
protected:
	std::vector<PointLight> pointLightList;
public:
	ShadowCastingObject();
	ShadowCastingObject(GLchar* name, const GLchar* vertexShader, const GLchar* fragmentShader);
	GLvoid updatePointLightList(std::vector<PointLight> &allPointLights);
	GLvoid updatePointLightList(std::vector<PointLight> &allPointLights, GLint  numberOfPointLightsToSelect);
	GLvoid updatePointLightList(std::vector<PointLight> &allPointLights, GLint  numberOfPointLightsToSelect, glm::vec3 positionOfOtherObject);
	std::vector<PointLight> calculateClosestLightsToObject(std::vector<PointLight> &objectList, GLint  numberOfObjectsToReturn);
	std::vector<PointLight> calculateClosestLightsToPosition(std::vector<PointLight> &objectList, GLint  numberOfObjectsToReturn, glm::vec3 positionOfOtherObject);
	GLfloat distanceFromOtherPosition(glm::vec3 position, GameObject otherGameObject);
	virtual GLvoid drawSimpleVAO() = 0;
	virtual GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow) = 0;
};