#pragma once
#pragma once
#include "GameWorld.h"
#include "Spotlight.h"
#include "DirectionalLight.h"
#include "PointLight.h"
#include "MultiLightCube.h"
#include "FlatPlane.h"
#include "InteriorShadowBox.h"
class ShadowRoomWorld : public GameWorld
{
private:
	Camera camera;
	DirectionalLight directionalLight;
	std::vector<PointLight> pointLightList;
	std::vector<MultiLightCube> cubeList;
	MultiLightCube myVerySpecialCube;
	FlatPlane myVerySpecialPlane;
	InteriorShadowBox interiorShadowBox;
	SpotLight mySpotlight;
	GLboolean lines = false;
	GLuint quadVAO = 0;
	GLuint quadVBO;
	//Shader debugDepthQuad = Shader("debug_quad.vs", "debug_quad.fs");
	GLboolean depthBufferRender = false;
	// Setup and compile our shaders




public:
	ShadowRoomWorld();
	GLvoid update(GLfloat deltaTime);
	GLvoid draw();
	void RenderQuad();
	GLvoid input(GLFWwindow* window, int key, int scancode, int action, int mode);
	GLvoid mouseButton(GLFWwindow* window, int button, int action, int mods);
	GLvoid mouseInput(GLFWwindow* window, double xPosition, double yPosition);
	GLvoid scrollInput(GLFWwindow* window, double xPosition, double yPosition);
};