/*Deprecated world*/
//#include "ShadowWorld.h"
//
//ShadowWorld::ShadowWorld()
//{
//	directionalLight = DirectionalLight(glm::vec4(-0.2f, -1.0f, -0.3f, 0.0));
//
//	myVerySpecialCube = MultiLightCube();
//	myVerySpecialCube.setPosition(glm::vec3(0, 10, 0));
//	//myVerySpecialCube.setScale(glm::vec3(1, 1, 1));
//
//	for (GLint  i = 0; i < 5; i++)
//	{
//
//		MultiLightCube multiCube = MultiLightCube();
//		multiCube.setPosition(glm::vec3(i * 4, i * 4, i * 4));
//		cubeList.push_back(multiCube);
//
//	}
//	cubeList.at(0).setPosition(glm::vec3(0, 4, 0));
//	
//	myVerySpecialPlane = FlatPlane();
//	myVerySpecialPlane.setPosition(glm::vec3(0, 0, 0));
//	myVerySpecialPlane.setScale(glm::vec3(0.5, 0.5, 0.5));
//
//
//
//	//std::vector<PointLight> pointLightList;
//	pointLightList.reserve(20);
//	for (GLint  i = 0; i < 5; i++)
//	{
//		PointLight pointLight = PointLight(glm::vec3(1.0, 0.0, 1.0));
//		pointLight.setPosition(glm::vec3(i * 10.0, i * 5.0, 0));
//		pointLightList.push_back(pointLight);
//	}
//
//
//	pointLightList.at(0).setPosition(glm::vec3(0, -30, 0));
//	printf("Position is: %f,%f,%f time", pointLightList.at(0).getPosition().x, pointLightList.at(0).getPosition().y, pointLightList.at(0).getPosition().z);
//	pointLightList.at(0).setColourEasy(glm::vec3(0.0, 1.0, 0.0));
//	pointLightList.at(1).setPosition(glm::vec3(20, -15, -5));
//	pointLightList.at(2).setPosition(glm::vec3(25, -15, 5));
//	pointLightList.at(2).setColourEasy(glm::vec3(1.0, 0.0, 0.0));
//	pointLightList.at(3).setPosition(glm::vec3(-25, -15, 5));
//	pointLightList.at(3).setColourEasy(glm::vec3(0.0, 0.0, 3.0));
//	pointLightList.at(4).setPosition(glm::vec3(2.0f, 15.0f, 2.0f));
//	pointLightList.at(4).setColourEasy(glm::vec3(1.0, 0.0, 0.0));
//
//	mySpotlight = SpotLight();
//	mySpotlight.setPosition(glm::vec3(myVerySpecialPlane.getPosition().x, myVerySpecialPlane.getPosition().y + 10, myVerySpecialPlane.getPosition().z));
//	mySpotlight.setTarget(glm::vec3(myVerySpecialPlane.getPosition().x, myVerySpecialPlane.getPosition().y, myVerySpecialPlane.getPosition().z));
//	mySpotlight.setColourEasy(glm::vec3(0.5,0.5,0.5));
//
//	myVerySpecialPlane.updatePointLightList(pointLightList);
//	myVerySpecialCube.updatePointLightList(pointLightList);
//	for (GLint  i = 0; i < cubeList.size(); i++)
//	{
//		cubeList.at(i).updatePointLightList(pointLightList);
//	}
//	cubeList.push_back(myVerySpecialCube);
//
//	for (GLint  i = 0; i < cubeList.size(); i++) //cubeList.at(i)
//	{
//		pointerObjectList.push_back(cubeList.at(i));
//	}
//
//}
//
//GLvoid ShadowWorld::RenderQuad()
//{
//	if (quadVAO == 0)
//	{
//		GLfloat quadVertices[] = {
//			// Positions        // Texture Coords
//			-1.0f,  1.0f, 0.0f,  0.0f, 1.0f,
//			-1.0f, -1.0f, 0.0f,  0.0f, 0.0f,
//			1.0f,  1.0f, 0.0f,  1.0f, 1.0f,
//			1.0f, -1.0f, 0.0f,  1.0f, 0.0f,
//		};
//		// Setup plane VAO
//		glGenVertexArrays(1, &quadVAO);
//		glGenBuffers(1, &quadVBO);
//		glBindVertexArray(quadVAO);
//		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
//		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
//		glEnableVertexAttribArray(0);
//		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
//		glEnableVertexAttribArray(1);
//		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
//	}
//	glBindVertexArray(quadVAO);
//	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
//	glBindVertexArray(0);
//}
//
//GLvoid ShadowWorld::update(GLfloat deltaTime)
//{
//	cubeList.at(0).setPosition(glm::vec3(cubeList.at(0).getPosition().x - 0.01, cubeList.at(0).getPosition().y, cubeList.at(0).getPosition().z - 0.01));
//	camera.move(deltaTime);
//
//}
//
//GLvoid ShadowWorld::draw()
//{
//	for (GLint  i = 0; i < cubeList.size(); i++)
//	{
//		cubeList.at(i).draw(camera.getView(), camera.getProjection(), camera.getCameraPosition(), directionalLight, pointLightList, mySpotlight);
//	}
//	myVerySpecialPlane.draw(camera.getView(), camera.getProjection(), camera.getCameraPosition(), directionalLight, pointLightList, mySpotlight, cubeList);
//	//myVerySpecialCube.draw(camera.getView(), camera.getProjection(), camera.getCameraPosition(), directionalLight, pointLightList, mySpotlight);
//	for (GLint  i = 0; i < pointLightList.size(); i++)
//	{
//		pointLightList.at(i).draw(camera.getView(), camera.getProjection());
//	}
//	// 3. DEBUG: visualize depth map by rendering it to plane
//	/*debugDepthQuad.useProgram();
//	GLfloat near_plane = 1.0f, far_plane = 15.0f;
//	glUniform1f(glGetUniformLocation(debugDepthQuad.getProgram() , "near_plane"), near_plane);
//	glUniform1f(glGetUniformLocation(debugDepthQuad.getProgram(), "far_plane"), far_plane);
//	glActiveTexture(GL_TEXTURE0);
//	glBindTexture(GL_TEXTURE_2D, myVerySpecialPlane.depthMap);*/
//	//glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
//	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	if (depthBufferRender)
//	{
//		RenderQuad();
//	}
//}
//
//GLvoid ShadowWorld::input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode)
//{
//	if (key == GLFW_KEY_ESCAPE)
//	{
//		exit(1);
//	}
//
//	if (key == GLFW_KEY_L && action == GLFW_RELEASE)
//	{
//		lines = !lines;
//		if (lines)
//			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//		else
//			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//	}
//
//	if (key == GLFW_KEY_G && action == GLFW_RELEASE)
//	{
//		depthBufferRender = !depthBufferRender;
//	}
//
//	if (action == GLFW_PRESS)
//	{
//		camera.setKey(key, true);
//	}
//
//	if (action == GLFW_RELEASE)
//	{
//		camera.setKey(key, false);
//	}
//
//	if (key == GLFW_KEY_UP && action == GLFW_RELEASE)
//	{
//		camera.addCameraSpeed(100);
//	}
//
//	if (key == GLFW_KEY_DOWN && action == GLFW_RELEASE)
//	{
//		camera.addCameraSpeed(-100);
//	}
//
//}
//
//GLvoid ShadowWorld::mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods)
//{
//	if (action == GLFW_PRESS)
//	{
//		camera.setKey(button, true);
//	}
//
//	if (action == GLFW_RELEASE)
//	{
//		camera.setKey(button, false);
//	}
//}
//
//GLvoid ShadowWorld::mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
//{
//	camera.updateFront(xPosition, yPosition);
//}
//
//GLvoid ShadowWorld::scrollInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
//{
//}
//
