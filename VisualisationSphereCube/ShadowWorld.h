/*This world is now deprecated but was a scene of a flat plane with lights and shadows casting on it*/
//#pragma once
//#include "GameWorld.h"
//#include "Spotlight.h"
//#include "DirectionalLight.h"
//#include "PointLight.h"
//#include "MultiLightCube.h"
//#include "FlatPlane.h"
//#include <memory>
//class ShadowWorld : public GameWorld
//{
//private:
//	Camera camera;
//	DirectionalLight directionalLight;
//	std::vector<PointLight> pointLightList;
//	std::vector<MultiLightCube> cubeList;
//	MultiLightCube myVerySpecialCube;
//	FlatPlane myVerySpecialPlane;
//	SpotLight mySpotlight;
//	GLboolean lines = false;
//	std::vector<std::unique_ptr<ShadowCastingObject>> &shadowCastingObjectList;
//	GLuint quadVAO = 0;
//	GLuint quadVBO;
//	Shader debugDepthQuad = Shader("debug_quad.vs", "debug_quad.fs");
//	GLboolean depthBufferRender = false;
//
//
//public:
//	ShadowWorld();
//	GLvoid update(GLfloat deltaTime);
//	GLvoid draw();
//	GLvoid RenderQuad();
//	GLvoid input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode);
//	GLvoid mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods);
//	GLvoid mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition);
//	GLvoid scrollInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition);
//};