#include "SpaceShip.h"

SpaceShip::SpaceShip()
	:Model("models/SpaceShip/Low_poly_UFO.obj", false, glm::vec3(5, 5, 5), glm::vec3(400, 700, 400))
{
	glm::vec3 position = orientation->getPosition();
	mySpotlight = SpotLight(position,glm::vec3(position.x, position.y -30,position.z));
	mySpotlight.setColourEasy(glm::vec3(0.0, 10000.0, 0.0));
}



SpaceShip::~SpaceShip()
{
}

GLvoid SpaceShip::update()
{

	rotation += speedOfRotation;
	setPosition(glm::vec3(glm::sin(rotation) * radius + 1500, getPosition().y, glm::cos(rotation) * radius + 1500));
	glm::vec3 position = orientation->getPosition();
	mySpotlight.setPosition(position);
	mySpotlight.setTarget(glm::vec3(position.x, position.y - 30, position.z));
}

GLvoid SpaceShip::draw(glm::mat4 view, glm::mat4 projection, GLfloat deltaTime)
{
	drawing(projection, view, deltaTime);
}
SpotLight SpaceShip::getSpotlight()
{
	return mySpotlight;
}


