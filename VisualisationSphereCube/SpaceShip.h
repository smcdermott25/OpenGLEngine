#pragma once
#include "Model.h"

class SpaceShip : public Model
{
public:
	SpaceShip();
	~SpaceShip();
	
	GLvoid update();
	GLvoid draw(glm::mat4 view, glm::mat4 projection, GLfloat  deltaTime);

	SpotLight getSpotlight();

private:
	SpotLight mySpotlight;

	const GLfloat radius = 500;
    const GLfloat speedOfRotation = 0.008;

	GLfloat rotation = 0.0;
};
