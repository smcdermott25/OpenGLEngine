#include "Spotlight.h"

/*Casts a light in a cone*/
SpotLight::SpotLight()
:SpotLight(glm::vec3(0, -15, 0), glm::vec3(0, 0, 0))
{
}
SpotLight::SpotLight(glm::vec3 spotlightPosition, glm::vec3 spotLightTarget)
	:BaseLight(glm::vec3(0.0f, 0.0f, 0.0), glm::vec3(0.0f, 0.0f, 0.0), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(1.0f, 1.0f, 1.0f))
{
	spotlight = Camera(spotlightPosition, spotLightTarget);
	constant = 1.0f;
	linear = 0.09f;
	quadratic = 0.032f;
	cutOff = glm::cos(glm::radians(12.5f));
	outerCutOff = glm::cos(glm::radians(15.0f));
}
GLvoid SpotLight::setPosition(glm::vec3 newPosition)
{
	spotlight.setCameraPosition(newPosition);
}
GLvoid SpotLight::setTarget(glm::vec3 newTarget)
{
	spotlight.setCameraTarget(newTarget);
}

glm::vec3 SpotLight::getFront()
{
	return spotlight.getCameraFront();
}

GLfloat SpotLight::getConstant()
{
	return constant;
}
GLfloat SpotLight::getLinear()
{
	return linear;
}
GLfloat SpotLight::getQuadratic()
{
	return quadratic;
}
GLfloat SpotLight::getCutOff()
{
	return cutOff;
}
GLfloat SpotLight::getOuterCutOff()
{
	return outerCutOff;
}
glm::vec3 SpotLight::getPosition()
{
	return spotlight.getCameraPosition();
}
