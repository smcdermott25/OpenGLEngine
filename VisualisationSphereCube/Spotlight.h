#pragma once
#include <glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "BaseLight.h"
#include "Camera.h"

class SpotLight:public BaseLight
{
public:
	SpotLight();
	SpotLight(glm::vec3 spotlightPosition, glm::vec3 spotLightTarget);
	GLvoid setPosition(glm::vec3 newPosition);
	GLvoid setTarget(glm::vec3 newTarget);
	glm::vec3 getPosition();
	glm::vec3 getFront();
	GLfloat getConstant();
	GLfloat getLinear();
	GLfloat getQuadratic();
	GLfloat getCutOff();
	GLfloat getOuterCutOff();

private:
	Camera spotlight;
	glm::vec3 front;
	GLfloat constant = 1.0f;
	GLfloat linear;
	GLfloat quadratic;
	GLfloat cutOff;
	GLfloat outerCutOff;

};