#include "Terrain.h"

/*
 * Robert Finn
 */
Terrain::Terrain(glm::vec3 p, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size, Noise* noise)
	:ShadowCastingObject()
{
	glEnable(GL_DEPTH_TEST);
	this->noise = noise;
	this->seed = seed;
	this->vertexCount = vertexCount;
	this->size = size;
	this->amplitude = amplitude;
	this->position = p;
	intersectionSize = vertexCount / 10;
	init();
}

Terrain::Terrain(glm::vec3 p, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size, Noise* noise, GLchar* textures[3])
	:ShadowCastingObject()
{
	defaultTextures[0] = textures[0];
	defaultTextures[1] = textures[1];
	defaultTextures[2] = textures[2];
	glEnable(GL_DEPTH_TEST);
	this->noise = noise;
	this->seed = seed;
	this->vertexCount = vertexCount;
	this->size = size;
	this->amplitude = amplitude;
	this->position = p;
	intersectionSize = vertexCount / 10;
	init();
}
GLvoid Terrain::drawSimpleVAO()
{
	glBindVertexArray(vertexArray);
	glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

Terrain::~Terrain()
{
	// free cpu memory
	delete(vertices);
	delete(textureCoords);
	delete(indices);
	delete(normals);

	delete(XCoords);
	delete(XCoords);
	delete(XCoords);
	
	for (GLint i = 0; i < vertexCount; i++)
	{
		delete(heights[i]);
	}

	//delete(terrainShader);
	//delete(simpleDepthShader);

	delete(normalVectors);
	delete(triangleCentroidCoordinates);
	delete(shadow);

	// Free gpu memory
	// buffers
	glDeleteVertexArrays(1, &vertexArray);
	glDeleteBuffers(1, &vertexBuffer);
	glDeleteBuffers(1, &indexBuffer);
	glDeleteBuffers(1, &normalBuffer);
	glDeleteBuffers(1, &textureBuffer);

	// textures
	glDeleteTextures(1, &snowTexture);
	glDeleteTextures(1, &grassTexture);
	glDeleteTextures(1, &rockTexture);
	glDeleteTextures(1, &waterTexture);
}


GLvoid Terrain::init()
{
	count = vertexCount * vertexCount;
	verticesCount = count * 3;
	textureCoordsCount = count * 2;
	indicesCount = 6 * (vertexCount - 1) * (vertexCount - 1);

	model = glm::mat4(1);
	model = glm::translate(model, position);


	createBuffers();
	setUpBuffers();
}

GLvoid Terrain::createBuffers()
{
	// Using heap memory due to the amount of memory required
	vertices = new GLfloat[verticesCount];
	normals = new GLfloat[verticesCount];
	textureCoords = new GLfloat[textureCoordsCount];
	indices = new GLuint[indicesCount];
	normalVectors = new glm::vec3[count*2];

}



GLvoid Terrain::createAndFillVertexArrayObject()
{
	shadow = new Shadow();
	terrainShader = &ShaderLoader::terrainShader;
	simpleDepthShader = &ShaderLoader::simpleDepthShader;

	// Create VAO, fill with buffers etc
	helper.createVertexArray(vertexArray);
	helper.fillVertexArrayWithVertices(vertexArray, vertexBuffer, vertices, verticesCount * sizeof(GLfloat), 0);
	helper.fillVertexArrayWithIndices(vertexArray, indexBuffer, indices, indicesCount * sizeof(GLuint));
	helper.fillVertexArrayWithNormals(vertexArray, normalBuffer, normals, verticesCount * sizeof(GLfloat), 1);
	helper.fillVertexArrayWithTextureCoords(vertexArray, textureBuffer, textureCoords, textureCoordsCount * sizeof(GLfloat), 2);

	shadow->createShadowTextureAndFBO();
	shadow->setPositionOfLightForShadow(glm::vec3(700, 700, 700));
	shadow->setPositionOfLightTargetForShadow(glm::vec3(604, 300, 604));
	//404, 300, 404 for terrainShadowWorld
	//shadow->setPositionOfLightForShadow(glm::vec3(1820, 500, 2765));
	//shadow->setPositionOfLightTargetForShadow(glm::vec3(1828, 400, 2771));
	shadow->setNearPlane(1.0f);
	shadow->setFarPlane(500.0f);
	shadow->setOrthoDimension(500.0f);

}

GLvoid Terrain::createTextures()
{
	// texture
	glGenBuffers(1, &snowTexture);
	helper.texture2DSetRepeatAndLinear(snowTexture);
	helper.loadTextureWithImage2D(snowTexture, defaultTextures[0], 0);

	glGenBuffers(1, &rockTexture);
	helper.loadTextureWithImage2D(rockTexture, defaultTextures[1], 1);

	glGenBuffers(1, &grassTexture);
	helper.loadTextureWithImage2D(grassTexture, defaultTextures[2], 2);
}

GLvoid Terrain::bindTextures(GLuint program)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, snowTexture);
	glUniform1i(glGetUniformLocation(program, "snowTexture"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, rockTexture);
	glUniform1i(glGetUniformLocation(program, "rockTexture"), 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, grassTexture);
	glUniform1i(glGetUniformLocation(program, "grassTexture"), 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, shadow->getDepthMapTexture());
	glUniform1i(glGetUniformLocation(program, "shadowMap"), 3);

	glActiveTexture(GL_TEXTURE0);
}

GLvoid Terrain::draw(glm::mat4 view, glm::mat4 projection)
{
	terrainShader->useProgram();
	GLuint program = terrainShader->getProgram();
	glUniform1f(glGetUniformLocation(program, "lightsOn"), 0.0);
	glUniform1f(glGetUniformLocation(program, "shadowsOn"), 0.0);
	glUniformMatrix4fv(glGetUniformLocation(program, "model"), 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniform1f(glGetUniformLocation(program, "repeat"), size / 128);
	glUniform1f(glGetUniformLocation(program, "amplitude"), amplitude);
	bindTextures(program);
	drawSimpleVAO();

}

// Specific uniforms added by Matthew Shaw for shadows
GLvoid Terrain::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow)
{
	shadow->renderDepthSceneToTexture(directionalLight, objectsThatCastShadow);
	//glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	terrainShader->useProgram();
	GLuint program = terrainShader->getProgram();

	glActiveTexture(GL_TEXTURE0);
	glUniformMatrix4fv(glGetUniformLocation(program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(shadow->getLightSpaceMatrix()));
	glUniform3fv(glGetUniformLocation(program, "lightPos"), 1, glm::value_ptr(shadow->getLightPositionForShadow()));
	glUniform1f(glGetUniformLocation(program, "lightsOn"), 1.0);
	glUniform1f(glGetUniformLocation(program, "shadowsOn"), 1.0);
	glUniform1f(glGetUniformLocation(program, "material.shininess"), 32.0f);
	glUniform3f(glGetUniformLocation(program, "viewPos"), cameraPos.x, cameraPos.y, cameraPos.z);

	glUniform3f(glGetUniformLocation(program, "dirLight.direction"), directionalLight.getDirectionOfLight().x, directionalLight.getDirectionOfLight().y, directionalLight.getDirectionOfLight().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.ambient"), directionalLight.getAmbient().x, directionalLight.getAmbient().y, directionalLight.getAmbient().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.diffuse"), directionalLight.getDiffuse().x, directionalLight.getDiffuse().y, directionalLight.getDiffuse().z);
	glUniform3f(glGetUniformLocation(program, "dirLight.specular"), directionalLight.getSpecular().x, directionalLight.getSpecular().y, directionalLight.getSpecular().z);
	glUniform1f(glGetUniformLocation(program, "pointLightListSize"), pointLightList.size());
	//Iterate over all point lights
	for (GLint i = 0; i < pointLightList.size(); i++)
	{
		std::string number = std::to_string(i);
		//printf("Point light positions are X:%2f, Y: %2f, Z: %2f\n", pointLightList.at(i).getPosition().x, pointLightList.at(i).getPosition().y, pointLightList.at(i).getPosition().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].position").c_str()), pointLightList.at(i).getPosition().x, pointLightList.at(i).getPosition().y, pointLightList.at(i).getPosition().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].ambient").c_str()), pointLightList.at(i).getAmbient().x, pointLightList.at(i).getAmbient().y, pointLightList.at(i).getAmbient().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].diffuse").c_str()), pointLightList.at(i).getDiffuse().x, pointLightList.at(i).getDiffuse().y, pointLightList.at(i).getDiffuse().z);
		glUniform3f(glGetUniformLocation(program, ("pointLights[" + number + "].specular").c_str()), pointLightList.at(i).getSpecular().x, pointLightList.at(i).getSpecular().y, pointLightList.at(i).getSpecular().z);
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].constant").c_str()), pointLightList.at(i).getConstant());
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].linear").c_str()), pointLightList.at(i).getLinear());
		glUniform1f(glGetUniformLocation(program, ("pointLights[" + number + "].quadratic").c_str()), pointLightList.at(i).getQuadratic());

	}
	// SpotLight
	glUniform3f(glGetUniformLocation(program, "spotLight.position"), spotLight.getPosition().x, spotLight.getPosition().y, spotLight.getPosition().z);
	//printf("Spotlight position is: X %f, Y %f Z %f", spotLight.getPosition().x, spotLight.getPosition().y, spotLight.getPosition().z);
	glUniform3f(glGetUniformLocation(program, "spotLight.direction"), spotLight.getFront().x, spotLight.getFront().y, spotLight.getFront().z);
	//printf("Front is: X %f, Y %f Z %f", spotLight.getFront().x, spotLight.getFront().y, spotLight.getFront().z);
	glUniform3f(glGetUniformLocation(program, "spotLight.ambient"), spotLight.getAmbient().r, spotLight.getAmbient().g, spotLight.getAmbient().b);
	glUniform3f(glGetUniformLocation(program, "spotLight.diffuse"), spotLight.getDiffuse().r, spotLight.getDiffuse().g, spotLight.getDiffuse().b);
	glUniform3f(glGetUniformLocation(program, "spotLight.specular"), spotLight.getSpecular().r, spotLight.getSpecular().g, spotLight.getSpecular().b);
	glUniform1f(glGetUniformLocation(program, "spotLight.constant"), spotLight.getConstant());
	glUniform1f(glGetUniformLocation(program, "spotLight.linear"), spotLight.getLinear());
	glUniform1f(glGetUniformLocation(program, "spotLight.quadratic"), spotLight.getQuadratic());
	glUniform1f(glGetUniformLocation(program, "spotLight.cutOff"), glm::cos(glm::radians(12.5f)));
	glUniform1f(glGetUniformLocation(program, "spotLight.outerCutOff"), glm::cos(glm::radians(15.0f)));
	glUniformMatrix4fv(glGetUniformLocation(program, "model"), 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniform1f(glGetUniformLocation(program, "repeat"), size / 128);
	glUniform1f(glGetUniformLocation(program, "amplitude"), amplitude);


	bindTextures(program);

	glBindVertexArray(vertexArray);
	glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

glm::vec3 Terrain::getPosition()
{
	return position;
}

GLuint Terrain::getSize()
{
	return size;
}

GLuint Terrain::getAmplitude()
{
	return amplitude;
}

GLuint Terrain::getVertexCount()
{
	return vertexCount;
}

GLuint Terrain::getSeed()
{
	return seed;
}
GLvoid Terrain::setIntersectionSize(GLint intersectionSize)
{
	this->intersectionSize = intersectionSize;
}

/*
 * Might change the design of this to: 
 * Give the y value of a vertex zero
 * Generate noise and pass it as a texture to the vertex array.
*/
GLvoid Terrain::setUpBuffers()
{
	// Possibly split this up into 3 different methods which can all run on a seperate thread
	GLuint vertexLocation = 0;
	//ValueNoise noise(seed, vertexCount, InterpolatedNoise::interpolationMethod::biLinear);
	// Generate vertices / indices
	for (GLuint i = 0; i < vertexCount; i++)
	{

		for (GLuint j = 0; j < vertexCount; j++)
		{

			// Vertices
			// x 
			vertices[vertexLocation * 3] = (GLfloat)j / ((GLfloat)vertexCount - 1) * size;

			// y 
			GLfloat height;
			if (i == 0 || j == 0)
			{
				vertices[vertexLocation * 3 + 1] = 0.0f;
			}
			else if (j / intersectionSize == 0 && i / intersectionSize == 0)
			{
				GLfloat ratioOne = ((GLfloat)j / (GLfloat)intersectionSize);
				GLfloat ratioTwo = ((GLfloat)i / (GLfloat)intersectionSize);

				GLdouble thetaOne = ratioOne * M_PI;
				GLfloat fOne = (1.0f - (GLfloat)cos(thetaOne)) * 0.5f;

				height = fOne * amplitude * noise->getNoise(intersectionSize, i);

				GLdouble thetaTwo = ratioTwo * M_PI;
				GLfloat fTwo = (1.0f - (GLfloat)cos(thetaTwo)) * 0.5f;

				GLfloat heightTwo = fTwo * amplitude * noise->getNoise(j, intersectionSize);

				GLfloat value = (ratioOne * heightTwo + ((1 - ratioOne) * height));

				vertices[vertexLocation * 3 + 1] = value;
			}
			else if (j > vertexCount - 1 - intersectionSize && i / intersectionSize == 0)
			{
				GLfloat ratioOne = (((GLfloat)(vertexCount - 1 - j) / (GLfloat)intersectionSize));
				GLfloat ratioTwo = ((GLfloat)i / (GLfloat)intersectionSize);

				GLdouble thetaOne = ratioOne * M_PI;
				GLfloat fOne = (1.0f - (GLfloat)cos(thetaOne)) * 0.5f;

				height = fOne * amplitude * noise->getNoise(intersectionSize, i);

				GLdouble thetaTwo = ratioTwo * M_PI;
				GLfloat fTwo = (1.0f - (GLfloat)cos(thetaTwo)) * 0.5f;

				GLfloat heightTwo = fTwo * amplitude * noise->getNoise(j, intersectionSize);

				GLfloat value = (ratioOne * heightTwo + ((1 - ratioOne) * height));

				vertices[vertexLocation * 3 + 1] = value;
			}
			else if (i > vertexCount - 1 - intersectionSize && j / intersectionSize == 0)
			{
				GLfloat ratioOne = (((GLfloat)(vertexCount - 1 - i) / (GLfloat)intersectionSize));
				GLfloat ratioTwo = ((GLfloat)j / (GLfloat)intersectionSize);

				GLdouble thetaOne = ratioOne * M_PI;
				GLfloat fOne = (1.0f - (GLfloat)cos(thetaOne)) * 0.5f;

				height = fOne * amplitude * noise->getNoise(intersectionSize, i);

				GLdouble thetaTwo = ratioTwo * M_PI;
				GLfloat fTwo = (1.0f - (GLfloat)cos(thetaTwo)) * 0.5f;

				GLfloat heightTwo = fTwo * amplitude * noise->getNoise(j, intersectionSize);

				GLfloat value = (ratioOne * heightTwo + ((1 - ratioOne) * height));

				vertices[vertexLocation * 3 + 1] = value;
			}
			else if (j > vertexCount - 1 - intersectionSize && i > vertexCount - 1 - intersectionSize)
			{
				GLfloat ratioOne = (((GLfloat)(vertexCount - 1 - i) / (GLfloat)intersectionSize));
				GLfloat ratioTwo = (((GLfloat)(vertexCount - 1 - j) / (GLfloat)intersectionSize));

				GLdouble thetaOne = ratioOne * M_PI;
				GLfloat fOne = (1.0f - (GLfloat)cos(thetaOne)) * 0.5f;

				height = fOne * amplitude * noise->getNoise(intersectionSize, i);

				GLdouble thetaTwo = ratioTwo * M_PI;
				GLfloat fTwo = (1.0f - (GLfloat)cos(thetaTwo)) * 0.5f;

				GLfloat heightTwo = fTwo * amplitude * noise->getNoise(j, intersectionSize);

				GLfloat value = (ratioOne * heightTwo + ((1 - ratioOne) * height));

				vertices[vertexLocation * 3 + 1] = 0;
			}
			else if (j / intersectionSize == 0)
			{
				GLdouble theta = ((GLfloat)j / (GLfloat)intersectionSize) * M_PI;
				GLfloat f = (1.0f - (GLfloat)cos(theta)) * 0.5f;

				height = f * amplitude * noise->getNoise(intersectionSize, i);
				vertices[vertexLocation * 3 + 1] = height;
			}
			else if (i / intersectionSize == 0)
			{
				GLdouble theta = ((GLfloat)i / (GLfloat)intersectionSize) * M_PI;
				GLfloat f = (1.0f - (GLfloat)cos(theta)) * 0.5f;

				height = f * amplitude * noise->getNoise(j, intersectionSize);
				vertices[vertexLocation * 3 + 1] = height;
			}
			else if (j > vertexCount - 1 - intersectionSize)
			{
				GLdouble theta = (((GLfloat)(vertexCount - 1 - j) / (GLfloat)intersectionSize)) * M_PI;
				GLfloat f = (1.0f - (GLfloat)cos(theta)) * 0.5f;

				height = f * amplitude * noise->getNoise(vertexCount - intersectionSize - 1 , i);
				vertices[vertexLocation * 3 + 1] = height;
			}
			else if (i > vertexCount - 1 - intersectionSize)
			{
				GLdouble theta = (((GLfloat)(vertexCount - 1 - i) / (GLfloat)intersectionSize)) * M_PI;
				GLfloat f = (1.0f - (GLfloat)cos(theta)) * 0.5f;

				height = f * amplitude * noise->getNoise(j, vertexCount - intersectionSize - 1);
				vertices[vertexLocation * 3 + 1] = height;
			}
			else
			{
				GLfloat value = noise->getNoise(j, i);
				value *= amplitude;
				height = value;
				vertices[vertexLocation * 3 + 1] = value;
			}
		
			// z
			vertices[vertexLocation * 3 + 2] = (GLfloat )i / ((GLfloat )vertexCount - 1) * size;

			// Generate normals
			//glm::vec3 normal = generateNormal(j, i);
			normals[vertexLocation * 3] = 0.0f;
			normals[vertexLocation * 3 + 1] = 1.0f;
			normals[vertexLocation * 3 + 2] = 0.0f;

			// texture coords
			textureCoords[vertexLocation * 2] = (GLfloat )j / ((GLfloat )vertexCount - 1);
			textureCoords[vertexLocation * 2 + 1] = (GLfloat )i / ((GLfloat )vertexCount - 1);

			vertexLocation++;
		}
	}

	//recording heights in a multidimensional array
	heights = new GLfloat*[vertexCount];
	for (GLint  rowNumber = 0; rowNumber < vertexCount; rowNumber++)
	{
		heights[rowNumber] = new GLfloat[vertexCount];
		for (GLint  columnNumber = 0; columnNumber < vertexCount; columnNumber++)
		{
			heights[rowNumber][columnNumber] = vertices[(columnNumber * vertexCount * 3) + (rowNumber * 3) + 1];
		}
	}


/*
 * Andrew Woods
*/
	//generating normals
	GLuint pointer = 0;
	GLuint triangleIndex = 0;
	for (GLuint zIncrement = 0; zIncrement < vertexCount - 1; zIncrement++)
	{
		for (GLuint xIncrement = 0; xIncrement < vertexCount - 1; xIncrement++)
		{
			//4 points of a quad
			GLuint bottomRight = (zIncrement * (vertexCount)) + xIncrement;
			GLuint bottomLeft = bottomRight + 1;
			GLuint topRight = ((zIncrement + 1) * (vertexCount)) + xIncrement;
			GLuint topLeft = topRight + 1;

			indices[pointer++] = bottomRight;
			indices[pointer++] = topRight;
			indices[pointer++] = topLeft;
			indices[pointer++] = topLeft;
			indices[pointer++] = bottomRight;
			indices[pointer++] = bottomLeft;


			//bottomRight
			glm::vec3 p1 = glm::vec3(vertices[bottomRight * 3], vertices[bottomRight * 3 + 1], vertices[bottomRight * 3 + 2]);

			//topRight
			glm::vec3 p2 = glm::vec3(vertices[topRight * 3], vertices[topRight * 3 + 1], vertices[topRight * 3 + 2]);

			//topLeft
			glm::vec3 p3 = glm::vec3(vertices[topLeft * 3], vertices[topLeft * 3 + 1], vertices[topLeft * 3 + 2]);

			//bottomLeft
			glm::vec3 p4 = glm::vec3(vertices[bottomLeft * 3], vertices[bottomLeft * 3 + 1], vertices[bottomLeft * 3 + 2]);

			//TRIANGLE 1 NORMALS
			normalVectors[triangleIndex] = generateNormal(p1, p2, p3);
			triangleIndex++;

			//TRIANGLE 2 NORMALS
			normalVectors[triangleIndex] = generateNormal(p3, p4, p1);
			triangleIndex++;
		}
	}
}

glm::vec3 Terrain::generateNormal(glm::vec3 trianglePoint1, glm::vec3 trianglePoint2, glm::vec3 trianglePoint3)
{	
	// point 2 - point 1
	glm::vec3 side1 = trianglePoint2 - trianglePoint1;
	
	// point 3 - point 1 
	glm::vec3 side2 = trianglePoint3 - trianglePoint1;
	
	//calculating the cross product
	glm::vec3 normalVector;
	normalVector.x = (side1.y * side2.z) - (side1.z * side2.y);
	normalVector.y = (side1.z * side2.x) - (side1.x * side2.z);
	normalVector.z = (side1.x * side2.y) - (side1.y * side2.x);

	//normalizing so all x,y and z only add up to 1.0 and it doesn't increase the force's magnitude
	glm::vec3 scaledNormalVector;
	scaledNormalVector.x = normalVector.x / (fabs(normalVector.x) + fabs(normalVector.y) + fabs(normalVector.z));
	scaledNormalVector.y = normalVector.y / (fabs(normalVector.x) + fabs(normalVector.y) + fabs(normalVector.z));
	scaledNormalVector.z = normalVector.z / (fabs(normalVector.x) + fabs(normalVector.y) + fabs(normalVector.z));

	return scaledNormalVector;
}

//used with methods in collision and movement to find the point of collision on a spot of terrain
GLfloat Terrain::getTerrainCollisionCoordinates(GameObject* gameObject)
{
		GLfloat terrainHeight = getTerrainHeight(gameObject);
		gameObject->movement->setYPosition(terrainHeight);
		gameObject->movement->setRadius(gameObject->getRadius());
		return terrainHeight;
}

//method to return all of the normalVectors should they be needed
glm::vec3* Terrain::getNormalVectors()
{
	return normalVectors;
}

//grabbing a single normal vector for a triangle
glm::vec3 Terrain::getSingleNormalVector(GLint  index)
{
	return normalVectors[index];
}

//For ball only, as it sets a height value thats only needed for ball physics calculations
//Same method is below that can be used to just get the height at a given xPosition and zPosition
GLfloat Terrain::getTerrainHeight(GameObject* obj)
{
	GLfloat x = obj->getPosition().x - position.x;
	GLfloat z = obj->getPosition().z - position.z;
	GLfloat arraySize = ((GLfloat)vertexCount - 1.0f);
	GLfloat gridSquareSize = (GLfloat)(size /arraySize);
	GLuint gridX = floor(x / gridSquareSize);
	GLuint gridZ = floor(z / gridSquareSize);
	if (gridX >= arraySize || gridZ >= arraySize || gridX < 0 || gridZ < 0)
	{
		return 0.0f;
	}
	//finding where on gridSquare the player is
	GLfloat xCoord = (GLfloat)(((GLuint)x % (GLuint)gridSquareSize) / gridSquareSize);
	GLfloat zCoord = (GLfloat)(((GLuint)z % (GLuint)gridSquareSize) / gridSquareSize);
	GLuint index;
	GLfloat answer;
	if (xCoord <= (1 - zCoord))
	{
		answer = barryCentric(glm::vec3(0.0f, heights[gridX][gridZ], 0.0f),
							  glm::vec3(1.0f, heights[gridX + 1][gridZ], 0.0f),
							  glm::vec3(0.0f, heights[gridX][gridZ + 1], 1.0f),
							  xCoord, 
							  zCoord);
		index = ((gridX * 2) + (512 * gridZ));
		obj->movement->setYPosition(answer);
	}
	else
	{
		answer = barryCentric(glm::vec3(1.0f, heights[gridX + 1][gridZ], 0.0f),
							  glm::vec3(1.0f, heights[gridX + 1][gridZ + 1], 1.0f),
							  glm::vec3(0.0f, heights[gridX][gridZ + 1], 1.0f),
							  xCoord,
							  zCoord);
		index = ((gridX * 2) + (512 * gridZ) + 1);
		obj->movement->setYPosition(answer);
	}
	obj->movement->setNormal(getSingleNormalVector(index));
	return answer;
}

//for use by other methods to grab the z coordinate corresponding to their X and Z coordinates
GLfloat Terrain::getTerrainHeight(GLfloat xPos, GLfloat zPos)
{
	GLfloat x = xPos - position.x;
	GLfloat z = zPos - position.z;
	GLfloat arraySize = ((GLfloat)vertexCount - 1.0f);
	GLfloat gridSquareSize = (GLfloat)(size / arraySize);
	GLuint gridX = floor(x / gridSquareSize);
	GLuint gridZ = floor(z / gridSquareSize);
	if (gridX >= arraySize || gridZ >= arraySize || gridX < 0 || gridZ < 0)
	{
		return 0.0f;
	}
	//finding where on gridSquare the player is
	GLfloat xCoord = (GLfloat)(((GLuint)x % (GLuint)gridSquareSize) / gridSquareSize);
	GLfloat zCoord = (GLfloat)(((GLuint)z % (GLuint)gridSquareSize) / gridSquareSize);
	GLuint index;
	GLfloat answer;
	if (xCoord <= (1 - zCoord))
	{
		answer = barryCentric(glm::vec3(0.0f, heights[gridX][gridZ], 0.0f),
			glm::vec3(1.0f, heights[gridX + 1][gridZ], 0.0f),
			glm::vec3(0.0f, heights[gridX][gridZ + 1], 1.0f),
			xCoord,
			zCoord);
		index = ((gridX * 2) + (512 * gridZ));
	}
	else
	{
		answer = barryCentric(glm::vec3(1.0f, heights[gridX + 1][gridZ], 0.0f),
			glm::vec3(1.0f, heights[gridX + 1][gridZ + 1], 1.0f),
			glm::vec3(0.0f, heights[gridX][gridZ + 1], 1.0f),
			xCoord,
			zCoord);
		index = ((gridX * 2) + (512 * gridZ) + 1);
	}
	return answer;
}

//an algorithm i researched online to find a specific spot on a 2d triangle
GLfloat Terrain:: barryCentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, GLfloat xPos, GLfloat zPos)
{
	GLfloat  det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
	GLfloat  l1 = ((p2.z - p3.z) * (xPos - p3.x) + (p3.x - p2.x) * (zPos - p3.z)) / det;
	GLfloat  l2 = ((p3.z - p1.z) * (xPos - p3.x) + (p1.x - p3.x) * (zPos - p3.z)) / det;
	GLfloat  l3 = 1.0f - l1 - l2;
	return l1 * p1.y + l2 * p2.y + l3 * p3.y;
}