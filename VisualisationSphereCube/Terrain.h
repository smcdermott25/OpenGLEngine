#pragma once
#include <glew.h>
#include <iostream>
#include <map>
#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "info.h"
#include "Shader.h"
#include "HeightGenerator.h"
#include "NoiseType.h"

// Different types of Noise
#include "ValueNoise.h"
#include "FractalNoise.h"
#include "DirectionalLight.h"
#include "MultiLightCube.h"
#include "Shadow.h"
#include "ShadowCastingObject.h"

/*
 * Texture used is determined by the height of a particular vertex.
 * They are static to allow every terrain object to refer to same textures in memory instead of
 * creating the same textures and sending them to the gpu for every terrain object.
*/
static GLuint snowTexture;
static GLuint grassTexture;
static GLuint rockTexture;
static GLuint waterTexture;

class Terrain : public ShadowCastingObject
{
public:
	// Using pointer for the vertices etc to stop stack overflow
	GLfloat size;
	GLfloat *vertices;
	GLfloat *XCoords;
	GLfloat *YCoords;
	GLfloat *ZCoords;
	GLfloat *normals;
	GLfloat **heights;
	glm::vec3 *normalVectors;
	glm::vec3 *triangleCentroidCoordinates;
	GLfloat *textureCoords;
	GLuint  *indices;

	//glm::vec3 closeTerrain[65000];
	GLint intersectionSize;

	// seed -> Used during the calculating of height values 
	GLint seed;
	GLint amplitude;
	glm::vec3 position;

	GLuint vertexCount;
	GLuint count;
	GLuint verticesCount;
	GLuint textureCoordsCount;
	GLuint indicesCount;
	GLuint noOfTriangles;

	GLuint vertexArray;
	GLuint vertexBuffer;
	GLuint indexBuffer;
	GLuint textureBuffer;
	GLuint normalBuffer;

	Shadow* shadow;

	// This will only be set if a heightmap is used instead of generating itself using the noise classes
	GLuint heightMap;
	Shader* simpleDepthShader;
	glm::mat4 model;

	Shader* terrainShader;
	Noise* noise;

	GLvoid init();
	GLvoid createBuffers();
	GLvoid setUpBuffers();
	GLvoid bindTextures(GLuint program);

	GLchar* defaultTextures[3]{ "Texture/terrainTextures/ground.png","Texture/terrainTextures/paving.jpg","Texture/terrainTextures/grass.jpg"};
	//std::vector<PointLight> pointLightList;
	//glm::vec3 generateNormal(GLint x, GLint z);
	glm::vec3 Terrain::generateNormal(glm::vec3 trianglePoint1, glm::vec3 trianglePoint2, glm::vec3 trianglePoint3);
	//glm::vec3 Terrain::generateTriangleCentroid(glm::vec3 trianglePoint1, glm::vec3 trianglePoint2, glm::vec3 trianglePoint3);

public:
	Terrain(glm::vec3 position, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size, Noise* noise);
	Terrain(glm::vec3 position, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size, Noise* noise, GLchar* textures[3]);
	
	~Terrain();
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow);
	GLvoid Terrain::draw(glm::mat4 view, glm::mat4 projection);

	glm::vec3 getPosition();
	GLvoid drawSimpleVAO();
	GLuint getSize();
	GLuint getAmplitude();
	GLuint getVertexCount();
	GLuint getSeed();

	GLvoid setIntersectionSize(GLint intersectionSize);
	GLvoid createAndFillVertexArrayObject();
	GLvoid createTextures();

	//GLvoid  Terrain::updatePointLightList(std::vector<PointLight> &allPointLights);
	//std::vector<PointLight> Terrain::calculateClosestLightsToObject(std::vector<PointLight> &objectList, GLint numberOfObjectsToReturn);
	GLfloat Terrain::getTerrainCollisionCoordinates(GameObject* gameObject);
	glm::vec3* getNormalVectors();
	glm::vec3 Terrain::getSingleNormalVector(GLint index);
	GLfloat Terrain::getTerrainHeight(GameObject* obj);
	GLfloat Terrain::getTerrainHeight(GLfloat xPos, GLfloat zPos);
	GLfloat Terrain::barryCentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, GLfloat xPos, GLfloat zPos);
};


