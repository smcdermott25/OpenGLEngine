#include "TerrainGenerator.h"

TerrainGenerator::TerrainGenerator()
{
	defaultNoise = new FractalNoise(log2(vertexCount), 1, 0.50);
	defaultPosition = glm::vec3(0, 0, 0);
}

TerrainGenerator::TerrainGenerator(glm::vec3 positions, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size)
{
	defaultNoise = new FractalNoise(log2(vertexCount), seed, 0.50);
	init(positions, seed, amplitude, vertexCount, size);
}

TerrainGenerator::TerrainGenerator(Noise* noise, glm::vec3 positions, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size)
{
	defaultNoise = noise;
	init(positions, seed, amplitude, vertexCount, size);
}

TerrainGenerator::~TerrainGenerator()
{
	if (yThread != nullptr)
	{
		yThread->join();
	}

	for (GLint i = 0; i < terrains.size(); i++)
	{
		delete(terrains.at(i));
	}

	delete(defaultNoise);
}

GLvoid TerrainGenerator::init(glm::vec3 positions, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size)
{
	this->vertexCount = vertexCount;
	this->size = size;
	this->amplitude = amplitude;
	defaultNoise = new FractalNoise(log2(vertexCount), seed, 0.50);
	terrain = new Terrain(positions, seed, amplitude, vertexCount, size, defaultNoise);
	terrain->createAndFillVertexArrayObject();
	terrain->createTextures();
	terrains.push_back(terrain);
	
	grass = new GrassSystem(positions, size, size);

	numTerrains++;
	seedMap[std::make_pair(0, 0)] = terrain;
}

//Set the heights of terrain components
GLvoid TerrainGenerator::setHeights(GLint index)
{
	//index = 0;
	std::vector<Orientation>* matrices = grass->getgrassModelOrientations();
	for (GLuint i = 0; i < matrices->size(); i++)
	{	
		matrices->at(i).setPositionY(terrains.at(index)->getTerrainHeight(matrices->at(i).getPosition().x, matrices->at(i).getPosition().z));
	}
	matrices = grass->getgrassBillboardOrientations();
	for (GLuint i = 0; i < matrices->size(); i++)
	{
		matrices->at(i).setPositionY(terrains.at(index)->getTerrainHeight(matrices->at(i).getPosition().x, matrices->at(i).getPosition().z));
	}

	std::vector<Model*>* imposterableModels = grass->getImposterableObjects();
	for (GLuint i = 0; i < imposterableModels->size(); i++)
	{
		imposterableModels->at(i)->orientation->setPositionY(getCurrentTerrain(grass->position)->getTerrainHeight(imposterableModels->at(i)->orientation->getPosition().x, imposterableModels->at(i)->orientation->getPosition().z));
	}
}

GLvoid TerrainGenerator::checkSeedMap(std::pair<GLint, GLint> seed)
{
	if (seedMap.find(seed) == seedMap.end() || yFinished)
	{
		if (yFinished)
		{
			yThread->join();
			numTerrains++;
			terrains.at(numTerrains - 1)->createAndFillVertexArrayObject();
			delete(yThread);
			yThread = nullptr;
			yFinished = false;

			setHeights(numTerrains - 1);
			grass->InitialUpdate();
		}
		else if (yThread == nullptr && !yFinished)
		{
			yThread = new std::thread(&TerrainGenerator::makeNewTerrain, this, seed, numTerrains);
		}
	}
}

GLvoid TerrainGenerator::removeTerrain(std::pair<GLint, GLint> seed)
{
	if (seedMap.find(seed) != seedMap.end())
	{
		delete(seedMap.at(seed));
	}
}

GLvoid TerrainGenerator::makeNewTerrain(std::pair<GLint, GLint> seed, GLint numTerrains)
{
	delete(defaultNoise);
	srand(seed.first * seed.second);
	GLint seedValue = rand() * 2342;
	std::cout << seedValue << std::endl;
	defaultNoise = new FractalNoise(log2(vertexCount), seedValue, 0.50);
	Terrain* temp = nullptr;	
	
	glm::vec3 corner = glm::vec3(seed.first * size, 0, seed.second * size);
	temp = new Terrain(corner, seedValue, amplitude, (GLint)vertexCount, size, defaultNoise);
	
	terrains.push_back(temp);
	seedMap[seed] = temp;

	grass->addComponents(terrains.at(numTerrains)->position, size, size);

	yFinished = true;
}

GLvoid TerrainGenerator::makeCorners(std::pair<GLint, GLint> seed, GLint numTerrains)
{
	
}

GLvoid TerrainGenerator::update(GLfloat deltaTime, ThirdPersonCamera camera, glm::vec3 playerPosition)
{
	GLint xPosition = playerPosition.x;
	GLint zPosition = playerPosition.z;

	if (xPosition < 0)
	{
		xPosition -= size;
	}

	if (zPosition < 0)
	{
		zPosition -= size;
	}

	GLint seedX = xPosition / size;
	GLint seedZ = zPosition / size;

	for (GLint i = -1; i < 2; i++)
	{
		// X axis tiles
		std::pair<GLint, GLint> seed = std::make_pair(seedX + i, seedZ);
		checkSeedMap(seed);

		// Z axis tiles
		seed = std::make_pair(seedX, seedZ + i);
		checkSeedMap(seed);

		// Corners
		seed = std::make_pair(seedX + i, seedZ + i);
		checkSeedMap(seed);

		seed = std::make_pair(seedX + i, seedZ - i);
		checkSeedMap(seed);
	}
}

GLvoid TerrainGenerator::update(GLfloat deltaTime, Camera camera)
{

	grass->update(camera.getView(), camera.getProjection(), camera.getCameraPosition());
}

GLvoid TerrainGenerator::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition)
{
	for (GLuint i = 0; i < numTerrains; i++)
	{
		terrains.at(i)->draw(view, projection);
	}

	grass->draw(view, projection, cameraPosition);
}

GLvoid TerrainGenerator::draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow)
{
	for (GLuint i = 0; i < numTerrains; i++)
	{
		terrains.at(i)->draw(view, projection, cameraPos, directionalLight, pointLightListExtra, spotLight, objectsThatCastShadow);
	}
}

Terrain* TerrainGenerator::getCurrentTerrain(glm::vec3 objPosition)
{
	GLint xPosition = objPosition.x;
	GLint zPosition = objPosition.z;

	if (xPosition < 0)
	{
		xPosition -= size;
	}

	if (zPosition < 0)
	{
		zPosition -= size;
	}

	GLint seedX = xPosition / size;
	GLint seedZ = zPosition / size;

	std::pair<GLint, GLint> seed = std::make_pair(seedX, seedZ);

	if (seedMap.find(seed) != seedMap.end())
	{
		return seedMap[seed];
	}
	else
	{
		return nullptr;
	}
}

// Methods used before the new noise system
GLvoid TerrainGenerator::redoPosition(glm::vec3 positions)
{
	//GLuint seed = terrain->getSeed();
	//GLuint amplitude = terrain->getAmplitude();
	//GLuint vertexCount = terrain->getVertexCount();
	//GLuint size = terrain->getSize();
	//delete(terrain);
	//terrain = new Terrain(positions, seed, amplitude, vertexCount, size, defaultNoise);
}

GLvoid TerrainGenerator::redoAmplitude(GLuint amplitude)
{
	//glm::vec3 positions = terrain->getPosition();
	//GLuint seed = terrain->getSeed();
	//GLuint vertexCount = terrain->getVertexCount();
	//GLuint size = terrain->getSize();
	//delete(terrain);
	//terrain = new Terrain(positions, seed, amplitude, vertexCount, size, defaultNoise);
}

GLvoid TerrainGenerator::redoVertexCount(GLuint vertexCount)
{
	//glm::vec3 positions = terrain->getPosition();
	//GLuint seed = terrain->getSeed();
	//GLuint amplitude = terrain->getAmplitude();
	//GLuint size = terrain->getSize();
	//delete(terrain);
	//terrain = new Terrain(positions, seed, amplitude, vertexCount, size, defaultNoise);
}

GLvoid TerrainGenerator::redoSize(GLuint size)
{
	//glm::vec3 positions = terrain->getPosition();
	//GLuint seed = terrain->getSeed();
	//GLuint amplitude = terrain->getAmplitude();
	//GLuint vertexCount = terrain->getVertexCount();
	//delete(terrain);
	//terrain = new Terrain(positions, seed, amplitude, vertexCount, size, defaultNoise);
}

GLvoid TerrainGenerator::redo(glm::vec3 positions, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size)
{
	//delete(terrain);
	//terrain = new Terrain(positions, seed, amplitude, vertexCount, size, defaultNoise);
}