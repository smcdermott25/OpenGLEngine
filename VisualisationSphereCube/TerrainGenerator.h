#pragma once
#include <iostream>
#include <map>
#include <thread>
#include <functional>
#include <utility>

#include <glew.h>
#include <glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "NoiseType.h"
#include "Terrain.h"
#include "Mountain.h"
#include "ThirdPersonCamera.h"
#include "Camera.h"
#include "info.h"
#include "GrassSystem.h"

/*
 * Class used to generate terrrain throughout the game world.
*/
class TerrainGenerator
{
private:
	Noise* defaultNoise;

	std::vector<Terrain*> terrains;
	std::map<Terrain, GLint> terrainMap;
    std::map<std::pair<GLint, GLint>, Terrain*> seedMap;

	GrassSystem* grass;

	GLint vertexCount = 0;
	GLint size = 0;
	GLint amplitude;
	GLint numTerrains = 0;

	glm::vec3 defaultPosition;

	// Hoping volatile has same effect as Java
	// Read somewhere it does if your using VC++ 
	volatile GLboolean  yFinished = false;
	volatile GLboolean  grassFinished = false;

	std::thread* yThread = nullptr;
	std::thread* cornersThread = nullptr;

	GLvoid checkSeedMap(std::pair<GLint, GLint> seed);
	GLvoid removeTerrain(std::pair<GLint, GLint> seed);
	GLvoid makeNewTerrain(std::pair<GLint, GLint> seed, GLint mark);
	GLvoid makeCorners(std::pair<GLint, GLint> seed, GLint mark);

public:
	Terrain* terrain;
	TerrainGenerator::TerrainGenerator();

	TerrainGenerator::TerrainGenerator(glm::vec3 positions, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size);
	TerrainGenerator::TerrainGenerator(Noise* noise, glm::vec3 positions, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size);
	TerrainGenerator::~TerrainGenerator();

	GLvoid init(glm::vec3 positions, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size);
	GLvoid setHeights(GLint index);

	GLvoid redoPosition(glm::vec3 positions);
	GLvoid redoAmplitude(GLuint amplitude);
	GLvoid redoVertexCount(GLuint vertexCount);
	GLvoid redoSize(GLuint size);
	GLvoid redo(glm::vec3 positions, GLint seed, GLint amplitude, GLuint vertexCount, GLuint size);

	GLvoid update(GLfloat deltaTime, ThirdPersonCamera camera, glm::vec3 playerPosition);
	GLvoid update(GLfloat deltaTime, Camera camera);
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPos, DirectionalLight directionalLight, std::vector<PointLight> &pointLightListExtra, SpotLight spotLight, std::vector<std::shared_ptr<ShadowCastingObject>> &objectsThatCastShadow);
	GLvoid draw(glm::mat4 view, glm::mat4 projection, glm::vec3 cameraPosition);
	
	Terrain* getCurrentTerrain(glm::vec3 objPosition);
};