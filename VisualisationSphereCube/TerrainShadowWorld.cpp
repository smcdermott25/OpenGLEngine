#include "TerrainShadowWorld.h"

TerrainShadowWorld::TerrainShadowWorld()
{
	camera.setCameraPosition(glm::vec3(50 * 4, 300, 50 * 4));
	terrainGenerator = new TerrainGenerator(glm::vec3(0, 0, 0), 10000000, 700, 257, 3200);
	flatGround = new FlatGround();
	directionalLight = DirectionalLight(glm::vec4(-0.2f, -1.0f, -0.3f, 0.0));
	for (GLint  i = 0; i < 1; i++)
	{
		multiLightCubeList.push_back(MultiLightCube());
		multiLightCubeList.at(i).setPosition(glm::vec3((i + 10) * 4, (i * 10) + 230, (i + 100) * 4));
		multiLightCubeList.at(i).setScale(glm::vec3(50, 50, 50));
	}
	for (GLint  i = 0; i < 1; i++)
	{
		shadowCastingObjectList.push_back(std::make_shared<MultiLightCube>(multiLightCubeList.at(i)));
	}

	pointLightList.reserve(20);
	for (GLint  i = 0; i < 5; i++)
	{
		PointLight pointLight = PointLight(glm::vec3(20.0, 0.0, 1.0));
		pointLight.setPosition(glm::vec3(glm::vec3((i + 95) * 4, (i * 10) + 250, (i + 100) * 4)));
		pointLightList.push_back(pointLight);
	}
	pointLightList.at(0).setColourEasy(glm::vec3(0.0, 5.0, 0.0));
	pointLightList.at(1).setColourEasy(glm::vec3(0.0, 5.0, 5.0));
	pointLightList.at(2).setColourEasy(glm::vec3(5.0, 0.0, 0.0));
	pointLightList.at(3).setColourEasy(glm::vec3(0.0, 0.0, 5.0));
	pointLightList.at(4).setPosition(glm::vec3(420, 1000, 400));
	pointLightList.at(4).setColourEasy(glm::vec3(5.0, 0.0, 0.0));

	mySpotlight = SpotLight();
	mySpotlight.setPosition(glm::vec3(385, 250, 400));
	mySpotlight.setTarget(glm::vec3(420, 250, 404));
	mySpotlight.setColourEasy(glm::vec3(1000.0, 0.0, 0.0));

	terrainGenerator->terrain->updatePointLightList(pointLightList, 2, glm::vec3(2000,2000,2000));
	for (GLint  i = 0; i < shadowCastingObjectList.size(); i++)
	{
		shadowCastingObjectList.at(i)->updatePointLightList(pointLightList);
	}
	terrainGenerator->terrain->shadow->setPositionOfLightForShadow(glm::vec3(420, 350, 400));
	terrainGenerator->terrain->shadow->setPositionOfLightTargetForShadow(glm::vec3(424, 300, 404));
	terrainGenerator->terrain->shadow->setNearPlane(1.0f);
	terrainGenerator->terrain->shadow->setFarPlane(500.0f);
	terrainGenerator->terrain->shadow->setOrthoDimension(500.0f);
}


TerrainShadowWorld::~TerrainShadowWorld()
{
}


GLvoid TerrainShadowWorld::update(GLfloat deltaTime)
{
	terrainGenerator->update(deltaTime, camera);
	//This moves a cube along to move it's shadow along
	//shadowCastingObjectList.at(1)->setPosition(glm::vec3(shadowCastingObjectList.at(0)->getPosition().x - 0.005, shadowCastingObjectList.at(1)->getPosition().y, shadowCastingObjectList.at(1)->getPosition().z - 0.01));
	camera.move(deltaTime);
}

GLvoid TerrainShadowWorld::draw()
{
	glm::mat4 view = camera.getView();
	glm::mat4 projection = camera.getProjection();


	for (GLint  i = 0; i < pointLightList.size(); i++)
	{
		pointLightList.at(i).draw(camera.getView(), camera.getProjection());
	}
	for (GLint  i = 0; i < shadowCastingObjectList.size(); i++)
	{
		shadowCastingObjectList.at(i)->draw(view, projection, camera.getCameraPosition(), directionalLight, pointLightList, mySpotlight, shadowCastingObjectList);
	}
	terrainGenerator->draw(view, projection, camera.getCameraPosition(), directionalLight, pointLightList, mySpotlight, shadowCastingObjectList);
	glDepthFunc(GL_LEQUAL);
	skyBox.draw(view,projection);
	glDepthFunc(GL_LESS);
}

GLvoid TerrainShadowWorld::input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode)
{
	if (key == GLFW_KEY_L && action == GLFW_RELEASE)
	{
		lines = !lines;
		if (lines)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	if (key == GLFW_KEY_G && action == GLFW_RELEASE)
	{
		depthBufferRender = !depthBufferRender;
	}
	if (key == GLFW_KEY_R)
	{
		terrainGenerator->redoSize(size *= 2);
	}

	if (key == GLFW_KEY_F)
	{
		terrainGenerator->redoSize(size /= 2);
	}

	if (key == GLFW_KEY_I)
	{
		terrainGenerator->redoVertexCount(vertexCount *= 2);
	}

	if (key == GLFW_KEY_K)
	{
		terrainGenerator->redoVertexCount(vertexCount /= 2);
	}

	if (action == GLFW_PRESS)
	{
		camera.setKey(key, true);
	}

	if (action == GLFW_RELEASE)
	{
		camera.setKey(key, false);
	}

	if (key == GLFW_KEY_LEFT_SHIFT)
	{
		camera.addCameraSpeed(100);
	}

	if (key == GLFW_KEY_DOWN)
	{
		camera.addCameraSpeed(-100);
	}
}

GLvoid TerrainShadowWorld::mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods)
{
	if (action == GLFW_PRESS)
	{
		camera.setKey(button, true);
	}

	if (action == GLFW_RELEASE)
	{
		camera.setKey(button, false);
	}
}

GLvoid TerrainShadowWorld::mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
{
	camera.updateFront(xPosition, yPosition);
}

GLvoid TerrainShadowWorld::scrollInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
{
}


GLvoid TerrainShadowWorld::RenderQuad()
{
	if (quadVAO == 0)
	{
		GLfloat quadVertices[] = {
			// Positions        // Texture Coords
			-1.0f,  1.0f, 0.0f,  0.0f, 1.0f,
			-1.0f, -1.0f, 0.0f,  0.0f, 0.0f,
			1.0f,  1.0f, 0.0f,  1.0f, 1.0f,
			1.0f, -1.0f, 0.0f,  1.0f, 0.0f,
		};
		// Setup plane VAO
		glGenVertexArrays(1, &quadVAO);
		glGenBuffers(1, &quadVBO);
		glBindVertexArray(quadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	}
	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
}