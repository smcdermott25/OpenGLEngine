#pragma once

#include "GameWorld.h"
#include "info.h"
#include "Shader.h"

#include "Camera.h"
#include "SkyBox.h"
#include "TerrainGenerator.h"
#include "FlatGround.h"
#include "PostProcess.h"
#include "PBR.h"
/*World used for testing shadow on terrain*/
class TerrainShadowWorld : public GameWorld
{
private:
	Camera camera;
	SkyBox skyBox;
	FlatGround* flatGround;
	TerrainGenerator* terrainGenerator;
	GLboolean depthBufferRender = false;
	GLboolean lines = false;
	GLuint quadVAO = 0;
	GLuint quadVBO;
	GLuint size = 3200;
	GLuint vertexCount = 4;
	PostProcess postprocess;

	DirectionalLight directionalLight;
	std::vector<PointLight> pointLightList;
	std::vector<std::shared_ptr<ShadowCastingObject>> shadowCastingObjectList;
	std::vector<MultiLightCube> multiLightCubeList;
	SpotLight mySpotlight;

public:
	TerrainShadowWorld();
	~TerrainShadowWorld();
	GLvoid RenderQuad();
	GLvoid update(GLfloat deltaTime);
	GLvoid draw();

	GLvoid input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode);
	GLvoid mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods);
	GLvoid mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition);
	GLvoid scrollInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition);
};

