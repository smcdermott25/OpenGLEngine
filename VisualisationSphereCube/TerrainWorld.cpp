#include "TerrainWorld.h"

/*
 Create required objects for this game world,
 terrainGenerator -> manages creation / deletion of Terrain objects
 player -> This instance will control this player
 tCam -> Third person camera
*/

TerrainWorld::TerrainWorld()
	:terrainGenerator(glm::vec3(0, 0, 0), 10000000, 500, 128, 4000),
	player("models/ArmyPilot/ArmyPilot.dae", terrainGenerator.terrain, 0),
	tCam{player}
{
	//players.resize(10);
	camera.setCameraPosition(glm::vec3(50 * 4, 300, 50 * 4));
	flatGround = new FlatGround();
	player.model->orientation->addPosition(glm::vec3(0, 10, -80));
	globalControls = Controls::getInstance();
}


TerrainWorld::~TerrainWorld()
{
	closeClient();
	delete(playerClient);

	for (GLint i = 0; i < players.size(); i++)
	{
		delete(players.at(i));
	}

	delete(flatGround);
}

// The instance of this application will act as a Client 
// if this method is called
GLvoid TerrainWorld::initClient()
{
	playerClient = new Client();
	playerClient->startLoop();
}

GLvoid TerrainWorld::closeClient()
{
	playerClient->closeConnection();
}

GLvoid TerrainWorld::update(GLfloat deltaTime)
{
	if (!serverSetUp)
	{
		// Make this instance a Client
		if (Controls::getInstance()->keys[GLFW_KEY_9])
		{
			initClient();
			Controls::getInstance()->keys[GLFW_KEY_9] = false;
			serverSetUp = true;
			client = true;

			// Client connected -> push back new player
			players.push_back(new Player("models/ArmyPilot/ArmyPilot.dae", terrainGenerator.terrain, 1));
		}
	}

	// Client
	if (client)
	{
		if (num < playerClient->getNumClients())
		{
			// Player has connected to the server
			players.push_back(new Player("models/ArmyPilot/ArmyPilot.dae", terrainGenerator.terrain, 1));
			num++;
		}

		playerClient->setModel(player.model->orientation->getModelMatrix());
	}

	// Update third person camera
	tCam.move();

	// Update terrain
	terrainGenerator.update(deltaTime, tCam, player.model->orientation->getPosition());

	// Update player of this instance 
	player.update(deltaTime);

	Terrain* temp = terrainGenerator.getCurrentTerrain(player.model->orientation->getPosition());
	if (temp != nullptr)
	{
		player.updateTerrain(temp);
	}

	// Update Client players animations etc
	for (GLint i = 0; i < players.size(); i++)
	{
		players.at(i)->update(deltaTime);
	}
}

GLvoid TerrainWorld::draw()
{
	glm::mat4 view = tCam.getView();
	glm::mat4 projection = tCam.getProjection();

	//flatGround->draw(view, projection, camera.getCameraPosition());
	terrainGenerator.draw(view, projection, camera.getCameraPosition());

	player.draw(view, projection);
	if (serverSetUp && client)
	{
		for (GLint i = 0; i < num; i++)
		{
			if (i != playerClient->getId())
				players.at(i)->drawClient(view, projection, playerClient->getModel(i));
		}
	}

	glDepthFunc(GL_LEQUAL);
	skyBox.draw(view, projection);
	glDepthFunc(GL_LESS);
}

GLvoid TerrainWorld::input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode)
{
	if (key == GLFW_KEY_ESCAPE)
	{
		exit(1);
	}

	if (key == GLFW_KEY_L && action == GLFW_RELEASE)
	{
		lines = !lines;
		if (lines)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	if (action == GLFW_PRESS)
	{
		camera.setKey(key, true);
		globalControls->setKey(key, true);
	}

	if (action == GLFW_RELEASE)
	{
		camera.setKey(key, false);
		globalControls->setKey(key, false);
		globalControls->firstClickedLeft = true;
		globalControls->firstClickedRight = true;
		globalControls->mouseDY = 0;
		globalControls->mouseDX = 0;
	}

	if (key == GLFW_KEY_UP && action == GLFW_RELEASE)
	{
		camera.addCameraSpeed(100);
	}

	if (key == GLFW_KEY_DOWN && action == GLFW_RELEASE)
	{
		camera.addCameraSpeed(-100);
	}
}

GLvoid TerrainWorld::mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods)
{
	if (action == GLFW_PRESS)
	{
		camera.setKey(button, true);
		globalControls->setKey(button, true);
	}

	if (action == GLFW_RELEASE)
	{
		camera.setKey(button, false);
		globalControls->setKey(button, false);
	}
}

GLvoid TerrainWorld::mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
{
	camera.updateFront(xPosition, yPosition);
	globalControls->updateMouseDifference(xPosition, yPosition);

}

GLvoid TerrainWorld::scrollInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
{
	globalControls->scrollValue += yPosition;
}

