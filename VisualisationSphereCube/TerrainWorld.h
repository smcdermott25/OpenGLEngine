#pragma once

#include "GameWorld.h"

#include "Client.h"
#include "Server.h"
#include "info.h"
#include "Shader.h"

#include "Camera.h"
#include "SkyBox.h"
#include "TerrainGenerator.h"
#include "FlatGround.h"
#include "Controls.h"
#include "Player.h"
#include "ThirdPersonCamera.h"

class TerrainWorld : public GameWorld
{
private:
	Camera camera;
	SkyBox skyBox;
	FlatGround* flatGround;
	TerrainGenerator terrainGenerator;

	Player player;
	std::vector<Player*> players;

	std::vector<Model> models;

	ThirdPersonCamera tCam;
	Controls* globalControls;

	GLboolean lines = false;
	GLuint size;
	GLuint vertexCount;
	GLint num = 1;

	GLboolean serverSetUp = false;
	GLboolean client = false;
	GLboolean server = false;

	Client* playerClient;
	GLvoid initClient();

	GLvoid closeClient();

public:
	TerrainWorld();
	~TerrainWorld();

	GLvoid update(GLfloat deltaTime);
	GLvoid draw();

	GLvoid input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode);
	GLvoid mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods);
	GLvoid mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition);
	GLvoid scrollInput(GLFWwindow* window, GLdouble  xScroll, GLdouble  yScroll);
};

