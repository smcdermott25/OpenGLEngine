#pragma once
#include "GameWorld.h"
#include "FractalNoise.h"
#include "Noise.h"
#include "Terrain.h"
#include "TerrainGenerator.h"

class TestMemoryLeaks : public GameWorld
{
private:
	Model* player;
public:
	TestMemoryLeaks()
	{
		player = new Model("models / ArmyPilot / ArmyPilot.dae", true, glm::vec3(1, 1, 1), glm::vec3(0, 0, 0));
		std::cout << "New world created " << std::endl;
	}

	~TestMemoryLeaks()
	{
		std::cout << "Deconstructor being called " << std::endl;
		delete(player);
	}

	GLvoid update(GLfloat deltaTime)
	{

	}
	GLvoid draw()
	{
	}

	GLvoid input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode)
	{

	}

	GLvoid mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods)
	{

	}

	GLvoid mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
	{

	}

	GLvoid scrollInput(GLFWwindow* window, GLdouble  xScroll, GLdouble  yScroll)
	{

	}
};