#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glew.h>
#include "GameObject.h"
#include "Shader.h"

class TexturedCube :public GameObject
{

private:
	GLuint VBO, VAO, cubeTexture;

public:
	TexturedCube::TexturedCube();
	int TexturedCube::draw(glm::mat4 view, glm::mat4 projection);


};
