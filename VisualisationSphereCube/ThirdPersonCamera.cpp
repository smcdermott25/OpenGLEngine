#include "ThirdPersonCamera.h"


ThirdPersonCamera::ThirdPersonCamera(Player& player)
	: player{player}
{
	position = glm::vec3(0.0f, 4.0f, 15.0f);

	projectionMatrix = glm::perspective(
		45.0f, ((GLfloat)WIDTH / (GLfloat)HEIGHT), 1.0f, 1000000.0f);

	projectionViewMatrix = projectionMatrix * viewMatrix;
}

GLvoid update()
{

}

GLvoid ThirdPersonCamera::move()
{
	calculateZoom();
	calculatePitch();
	calculateAngleAroundPlayer();

	GLfloat horizontalDistance = calculateHorizontalDistance();
	GLfloat verticleDistance = calculateVerticleDistance();
	calculateCameraPosition(horizontalDistance, verticleDistance);
	yaw = 180 - (player.model->orientation->getRotationAngle() + angleAroundPlayer);
}

GLvoid ThirdPersonCamera::calculateZoom()
{
	GLfloat zoomLevel = Controls::getInstance()->scrollValue;

	zoomLevel += ((Controls::getController(0)->getLeftBumperTwo() + 1) / 2) * 25;
	zoomLevel -= ((Controls::getController(0)->getRightBumperTwo() + 1) / 2) * 25;

	distanceFromPlayer -= zoomLevel;
}


GLvoid ThirdPersonCamera::calculatePitch()
{
	if (Controls::getInstance()->keys[GLFW_MOUSE_BUTTON_LEFT])
	{
		GLfloat pitchChange = Controls::getInstance()->mouseDY * 0.05f;
		pitch += pitchChange;
	}

	Controller* controller = Controls::getInstance()->getController(player.getControllerId());
	if (controller != nullptr)
	{
		GLfloat pitchChange = controller->getRightVertical() * 1.0f;
		pitch -= pitchChange;
	}
}

GLvoid ThirdPersonCamera::calculateAngleAroundPlayer()
{
	// Keyboard / mouse
	GLfloat angleChange;
	if (Controls::getInstance()->keys[GLFW_MOUSE_BUTTON_RIGHT])
	{
		angleChange = Controls::getInstance()->mouseDX * 0.05f;
		angleAroundPlayer -= angleChange;
	}

	// Controller
	Controller* controller = Controls::getInstance()->getController(player.getControllerId());
	if (controller != nullptr)
	{
		angleChange = Controls::getInstance()->getController(0)->getRightHorizontal() * 10.0f;
		angleAroundPlayer -= angleChange;
	}

	// Player
	angleChange = player.model->orientation->getRotationAngle();
	angleAroundPlayer += angleChange;

}

GLfloat ThirdPersonCamera::calculateHorizontalDistance()
{
	return distanceFromPlayer * cos(glm::radians(pitch));
}

GLfloat ThirdPersonCamera::calculateVerticleDistance()
{
	return distanceFromPlayer * sin(glm::radians(pitch));
}

GLvoid ThirdPersonCamera::calculateCameraPosition(GLfloat horizontalDistance, GLfloat verticleDistance)
{
	//glm::vec3 playerPosition = player.nightWing.orientation.getPosition();
	GLfloat theta = angleAroundPlayer + player.model->orientation->getRotationAngle();
	GLfloat xOffset = (GLfloat)horizontalDistance * sin(glm::radians(theta));
	GLfloat zOffset = (GLfloat)horizontalDistance * cos(glm::radians(theta));

	glm::vec3 playerPosition = player.model->orientation->getPosition();
	position.x = playerPosition.x - xOffset;
	position.z = playerPosition.z - zOffset;
	position.y = playerPosition.y + verticleDistance;
}
