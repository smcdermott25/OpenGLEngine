#pragma once

#include "Controls.h"
#include "Model.h"
#include "Cube.h"
#include "Player.h"
#include "Orientation.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/*
* Just creating the third person camera class by itself at the minute.
* Will update the camera system to use a shared hierarchy when finished.
*/
class ThirdPersonCamera
{
private:
	GLfloat distanceFromPlayer = 250.0f;
	GLfloat angleAroundPlayer = 0.0f;

	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;
	glm::mat4 projectionViewMatrix;

	glm::vec3 position;
	glm::vec3 cameraTarget;
	glm::vec3 cameraFront;
	glm::vec3 upVector;
	glm::vec3 rightVector;

	GLfloat pitch = 20;
	GLfloat yaw = 0;

	Player& player;

	GLfloat calculateHorizontalDistance();
	GLfloat calculateVerticleDistance();
	GLvoid calculateCameraPosition(GLfloat horizontalDistance, GLfloat verticleDistance);
	GLvoid calculateZoom();
	GLvoid calculatePitch();
	GLvoid calculateAngleAroundPlayer();

public:
	ThirdPersonCamera(Player& player);

	inline GLfloat ThirdPersonCamera::getDistance()
	{
		return distanceFromPlayer;
	}

	inline GLfloat ThirdPersonCamera::getAngleAroundPlayer()
	{
		return angleAroundPlayer;
	}

	inline GLfloat ThirdPersonCamera::getPitch()
	{
		return pitch;
	}

	inline GLfloat ThirdPersonCamera::getYaw()
	{
		return yaw;
	}

	inline glm::vec3 ThirdPersonCamera::getCameraPosition()
	{
		return position;
	}

	inline glm::mat4 ThirdPersonCamera::getView()
	{
		glm::mat4 viewMatrix = glm::mat4(1);
		glm::vec3 negativePosition = glm::vec3(-position.x, -position.y, -position.z);

		viewMatrix = glm::rotate(viewMatrix, glm::radians(pitch), glm::vec3(1, 0, 0));
		viewMatrix = glm::rotate(viewMatrix, glm::radians(yaw), glm::vec3(0, 1, 0));
		viewMatrix = glm::translate(viewMatrix, negativePosition);
		return viewMatrix;
	}

	glm::mat4 ThirdPersonCamera::getProjection()
	{
		return projectionMatrix;
	}
	inline glm::mat4 ThirdPersonCamera::getProjectionViewMatrix()
	{
		return projectionViewMatrix;
	}

	inline GLvoid ThirdPersonCamera::setDistance(GLfloat newDistance)
	{
		distanceFromPlayer = newDistance;
	}

	inline GLvoid ThirdPersonCamera::setAngleAroundPlayer(GLfloat newAngle)
	{
		angleAroundPlayer = newAngle;
	}

	GLvoid move();
};