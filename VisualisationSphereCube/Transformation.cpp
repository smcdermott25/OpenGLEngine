#include "Transformation.h"


Transformation::Transformation()
{
	modelMatrix = glm::mat4(1);
}


Transformation::~Transformation()
{

}

glm::mat4 Transformation::getModelMatrix()
{
	return modelMatrix;
}
