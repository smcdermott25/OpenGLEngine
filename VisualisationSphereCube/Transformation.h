#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

class Transformation
{
private:
	glm::mat4 modelMatrix;

public:
	Transformation();
	~Transformation();

	glm::mat4 getModelMatrix();
};

