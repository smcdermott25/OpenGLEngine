#include "ValueNoise.h"

ValueNoise::ValueNoise(GLint seed) 
	: Noise()
{
	this->seed = seed;
};

ValueNoise::~ValueNoise()
{

}

GLfloat ValueNoise::randomNoise(GLint valueOne, GLint valueTwo)
{
	srand(seed * valueOne * 12234 * valueTwo * 2342);
	
	GLfloat randomValue = (GLfloat)rand() / (GLfloat)RAND_MAX;
	return randomValue;
}

/*
 * Returns the mean value of this points noise value and the points around it.
 * A greater emphasis is put on the center pointer than the sides.
 * A greater emphasis is put on the sides than the corners.
 * This noise generation method uses the immediate points around it (in a 2d grid) to 
 * return a value of noise. e.g. -1 and + 1
*/
GLfloat ValueNoise::getMeanNoise(GLint x, GLint z)
{
	GLfloat bottomLeft = randomNoise(x - 1, z - 1);
	GLfloat bottomRight = randomNoise(x + 1, z - 1);
	GLfloat topLeft = randomNoise(x - 1, z + 1);
	GLfloat topRight = randomNoise(x + 1, z + 1);

	GLfloat leftSide = randomNoise(x - 1, z);
	GLfloat rightSide = randomNoise(x + 1, z);
	GLfloat top = randomNoise(x, z + 1);
	GLfloat bottom = randomNoise(x, z - 1);

	GLfloat corners = (bottomLeft + bottomRight + topLeft + topRight) / 16.0f;
	GLfloat sides = (leftSide + rightSide + top + bottom) / 8.0f;

	GLfloat center = randomNoise(x, z) / 4.0f;

	return center + sides + corners;
}

GLfloat ValueNoise::getNoise(GLint xIn, GLint yIn)
{
	GLfloat x = (GLfloat)xIn;
	GLfloat y = (GLfloat)yIn;

	x /= 8.0f;
	y /= 8.0f;

	GLint intX = (GLint)x;
	GLint intY = (GLint)y;

	GLfloat fractionX = x - intX;
	GLfloat fractionY = y - intY;

	GLfloat v1 = getMeanNoise(intX, intY);
	GLfloat v2 = getMeanNoise(intX + 1, intY);
	GLfloat v3 = getMeanNoise(intX, intY + 1);
	GLfloat v4 = getMeanNoise(intX + 1, intY + 1);

	GLfloat interpolatedValue = interpolate.cosInterpolateTwo(v1, v2, fractionX);
	GLfloat interpolatedValueTwo = interpolate.cosInterpolateTwo(v3, v4, fractionX);

	GLfloat finalValue = interpolate.cosInterpolateTwo(interpolatedValue, interpolatedValueTwo, fractionY);

	return finalValue;
}
