#pragma once
#include "Noise.h"
#include "Interpolation.h"
#include <iostream>
#include <random>

class ValueNoise : public Noise
{
private:
	Interpolation interpolate;
	GLfloat randomNoise(GLint valueOne, GLint valueTwo);
	GLfloat getMeanNoise(GLint valueOne, GLint valueTwo);

public:
	ValueNoise(GLint seed);
	~ValueNoise();

	GLfloat getNoise(GLint valueOne, GLint valueTwo);
};


