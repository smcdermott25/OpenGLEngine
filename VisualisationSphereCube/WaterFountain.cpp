#pragma once
#include "WaterFountain.h"


WaterFountain::WaterFountain(glm::vec3 position, glm::vec3 scale)
{
	for (GLint  i = 0; i<4; i ++)
	{
	/*	Metaball m = Metaball(glm::vec3((5*i)+position.x, (5*i) + position.y, (5*i) + position.z), 10);
		metaballs.push_back(m);*/
	}

	//Metaball m = Metaball(glm::vec3((0) , (0) , (0) ), 10);
	//metaballs.push_back(m);

	Metaball m = Metaball(glm::vec3((5) , (10) , (5) ), 10);
	metaballs.push_back(m);

	//Metaball m = Metaball(glm::vec3(0, 50 , 0), 500);
	//metaballs.push_back(m);
		

		//Metaball m1 = Metaball(glm::vec3(0, 20, 0), 5);
		//metaballs.push_back(m1);

		//Metaball m2 = Metaball(glm::vec3(0, 30, 0), 5);
		//metaballs.push_back(m2);

		//Metaball m3 = Metaball(glm::vec3(0, 20, 0), 5);
		//metaballs.push_back(m3);

		// m3 = Metaball(glm::vec3(0, 25, 0), 5);
		//metaballs.push_back(m3);

		// m3 = Metaball(glm::vec3(0, 27, 0), 5);
		//metaballs.push_back(m3);

		// m3 = Metaball(glm::vec3(0, 20, 0), 5);
		//metaballs.push_back(m3);

		// m3 = Metaball(glm::vec3(0, 20, 0), 5);
		//metaballs.push_back(m3);
	
	
	gridSystem.createMemory(100, 1);
	gridSystem.createGrid(position,30, 10,20);

	gridSystem.update(&metaballs[0], metaballs.size());

	fountain = new Model("models/fountainOBJ/fountain.obj", false, glm::vec3(0.5, 0.5, 0.5), glm::vec3(0, 0, 0));
	
}

WaterFountain::~WaterFountain()
{
	
}

GLvoid WaterFountain::update(GLfloat deltaTime)
{

	//GLfloat  c = (GLfloat )cos(deltaTime / 600);

//	metaballs[0].setXPosition( -4.0f*(GLfloat )cos(deltaTime / 700) - c);
	//metaballs[0].setYPosition(metaballs[0].getPosition().y + 0.1);
//	metaballs[2].setYPosition(metaballs[2].getPosition().y + -0.1);

	
	
	
	//}

	//std::cout << " cos " << glfwGetTime() <<"Compared to  "<< 2 + lastUpdate << std::endl;
}
GLvoid WaterFountain::draw(glm::mat4 projection, glm::mat4 view, glm::vec3 cameraPosition)
{


		fountain->drawing(projection, view, glfwGetTime());

		//blender.transparentSetUp();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		gridSystem.drawSurface(1.0f, projection, view);
		//	blender.disableBlending();
	
}