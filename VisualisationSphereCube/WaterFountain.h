#pragma once
#include "GameObject.h"
#include "IsoSurfaceGrid.h"
#include "Metaball.h"
#include "Model.h"
#include "AlphaBlenderTool.h"


class WaterFountain : public GameObject
{

public:
	WaterFountain(glm::vec3 position, glm::vec3 scale);
	~WaterFountain();

	GLvoid update(GLfloat deltaTime);
	GLvoid draw(glm::mat4 projection, glm::mat4 view, glm::vec3 cameraPosition);

private:
	IsoSurfaceGrid gridSystem = IsoSurfaceGrid(" ");
	std::vector<Metaball> metaballs;
	Model* fountain;
	AlphaBlenderTool blender;
	GLfloat  lastUpdate = 0;
};