#include "XboxController.h"
#include "iostream"

GLint Controller::count = 0;

XboxController::XboxController(GLint id)
	:id(id)
{
	Controller::count++;
}

XboxController::~XboxController()
{

}

GLvoid XboxController::updateAxis()
{
	GLint count = 0;
	if (glfwGetJoystickAxes(id, &count) != NULL)
	{
		const GLfloat* axis = glfwGetJoystickAxes(id, &count);

		leftBumperTwo = axis[4];
		rightBumperTwo = axis[5];

		leftStickHorizontalValue = axis[0];
		leftStickVerticleValue = axis[1];
		rightStickHorizontalValue = axis[2];
		rightStickVerticleValue = axis[3];
	}
}

GLvoid XboxController::updateButtons()
{
	GLint count = 0;
	if (glfwGetJoystickButtons(id, &count) != NULL)
	{
		const unsigned char* buttons = glfwGetJoystickButtons(id, &count);
		aButton = buttons[0];
		xButton = buttons[2];
		yButton = buttons[3];
		bButton = buttons[1];
		leftBumperOne = buttons[4];
		rightBumperOne = buttons[5];
	}
}
