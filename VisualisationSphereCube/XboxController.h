#pragma once
#include "Controller.h"

class XboxController : public Controller
{
private:
	GLint id;
public:
	XboxController(GLint id);
	~XboxController();

	inline GLvoid updateAxis();
	inline virtual GLvoid updateButtons();
};