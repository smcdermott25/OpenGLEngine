#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glew.h>
#include "../GameObject.h"
#include "../Shader.h"

class ManualVertexCube :public GameObject
{

private:
	GLuint VBO, VAO;
	typedef GameObject super;
	
public:
	ManualVertexCube::ManualVertexCube();
	GLint  ManualVertexCube::draw(glm::mat4 view, glm::mat4 controller);


};
