#pragma once
#include <glew.h>
#include "Helper.h"
#include "Controls.h"

/*
 * This header is used for variables that are used through the whole codebase,
 * e.g. The width and height of the canvas and a Helper object.
*/

// Only one helper required
static Helper helper;

//static Controls controls;

//const GLuint WIDTH = 200;
//const GLuint HEIGHT = 200;
const GLuint WIDTH = 1600;
const GLuint HEIGHT = 900;


