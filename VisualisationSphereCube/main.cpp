#define GLEW_STATIC

#include <stdlib.h>

#include <glew.h> 
#include <glfw3.h>

#include "GameWorld.h"
#include "TerrainWorld.h"
#include "BillboardWorld1.h"
#include "CollisionWorld.h"
#include "MultiLightWorld.h"
#include "AnimationWorld.h"
#include "PostProcessWorld.h"
#include "TestMemoryLeaks.h"

#include "ParticleEffectWorld.h"
#include "ShaderLoader.h"
#include "ShadowWorld.h"
#include "TerrainShadowWorld.h"
#include "ShadowRoomWorld.h"

#include "Materials.h"

GLFWwindow* window;

GLvoid update(GLfloat deltaTime);
GLvoid input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode);
GLvoid mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods);
GLvoid mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition);
GLvoid scrollInput(GLFWwindow* window, GLdouble  xScroll, GLdouble  yScroll);
GLvoid controllerCallback(GLint joy, GLint event);

GLint initialiseContext();
GLdouble timeOfLastUpdate = 0.0f;
static GLdouble scroll = 0.0f;

GameWorld *currentWorld;

GLint main()
{

	GLint success = initialiseContext();
	if (success == 1)
	{
		return 1;
	}

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// Variables used for calculating the time between frames
	GLfloat deltaTime = 0.0f;
	GLfloat previousTime = glfwGetTime();

	ShaderLoader::ShaderLoader();
	Materials::Materials();
	// Set initial world
	//currentWorld = new MultiLightWorld();

	//currentWorld = new BillboardWorld1();
	currentWorld = new CollisionWorld();

	//currentWorld = new BillboardWorld1();
	//currentWorld = new TerrainWorld();
	//currentWorld = new TestMemoryLeaks();
	
	//currentWorld = new TerrainShadowWorld();

	//currentWorld = new PostProcessWorld();

	//currentWorld = new TerrainWorld();

	//currentWorld = new AnimationWorld();
	//currentWorld = new CollisionWorld();
	while (!glfwWindowShouldClose(window))
	{		
		glfwMakeContextCurrent(window);
		deltaTime = glfwGetTime() - previousTime;
		previousTime = glfwGetTime();

		// Check input
		glfwPollEvents();

		glClearColor(1.0f, 0.2f, 0.2f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Decide if it's time to update or not
		GLdouble currentTime = glfwGetTime();
		GLdouble timeSinceLastUpdate = currentTime - timeOfLastUpdate;
		if (timeSinceLastUpdate > 0.010)
		{
			currentWorld->update(deltaTime);
		}

		currentWorld->draw();
		glfwSwapBuffers(window);
	}


	return 0;
}

GLvoid update(GLfloat deltaTime)
{
	//Update Current Game World
	currentWorld->update(deltaTime);
}

GLint initialiseContext()
{
	glfwInit();
	glfwWindowHint(GLFW_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_VERSION_MINOR, 4);
	window = glfwCreateWindow(WIDTH, HEIGHT, "Bear Thrills 2.0", nullptr, nullptr);
	glfwSetWindowPos(window, 200, 200);
	glEnable(GL_MULTISAMPLE);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	 //check window was created 
	if (window == nullptr)
	{
		std::cout << "Did not create window successfully: " << std::endl;
		glfwTerminate();
		return 1;
	}

	glfwMakeContextCurrent(window);

	// set key callback function
	glfwSetKeyCallback(window, input);
	glfwSetJoystickCallback(controllerCallback);
	glfwSetMouseButtonCallback(window, mouseButton);
	glfwSetCursorPosCallback(window, mouseInput);
	glfwSetScrollCallback(window, scrollInput);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Did not sucessfully initialise glew: " << std::endl;
		glfwTerminate();
		return 1;
	}

	glViewport(0, 0, WIDTH, HEIGHT);
	

	return 0;
}


GLvoid input(GLFWwindow* window, GLint  key, GLint  scancode, GLint  action, GLint  mode)
{
	if (glfwGetCurrentContext() == window)
	{
		if (key == GLFW_KEY_ESCAPE)
		{
			exit(1);
		}

		if (key == GLFW_KEY_1 && action == GLFW_RELEASE)
		{
			delete(currentWorld);
			currentWorld = new TerrainWorld();
		}

		if (key == GLFW_KEY_2 && action == GLFW_RELEASE)
		{
			delete(currentWorld);
			currentWorld = new BillboardWorld1();

		}

		// e.g. put other worlds here
		if (key == GLFW_KEY_3 && action == GLFW_RELEASE)
		{
			delete(currentWorld);
			currentWorld = new CollisionWorld();

		}

		if (key == GLFW_KEY_4 && action == GLFW_RELEASE)
		{
			delete(currentWorld);
			currentWorld = new AnimationWorld();

		}

		if (key == GLFW_KEY_5 && action == GLFW_RELEASE)
		{
			delete(currentWorld);
			currentWorld = new ParticleEffectWorld();
		}

		if (key == GLFW_KEY_6 && action == GLFW_RELEASE)
		{
			delete(currentWorld);
			currentWorld = new TerrainShadowWorld();
		}
		currentWorld->input(window, key, scancode, action, mode);
	}
}

GLvoid mouseInput(GLFWwindow* window, GLdouble  xPosition, GLdouble  yPosition)
{
	if (glfwGetCurrentContext() == window)
	{
		currentWorld->mouseInput(window, xPosition, yPosition);
	}
}

GLvoid scrollInput(GLFWwindow* window, GLdouble  xScroll, GLdouble  yScroll)
{
	if (glfwGetCurrentContext() == window)
	{
		currentWorld->scrollInput(window, xScroll, yScroll);
	}
}

GLvoid mouseButton(GLFWwindow* window, GLint  button, GLint  action, GLint  mods)
{
	if (glfwGetCurrentContext() == window)
	{
		currentWorld->mouseButton(window, button, action, mods);
	}
}

GLvoid controllerCallback(GLint joy, GLint event)
{
	if (event == GLFW_CONNECTED)
	{
		Controls::controllerConnected(glfwGetJoystickName(joy));
	}
	else if (event == GLFW_DISCONNECTED)
	{
		std::cout << "Controller disconnected: " << std::endl;
	}
}
